///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.sib.ejb.message;
//
//import com.sib.entity.BancaImporta;
//import com.sib.util.Plano;
//import java.util.ArrayList;
//import javax.ejb.ActivationConfigProperty;
//import javax.ejb.MessageDriven;
//import javax.jms.Message;
//import javax.jms.MessageListener;
//import javax.jms.ObjectMessage;
//import javax.jms.TextMessage;
//
///**
// *
// * @author julio
// */
//@MessageDriven(mappedName = "jms/sibQueue",activationConfig = {
//    @ActivationConfigProperty(propertyName = "acknowledgeMode",
//        propertyValue = "Auto-acknowledge")
//})
//public class PlanoMDB implements MessageListener{
//
//    @Override
//    public void onMessage(Message message) {
//        
//         try {
//            ObjectMessage om=(ObjectMessage) message;
//            ArrayList<Plano> lp=(ArrayList<Plano>)om.getObject();
//            
//            for(Plano p : lp) {
//                System.out.println(p.getFecha() +" "+ 
//                p.getGrupo() + " "+ p.getLoteria() + " "+ p.getBanca());
//            } 
//        } catch (Exception e) {
//        }
//        
//    }
//    
//}
