///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.sib.ejb.dao;
//
//import com.sib.entity.Juego;
//import java.util.List;
//import javax.ejb.Local;
//
///**
// *
// * @author julio
// */
//@Local
//public interface JuegoDao {
//    
//    public void crearJuego(Juego juego);
//    public List<Juego> getJuegos();
//    public Juego getJuegoById(int id);
//    public Juego editarJuego(Juego juego);
//}
