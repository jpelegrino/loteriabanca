///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.sib.ejb.dao;
//
//import com.sib.entity.CobroHead;
//import java.util.List;
//import javax.ejb.Local;
//
///**
// *
// * @author julio
// */
//@Local
//public interface CobroHeadDao {
//    
//    public void crearCobroHead(CobroHead cobroHead);
//    public List<CobroHead> getCobroHeads();
//    public CobroHead getCobroHeadById(int id);
//    
//}
