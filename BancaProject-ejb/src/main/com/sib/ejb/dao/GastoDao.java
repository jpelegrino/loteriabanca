///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.sib.ejb.dao;
//
//import com.sib.entity.Gasto;
//import java.util.List;
//import javax.ejb.Local;
//
///**
// *
// * @author julio
// */
//@Local
//public interface GastoDao {
//    
//    public void crearGasto(Gasto gasto);
//    public List<Gasto> getGastos();
//    public Gasto getGastoById(int id);
//    public Gasto editarGasto(Gasto gasto);
//    
//}
