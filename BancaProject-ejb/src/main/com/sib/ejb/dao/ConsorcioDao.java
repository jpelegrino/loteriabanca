/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.ejb.dao;

import com.sib.entity.BancaConsorcio;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author jpelegrino
 */
@Local
public interface ConsorcioDao {

    public void crearConsorcio(BancaConsorcio con);

    public BancaConsorcio getConsorcioById(int id);

    public BancaConsorcio editarConsorcio(BancaConsorcio consorcio);

//    public List<BancaConsorcio> getConsorcios();
}
