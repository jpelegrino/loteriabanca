/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.ejb.dao;

import com.sib.entity.BancaCliente;
import com.sib.entity.BancaClientePK;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author jpelegrino
 */
@Local
public interface BancaDao extends ServiceDao {
    public void crearBanca(BancaCliente banca);
    public List<BancaCliente> getBancas();
    public BancaCliente getBancaById(BancaClientePK id);
    public BancaCliente editarBanca(BancaCliente banca);
    public BancaCliente getBancaByCodigo(String bcod,String gcod);
}
