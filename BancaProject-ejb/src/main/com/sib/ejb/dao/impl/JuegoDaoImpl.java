///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.sib.ejb.dao.impl;
//
//import com.sib.ejb.dao.JuegoDao;
//import com.sib.entity.Juego;
//import com.sib.util.ConnectionDB;
//import java.util.List;
//import javax.ejb.Stateless;
//import javax.persistence.EntityManager;
//import javax.persistence.Query;
//import org.apache.log4j.Logger;
//
///**
// *
// * @author julio
// */
//@Stateless
//public class JuegoDaoImpl implements JuegoDao {
//    
//    private static Logger LOG=Logger.getLogger(GrupoDaoImpl.class);
//    EntityManager em= ConnectionDB.getConnection().createEntityManager();
//    private Query query;
//
//    @Override
//    public void crearJuego(Juego juego) {
//        em.getTransaction().begin();
//        em.persist(juego);
//        em.getTransaction().commit();
//    }
//
//    @Override
//    public List<Juego> getJuegos() {
//        query=em.createNamedQuery("juegos");
//        return query.getResultList();
//    }
//
//    @Override
//    public Juego getJuegoById(int id) {
//        return (Juego) em.find(Juego.class, id);
//    }
//
//    @Override
//    public Juego editarJuego(Juego juego) {
//        em.getTransaction().begin();
//        em.merge(juego);
//        em.getTransaction().commit();
//        return juego;
//    }
//    
//}
