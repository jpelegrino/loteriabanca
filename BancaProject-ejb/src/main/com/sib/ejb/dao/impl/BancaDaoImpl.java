/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.ejb.dao.impl;

import com.sib.entity.BancaCliente;
import com.sib.ejb.dao.BancaDao;
import com.sib.ejb.dao.ServiceDao;
import com.sib.entity.BancaCliente;
import com.sib.entity.BancaClientePK;
import com.sib.util.ConnectionDB;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author jpelegrino
 */
@Stateless
public class BancaDaoImpl implements BancaDao{
    public static Logger LOG=Logger.getLogger(BancaDaoImpl.class);
  EntityManager em= ConnectionDB.getConnection().createEntityManager();
  private Query query;
    
    @Override
    public void crearBanca(BancaCliente banca) {
        LOG.debug("Creating a bank");
        em.getTransaction().begin();
        em.merge(banca);
        em.getTransaction().commit();
    }

    @Override
    public List<BancaCliente> getBancas() {
        query=em.createNamedQuery("BancaCliente.findAll");
        return query.getResultList();
    }

    @Override
    public BancaCliente getBancaById(BancaClientePK id) {
        return (BancaCliente) em.find(BancaCliente.class, id);
    }

    @Override
    public BancaCliente editarBanca(BancaCliente banca) {
        em.getTransaction().begin();
        em.merge(banca);
        em.getTransaction().commit();
        return banca;
    }

    @Override
    public String getName() {
        return "BancaDaoImpl";
    }

    @Override
    public BancaCliente getBancaByCodigo(String bcod,String gcod) {
        query=em.createNamedQuery("getBanca");
        query.setParameter("bcod", bcod);
        query.setParameter("gcod", gcod);
        return (BancaCliente) query.getSingleResult();
    }

    
}
