///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.sib.ejb.dao.impl;
//
//import com.sib.ejb.dao.TransaccionHeadDao;
//import com.sib.entity.TransaccionHead;
//import com.sib.util.ConnectionDB;
//import java.util.List;
//import javax.ejb.Stateless;
//import javax.persistence.EntityManager;
//import javax.persistence.Query;
//import org.apache.log4j.Logger;
//
///**
// *
// * @author julio
// */
//@Stateless
//public class TransaccionHeadDaoImpl implements TransaccionHeadDao {
//
//    public static Logger LOG=Logger.getLogger(TransaccionHeadDaoImpl.class);
//    EntityManager em= ConnectionDB.getConnection().createEntityManager();
//    private Query query;
//    
//    @Override
//    public void crearTransaccionHead(TransaccionHead th) {
//        em.getTransaction().begin();
//        em.persist(th);
//        em.getTransaction().commit();
//    }
//
//    @Override
//    public List<TransaccionHead> getTransaccionHead() {
//        query=em.createNamedQuery("allTransaccionHead");
//        return query.getResultList();
//    }
//    
//}
