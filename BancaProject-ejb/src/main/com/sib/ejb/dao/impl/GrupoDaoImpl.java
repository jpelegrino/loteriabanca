/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.ejb.dao.impl;

import com.sib.ejb.dao.GrupoDao;
import com.sib.ejb.dao.ServiceDao;
import com.sib.entity.BancaGrupo;
import com.sib.entity.BancaGrupoPK;
import com.sib.util.ConnectionDB;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author julio
 */
@Stateless
public class GrupoDaoImpl implements GrupoDao {

    public static Logger LOG=Logger.getLogger(GrupoDaoImpl.class);
    EntityManager em= ConnectionDB.getConnection().createEntityManager();
    private Query query;
    
    @Override
    public void crearGrupo(BancaGrupo grupo) {
        em.getTransaction().begin();
        em.persist(grupo);
        em.getTransaction().commit();
    }

    @Override
    public List<BancaGrupo> getGrupos() {
        query=em.createNamedQuery("BancaGrupo.findAll");
        return query.getResultList();
    }

    @Override
    public BancaGrupo getGrupoById(BancaGrupoPK id) {
        return (BancaGrupo) em.find(BancaGrupo.class, id);
    }

    @Override
    public BancaGrupo editarGrupo(BancaGrupo grupo) {
        em.getTransaction().begin();
        em.merge(grupo);
        em.getTransaction().commit();
        return grupo;
    }

    @Override
    public String getName() {
        return "GrupoDaoImpl";
    }

    @Override
    public BancaGrupo getGrupoByCodigo(String cod) {
        query=em.createNamedQuery("getGrupo");
        query.setParameter("cod", cod);
        return (BancaGrupo) query.getSingleResult();
    }
    
}
