///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.sib.ejb.dao.impl;
//
//import com.sib.ejb.dao.BancaImportaDao;
//import com.sib.entity.BancaImporta;
//import com.sib.util.ConnectionDB;
//import javax.ejb.Stateless;
//import javax.persistence.EntityManager;
//import org.apache.log4j.Logger;
//
///**
// *
// * @author julio
// */
//@Stateless
//public class BancaImportaDaoImpl implements BancaImportaDao{
//    public static Logger LOG=Logger.getLogger(BancaImportaDaoImpl.class);
//    EntityManager em= ConnectionDB.getConnection().createEntityManager();
//
//    @Override
//    public void crearBancaImporta(BancaImporta bancaImporta) {
//        em.getTransaction().begin();
//        em.persist(bancaImporta);
//        em.getTransaction().commit();
//    }
//    
//}
