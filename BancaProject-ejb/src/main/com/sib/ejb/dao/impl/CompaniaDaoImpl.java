/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.ejb.dao.impl;

import com.sib.entity.SysCompania;
import com.sib.ejb.dao.CompaniaDao;
import com.sib.util.ConnectionDB;
import java.io.Serializable;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;

/**
 *
 * @author jpelegrino
 */
@Stateless
public class CompaniaDaoImpl implements CompaniaDao ,Serializable {

    public static Logger LOG = Logger.getLogger(CompaniaDaoImpl.class);
    EntityManager em = ConnectionDB.getConnection().createEntityManager();

    @Override
    public void crearCompania(SysCompania comp) {
        em.getTransaction().begin();
        em.persist(comp);
        em.getTransaction().commit();
    }

    @Override
    public SysCompania getCompaniaById(int id) {
        return (SysCompania) em.find(SysCompania.class, id);
    }

    @Override
    public SysCompania editarCompania(SysCompania compania) {
        em.getTransaction().begin();
        em.merge(compania);
        em.getTransaction().commit();
        
        return compania;
    }

}
