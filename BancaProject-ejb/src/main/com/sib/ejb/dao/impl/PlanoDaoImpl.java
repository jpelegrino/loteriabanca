/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.ejb.dao.impl;

import com.sib.ejb.dao.PlanoDao;
import com.sib.util.PlanoParse;
import java.io.InputStream;
import javax.ejb.Stateless;
import org.apache.log4j.Logger;

/**
 *
 * @author julio
 */
@Stateless
public class PlanoDaoImpl implements PlanoDao {
    private static Logger LOG=Logger.getLogger(PlanoDaoImpl.class);

    @Override
    public void send(InputStream is) {        
        PlanoParse.parse(is);
        LOG.info("***********************Entre al parseador*****************");
    }
    
}
