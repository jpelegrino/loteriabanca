/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.ejb.dao.impl;

import com.sib.entity.BancaConsorcio;
import com.sib.ejb.dao.ConsorcioDao;
import com.sib.util.ConnectionDB;
import java.io.Serializable;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;

/**
 *
 * @author jpelegrino
 */
@Stateless
public class ConsorcioDaoImpl implements ConsorcioDao,Serializable {

    public static Logger LOG = Logger.getLogger(ConsorcioDaoImpl.class);
    EntityManager em = ConnectionDB.getConnection().createEntityManager();

    

    @Override
    public void crearConsorcio(BancaConsorcio con) {
        em.getTransaction().begin();
        em.persist(con);
        em.getTransaction().commit();
    }

    @Override
    public BancaConsorcio getConsorcioById(int id) {
        return (BancaConsorcio) em.find(BancaConsorcio.class, id);
    }

    @Override
    public BancaConsorcio editarConsorcio(BancaConsorcio consorcio) {
        em.getTransaction().begin();
        em.merge(consorcio);
        em.getTransaction().commit();
        
        return consorcio;
    }


}
