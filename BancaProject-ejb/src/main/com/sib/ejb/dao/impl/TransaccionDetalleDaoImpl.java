///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.sib.ejb.dao.impl;
//
//import com.sib.ejb.dao.TransaccionDetalleDao;
//import com.sib.entity.TransaccionDetalle;
//import com.sib.util.ConnectionDB;
//import java.util.List;
//import javax.ejb.Stateless;
//import javax.persistence.EntityManager;
//import javax.persistence.Query;
//import org.apache.log4j.Logger;
//
///**
// *
// * @author julio
// */
//@Stateless
//public class TransaccionDetalleDaoImpl 
//implements TransaccionDetalleDao  {
//
//    public static Logger LOG=Logger.getLogger(TransaccionDetalleDaoImpl.class);
//    EntityManager em= ConnectionDB.getConnection().createEntityManager();
//    private Query query;
//    
//    @Override
//    public void crearTransaccionDetalle(TransaccionDetalle td) {
//        em.getTransaction().begin();
//        em.persist(td);
//        em.getTransaction().commit();
//    }
//
//    @Override
//    public List<TransaccionDetalle> getTransaccionDetalles() {
//        query=em.createNamedQuery("allTransaccionDetalle");
//        return query.getResultList();
//        
//    }
//    
//}
