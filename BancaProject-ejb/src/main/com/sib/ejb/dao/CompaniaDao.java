/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.ejb.dao;

import com.sib.entity.SysCompania;
import javax.ejb.Local;

/**
 *
 * @author jpelegrino
 */
@Local
public interface CompaniaDao {
    
    public void crearCompania(SysCompania comp);    
    public SysCompania getCompaniaById(int id);
    public SysCompania editarCompania(SysCompania compania);
    
}
