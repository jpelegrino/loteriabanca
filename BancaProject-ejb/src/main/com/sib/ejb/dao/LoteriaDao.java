///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.sib.ejb.dao;
//
//import com.sib.entity.Loteria;
//import java.util.List;
//import javax.ejb.Local;
//
///**
// *
// * @author jpelegrino
// */
//@Local
//public interface LoteriaDao extends ServiceDao {
//    public void crearLoteria(Loteria loteria);
//    public List<Loteria> getLoterias();
//    public Loteria getLoteriaById(int id);
//    public Loteria editarLoteria(Loteria loteria);
//    public Loteria getLoteriaByCodigo(String cod);
//}
