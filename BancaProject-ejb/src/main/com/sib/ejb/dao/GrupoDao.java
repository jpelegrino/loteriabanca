/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.ejb.dao;

import com.sib.entity.BancaGrupo;
import com.sib.entity.BancaGrupoPK;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author julio
 */
@Local
public interface GrupoDao extends ServiceDao{
    public void crearGrupo(BancaGrupo grupo);
    public List<BancaGrupo> getGrupos();
    public BancaGrupo getGrupoById(BancaGrupoPK id);
    public BancaGrupo editarGrupo(BancaGrupo grupo);
    public BancaGrupo getGrupoByCodigo(String cod);
}
