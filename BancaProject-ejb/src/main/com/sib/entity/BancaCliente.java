/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "banca_cliente")
@NamedQueries({
    @NamedQuery(name = "BancaCliente.findAll", query = "SELECT b FROM BancaCliente b"),
    @NamedQuery(name = "BancaCliente.findByCompaniaId", query = "SELECT b FROM BancaCliente b WHERE b.bancaClientePK.companiaId = :companiaId"),
    @NamedQuery(name = "BancaCliente.findByConsorcioId", query = "SELECT b FROM BancaCliente b WHERE b.bancaClientePK.consorcioId = :consorcioId"),
    @NamedQuery(name = "BancaCliente.findByGrupoId", query = "SELECT b FROM BancaCliente b WHERE b.bancaClientePK.grupoId = :grupoId"),
    @NamedQuery(name = "BancaCliente.findByBancaId", query = "SELECT b FROM BancaCliente b WHERE b.bancaClientePK.bancaId = :bancaId"),
    @NamedQuery(name = "BancaCliente.findByBancaTxt", query = "SELECT b FROM BancaCliente b WHERE b.bancaTxt = :bancaTxt"),
    @NamedQuery(name = "BancaCliente.findByContacto", query = "SELECT b FROM BancaCliente b WHERE b.contacto = :contacto"),
    @NamedQuery(name = "BancaCliente.findByDireccion", query = "SELECT b FROM BancaCliente b WHERE b.direccion = :direccion"),
    @NamedQuery(name = "BancaCliente.findByCiudad", query = "SELECT b FROM BancaCliente b WHERE b.ciudad = :ciudad"),
    @NamedQuery(name = "BancaCliente.findBySector", query = "SELECT b FROM BancaCliente b WHERE b.sector = :sector"),
    @NamedQuery(name = "BancaCliente.findByPais", query = "SELECT b FROM BancaCliente b WHERE b.pais = :pais"),
    @NamedQuery(name = "BancaCliente.findByTelefono", query = "SELECT b FROM BancaCliente b WHERE b.telefono = :telefono"),
    @NamedQuery(name = "BancaCliente.findByCelular", query = "SELECT b FROM BancaCliente b WHERE b.celular = :celular"),
    @NamedQuery(name = "BancaCliente.findByOficina", query = "SELECT b FROM BancaCliente b WHERE b.oficina = :oficina"),
    @NamedQuery(name = "BancaCliente.findByMontoTotal", query = "SELECT b FROM BancaCliente b WHERE b.montoTotal = :montoTotal"),
    @NamedQuery(name = "BancaCliente.findByMontoPorciento", query = "SELECT b FROM BancaCliente b WHERE b.montoPorciento = :montoPorciento"),
    @NamedQuery(name = "BancaCliente.findByMontoCobrado", query = "SELECT b FROM BancaCliente b WHERE b.montoCobrado = :montoCobrado"),
    @NamedQuery(name = "BancaCliente.findByMontoPagado", query = "SELECT b FROM BancaCliente b WHERE b.montoPagado = :montoPagado"),
    @NamedQuery(name = "BancaCliente.findByMontoGasto", query = "SELECT b FROM BancaCliente b WHERE b.montoGasto = :montoGasto"),
    @NamedQuery(name = "BancaCliente.findByMontoPendiente", query = "SELECT b FROM BancaCliente b WHERE b.montoPendiente = :montoPendiente"),
    @NamedQuery(name = "BancaCliente.findByUltimoCobro", query = "SELECT b FROM BancaCliente b WHERE b.ultimoCobro = :ultimoCobro"),
    @NamedQuery(name = "BancaCliente.findByMontoUltimoCobro", query = "SELECT b FROM BancaCliente b WHERE b.montoUltimoCobro = :montoUltimoCobro"),
    @NamedQuery(name = "BancaCliente.findByUltimoPago", query = "SELECT b FROM BancaCliente b WHERE b.ultimoPago = :ultimoPago"),
    @NamedQuery(name = "BancaCliente.findByMontoUltimoPago", query = "SELECT b FROM BancaCliente b WHERE b.montoUltimoPago = :montoUltimoPago"),
    @NamedQuery(name = "BancaCliente.findByStatus", query = "SELECT b FROM BancaCliente b WHERE b.status = :status")})
public class BancaCliente implements Serializable,Entidad {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BancaClientePK bancaClientePK;
    @Size(max = 60)
    @Column(name = "BANCA_TXT")
    private String bancaTxt;
    @Size(max = 45)
    @Column(name = "CONTACTO")
    private String contacto;
    @Size(max = 45)
    @Column(name = "DIRECCION")
    private String direccion;
    @Size(max = 25)
    @Column(name = "CIUDAD")
    private String ciudad;
    @Size(max = 25)
    @Column(name = "SECTOR")
    private String sector;
    @Size(max = 25)
    @Column(name = "PAIS")
    private String pais;
    @Size(max = 24)
    @Column(name = "TELEFONO")
    private String telefono;
    @Size(max = 24)
    @Column(name = "CELULAR")
    private String celular;
    @Size(max = 24)
    @Column(name = "OFICINA")
    private String oficina;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTO_TOTAL")
    private Double montoTotal;
    @Column(name = "MONTO_PORCIENTO")
    private Double montoPorciento;
    @Column(name = "MONTO_COBRADO")
    private Double montoCobrado;
    @Column(name = "MONTO_PAGADO")
    private Double montoPagado;
    @Column(name = "MONTO_GASTO")
    private Double montoGasto;
    @Column(name = "MONTO_PENDIENTE")
    private Double montoPendiente;
    @Column(name = "ULTIMO_COBRO")
    @Temporal(TemporalType.DATE)
    private Date ultimoCobro;
    @Column(name = "MONTO_ULTIMO_COBRO")
    private Double montoUltimoCobro;
    @Column(name = "ULTIMO_PAGO")
    @Temporal(TemporalType.DATE)
    private Date ultimoPago;
    @Column(name = "MONTO_ULTIMO_PAGO")
    private Double montoUltimoPago;
    @Size(max = 1)
    @Column(name = "STATUS")
    private String status;

    public BancaCliente() {
    }

    public BancaCliente(BancaClientePK bancaClientePK) {
        this.bancaClientePK = bancaClientePK;
    }

    public BancaCliente(int companiaId, int consorcioId, int grupoId, int bancaId) {
        this.bancaClientePK = new BancaClientePK(companiaId, consorcioId, grupoId, bancaId);
    }

    public BancaClientePK getBancaClientePK() {
        return bancaClientePK;
    }

    public void setBancaClientePK(BancaClientePK bancaClientePK) {
        this.bancaClientePK = bancaClientePK;
    }

    public String getBancaTxt() {
        return bancaTxt;
    }

    public void setBancaTxt(String bancaTxt) {
        this.bancaTxt = bancaTxt;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getOficina() {
        return oficina;
    }

    public void setOficina(String oficina) {
        this.oficina = oficina;
    }

    public Double getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(Double montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Double getMontoPorciento() {
        return montoPorciento;
    }

    public void setMontoPorciento(Double montoPorciento) {
        this.montoPorciento = montoPorciento;
    }

    public Double getMontoCobrado() {
        return montoCobrado;
    }

    public void setMontoCobrado(Double montoCobrado) {
        this.montoCobrado = montoCobrado;
    }

    public Double getMontoPagado() {
        return montoPagado;
    }

    public void setMontoPagado(Double montoPagado) {
        this.montoPagado = montoPagado;
    }

    public Double getMontoGasto() {
        return montoGasto;
    }

    public void setMontoGasto(Double montoGasto) {
        this.montoGasto = montoGasto;
    }

    public Double getMontoPendiente() {
        return montoPendiente;
    }

    public void setMontoPendiente(Double montoPendiente) {
        this.montoPendiente = montoPendiente;
    }

    public Date getUltimoCobro() {
        return ultimoCobro;
    }

    public void setUltimoCobro(Date ultimoCobro) {
        this.ultimoCobro = ultimoCobro;
    }

    public Double getMontoUltimoCobro() {
        return montoUltimoCobro;
    }

    public void setMontoUltimoCobro(Double montoUltimoCobro) {
        this.montoUltimoCobro = montoUltimoCobro;
    }

    public Date getUltimoPago() {
        return ultimoPago;
    }

    public void setUltimoPago(Date ultimoPago) {
        this.ultimoPago = ultimoPago;
    }

    public Double getMontoUltimoPago() {
        return montoUltimoPago;
    }

    public void setMontoUltimoPago(Double montoUltimoPago) {
        this.montoUltimoPago = montoUltimoPago;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bancaClientePK != null ? bancaClientePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BancaCliente)) {
            return false;
        }
        BancaCliente other = (BancaCliente) object;
        if ((this.bancaClientePK == null && other.bancaClientePK != null) || (this.bancaClientePK != null && !this.bancaClientePK.equals(other.bancaClientePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.BancaCliente[ bancaClientePK=" + bancaClientePK + " ]";
    }
    
    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "";
    }
    
}
