/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author julio
 */
@Embeddable
public class BancaTransacciondPK implements Serializable,Entidad {
    @Basic(optional = false)
    @NotNull
    @Column(name = "COMPANIA_ID")
    private int companiaId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CONSORCIO_ID")
    private int consorcioId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "GRUPO_ID")
    private int grupoId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BANCA_ID")
    private int bancaId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @Column(name = "NXT_LINEA")
    private long nxtLinea;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRANSACCIONH_ID")
    private int transaccionhId;

    public BancaTransacciondPK() {
    }

    public BancaTransacciondPK(int companiaId, int consorcioId, int grupoId, int bancaId, Date fecha, long nxtLinea, int transaccionhId) {
        this.companiaId = companiaId;
        this.consorcioId = consorcioId;
        this.grupoId = grupoId;
        this.bancaId = bancaId;
        this.fecha = fecha;
        this.nxtLinea = nxtLinea;
        this.transaccionhId = transaccionhId;
    }

    public int getCompaniaId() {
        return companiaId;
    }

    public void setCompaniaId(int companiaId) {
        this.companiaId = companiaId;
    }

    public int getConsorcioId() {
        return consorcioId;
    }

    public void setConsorcioId(int consorcioId) {
        this.consorcioId = consorcioId;
    }

    public int getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(int grupoId) {
        this.grupoId = grupoId;
    }

    public int getBancaId() {
        return bancaId;
    }

    public void setBancaId(int bancaId) {
        this.bancaId = bancaId;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public long getNxtLinea() {
        return nxtLinea;
    }

    public void setNxtLinea(long nxtLinea) {
        this.nxtLinea = nxtLinea;
    }

    public int getTransaccionhId() {
        return transaccionhId;
    }

    public void setTransaccionhId(int transaccionhId) {
        this.transaccionhId = transaccionhId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) companiaId;
        hash += (int) consorcioId;
        hash += (int) grupoId;
        hash += (int) bancaId;
        hash += (fecha != null ? fecha.hashCode() : 0);
        hash += (int) nxtLinea;
        hash += (int) transaccionhId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BancaTransacciondPK)) {
            return false;
        }
        BancaTransacciondPK other = (BancaTransacciondPK) object;
        if (this.companiaId != other.companiaId) {
            return false;
        }
        if (this.consorcioId != other.consorcioId) {
            return false;
        }
        if (this.grupoId != other.grupoId) {
            return false;
        }
        if (this.bancaId != other.bancaId) {
            return false;
        }
        if ((this.fecha == null && other.fecha != null) || (this.fecha != null && !this.fecha.equals(other.fecha))) {
            return false;
        }
        if (this.nxtLinea != other.nxtLinea) {
            return false;
        }
        if (this.transaccionhId != other.transaccionhId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.BancaTransacciondPK[ companiaId=" + companiaId + ", consorcioId=" + consorcioId + ", grupoId=" + grupoId + ", bancaId=" + bancaId + ", fecha=" + fecha + ", nxtLinea=" + nxtLinea + ", transaccionhId=" + transaccionhId + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "";
    }
    
}
