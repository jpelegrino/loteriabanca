/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "banca_consorcio")
@NamedQueries({
    @NamedQuery(name = "BancaConsorcio.findAll", query = "SELECT b FROM BancaConsorcio b"),
    @NamedQuery(name = "BancaConsorcio.findByConsorcioId", query = "SELECT b FROM BancaConsorcio b WHERE b.consorcioId = :consorcioId"),
    @NamedQuery(name = "BancaConsorcio.findByConsorcioTxt", query = "SELECT b FROM BancaConsorcio b WHERE b.consorcioTxt = :consorcioTxt"),
    @NamedQuery(name = "BancaConsorcio.findByEstatus", query = "SELECT b FROM BancaConsorcio b WHERE b.estatus = :estatus")})
public class BancaConsorcio implements Serializable,Entidad {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "consorcio_id")
    private Integer consorcioId;
    @Size(max = 60)
    @Column(name = "consorcio_txt")
    private String consorcioTxt;
    @Column(name = "estatus")
    private Character estatus;
    @JoinColumn(name = "COMPANIA_compania_id", referencedColumnName = "compania_id")
    @ManyToOne
    private SysCompania cOMPANIAcompaniaid;

    public BancaConsorcio() {
    }

    public BancaConsorcio(Integer consorcioId) {
        this.consorcioId = consorcioId;
    }

    public Integer getConsorcioId() {
        return consorcioId;
    }

    public void setConsorcioId(Integer consorcioId) {
        this.consorcioId = consorcioId;
    }

    public String getConsorcioTxt() {
        return consorcioTxt;
    }

    public void setConsorcioTxt(String consorcioTxt) {
        this.consorcioTxt = consorcioTxt;
    }

    public Character getEstatus() {
        return estatus;
    }

    public void setEstatus(Character estatus) {
        this.estatus = estatus;
    }

    public SysCompania getCOMPANIAcompaniaid() {
        return cOMPANIAcompaniaid;
    }

    public void setCOMPANIAcompaniaid(SysCompania cOMPANIAcompaniaid) {
        this.cOMPANIAcompaniaid = cOMPANIAcompaniaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (consorcioId != null ? consorcioId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BancaConsorcio)) {
            return false;
        }
        BancaConsorcio other = (BancaConsorcio) object;
        if ((this.consorcioId == null && other.consorcioId != null) || (this.consorcioId != null && !this.consorcioId.equals(other.consorcioId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.BancaConsorcio[ consorcioId=" + consorcioId + " ]";
    }

   @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "";
    }
    
}
