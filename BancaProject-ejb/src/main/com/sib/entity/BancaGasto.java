/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "banca_gasto")
@NamedQueries({
    @NamedQuery(name = "BancaGasto.findAll", query = "SELECT b FROM BancaGasto b"),
    @NamedQuery(name = "BancaGasto.findByGastoId", query = "SELECT b FROM BancaGasto b WHERE b.gastoId = :gastoId"),
    @NamedQuery(name = "BancaGasto.findByGastoTxt", query = "SELECT b FROM BancaGasto b WHERE b.gastoTxt = :gastoTxt"),
    @NamedQuery(name = "BancaGasto.findByEstatus", query = "SELECT b FROM BancaGasto b WHERE b.estatus = :estatus")})
public class BancaGasto implements Serializable,Entidad {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "GASTO_ID")
    private Integer gastoId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "GASTO_TXT")
    private String gastoTxt;
    @Column(name = "ESTATUS")
    private Character estatus;

    public BancaGasto() {
    }

    public BancaGasto(Integer gastoId) {
        this.gastoId = gastoId;
    }

    public BancaGasto(Integer gastoId, String gastoTxt) {
        this.gastoId = gastoId;
        this.gastoTxt = gastoTxt;
    }

    public Integer getGastoId() {
        return gastoId;
    }

    public void setGastoId(Integer gastoId) {
        this.gastoId = gastoId;
    }

    public String getGastoTxt() {
        return gastoTxt;
    }

    public void setGastoTxt(String gastoTxt) {
        this.gastoTxt = gastoTxt;
    }

    public Character getEstatus() {
        return estatus;
    }

    public void setEstatus(Character estatus) {
        this.estatus = estatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gastoId != null ? gastoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BancaGasto)) {
            return false;
        }
        BancaGasto other = (BancaGasto) object;
        if ((this.gastoId == null && other.gastoId != null) || (this.gastoId != null && !this.gastoId.equals(other.gastoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.BancaGasto[ gastoId=" + gastoId + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "";
    }
    
}
