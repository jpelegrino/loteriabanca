/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "banca_cobrod")
@NamedQueries({
    @NamedQuery(name = "BancaCobrod.findAll", query = "SELECT b FROM BancaCobrod b"),
    @NamedQuery(name = "BancaCobrod.findByCompaniaId", query = "SELECT b FROM BancaCobrod b WHERE b.bancaCobrodPK.companiaId = :companiaId"),
    @NamedQuery(name = "BancaCobrod.findByConsorcioId", query = "SELECT b FROM BancaCobrod b WHERE b.bancaCobrodPK.consorcioId = :consorcioId"),
    @NamedQuery(name = "BancaCobrod.findByGrupoId", query = "SELECT b FROM BancaCobrod b WHERE b.bancaCobrodPK.grupoId = :grupoId"),
    @NamedQuery(name = "BancaCobrod.findByBancaId", query = "SELECT b FROM BancaCobrod b WHERE b.bancaCobrodPK.bancaId = :bancaId"),
    @NamedQuery(name = "BancaCobrod.findByCobroId", query = "SELECT b FROM BancaCobrod b WHERE b.bancaCobrodPK.cobroId = :cobroId"),
    @NamedQuery(name = "BancaCobrod.findByFechaCobro", query = "SELECT b FROM BancaCobrod b WHERE b.bancaCobrodPK.fechaCobro = :fechaCobro"),
    @NamedQuery(name = "BancaCobrod.findByNxtLinea", query = "SELECT b FROM BancaCobrod b WHERE b.bancaCobrodPK.nxtLinea = :nxtLinea"),
    @NamedQuery(name = "BancaCobrod.findByMontoCobrado", query = "SELECT b FROM BancaCobrod b WHERE b.montoCobrado = :montoCobrado"),
    @NamedQuery(name = "BancaCobrod.findByMontoPendiente", query = "SELECT b FROM BancaCobrod b WHERE b.montoPendiente = :montoPendiente"),
    @NamedQuery(name = "BancaCobrod.findByEstatus", query = "SELECT b FROM BancaCobrod b WHERE b.estatus = :estatus")})
public class BancaCobrod implements Serializable,Entidad {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BancaCobrodPK bancaCobrodPK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTO_COBRADO")
    private Double montoCobrado;
    @Column(name = "MONTO_PENDIENTE")
    private Double montoPendiente;
    @Column(name = "ESTATUS")
    private Character estatus;

    public BancaCobrod() {
    }

    public BancaCobrod(BancaCobrodPK bancaCobrodPK) {
        this.bancaCobrodPK = bancaCobrodPK;
    }

    public BancaCobrod(int companiaId, int consorcioId, int grupoId, int bancaId, long cobroId, Date fechaCobro, long nxtLinea) {
        this.bancaCobrodPK = new BancaCobrodPK(companiaId, consorcioId, grupoId, bancaId, cobroId, fechaCobro, nxtLinea);
    }

    public BancaCobrodPK getBancaCobrodPK() {
        return bancaCobrodPK;
    }

    public void setBancaCobrodPK(BancaCobrodPK bancaCobrodPK) {
        this.bancaCobrodPK = bancaCobrodPK;
    }

    public Double getMontoCobrado() {
        return montoCobrado;
    }

    public void setMontoCobrado(Double montoCobrado) {
        this.montoCobrado = montoCobrado;
    }

    public Double getMontoPendiente() {
        return montoPendiente;
    }

    public void setMontoPendiente(Double montoPendiente) {
        this.montoPendiente = montoPendiente;
    }

    public Character getEstatus() {
        return estatus;
    }

    public void setEstatus(Character estatus) {
        this.estatus = estatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bancaCobrodPK != null ? bancaCobrodPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BancaCobrod)) {
            return false;
        }
        BancaCobrod other = (BancaCobrod) object;
        if ((this.bancaCobrodPK == null && other.bancaCobrodPK != null) || (this.bancaCobrodPK != null && !this.bancaCobrodPK.equals(other.bancaCobrodPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.BancaCobrod[ bancaCobrodPK=" + bancaCobrodPK + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "";
    }
    
}
