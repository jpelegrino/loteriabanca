/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author julio
 */
@Embeddable
public class BancaGastohPK implements Serializable,Entidad {
    @Basic(optional = false)
    @NotNull
    @Column(name = "COMPANIA_ID")
    private int companiaId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CONSORCIO_ID")
    private int consorcioId;    
    @Basic(optional = false)
    @NotNull
    @Column(name = "GRUPO_ID")
    private int grupoId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @Column(name = "GASTOH_ID")
    private int gastohId;

    public BancaGastohPK() {
    }

    public BancaGastohPK(int companiaId, int consorcioId, int grupoId, Date fecha, int gastohId) {
        this.companiaId = companiaId;
        this.consorcioId = consorcioId;
        this.grupoId = grupoId;
        this.fecha = fecha;
        this.gastohId = gastohId;
    }

    public int getCompaniaId() {
        return companiaId;
    }

    public void setCompaniaId(int companiaId) {
        this.companiaId = companiaId;
    }

    public int getConsorcioId() {
        return consorcioId;
    }

    public void setConsorcioId(int consorcioId) {
        this.consorcioId = consorcioId;
    }

    public int getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(int grupoId) {
        this.grupoId = grupoId;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getGastohId() {
        return gastohId;
    }

    public void setGastohId(int gastohId) {
        this.gastohId = gastohId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) companiaId;
        hash += (int) consorcioId;
        hash += (int) grupoId;
        hash += (fecha != null ? fecha.hashCode() : 0);
        hash += (int) gastohId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BancaGastohPK)) {
            return false;
        }
        BancaGastohPK other = (BancaGastohPK) object;
        if (this.companiaId != other.companiaId) {
            return false;
        }
        if (this.consorcioId != other.consorcioId) {
            return false;
        }
        if (this.grupoId != other.grupoId) {
            return false;
        }
        if ((this.fecha == null && other.fecha != null) || (this.fecha != null && !this.fecha.equals(other.fecha))) {
            return false;
        }
        if (this.gastohId != other.gastohId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.BancaGastohPK[ companiaId=" + companiaId + ", consorcioId=" + consorcioId + ", grupoId=" + grupoId + ", fecha=" + fecha + ", gastohId=" + gastohId + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "";
    }
    
}
