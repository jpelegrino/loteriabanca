/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "banca_juego")
@NamedQueries({
    @NamedQuery(name = "BancaJuego.findAll", query = "SELECT b FROM BancaJuego b"),
    @NamedQuery(name = "BancaJuego.findByCompaniaId", query = "SELECT b FROM BancaJuego b WHERE b.bancaJuegoPK.companiaId = :companiaId"),
    @NamedQuery(name = "BancaJuego.findByConsorcioId", query = "SELECT b FROM BancaJuego b WHERE b.bancaJuegoPK.consorcioId = :consorcioId"),
    @NamedQuery(name = "BancaJuego.findByGrupoId", query = "SELECT b FROM BancaJuego b WHERE b.bancaJuegoPK.grupoId = :grupoId"),
    @NamedQuery(name = "BancaJuego.findByBancaId", query = "SELECT b FROM BancaJuego b WHERE b.bancaJuegoPK.bancaId = :bancaId"),
    @NamedQuery(name = "BancaJuego.findByLoteriaId", query = "SELECT b FROM BancaJuego b WHERE b.bancaJuegoPK.loteriaId = :loteriaId"),
    @NamedQuery(name = "BancaJuego.findByJuegoId", query = "SELECT b FROM BancaJuego b WHERE b.bancaJuegoPK.juegoId = :juegoId"),
    @NamedQuery(name = "BancaJuego.findByJuegoTxt", query = "SELECT b FROM BancaJuego b WHERE b.juegoTxt = :juegoTxt"),
    @NamedQuery(name = "BancaJuego.findByPorciento", query = "SELECT b FROM BancaJuego b WHERE b.porciento = :porciento"),
    @NamedQuery(name = "BancaJuego.findByEstatus", query = "SELECT b FROM BancaJuego b WHERE b.estatus = :estatus")})
public class BancaJuego implements Serializable,Entidad {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BancaJuegoPK bancaJuegoPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "JUEGO_TXT")
    private String juegoTxt;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PORCIENTO")
    private Double porciento;
    @Column(name = "ESTATUS")
    private Character estatus;

    public BancaJuego() {
    }

    public BancaJuego(BancaJuegoPK bancaJuegoPK) {
        this.bancaJuegoPK = bancaJuegoPK;
    }

    public BancaJuego(BancaJuegoPK bancaJuegoPK, String juegoTxt) {
        this.bancaJuegoPK = bancaJuegoPK;
        this.juegoTxt = juegoTxt;
    }

    public BancaJuego(int companiaId, int consorcioId, int grupoId, int bancaId, int loteriaId, int juegoId) {
        this.bancaJuegoPK = new BancaJuegoPK(companiaId, consorcioId, grupoId, bancaId, loteriaId, juegoId);
    }

    public BancaJuegoPK getBancaJuegoPK() {
        return bancaJuegoPK;
    }

    public void setBancaJuegoPK(BancaJuegoPK bancaJuegoPK) {
        this.bancaJuegoPK = bancaJuegoPK;
    }

    public String getJuegoTxt() {
        return juegoTxt;
    }

    public void setJuegoTxt(String juegoTxt) {
        this.juegoTxt = juegoTxt;
    }

    public Double getPorciento() {
        return porciento;
    }

    public void setPorciento(Double porciento) {
        this.porciento = porciento;
    }

    public Character getEstatus() {
        return estatus;
    }

    public void setEstatus(Character estatus) {
        this.estatus = estatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bancaJuegoPK != null ? bancaJuegoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BancaJuego)) {
            return false;
        }
        BancaJuego other = (BancaJuego) object;
        if ((this.bancaJuegoPK == null && other.bancaJuegoPK != null) || (this.bancaJuegoPK != null && !this.bancaJuegoPK.equals(other.bancaJuegoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.BancaJuego[ bancaJuegoPK=" + bancaJuegoPK + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "";
    }
    
}
