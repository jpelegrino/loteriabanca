/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "banca_loteria")
@NamedQueries({
    @NamedQuery(name = "BancaLoteria.findAll", query = "SELECT b FROM BancaLoteria b"),
    @NamedQuery(name = "BancaLoteria.findByLoteriaId", query = "SELECT b FROM BancaLoteria b WHERE b.loteriaId = :loteriaId"),
    @NamedQuery(name = "BancaLoteria.findByLoteriaTxt", query = "SELECT b FROM BancaLoteria b WHERE b.loteriaTxt = :loteriaTxt"),
    @NamedQuery(name = "BancaLoteria.findByEstatus", query = "SELECT b FROM BancaLoteria b WHERE b.estatus = :estatus")})
public class BancaLoteria implements Serializable,Entidad {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "LOTERIA_ID")
    private Integer loteriaId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "LOTERIA_TXT")
    private String loteriaTxt;
    @Column(name = "ESTATUS")
    private Character estatus;

    public BancaLoteria() {
    }

    public BancaLoteria(Integer loteriaId) {
        this.loteriaId = loteriaId;
    }

    public BancaLoteria(Integer loteriaId, String loteriaTxt) {
        this.loteriaId = loteriaId;
        this.loteriaTxt = loteriaTxt;
    }

    public Integer getLoteriaId() {
        return loteriaId;
    }

    public void setLoteriaId(Integer loteriaId) {
        this.loteriaId = loteriaId;
    }

    public String getLoteriaTxt() {
        return loteriaTxt;
    }

    public void setLoteriaTxt(String loteriaTxt) {
        this.loteriaTxt = loteriaTxt;
    }

    public Character getEstatus() {
        return estatus;
    }

    public void setEstatus(Character estatus) {
        this.estatus = estatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (loteriaId != null ? loteriaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BancaLoteria)) {
            return false;
        }
        BancaLoteria other = (BancaLoteria) object;
        if ((this.loteriaId == null && other.loteriaId != null) || (this.loteriaId != null && !this.loteriaId.equals(other.loteriaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.BancaLoteria[ loteriaId=" + loteriaId + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "";
    }
    
}
