/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.entity;

/**
 *
 * @author jpelegrino
 */
public class NullEntity implements Entidad{

    @Override
    public boolean isNull() {
        return true;
    }

    @Override
    public String getName() {
        return "Este objeto no existe en memoria";
    }
    
}
