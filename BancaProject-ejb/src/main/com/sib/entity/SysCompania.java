/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "sys_compania")
@NamedQueries({
    @NamedQuery(name = "SysCompania.findAll", query = "SELECT s FROM SysCompania s"),
    @NamedQuery(name = "SysCompania.findByCompaniaId", query = "SELECT s FROM SysCompania s WHERE s.companiaId = :companiaId"),
    @NamedQuery(name = "SysCompania.findByCompaniaTxt", query = "SELECT s FROM SysCompania s WHERE s.companiaTxt = :companiaTxt")})
public class SysCompania implements Serializable,Entidad {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "compania_id")
    private Integer companiaId;
    @Size(max = 60)
    @Column(name = "compania_txt")
    private String companiaTxt;
    @OneToMany(mappedBy = "cOMPANIAcompaniaid")
    private List<BancaConsorcio> bancaConsorcioList;

    public SysCompania() {
    }

    public SysCompania(Integer companiaId) {
        this.companiaId = companiaId;
    }

    public Integer getCompaniaId() {
        return companiaId;
    }

    public void setCompaniaId(Integer companiaId) {
        this.companiaId = companiaId;
    }

    public String getCompaniaTxt() {
        return companiaTxt;
    }

    public void setCompaniaTxt(String companiaTxt) {
        this.companiaTxt = companiaTxt;
    }

    public List<BancaConsorcio> getBancaConsorcioList() {
        return bancaConsorcioList;
    }

    public void setBancaConsorcioList(List<BancaConsorcio> bancaConsorcioList) {
        this.bancaConsorcioList = bancaConsorcioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (companiaId != null ? companiaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SysCompania)) {
            return false;
        }
        SysCompania other = (SysCompania) object;
        if ((this.companiaId == null && other.companiaId != null) || (this.companiaId != null && !this.companiaId.equals(other.companiaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.SysCompania[ companiaId=" + companiaId + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "SysCompania";
    }
    
}
