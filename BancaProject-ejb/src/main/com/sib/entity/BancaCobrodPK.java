/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author julio
 */
@Embeddable
public class BancaCobrodPK implements Serializable,Entidad {
    @Basic(optional = false)
    @NotNull
    @Column(name = "COMPANIA_ID")
    private int companiaId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CONSORCIO_ID")
    private int consorcioId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "GRUPO_ID")
    private int grupoId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BANCA_ID")
    private int bancaId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COBRO_ID")
    private long cobroId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_COBRO")
    @Temporal(TemporalType.DATE)
    private Date fechaCobro;
    @Basic(optional = false)
    @Column(name = "NXT_LINEA")
    private long nxtLinea;

    public BancaCobrodPK() {
    }

    public BancaCobrodPK(int companiaId, int consorcioId, int grupoId, int bancaId, long cobroId, Date fechaCobro, long nxtLinea) {
        this.companiaId = companiaId;
        this.consorcioId = consorcioId;
        this.grupoId = grupoId;
        this.bancaId = bancaId;
        this.cobroId = cobroId;
        this.fechaCobro = fechaCobro;
        this.nxtLinea = nxtLinea;
    }

    public int getCompaniaId() {
        return companiaId;
    }

    public void setCompaniaId(int companiaId) {
        this.companiaId = companiaId;
    }

    public int getConsorcioId() {
        return consorcioId;
    }

    public void setConsorcioId(int consorcioId) {
        this.consorcioId = consorcioId;
    }

    public int getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(int grupoId) {
        this.grupoId = grupoId;
    }

    public int getBancaId() {
        return bancaId;
    }

    public void setBancaId(int bancaId) {
        this.bancaId = bancaId;
    }

    public long getCobroId() {
        return cobroId;
    }

    public void setCobroId(long cobroId) {
        this.cobroId = cobroId;
    }

    public Date getFechaCobro() {
        return fechaCobro;
    }

    public void setFechaCobro(Date fechaCobro) {
        this.fechaCobro = fechaCobro;
    }

    public long getNxtLinea() {
        return nxtLinea;
    }

    public void setNxtLinea(long nxtLinea) {
        this.nxtLinea = nxtLinea;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) companiaId;
        hash += (int) consorcioId;
        hash += (int) grupoId;
        hash += (int) bancaId;
        hash += (int) cobroId;
        hash += (fechaCobro != null ? fechaCobro.hashCode() : 0);
        hash += (int) nxtLinea;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BancaCobrodPK)) {
            return false;
        }
        BancaCobrodPK other = (BancaCobrodPK) object;
        if (this.companiaId != other.companiaId) {
            return false;
        }
        if (this.consorcioId != other.consorcioId) {
            return false;
        }
        if (this.grupoId != other.grupoId) {
            return false;
        }
        if (this.bancaId != other.bancaId) {
            return false;
        }
        if (this.cobroId != other.cobroId) {
            return false;
        }
        if ((this.fechaCobro == null && other.fechaCobro != null) || (this.fechaCobro != null && !this.fechaCobro.equals(other.fechaCobro))) {
            return false;
        }
        if (this.nxtLinea != other.nxtLinea) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.BancaCobrodPK[ companiaId=" + companiaId + ", consorcioId=" + consorcioId + ", grupoId=" + grupoId + ", bancaId=" + bancaId + ", cobroId=" + cobroId + ", fechaCobro=" + fechaCobro + ", nxtLinea=" + nxtLinea + " ]";
    }

   @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "";
    }
    
}
