/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author julio
 */
@Embeddable
public class BancaGastodPK implements Serializable,Entidad {
    @Basic(optional = false)
    @NotNull
    @Column(name = "COMPANIA_ID")
    private int companiaId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CONSORCIO_ID")
    private int consorcioId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "GRUPO_ID")
    private int grupoId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BANCA_ID")
    private int bancaId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "GASTOH_ID")
    private int gastohId;
    @Basic(optional = false)
    @Column(name = "NXT_LINEA")
    private int nxtLinea;

    public BancaGastodPK() {
    }

    public BancaGastodPK(int companiaId, int consorcioId, int grupoId, int bancaId, int gastohId, int nxtLinea) {
        this.companiaId = companiaId;
        this.consorcioId = consorcioId;
        this.grupoId = grupoId;
        this.bancaId = bancaId;
        this.gastohId = gastohId;
        this.nxtLinea = nxtLinea;
    }

    public int getCompaniaId() {
        return companiaId;
    }

    public void setCompaniaId(int companiaId) {
        this.companiaId = companiaId;
    }

    public int getConsorcioId() {
        return consorcioId;
    }

    public void setConsorcioId(int consorcioId) {
        this.consorcioId = consorcioId;
    }

    public int getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(int grupoId) {
        this.grupoId = grupoId;
    }

    public int getBancaId() {
        return bancaId;
    }

    public void setBancaId(int bancaId) {
        this.bancaId = bancaId;
    }

    public int getGastohId() {
        return gastohId;
    }

    public void setGastohId(int gastohId) {
        this.gastohId = gastohId;
    }

    public int getNxtLinea() {
        return nxtLinea;
    }

    public void setNxtLinea(int nxtLinea) {
        this.nxtLinea = nxtLinea;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) companiaId;
        hash += (int) consorcioId;
        hash += (int) grupoId;
        hash += (int) bancaId;
        hash += (int) gastohId;
        hash += (int) nxtLinea;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BancaGastodPK)) {
            return false;
        }
        BancaGastodPK other = (BancaGastodPK) object;
        if (this.companiaId != other.companiaId) {
            return false;
        }
        if (this.consorcioId != other.consorcioId) {
            return false;
        }
        if (this.grupoId != other.grupoId) {
            return false;
        }
        if (this.bancaId != other.bancaId) {
            return false;
        }
        if (this.gastohId != other.gastohId) {
            return false;
        }
        if (this.nxtLinea != other.nxtLinea) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.BancaGastodPK[ companiaId=" + companiaId + ", consorcioId=" + consorcioId + ", grupoId=" + grupoId + ", bancaId=" + bancaId + ", gastohId=" + gastohId + ", nxtLinea=" + nxtLinea + " ]";
    }

   @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "";
    }
    
}
