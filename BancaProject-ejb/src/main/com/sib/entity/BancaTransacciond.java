/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "banca_transacciond")
@NamedQueries({
    @NamedQuery(name = "BancaTransacciond.findAll", query = "SELECT b FROM BancaTransacciond b"),
    @NamedQuery(name = "BancaTransacciond.findByCompaniaId", query = "SELECT b FROM BancaTransacciond b WHERE b.bancaTransacciondPK.companiaId = :companiaId"),
    @NamedQuery(name = "BancaTransacciond.findByConsorcioId", query = "SELECT b FROM BancaTransacciond b WHERE b.bancaTransacciondPK.consorcioId = :consorcioId"),
    @NamedQuery(name = "BancaTransacciond.findByGrupoId", query = "SELECT b FROM BancaTransacciond b WHERE b.bancaTransacciondPK.grupoId = :grupoId"),
    @NamedQuery(name = "BancaTransacciond.findByBancaId", query = "SELECT b FROM BancaTransacciond b WHERE b.bancaTransacciondPK.bancaId = :bancaId"),
    @NamedQuery(name = "BancaTransacciond.findByFecha", query = "SELECT b FROM BancaTransacciond b WHERE b.bancaTransacciondPK.fecha = :fecha"),
    @NamedQuery(name = "BancaTransacciond.findByNxtLinea", query = "SELECT b FROM BancaTransacciond b WHERE b.bancaTransacciondPK.nxtLinea = :nxtLinea"),
    @NamedQuery(name = "BancaTransacciond.findByTransaccionhId", query = "SELECT b FROM BancaTransacciond b WHERE b.bancaTransacciondPK.transaccionhId = :transaccionhId"),
    @NamedQuery(name = "BancaTransacciond.findByLoteriaId", query = "SELECT b FROM BancaTransacciond b WHERE b.loteriaId = :loteriaId"),
    @NamedQuery(name = "BancaTransacciond.findByTotalVenta", query = "SELECT b FROM BancaTransacciond b WHERE b.totalVenta = :totalVenta"),
    @NamedQuery(name = "BancaTransacciond.findByTotalPagado", query = "SELECT b FROM BancaTransacciond b WHERE b.totalPagado = :totalPagado"),
    @NamedQuery(name = "BancaTransacciond.findByQuiniela", query = "SELECT b FROM BancaTransacciond b WHERE b.quiniela = :quiniela"),
    @NamedQuery(name = "BancaTransacciond.findByPale", query = "SELECT b FROM BancaTransacciond b WHERE b.pale = :pale"),
    @NamedQuery(name = "BancaTransacciond.findByTripleta", query = "SELECT b FROM BancaTransacciond b WHERE b.tripleta = :tripleta"),
    @NamedQuery(name = "BancaTransacciond.findBySuperpale", query = "SELECT b FROM BancaTransacciond b WHERE b.superpale = :superpale"),
    @NamedQuery(name = "BancaTransacciond.findByEstatus", query = "SELECT b FROM BancaTransacciond b WHERE b.estatus = :estatus")})
public class BancaTransacciond implements Serializable,Entidad {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BancaTransacciondPK bancaTransacciondPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LOTERIA_ID")
    private int loteriaId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "TOTAL_VENTA")
    private Double totalVenta;
    @Column(name = "TOTAL_PAGADO")
    private Double totalPagado;
    @Column(name = "QUINIELA")
    private Double quiniela;
    @Column(name = "PALE")
    private Double pale;
    @Column(name = "TRIPLETA")
    private Double tripleta;
    @Column(name = "SUPERPALE")
    private Double superpale;
    @Column(name = "ESTATUS")
    private Character estatus;

    public BancaTransacciond() {
    }

    public BancaTransacciond(BancaTransacciondPK bancaTransacciondPK) {
        this.bancaTransacciondPK = bancaTransacciondPK;
    }

    public BancaTransacciond(BancaTransacciondPK bancaTransacciondPK, int loteriaId) {
        this.bancaTransacciondPK = bancaTransacciondPK;
        this.loteriaId = loteriaId;
    }

    public BancaTransacciond(int companiaId, int consorcioId, int grupoId, int bancaId, Date fecha, long nxtLinea, int transaccionhId) {
        this.bancaTransacciondPK = new BancaTransacciondPK(companiaId, consorcioId, grupoId, bancaId, fecha, nxtLinea, transaccionhId);
    }

    public BancaTransacciondPK getBancaTransacciondPK() {
        return bancaTransacciondPK;
    }

    public void setBancaTransacciondPK(BancaTransacciondPK bancaTransacciondPK) {
        this.bancaTransacciondPK = bancaTransacciondPK;
    }

    public int getLoteriaId() {
        return loteriaId;
    }

    public void setLoteriaId(int loteriaId) {
        this.loteriaId = loteriaId;
    }

    public Double getTotalVenta() {
        return totalVenta;
    }

    public void setTotalVenta(Double totalVenta) {
        this.totalVenta = totalVenta;
    }

    public Double getTotalPagado() {
        return totalPagado;
    }

    public void setTotalPagado(Double totalPagado) {
        this.totalPagado = totalPagado;
    }

    public Double getQuiniela() {
        return quiniela;
    }

    public void setQuiniela(Double quiniela) {
        this.quiniela = quiniela;
    }

    public Double getPale() {
        return pale;
    }

    public void setPale(Double pale) {
        this.pale = pale;
    }

    public Double getTripleta() {
        return tripleta;
    }

    public void setTripleta(Double tripleta) {
        this.tripleta = tripleta;
    }

    public Double getSuperpale() {
        return superpale;
    }

    public void setSuperpale(Double superpale) {
        this.superpale = superpale;
    }

    public Character getEstatus() {
        return estatus;
    }

    public void setEstatus(Character estatus) {
        this.estatus = estatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bancaTransacciondPK != null ? bancaTransacciondPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BancaTransacciond)) {
            return false;
        }
        BancaTransacciond other = (BancaTransacciond) object;
        if ((this.bancaTransacciondPK == null && other.bancaTransacciondPK != null) || (this.bancaTransacciondPK != null && !this.bancaTransacciondPK.equals(other.bancaTransacciondPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.BancaTransacciond[ bancaTransacciondPK=" + bancaTransacciondPK + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "";
    }
    
}
