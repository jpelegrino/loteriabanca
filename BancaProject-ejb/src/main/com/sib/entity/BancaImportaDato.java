/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "banca_importa_dato")
@NamedQueries({
    @NamedQuery(name = "BancaImportaDato.findAll", query = "SELECT b FROM BancaImportaDato b"),
    @NamedQuery(name = "BancaImportaDato.findByCompaniaId", query = "SELECT b FROM BancaImportaDato b WHERE b.bancaImportaDatoPK.companiaId = :companiaId"),
    @NamedQuery(name = "BancaImportaDato.findByConsorcioId", query = "SELECT b FROM BancaImportaDato b WHERE b.bancaImportaDatoPK.consorcioId = :consorcioId"),
    @NamedQuery(name = "BancaImportaDato.findByGrupoId", query = "SELECT b FROM BancaImportaDato b WHERE b.bancaImportaDatoPK.grupoId = :grupoId"),
    @NamedQuery(name = "BancaImportaDato.findByBancaId", query = "SELECT b FROM BancaImportaDato b WHERE b.bancaImportaDatoPK.bancaId = :bancaId"),
    @NamedQuery(name = "BancaImportaDato.findByFecha", query = "SELECT b FROM BancaImportaDato b WHERE b.bancaImportaDatoPK.fecha = :fecha"),
    @NamedQuery(name = "BancaImportaDato.findByNxtLinea", query = "SELECT b FROM BancaImportaDato b WHERE b.bancaImportaDatoPK.nxtLinea = :nxtLinea"),
    @NamedQuery(name = "BancaImportaDato.findByLoteriaId", query = "SELECT b FROM BancaImportaDato b WHERE b.loteriaId = :loteriaId"),
    @NamedQuery(name = "BancaImportaDato.findByQuiniela", query = "SELECT b FROM BancaImportaDato b WHERE b.quiniela = :quiniela"),
    @NamedQuery(name = "BancaImportaDato.findByPale", query = "SELECT b FROM BancaImportaDato b WHERE b.pale = :pale"),
    @NamedQuery(name = "BancaImportaDato.findByTripleta", query = "SELECT b FROM BancaImportaDato b WHERE b.tripleta = :tripleta"),
    @NamedQuery(name = "BancaImportaDato.findBySuperpale", query = "SELECT b FROM BancaImportaDato b WHERE b.superpale = :superpale"),
    @NamedQuery(name = "BancaImportaDato.findByTotalVenta", query = "SELECT b FROM BancaImportaDato b WHERE b.totalVenta = :totalVenta"),
    @NamedQuery(name = "BancaImportaDato.findByTotalPagado", query = "SELECT b FROM BancaImportaDato b WHERE b.totalPagado = :totalPagado")})
public class BancaImportaDato implements Serializable,Entidad {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BancaImportaDatoPK bancaImportaDatoPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LOTERIA_ID")
    private int loteriaId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "QUINIELA")
    private Double quiniela;
    @Column(name = "PALE")
    private Double pale;
    @Column(name = "TRIPLETA")
    private Double tripleta;
    @Column(name = "SUPERPALE")
    private Double superpale;
    @Column(name = "TOTAL_VENTA")
    private Double totalVenta;
    @Column(name = "TOTAL_PAGADO")
    private Double totalPagado;

    public BancaImportaDato() {
    }

    public BancaImportaDato(BancaImportaDatoPK bancaImportaDatoPK) {
        this.bancaImportaDatoPK = bancaImportaDatoPK;
    }

    public BancaImportaDato(BancaImportaDatoPK bancaImportaDatoPK, int loteriaId) {
        this.bancaImportaDatoPK = bancaImportaDatoPK;
        this.loteriaId = loteriaId;
    }

    public BancaImportaDato(int companiaId, int consorcioId, int grupoId, int bancaId, Date fecha, long nxtLinea) {
        this.bancaImportaDatoPK = new BancaImportaDatoPK(companiaId, consorcioId, grupoId, bancaId, fecha, nxtLinea);
    }

    public BancaImportaDatoPK getBancaImportaDatoPK() {
        return bancaImportaDatoPK;
    }

    public void setBancaImportaDatoPK(BancaImportaDatoPK bancaImportaDatoPK) {
        this.bancaImportaDatoPK = bancaImportaDatoPK;
    }

    public int getLoteriaId() {
        return loteriaId;
    }

    public void setLoteriaId(int loteriaId) {
        this.loteriaId = loteriaId;
    }

    public Double getQuiniela() {
        return quiniela;
    }

    public void setQuiniela(Double quiniela) {
        this.quiniela = quiniela;
    }

    public Double getPale() {
        return pale;
    }

    public void setPale(Double pale) {
        this.pale = pale;
    }

    public Double getTripleta() {
        return tripleta;
    }

    public void setTripleta(Double tripleta) {
        this.tripleta = tripleta;
    }

    public Double getSuperpale() {
        return superpale;
    }

    public void setSuperpale(Double superpale) {
        this.superpale = superpale;
    }

    public Double getTotalVenta() {
        return totalVenta;
    }

    public void setTotalVenta(Double totalVenta) {
        this.totalVenta = totalVenta;
    }

    public Double getTotalPagado() {
        return totalPagado;
    }

    public void setTotalPagado(Double totalPagado) {
        this.totalPagado = totalPagado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bancaImportaDatoPK != null ? bancaImportaDatoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BancaImportaDato)) {
            return false;
        }
        BancaImportaDato other = (BancaImportaDato) object;
        if ((this.bancaImportaDatoPK == null && other.bancaImportaDatoPK != null) || (this.bancaImportaDatoPK != null && !this.bancaImportaDatoPK.equals(other.bancaImportaDatoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.BancaImportaDato[ bancaImportaDatoPK=" + bancaImportaDatoPK + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "";
    }
    
}
