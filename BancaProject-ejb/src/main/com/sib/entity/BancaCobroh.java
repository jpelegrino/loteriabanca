/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "banca_cobroh")
@NamedQueries({
    @NamedQuery(name = "BancaCobroh.findAll", query = "SELECT b FROM BancaCobroh b"),
    @NamedQuery(name = "BancaCobroh.findByCompaniaId", query = "SELECT b FROM BancaCobroh b WHERE b.bancaCobrohPK.companiaId = :companiaId"),
    @NamedQuery(name = "BancaCobroh.findByConsorcioId", query = "SELECT b FROM BancaCobroh b WHERE b.bancaCobrohPK.consorcioId = :consorcioId"),
    @NamedQuery(name = "BancaCobroh.findByGrupoId", query = "SELECT b FROM BancaCobroh b WHERE b.bancaCobrohPK.grupoId = :grupoId"),
    @NamedQuery(name = "BancaCobroh.findByCobroId", query = "SELECT b FROM BancaCobroh b WHERE b.bancaCobrohPK.cobroId = :cobroId"),
    @NamedQuery(name = "BancaCobroh.findByFechaCobro", query = "SELECT b FROM BancaCobroh b WHERE b.bancaCobrohPK.fechaCobro = :fechaCobro"),
    @NamedQuery(name = "BancaCobroh.findByCobroTxt", query = "SELECT b FROM BancaCobroh b WHERE b.cobroTxt = :cobroTxt"),
    @NamedQuery(name = "BancaCobroh.findByMontoCobrado", query = "SELECT b FROM BancaCobroh b WHERE b.montoCobrado = :montoCobrado"),
    @NamedQuery(name = "BancaCobroh.findByMontoPendiente", query = "SELECT b FROM BancaCobroh b WHERE b.montoPendiente = :montoPendiente"),
    @NamedQuery(name = "BancaCobroh.findByEstatus", query = "SELECT b FROM BancaCobroh b WHERE b.estatus = :estatus")})
public class BancaCobroh implements Serializable,Entidad {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BancaCobrohPK bancaCobrohPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 160)
    @Column(name = "COBRO_TXT")
    private String cobroTxt;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTO_COBRADO")
    private Double montoCobrado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MONTO_PENDIENTE")
    private double montoPendiente;
    @Column(name = "ESTATUS")
    private Character estatus;

    public BancaCobroh() {
    }

    public BancaCobroh(BancaCobrohPK bancaCobrohPK) {
        this.bancaCobrohPK = bancaCobrohPK;
    }

    public BancaCobroh(BancaCobrohPK bancaCobrohPK, String cobroTxt, double montoPendiente) {
        this.bancaCobrohPK = bancaCobrohPK;
        this.cobroTxt = cobroTxt;
        this.montoPendiente = montoPendiente;
    }

    public BancaCobroh(int companiaId, int consorcioId, int grupoId, long cobroId, Date fechaCobro) {
        this.bancaCobrohPK = new BancaCobrohPK(companiaId, consorcioId, grupoId, cobroId, fechaCobro);
    }

    public BancaCobrohPK getBancaCobrohPK() {
        return bancaCobrohPK;
    }

    public void setBancaCobrohPK(BancaCobrohPK bancaCobrohPK) {
        this.bancaCobrohPK = bancaCobrohPK;
    }

    public String getCobroTxt() {
        return cobroTxt;
    }

    public void setCobroTxt(String cobroTxt) {
        this.cobroTxt = cobroTxt;
    }

    public Double getMontoCobrado() {
        return montoCobrado;
    }

    public void setMontoCobrado(Double montoCobrado) {
        this.montoCobrado = montoCobrado;
    }

    public double getMontoPendiente() {
        return montoPendiente;
    }

    public void setMontoPendiente(double montoPendiente) {
        this.montoPendiente = montoPendiente;
    }

    public Character getEstatus() {
        return estatus;
    }

    public void setEstatus(Character estatus) {
        this.estatus = estatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bancaCobrohPK != null ? bancaCobrohPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BancaCobroh)) {
            return false;
        }
        BancaCobroh other = (BancaCobroh) object;
        if ((this.bancaCobrohPK == null && other.bancaCobrohPK != null) || (this.bancaCobrohPK != null && !this.bancaCobrohPK.equals(other.bancaCobrohPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.BancaCobroh[ bancaCobrohPK=" + bancaCobrohPK + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "";
    }
    
}
