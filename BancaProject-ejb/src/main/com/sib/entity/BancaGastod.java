/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "banca_gastod")
@NamedQueries({
    @NamedQuery(name = "BancaGastod.findAll", query = "SELECT b FROM BancaGastod b"),
    @NamedQuery(name = "BancaGastod.findByCompaniaId", query = "SELECT b FROM BancaGastod b WHERE b.bancaGastodPK.companiaId = :companiaId"),
    @NamedQuery(name = "BancaGastod.findByConsorcioId", query = "SELECT b FROM BancaGastod b WHERE b.bancaGastodPK.consorcioId = :consorcioId"),
    @NamedQuery(name = "BancaGastod.findByGrupoId", query = "SELECT b FROM BancaGastod b WHERE b.bancaGastodPK.grupoId = :grupoId"),
    @NamedQuery(name = "BancaGastod.findByBancaId", query = "SELECT b FROM BancaGastod b WHERE b.bancaGastodPK.bancaId = :bancaId"),
    @NamedQuery(name = "BancaGastod.findByGastohId", query = "SELECT b FROM BancaGastod b WHERE b.bancaGastodPK.gastohId = :gastohId"),
    @NamedQuery(name = "BancaGastod.findByNxtLinea", query = "SELECT b FROM BancaGastod b WHERE b.bancaGastodPK.nxtLinea = :nxtLinea"),
    @NamedQuery(name = "BancaGastod.findByFecha", query = "SELECT b FROM BancaGastod b WHERE b.fecha = :fecha"),
    @NamedQuery(name = "BancaGastod.findByGastoId", query = "SELECT b FROM BancaGastod b WHERE b.gastoId = :gastoId"),
    @NamedQuery(name = "BancaGastod.findByMontoGasto", query = "SELECT b FROM BancaGastod b WHERE b.montoGasto = :montoGasto"),
    @NamedQuery(name = "BancaGastod.findByEstatus", query = "SELECT b FROM BancaGastod b WHERE b.estatus = :estatus")})
public class BancaGastod implements Serializable,Entidad {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BancaGastodPK bancaGastodPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "GASTO_ID")
    private int gastoId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTO_GASTO")
    private Double montoGasto;
    @Column(name = "ESTATUS")
    private Character estatus;

    public BancaGastod() {
    }

    public BancaGastod(BancaGastodPK bancaGastodPK) {
        this.bancaGastodPK = bancaGastodPK;
    }

    public BancaGastod(BancaGastodPK bancaGastodPK, Date fecha, int gastoId) {
        this.bancaGastodPK = bancaGastodPK;
        this.fecha = fecha;
        this.gastoId = gastoId;
    }

    public BancaGastod(int companiaId, int consorcioId, int grupoId, int bancaId, int gastohId, int nxtLinea) {
        this.bancaGastodPK = new BancaGastodPK(companiaId, consorcioId, grupoId, bancaId, gastohId, nxtLinea);
    }

    public BancaGastodPK getBancaGastodPK() {
        return bancaGastodPK;
    }

    public void setBancaGastodPK(BancaGastodPK bancaGastodPK) {
        this.bancaGastodPK = bancaGastodPK;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getGastoId() {
        return gastoId;
    }

    public void setGastoId(int gastoId) {
        this.gastoId = gastoId;
    }

    public Double getMontoGasto() {
        return montoGasto;
    }

    public void setMontoGasto(Double montoGasto) {
        this.montoGasto = montoGasto;
    }

    public Character getEstatus() {
        return estatus;
    }

    public void setEstatus(Character estatus) {
        this.estatus = estatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bancaGastodPK != null ? bancaGastodPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BancaGastod)) {
            return false;
        }
        BancaGastod other = (BancaGastod) object;
        if ((this.bancaGastodPK == null && other.bancaGastodPK != null) || (this.bancaGastodPK != null && !this.bancaGastodPK.equals(other.bancaGastodPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.BancaGastod[ bancaGastodPK=" + bancaGastodPK + " ]";
    }

   @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "";
    }
    
}
