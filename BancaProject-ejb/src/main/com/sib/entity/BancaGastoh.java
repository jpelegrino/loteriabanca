/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "banca_gastoh")
@NamedQueries({
    @NamedQuery(name = "BancaGastoh.findAll", query = "SELECT b FROM BancaGastoh b"),
    @NamedQuery(name = "BancaGastoh.findByCompaniaId", query = "SELECT b FROM BancaGastoh b WHERE b.bancaGastohPK.companiaId = :companiaId"),
    @NamedQuery(name = "BancaGastoh.findByConsorcioId", query = "SELECT b FROM BancaGastoh b WHERE b.bancaGastohPK.consorcioId = :consorcioId"),
    @NamedQuery(name = "BancaGastoh.findByGrupoId", query = "SELECT b FROM BancaGastoh b WHERE b.bancaGastohPK.grupoId = :grupoId"),
    @NamedQuery(name = "BancaGastoh.findByFecha", query = "SELECT b FROM BancaGastoh b WHERE b.bancaGastohPK.fecha = :fecha"),
    @NamedQuery(name = "BancaGastoh.findByGastohId", query = "SELECT b FROM BancaGastoh b WHERE b.bancaGastohPK.gastohId = :gastohId"),
    @NamedQuery(name = "BancaGastoh.findByGastoTxt", query = "SELECT b FROM BancaGastoh b WHERE b.gastoTxt = :gastoTxt"),
    @NamedQuery(name = "BancaGastoh.findByMontoTotal", query = "SELECT b FROM BancaGastoh b WHERE b.montoTotal = :montoTotal"),
    @NamedQuery(name = "BancaGastoh.findByEstatus", query = "SELECT b FROM BancaGastoh b WHERE b.estatus = :estatus")})
public class BancaGastoh implements Serializable,Entidad {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BancaGastohPK bancaGastohPK;
    @Size(max = 60)
    @Column(name = "GASTO_TXT")
    private String gastoTxt;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTO_TOTAL")
    private Double montoTotal;
    @Column(name = "ESTATUS")
    private Character estatus;

    public BancaGastoh() {
    }

    public BancaGastoh(BancaGastohPK bancaGastohPK) {
        this.bancaGastohPK = bancaGastohPK;
    }

    public BancaGastoh(int companiaId, int consorcioId, int grupoId, Date fecha, int gastohId) {
        this.bancaGastohPK = new BancaGastohPK(companiaId, consorcioId, grupoId, fecha, gastohId);
    }

    public BancaGastohPK getBancaGastohPK() {
        return bancaGastohPK;
    }

    public void setBancaGastohPK(BancaGastohPK bancaGastohPK) {
        this.bancaGastohPK = bancaGastohPK;
    }

    public String getGastoTxt() {
        return gastoTxt;
    }

    public void setGastoTxt(String gastoTxt) {
        this.gastoTxt = gastoTxt;
    }

    public Double getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(Double montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Character getEstatus() {
        return estatus;
    }

    public void setEstatus(Character estatus) {
        this.estatus = estatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bancaGastohPK != null ? bancaGastohPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BancaGastoh)) {
            return false;
        }
        BancaGastoh other = (BancaGastoh) object;
        if ((this.bancaGastohPK == null && other.bancaGastohPK != null) || (this.bancaGastohPK != null && !this.bancaGastohPK.equals(other.bancaGastohPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.BancaGastoh[ bancaGastohPK=" + bancaGastohPK + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "";
    }
    
}
