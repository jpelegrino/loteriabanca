/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "banca_grupo")
@NamedQueries({
    @NamedQuery(name = "BancaGrupo.findAll", query = "SELECT b FROM BancaGrupo b"),
    @NamedQuery(name = "BancaGrupo.findByCompaniaId", query = "SELECT b FROM BancaGrupo b WHERE b.bancaGrupoPK.companiaId = :companiaId"),
    @NamedQuery(name = "BancaGrupo.findByConsorcioId", query = "SELECT b FROM BancaGrupo b WHERE b.bancaGrupoPK.consorcioId = :consorcioId"),
    @NamedQuery(name = "BancaGrupo.findByGrupoId", query = "SELECT b FROM BancaGrupo b WHERE b.bancaGrupoPK.grupoId = :grupoId"),
    @NamedQuery(name = "BancaGrupo.findByGrupoTxt", query = "SELECT b FROM BancaGrupo b WHERE b.grupoTxt = :grupoTxt"),
    @NamedQuery(name = "BancaGrupo.findByStatus", query = "SELECT b FROM BancaGrupo b WHERE b.status = :status")})
public class BancaGrupo implements Serializable,Entidad {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BancaGrupoPK bancaGrupoPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "GRUPO_TXT")
    private String grupoTxt;
    @Size(max = 1)
    @Column(name = "STATUS")
    private String status;

    public BancaGrupo() {
    }

    public BancaGrupo(BancaGrupoPK bancaGrupoPK) {
        this.bancaGrupoPK = bancaGrupoPK;
    }

    public BancaGrupo(BancaGrupoPK bancaGrupoPK, String grupoTxt) {
        this.bancaGrupoPK = bancaGrupoPK;
        this.grupoTxt = grupoTxt;
    }

    public BancaGrupo(int companiaId, int consorcioId, int grupoId) {
        this.bancaGrupoPK = new BancaGrupoPK(companiaId, consorcioId, grupoId);
    }

    public BancaGrupoPK getBancaGrupoPK() {
        return bancaGrupoPK;
    }

    public void setBancaGrupoPK(BancaGrupoPK bancaGrupoPK) {
        this.bancaGrupoPK = bancaGrupoPK;
    }

    public String getGrupoTxt() {
        return grupoTxt;
    }

    public void setGrupoTxt(String grupoTxt) {
        this.grupoTxt = grupoTxt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bancaGrupoPK != null ? bancaGrupoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BancaGrupo)) {
            return false;
        }
        BancaGrupo other = (BancaGrupo) object;
        if ((this.bancaGrupoPK == null && other.bancaGrupoPK != null) || (this.bancaGrupoPK != null && !this.bancaGrupoPK.equals(other.bancaGrupoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.BancaGrupo[ bancaGrupoPK=" + bancaGrupoPK + " ]";
    }
    
    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "";
    }
    
}
