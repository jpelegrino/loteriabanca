/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "banca_transaccionh")
@NamedQueries({
    @NamedQuery(name = "BancaTransaccionh.findAll", query = "SELECT b FROM BancaTransaccionh b"),
    @NamedQuery(name = "BancaTransaccionh.findByCompaniaId", query = "SELECT b FROM BancaTransaccionh b WHERE b.bancaTransaccionhPK.companiaId = :companiaId"),
    @NamedQuery(name = "BancaTransaccionh.findByConsorcioId", query = "SELECT b FROM BancaTransaccionh b WHERE b.bancaTransaccionhPK.consorcioId = :consorcioId"),
    @NamedQuery(name = "BancaTransaccionh.findByGrupoId", query = "SELECT b FROM BancaTransaccionh b WHERE b.bancaTransaccionhPK.grupoId = :grupoId"),
    @NamedQuery(name = "BancaTransaccionh.findByBancaId", query = "SELECT b FROM BancaTransaccionh b WHERE b.bancaTransaccionhPK.bancaId = :bancaId"),
    @NamedQuery(name = "BancaTransaccionh.findByTransaccionhId", query = "SELECT b FROM BancaTransaccionh b WHERE b.bancaTransaccionhPK.transaccionhId = :transaccionhId"),
    @NamedQuery(name = "BancaTransaccionh.findByFecha", query = "SELECT b FROM BancaTransaccionh b WHERE b.fecha = :fecha"),
    @NamedQuery(name = "BancaTransaccionh.findByTotalVendido", query = "SELECT b FROM BancaTransaccionh b WHERE b.totalVendido = :totalVendido"),
    @NamedQuery(name = "BancaTransaccionh.findByTotalPorciento", query = "SELECT b FROM BancaTransaccionh b WHERE b.totalPorciento = :totalPorciento"),
    @NamedQuery(name = "BancaTransaccionh.findByTotalPagado", query = "SELECT b FROM BancaTransaccionh b WHERE b.totalPagado = :totalPagado"),
    @NamedQuery(name = "BancaTransaccionh.findByEstatus", query = "SELECT b FROM BancaTransaccionh b WHERE b.estatus = :estatus")})
public class BancaTransaccionh implements Serializable,Entidad {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BancaTransaccionhPK bancaTransaccionhPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "TOTAL_VENDIDO")
    private Double totalVendido;
    @Column(name = "TOTAL_PORCIENTO")
    private Double totalPorciento;
    @Column(name = "TOTAL_PAGADO")
    private Double totalPagado;
    @Size(max = 45)
    @Column(name = "ESTATUS")
    private String estatus;

    public BancaTransaccionh() {
    }

    public BancaTransaccionh(BancaTransaccionhPK bancaTransaccionhPK) {
        this.bancaTransaccionhPK = bancaTransaccionhPK;
    }

    public BancaTransaccionh(BancaTransaccionhPK bancaTransaccionhPK, Date fecha) {
        this.bancaTransaccionhPK = bancaTransaccionhPK;
        this.fecha = fecha;
    }

    public BancaTransaccionh(int companiaId, int consorcioId, int grupoId, int bancaId, int transaccionhId) {
        this.bancaTransaccionhPK = new BancaTransaccionhPK(companiaId, consorcioId, grupoId, bancaId, transaccionhId);
    }

    public BancaTransaccionhPK getBancaTransaccionhPK() {
        return bancaTransaccionhPK;
    }

    public void setBancaTransaccionhPK(BancaTransaccionhPK bancaTransaccionhPK) {
        this.bancaTransaccionhPK = bancaTransaccionhPK;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Double getTotalVendido() {
        return totalVendido;
    }

    public void setTotalVendido(Double totalVendido) {
        this.totalVendido = totalVendido;
    }

    public Double getTotalPorciento() {
        return totalPorciento;
    }

    public void setTotalPorciento(Double totalPorciento) {
        this.totalPorciento = totalPorciento;
    }

    public Double getTotalPagado() {
        return totalPagado;
    }

    public void setTotalPagado(Double totalPagado) {
        this.totalPagado = totalPagado;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bancaTransaccionhPK != null ? bancaTransaccionhPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BancaTransaccionh)) {
            return false;
        }
        BancaTransaccionh other = (BancaTransaccionh) object;
        if ((this.bancaTransaccionhPK == null && other.bancaTransaccionhPK != null) || (this.bancaTransaccionhPK != null && !this.bancaTransaccionhPK.equals(other.bancaTransaccionhPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.BancaTransaccionh[ bancaTransaccionhPK=" + bancaTransaccionhPK + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "";
    }
    
}
