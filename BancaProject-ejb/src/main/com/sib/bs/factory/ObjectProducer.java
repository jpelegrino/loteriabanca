/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.bs.factory;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author julio
 */
public class ObjectProducer {
    
    private static Map<String,ManageFactory> mf=new HashMap<String,ManageFactory>();
    
    public static ManageFactory getFactory(String factory){
        ManageFactory manage=(ManageFactory)mf.get(factory);
        
        if (manage==null) {
            manage=FactoryProducer.getFactory(factory);
            mf.put(factory, manage);
        }
        
        return manage;
        
        
    
    }
    
}
