/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.bs.factory;

/**
 *
 * @author julio
 */
public class FactoryProducer {
    
    public static ManageFactory getFactory(String factory) {
        ManageFactory manage=null;
        String facto=factory.toLowerCase();
        if (facto.equalsIgnoreCase("entityfactory")) {
            manage=new EntityFactory();
        }
     
        return manage;
    
    }
    
}
