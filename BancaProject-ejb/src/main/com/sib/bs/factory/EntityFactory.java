/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.bs.factory;

import com.sib.entity.*;

/**
 *
 * @author julio
 */
public class EntityFactory extends ManageFactory{

    @Override
    public Entidad getEntity(String entity) {
        Entidad entidad=null;
        String low=entity.toLowerCase();
        
        if (low.equalsIgnoreCase("bancacliente")) {
            entidad=new BancaCliente();
        }else if(low.equalsIgnoreCase("bancaclientepk")) {
            entidad=new BancaClientePK();
        }else if(low.equalsIgnoreCase("bancacobrod")) {
            entidad=new BancaCobrod();
        }else if(low.equalsIgnoreCase("bancacobropk")) {
            entidad=new BancaCobrodPK();
        }else if(low.equalsIgnoreCase("bancacobroh")) {
            entidad=new BancaCobroh();
        }else if(low.equalsIgnoreCase("bancacobrohpk")) {
            entidad=new BancaCobrohPK();
        }else if(low.equalsIgnoreCase("bancaconsorcio")) {
            entidad=new BancaConsorcio();
        }else if(low.equalsIgnoreCase("bancagasto")) {
            entidad=new BancaGasto();
        }else if(low.equalsIgnoreCase("bancagastod")) {
            entidad=new BancaGastod();
        }else if(low.equalsIgnoreCase("bancagastodpk")) {
            entidad=new BancaGastodPK();
        }else if(low.equalsIgnoreCase("bancagastoh")) {
            entidad=new BancaGastoh();
        }else if(low.equalsIgnoreCase("bancagastohpk")) {
            entidad=new BancaGastohPK();
        }else if(low.equalsIgnoreCase("bancagrupo")) {
            entidad=new BancaGrupo();
        }else if(low.equalsIgnoreCase("bancagrupopk")) {
            entidad=new BancaGrupoPK();
        }else if(low.equalsIgnoreCase("bancaimportadato")) {
            entidad=new BancaImportaDato();
        }else if(low.equalsIgnoreCase("bancaimportadatopk")) {
            entidad=new BancaImportaDatoPK();
        }else if(low.equalsIgnoreCase("bancajuego")) {
            entidad=new BancaJuego();
        }else if(low.equalsIgnoreCase("bancajuegopk")) {
            entidad=new BancaJuegoPK();
        }else if(low.equalsIgnoreCase("bancaloteria")) {
            entidad=new BancaLoteria();
        }else if(low.equalsIgnoreCase("bancatransacciond")) {
            entidad=new BancaTransacciond();
        }else if(low.equalsIgnoreCase("bancaTransaccionpk")) {
            entidad=new BancaTransaccionhPK();
        }else if(low.equalsIgnoreCase("syscompania")) {
            entidad=new SysCompania();
        }
        
        else {
            entidad=new NullEntity();
            
        }

        
        return entidad;
    }

   
    
}
