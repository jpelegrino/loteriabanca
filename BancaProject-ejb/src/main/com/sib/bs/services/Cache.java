/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.bs.services;

import com.sib.ejb.dao.ServiceDao;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author julio
 */
public class Cache {
    
    private List<ServiceDao> services=new ArrayList<ServiceDao>();
    
    public ServiceDao getService(String serv) {
        for (ServiceDao s : services) {
            if (s.getName().equals(serv)) {
                return s;
            }
        }
        
        return null;
    
    }
    
    
    public void addService(ServiceDao service) {
       boolean bool=false;
       for (ServiceDao s : services) {
           if (s.getName().equals(service.getName())) {
               bool=true;
           }
       }
       
       if (bool) {
           services.add(service);
       }
    }
    
}
