/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.bs.services;

import com.sib.ejb.dao.ServiceDao;
import javax.naming.Context;
import javax.naming.InitialContext;

/**
 *
 * @author julio
 */
public class LocateService {
    
    static Cache CACHE;
    
    static {
        CACHE=new Cache();
    }
    
    public static ServiceDao getService(String serv) {
        ServiceDao service=CACHE.getService(serv);
        
        if (service==null) {
            try {
                Context cxt=new InitialContext();
                service=(ServiceDao) cxt.lookup("java:global/BancaProject-web/"+serv);
            } catch (Exception e) {
            }
        }
        
        return service;
    
    }
}
