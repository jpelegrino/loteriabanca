/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.bs.factory;

import com.sib.entity.*;

/**
 *
 * @author julio
 */
public class EntityFactory extends ManageFactory{

    @Override
    public Entidad getEntity(String entity) {
        Entidad entidad=null;
        
        
        if (entity.equalsIgnoreCase("Banca")) {
            entidad=new Banca();
        }else if (entity.equalsIgnoreCase("BancaLoteria")) {
            entidad=new BancaLoteria();
        }else if (entity.equalsIgnoreCase("BancaLoteriaJuego")) {
            entidad=new BancaLoteriaJuego();
        }else if (entity.equalsIgnoreCase("BancaLoteriaJuegoPK")) {
            entidad=new BancaLoteriaJuegoPK();
        }else if (entity.equalsIgnoreCase("BancaLoteriaPK")) {
            entidad=new BancaLoteriaPK();
        }else if (entity.equalsIgnoreCase("Cobro")) {
            entidad=new Cobro();
        }else if (entity.equalsIgnoreCase("General")) {
            entidad=new General();
        }else if (entity.equalsIgnoreCase("DetalleGeneral")) {
            entidad=new DetalleGeneral();
        }
        else if (entity.equalsIgnoreCase("Grupo")) {
            entidad=new Grupo();
        }else if (entity.equalsIgnoreCase("Juego")) {
            entidad=new Juego();
        }else if (entity.equalsIgnoreCase("Loteria")) {
            entidad=new Loteria();
        }else if (entity.equalsIgnoreCase("LoteriaJuego")) {
            entidad=new LoteriaJuego();
        }else if (entity.equalsIgnoreCase("LoteriaJuegoPK")) {
            entidad=new LoteriaJuegoPK();
        }else if (entity.equalsIgnoreCase("TipoGasto")) {
            entidad=new TipoGasto();
        }else if (entity.equalsIgnoreCase("Gasto")) {
            entidad=new Gasto();
        }
        else{       
            entidad=new NullEntity();            
        }

        
        return entidad;
    }

   
    
}