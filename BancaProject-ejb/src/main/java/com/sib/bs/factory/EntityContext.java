/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.bs.factory;

import com.sib.entity.Entidad;


/**
 *
 * @author julio
 */
public  class EntityContext {
    
    public static Entidad getEntity(String e) {
        EntityFactory factory=(EntityFactory)ObjectProducer.getFactory("entityfactory");
        return factory.getEntity(e);
    }
}
