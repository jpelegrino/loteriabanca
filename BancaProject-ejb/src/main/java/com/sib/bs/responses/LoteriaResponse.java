/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.bs.responses;

import com.sib.entity.Loteria;
import java.util.List;

/**
 *
 * @author julio
 */
public class LoteriaResponse extends SibMessage {
    
    private Loteria loteria;
    private List<Loteria> loterias;

    public Loteria getLoteria() {
        return loteria;
    }

    public void setLoteria(Loteria loteria) {
        this.loteria = loteria;
    }

    public List<Loteria> getLoterias() {
        return loterias;
    }

    public void setLoterias(List<Loteria> loterias) {
        this.loterias = loterias;
    }
    
    
    
}
