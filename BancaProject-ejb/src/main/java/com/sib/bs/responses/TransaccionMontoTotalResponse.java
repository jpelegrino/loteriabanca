/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.bs.responses;

import com.sib.entity.SumatoriaTransaccionCustomEntity;
import java.util.List;

/**
 *
 * @author julio
 */
public class TransaccionMontoTotalResponse extends SibMessage {
    
    private SumatoriaTransaccionCustomEntity sumatoriaTransaccion;
    private List<SumatoriaTransaccionCustomEntity> sumatoriaTransacciones;

    public SumatoriaTransaccionCustomEntity getSumatoriaTransaccion() {
        return sumatoriaTransaccion;
    }

    public void setSumatoriaTransaccion(SumatoriaTransaccionCustomEntity sumatoriaTransaccion) {
        this.sumatoriaTransaccion = sumatoriaTransaccion;
    }

    public List<SumatoriaTransaccionCustomEntity> getSumatoriaTransacciones() {
        return sumatoriaTransacciones;
    }

    public void setSumatoriaTransacciones(List<SumatoriaTransaccionCustomEntity> sumatoriaTransacciones) {
        this.sumatoriaTransacciones = sumatoriaTransacciones;
    }
    
    
    
    
}
