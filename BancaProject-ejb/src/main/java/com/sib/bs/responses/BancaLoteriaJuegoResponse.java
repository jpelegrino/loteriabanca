/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.bs.responses;

import com.sib.entity.BancaLoteriaJuego;
import java.util.List;

/**
 *
 * @author julio
 */
public class BancaLoteriaJuegoResponse extends SibMessage {
    
    private BancaLoteriaJuego bancaLoteriaJuego;
    private List<BancaLoteriaJuego> bancaLoteriaJuegos;

    public BancaLoteriaJuego getBancaLoteriaJuego() {
        return bancaLoteriaJuego;
    }

    public void setBancaLoteriaJuego(BancaLoteriaJuego bancaLoteriaJuego) {
        this.bancaLoteriaJuego = bancaLoteriaJuego;
    }

    public List<BancaLoteriaJuego> getBancaLoteriaJuegos() {
        return bancaLoteriaJuegos;
    }

    public void setBancaLoteriaJuegos(List<BancaLoteriaJuego> bancaLoteriaJuegos) {
        this.bancaLoteriaJuegos = bancaLoteriaJuegos;
    }
    
    
    
}
