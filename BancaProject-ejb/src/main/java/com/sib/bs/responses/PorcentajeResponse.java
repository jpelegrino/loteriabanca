/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.bs.responses;

import com.sib.entity.Porcentajes;
import java.util.List;

/**
 *
 * @author julio
 */
public class PorcentajeResponse extends SibMessage {
    
    private Porcentajes porcentaje;
    private List<Porcentajes> porcentajes;

    public Porcentajes getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(Porcentajes porcentaje) {
        this.porcentaje = porcentaje;
    }

    public List<Porcentajes> getPorcentajes() {
        return porcentajes;
    }

    public void setPorcentajes(List<Porcentajes> porcentajes) {
        this.porcentajes = porcentajes;
    }
    
    
}
