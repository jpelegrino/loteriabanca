/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.bs.responses;

/**
 *
 * @author julio
 */
public enum MessageEnum {
 
    SUCCESS(1,"Success"),
    FAIL(0,"Fail"),
    VALIDATION(2,"Contacte al Administrador");
    
    MessageEnum(int codeMessage,String msg) {
        this.code=codeMessage;
        this.message=msg;
        
    }
    
    private int code;
    private String message;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
    
    
    
}
