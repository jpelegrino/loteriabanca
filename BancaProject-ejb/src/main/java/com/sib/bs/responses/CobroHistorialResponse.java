package com.sib.bs.responses;

import com.sib.entity.CobroHistorial;
import java.util.List;

/**
 *
 * @author Snailin Inoa
 */
public class CobroHistorialResponse extends SibMessage {
    
    private CobroHistorial cobroHistorial;
    private List<CobroHistorial> cobrosHistorialList;

    public CobroHistorial getCobroHistorial() {
        return cobroHistorial;
    }

    public void setCobroHistorial(CobroHistorial cobroHistorial) {
        this.cobroHistorial = cobroHistorial;
    }

    public List<CobroHistorial> getCobrosHistorialList() {
        return cobrosHistorialList;
    }

    public void setCobrosHistorialList(List<CobroHistorial> cobrosHistorialList) {
        this.cobrosHistorialList = cobrosHistorialList;
    }
    
    
    

}
