/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.bs.responses;

import com.sib.entity.General;
import java.util.List;

/**
 *
 * @author julio
 */
public class GeneralResponse extends SibMessage{
    
    private General general;
    private List<General> generals;

    public General getGeneral() {
        return general;
    }

    public void setGeneral(General general) {
        this.general = general;
    }

    public List<General> getGenerals() {
        return generals;
    }

    public void setGenerals(List<General> generals) {
        this.generals = generals;
    }
    
    
    
}
