/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.bs.responses;

import com.sib.entity.DetalleGeneral;
import java.util.List;

/**
 *
 * @author julio
 */
public class DetalleGeneralResponse extends SibMessage {
    
    private DetalleGeneral detalleGeneral;
    private List<DetalleGeneral> detalleGenerals;
    private int counter;

    public DetalleGeneral getDetalleGeneral() {
        return detalleGeneral;
    }

    public void setDetalleGeneral(DetalleGeneral detalleGeneral) {
        this.detalleGeneral = detalleGeneral;
    }

    public List<DetalleGeneral> getDetalleGenerals() {
        return detalleGenerals;
    }

    public void setDetalleGenerals(List<DetalleGeneral> detalleGenerals) {
        this.detalleGenerals = detalleGenerals;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
    
    
    
    
}
