package com.sib.bs.responses;

import com.sib.entity.Cobro;
import java.util.List;

/**
 *
 * @author Snailin Inoa
 */
public class CobroResponse extends SibMessage {

    private Cobro cobro;
    private List<Cobro> cobros;

    public Cobro getCobro() {
        return cobro;
    }

    public void setCobro(Cobro cobro) {
        this.cobro = cobro;
    }

    public List<Cobro> getCobros() {
        return cobros;
    }

    public void setCobros(List<Cobro> cobros) {
        this.cobros = cobros;
    }

}
