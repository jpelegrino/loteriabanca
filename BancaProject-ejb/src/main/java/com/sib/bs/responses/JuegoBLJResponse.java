/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.bs.responses;

import com.sib.entity.JuegoAndBLJ;
import java.util.List;

/**
 *
 * @author julio
 */
public class JuegoBLJResponse extends SibMessage{
    
    private JuegoAndBLJ juegoAndBLJ;
    private List<JuegoAndBLJ> juegosAndBLJ;

    public JuegoAndBLJ getJuegoAndBLJ() {
        return juegoAndBLJ;
    }

    public void setJuegoAndBLJ(JuegoAndBLJ juegoAndBLJ) {
        this.juegoAndBLJ = juegoAndBLJ;
    }

    public List<JuegoAndBLJ> getJuegosAndBLJ() {
        return juegosAndBLJ;
    }

    public void setJuegosAndBLJ(List<JuegoAndBLJ> juegosAndBLJ) {
        this.juegosAndBLJ = juegosAndBLJ;
    }
    
    
    
}
