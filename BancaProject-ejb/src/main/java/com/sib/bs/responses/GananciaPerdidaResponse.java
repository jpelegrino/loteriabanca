/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.bs.responses;

import com.sib.entity.GananciaPerdida;
import java.util.List;

/**
 *
 * @author julio
 */
public class GananciaPerdidaResponse extends SibMessage {
    
    private GananciaPerdida gananciaPerdida;
    private List<GananciaPerdida> gananciasPerdidas;

    public GananciaPerdida getGananciaPerdida() {
        return gananciaPerdida;
    }

    public void setGananciaPerdida(GananciaPerdida gananciaPerdida) {
        this.gananciaPerdida = gananciaPerdida;
    }

    public List<GananciaPerdida> getGananciasPerdidas() {
        return gananciasPerdidas;
    }

    public void setGananciasPerdidas(List<GananciaPerdida> gananciasPerdidas) {
        this.gananciasPerdidas = gananciasPerdidas;
    }
    
    
    
}
