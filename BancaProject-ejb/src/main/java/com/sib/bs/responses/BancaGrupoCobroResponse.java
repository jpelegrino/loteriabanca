package com.sib.bs.responses;

import com.sib.entity.BancaGrupoCobro;
import java.util.List;

/**
 *
 * @author Snailin Inoa
 */
public class BancaGrupoCobroResponse extends SibMessage {

    private List<BancaGrupoCobro> bancaGrupoCobro;
    private List<Long> detallesGeneralesID;

    public List<Long> getDetallesGeneralesID() {
        return detallesGeneralesID;
    }

    public void setDetallesGeneralesID(List<Long> detallesGeneralesID) {
        this.detallesGeneralesID = detallesGeneralesID;
    }

    public List<BancaGrupoCobro> getBancaGrupoCobro() {
        return bancaGrupoCobro;
    }

    public void setBancaGrupoCobro(List<BancaGrupoCobro> bancaGrupoCobro) {
        this.bancaGrupoCobro = bancaGrupoCobro;
    }

}
