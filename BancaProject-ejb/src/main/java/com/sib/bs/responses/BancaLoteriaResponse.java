/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.bs.responses;

import com.sib.entity.BancaLoteria;
import java.util.List;

/**
 *
 * @author julio
 */
public class BancaLoteriaResponse extends SibMessage {
    
    private BancaLoteria bancaLoteria;
    private List<BancaLoteria> bancaLoterias;

    public BancaLoteria getBancaLoteria() {
        return bancaLoteria;
    }

    public void setBancaLoteria(BancaLoteria bancaLoteria) {
        this.bancaLoteria = bancaLoteria;
    }

    public List<BancaLoteria> getBancaLoterias() {
        return bancaLoterias;
    }

    public void setBancaLoterias(List<BancaLoteria> bancaLoterias) {
        this.bancaLoterias = bancaLoterias;
    }
    
    
    
}
