/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.bs.responses;

import com.sib.entity.LoteriaJuego;
import java.util.List;

/**
 *
 * @author julio
 */
public class LoteriaJuegoResponse extends SibMessage {
    
    private LoteriaJuego loteriaJuego;
    private List<LoteriaJuego> loteriaJuegos;

    public LoteriaJuego getLoteriaJuego() {
        return loteriaJuego;
    }

    public void setLoteriaJuego(LoteriaJuego loteriaJuego) {
        this.loteriaJuego = loteriaJuego;
    }

    public List<LoteriaJuego> getLoteriaJuegos() {
        return loteriaJuegos;
    }

    public void setLoteriaJuegos(List<LoteriaJuego> loteriaJuegos) {
        this.loteriaJuegos = loteriaJuegos;
    }
    
    
    
}
