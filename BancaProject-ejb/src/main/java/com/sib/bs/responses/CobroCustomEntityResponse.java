package com.sib.bs.responses;

import com.sib.entity.CobroCustomEntity;
import java.util.List;

/**
 *
 * @author Snailin Inoa
 */
public class CobroCustomEntityResponse extends SibMessage {

    private List<CobroCustomEntity> cobroCustomEntityList;
    private CobroCustomEntity cobroCustomEntity;

    public List<CobroCustomEntity> getCobroCustomEntityList() {
        return cobroCustomEntityList;
    }

    public void setCobroCustomEntityList(List<CobroCustomEntity> cobroCustomEntityList) {
        this.cobroCustomEntityList = cobroCustomEntityList;
    }

    public CobroCustomEntity getCobroCustomEntity() {
        return cobroCustomEntity;
    }

    public void setCobroCustomEntity(CobroCustomEntity cobroCustomEntity) {
        this.cobroCustomEntity = cobroCustomEntity;
    }

}
