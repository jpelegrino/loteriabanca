package com.sib.bs.responses;

import com.sib.entity.CobroVistaDetalle;
import java.util.List;

/**
 *
 * @author Snailin Inoa
 */
public class CobroVistaDetalleResponse extends SibMessage {

    private List<CobroVistaDetalle> cobroVistaDetalleList;

    public List<CobroVistaDetalle> getCobroVistaDetalleList() {
        return cobroVistaDetalleList;
    }

    public void setCobroVistaDetalleList(List<CobroVistaDetalle> cobroVistaDetalleList) {
        this.cobroVistaDetalleList = cobroVistaDetalleList;
    }

}
