/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.bs.responses;

import com.sib.entity.TipoGasto;
import java.util.List;

/**
 *
 * @author julio
 */
public class TipoGastoResponse extends SibMessage{
    
    private TipoGasto tipoGasto;
    private List<TipoGasto> tipoGastos;

    public TipoGasto getTipoGasto() {
        return tipoGasto;
    }

    public void setTipoGasto(TipoGasto tipoGasto) {
        this.tipoGasto = tipoGasto;
    }

    public List<TipoGasto> getTipoGastos() {
        return tipoGastos;
    }

    public void setTipoGastos(List<TipoGasto> tipoGastos) {
        this.tipoGastos = tipoGastos;
    }
    
    
    
}
