/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.bs.responses;

import com.sib.entity.SumatoriaGastoCustomEntity;
import java.util.List;

/**
 *
 * @author julio
 */
public class GastoMontoTotalResponse extends SibMessage {
    
    private SumatoriaGastoCustomEntity sumatoriaGasto;
    private List<SumatoriaGastoCustomEntity> sumatoriaGastos;

    public SumatoriaGastoCustomEntity getSumatoriaGasto() {
        return sumatoriaGasto;
    }

    public void setSumatoriaGasto(SumatoriaGastoCustomEntity sumatoriaGasto) {
        this.sumatoriaGasto = sumatoriaGasto;
    }

    public List<SumatoriaGastoCustomEntity> getSumatoriaGastos() {
        return sumatoriaGastos;
    }

    public void setSumatoriaGastos(List<SumatoriaGastoCustomEntity> sumatoriaGastos) {
        this.sumatoriaGastos = sumatoriaGastos;
    }
    
    
    
}
