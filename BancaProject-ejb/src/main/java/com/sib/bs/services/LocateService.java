/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.bs.services;

import com.sib.ejb.dao.ServiceDao;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author julio
 */
public class LocateService {
    
    static Cache CACHE;
    
    static {
        CACHE=new Cache();
    }
    
    public static ServiceDao getService(String serv) {
        ServiceDao service=CACHE.getService(serv);
        
        if (service==null) {
            try {
                Context cxt=new InitialContext();
                System.out.println("serv: "+ serv);
                service=(ServiceDao) cxt.lookup("java:global/BancaProject-ear/BancaProject-ejb-1.0-SNAPSHOT/"+serv);
            } catch (NamingException e) {
            }
        }
        
        return service;
    
    }
}