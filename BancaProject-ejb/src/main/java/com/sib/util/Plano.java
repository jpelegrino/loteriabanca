/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.util;

import java.io.Serializable;

/**
 *
 * @author julio
 */
public class Plano implements Serializable{
    
    private String fecha;
    private String grupo;
    private String loteria;
    private String banca;
    private String totalVenta;
    private String vquiniela;
    private String vpale;
    private String vtripleta;
    private String vpega3;
    private String totalPremio;
    private String pquiniela;
    private String ppale;
    private String ptripleta;
    private String ppega3;

    public Plano(String fecha, String grupo,  String banca, String loteria, String totalVenta, String vquiniela, String vpale, String vtripleta, String vpega3, String totalPremio, String pquiniela, String ppale, String ptripleta, String ppega3) {
        this.fecha = fecha;
        this.grupo = grupo;
        this.loteria = loteria;
        this.banca = banca;
        this.totalVenta = totalVenta;
        this.vquiniela = vquiniela;
        this.vpale = vpale;
        this.vtripleta = vtripleta;
        this.vpega3 = vpega3;
        this.totalPremio = totalPremio;
        this.pquiniela = pquiniela;
        this.ppale = ppale;
        this.ptripleta = ptripleta;
        this.ppega3 = ppega3;
    }

    public Plano() {
    }
    
    
    

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getLoteria() {
        return loteria;
    }

    public void setLoteria(String loteria) {
        this.loteria = loteria;
    }

    public String getBanca() {
        return banca;
    }

    public void setBanca(String banca) {
        this.banca = banca;
    }

    public String getTotalVenta() {
        return totalVenta;
    }

    public void setTotalVenta(String totalVenta) {
        this.totalVenta = totalVenta;
    }

    public String getVquiniela() {
        return vquiniela;
    }

    public void setVquiniela(String vquiniela) {
        this.vquiniela = vquiniela;
    }

    public String getVpale() {
        return vpale;
    }

    public void setVpale(String vpale) {
        this.vpale = vpale;
    }

    public String getVtripleta() {
        return vtripleta;
    }

    public void setVtripleta(String vtripleta) {
        this.vtripleta = vtripleta;
    }

    public String getVpega3() {
        return vpega3;
    }

    public void setVpega3(String vpega3) {
        this.vpega3 = vpega3;
    }

    public String getTotalPremio() {
        return totalPremio;
    }

    public void setTotalPremio(String totalPremio) {
        this.totalPremio = totalPremio;
    }

    public String getPquiniela() {
        return pquiniela;
    }

    public void setPquiniela(String pquiniela) {
        this.pquiniela = pquiniela;
    }

    public String getPpale() {
        return ppale;
    }

    public void setPpale(String ppale) {
        this.ppale = ppale;
    }

    public String getPtripleta() {
        return ptripleta;
    }

    public void setPtripleta(String ptripleta) {
        this.ptripleta = ptripleta;
    }

    public String getPpega3() {
        return ppega3;
    }

    public void setPpega3(String ppega3) {
        this.ppega3 = ppega3;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + (this.fecha != null ? this.fecha.hashCode() : 0);
        hash = 79 * hash + (this.grupo != null ? this.grupo.hashCode() : 0);
        hash = 79 * hash + (this.loteria != null ? this.loteria.hashCode() : 0);
        hash = 79 * hash + (this.banca != null ? this.banca.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Plano other = (Plano) obj;
        if ((this.fecha == null) ? (other.fecha != null) : !this.fecha.equals(other.fecha)) {
            return false;
        }
        if ((this.grupo == null) ? (other.grupo != null) : !this.grupo.equals(other.grupo)) {
            return false;
        }
        if ((this.loteria == null) ? (other.loteria != null) : !this.loteria.equals(other.loteria)) {
            return false;
        }
        if ((this.banca == null) ? (other.banca != null) : !this.banca.equals(other.banca)) {
            return false;
        }
        return true;
    }
    
    
    
}