/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;

/**
 *
 * @author julio
 */
public class ManageUtils {
    private static SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
    private static SimpleDateFormat sdf2=new SimpleDateFormat("yyyyMMdd");
    private static Logger LOG=Logger.getLogger(ManageUtils.class);
    
    
    
    public static Date convertToDate(String str) {
        
        Date fecha=null;
        try {
            fecha=sdf.parse(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return fecha;
    
    }
    
     public static Date parseToDate(String str) {
        
        Date fecha=null;
        try {
            fecha=sdf2.parse(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return fecha;
    
    }
     
     public static String parseStrDate(String fecha) {
         String value="";
         if (fecha!=null || !fecha.isEmpty()) {
            String a=fecha.substring(0,4);
            String m=fecha.substring(5,7);
            String d=fecha.substring(8,10);
            value="\'"+a+","+m+","+d+"\'";
         }
         return value;
     }
    
    public static int convertToInt(String i) {
        try {
            return Integer.parseInt(i);
        } catch (NumberFormatException e) {
            return 0;
        }
    }
    
    public static Float convertToFloat(String i) {
        Float value=0.0f;
        try {
            if (!i.isEmpty()) {
            value= Float.valueOf(i);
            }
        } catch (NumberFormatException e) {
            value=0.0f;
        }catch(NullPointerException e) {
            value=0.0f;
        }
        
        return value;
    }
    
    public static Float redondearFloat(Float value) {
        Float result = value;
        
        try {
            if (value != null) {
                double number = Double.parseDouble(String.valueOf(value.floatValue()));
                number = Math.round(number * Math.pow(10, 0)) / Math.pow(10, 0);
                result = Float.parseFloat(String.valueOf(number));
            }
            
        } catch (Exception e) {
            LOG.error("Error tratando de redondear un Float ::" + e);
        }
        return result;
    }
    
    
   
    
}