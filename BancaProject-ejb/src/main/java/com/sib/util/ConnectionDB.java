/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.util;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author jpelegrino
 */
public class ConnectionDB {
    
    private static EntityManagerFactory factory;

    private ConnectionDB() {
    }
    
    public static EntityManagerFactory getConnection() {
        if (factory==null) {
            factory= Persistence.createEntityManagerFactory("sibPU");
        }
        return factory;
    }
    
}
