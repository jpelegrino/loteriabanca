/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.util;

import com.sib.bs.factory.EntityContext;
import com.sib.bs.responses.BancaLoteriaJuegoResponse;
import com.sib.bs.responses.BancaLoteriaResponse;
import com.sib.bs.responses.BancaResponse;
import com.sib.bs.responses.CobroHistorialResponse;
import com.sib.bs.responses.CobroResponse;
import com.sib.bs.responses.DetalleGeneralResponse;
import com.sib.bs.responses.GeneralResponse;
import com.sib.bs.responses.GrupoResponse;
import com.sib.bs.responses.JuegoResponse;
import com.sib.bs.responses.LoteriaResponse;
import com.sib.bs.services.LocateService;
import com.sib.ejb.dao.BancaDao;
import com.sib.ejb.dao.BancaLoteriaDao;
import com.sib.ejb.dao.BancaLoteriaJuegoDao;
import com.sib.ejb.dao.CobroDao;
import com.sib.ejb.dao.CobroHistorialDao;
import com.sib.ejb.dao.DetalleGeneralDao;
import com.sib.ejb.dao.GeneralDao;
import com.sib.ejb.dao.GrupoDao;
import com.sib.ejb.dao.LoteriaDao;
import com.sib.ejb.dao.LoteriaJuegoDao;
import com.sib.entity.Cobro;
import com.sib.entity.CobroHistorial;
import com.sib.entity.DetalleGeneral;
import com.sib.entity.General;
import com.sib.entity.Juego;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.util.Set;

/**
 *
 * @author julio
 */
public class PlanoParse {
    private  static Connection con;
    private  static Session session;
    
    public static void parse(InputStream is) {
        if (is==null) {
            System.out.println("No data");
        }else {
        
            try {

                System.out.println("SE ESTA ENTRANDO AQUI SIN NINGUN PROBLEMA");
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                
                //CODIGO NUEVO*********************************************
                
                List<Plano> listPlano = new ArrayList<Plano>();
            List<String> codigosGrupo = new ArrayList<String>();
            boolean existeFechaEnDB = false;
            GrupoDao grupoDao = (GrupoDao) LocateService.getService("GrupoDaoImpl");
            BancaDao bancaDao = (BancaDao) LocateService.getService("BancaDaoImpl");
            LoteriaDao loteriaDao = (LoteriaDao) LocateService.getService("LoteriaDaoImpl");
            BancaLoteriaDao bancaLoteriaDao = (BancaLoteriaDao) LocateService.getService("BancaLoteriaDaoImpl");
            LoteriaJuegoDao loteriaJuegoDao = (LoteriaJuegoDao) LocateService.getService("LoteriaJuegoDaoImpl");
            BancaLoteriaJuegoDao bancaLoteriaJuegoDao = (BancaLoteriaJuegoDao) LocateService.getService("BancaLoteriaJuegoDaoImpl");
            DetalleGeneralDao detalleGeneralDao = (DetalleGeneralDao) LocateService.getService("DetalleGeneralDaoImpl");
            GeneralDao generalDao = (GeneralDao) LocateService.getService("GeneralDaoImpl");
            CobroHistorialDao cobroHistorialDao = (CobroHistorialDao) LocateService.getService("CobroHistorialImpl");
            CobroDao cobroDao = (CobroDao) LocateService.getService("CobroDaoImpl");
            String line = "";

            //new code
            String fecha = null;
            String message=null;
            //new code            

            while ((line = br.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(line, ",");

                // proceso de parseo del archivo
                if (st.hasMoreTokens()) {

                    Plano planoObject = new Plano(st.nextToken(), st.nextToken(),
                            st.nextToken(), st.nextToken(),
                            st.nextToken(), st.nextToken(),
                            st.nextToken(), st.nextToken(),
                            st.nextToken(), st.nextToken(),
                            st.nextToken(), st.nextToken(),
                            st.nextToken(), st.nextToken());

                    //NEW CODE
                    //seteando valor a fecha para usarla verificar luego si esa fecha ya existen en la DB
                    if (fecha == null || fecha.isEmpty()) {
                        fecha = planoObject.getFecha();
                    }
                    //NEW CODE

                    listPlano.add(planoObject);

                    if (!codigosGrupo.contains(planoObject.getGrupo())) {
                        codigosGrupo.add(planoObject.getGrupo());
                    }
                }
            }

            // Mapa para organizar los row del archivo usando como KEY el codigo del GRUPO
            Map<String, List<Plano>> maplist = new LinkedHashMap<String, List<Plano>>();

            for (int i = 0; i < codigosGrupo.size(); i++) {
                List<Plano> p = new ArrayList<Plano>();

                for (int z = 0; z < listPlano.size(); z++) {
                    if (listPlano.get(z).getGrupo().equals(codigosGrupo.get(i))) {
                        p.add(listPlano.get(z));
                    }
                }
                maplist.put(codigosGrupo.get(i), p);

            }

            //NEW CODE
            List<Long> generalIDs = null;
            Map<List<String>, DetalleGeneral> mapFromDB = null;
            if (fecha != null && !fecha.isEmpty()) {
                System.out.println("Fecha -->" + fecha);
                GeneralResponse generalResponse = generalDao.getGeneralListByFecha(fecha);
                if (generalResponse.getMsgEnum().getCode() == 1) {
                    if (generalResponse.getGenerals() != null && !generalResponse.getGenerals().isEmpty()) {
                        //Existe Reocrds con esa fecha en la DB
                        existeFechaEnDB = true;
                        generalIDs = new ArrayList<Long>();
                        for (General general : generalResponse.getGenerals()) {
                            generalIDs.add(general.getId());
                        }

                        DetalleGeneralResponse detalleGeneralResponse = detalleGeneralDao.getDetallesGeneralesListByGeneralIDs(generalIDs);
                        if (detalleGeneralResponse.getMsgEnum().getCode() == 1) {
                            if (detalleGeneralResponse.getDetalleGenerals() != null && !detalleGeneralResponse.getDetalleGenerals().isEmpty()) {
                                mapFromDB = new HashMap<List<String>, DetalleGeneral>();
                                for (DetalleGeneral detalleGeneral : detalleGeneralResponse.getDetalleGenerals()) {
                                    List<String> grupoBancaLoteriaCodigos = new ArrayList<String>();
                                    grupoBancaLoteriaCodigos.add(detalleGeneral.getBancaLoteria().getBanca().getCodigo());
                                    grupoBancaLoteriaCodigos.add(detalleGeneral.getBancaLoteria().getLoteria().getCodigo());
                                    grupoBancaLoteriaCodigos.add(detalleGeneral.getGeneralId().getGrupoId().getCodigo());
                                    //mAPA CON TODOS LOS RECORDS DE DETALLE GENERAL PARA UNA FECHA DETERMINADA ORGANIZADA EN EL MAPA POR LOS CODIGO DE BANCA,LOTERIA Y GRUPO
                                    mapFromDB.put(grupoBancaLoteriaCodigos, detalleGeneral);

                                }

                                System.out.println("Map --->" + mapFromDB);

                            }
                        }

                    }
                }

            }

            //END OF NEW CODE
            
            for (Map.Entry<String, List<Plano>> entry : maplist.entrySet()) {
                String keyCodigoGrupo = entry.getKey();
                List<Plano> listPlanos = entry.getValue();
                General generalEntity = (General) EntityContext.getEntity("General");

                //Buscar el ID del Grupo atraves del Codigo que viene del archivo.
                GrupoResponse grupoResponse = grupoDao.findGrupoByCodigo(keyCodigoGrupo);
                if (grupoResponse.getMsgEnum().getCode() == 0) {
                    System.out.println("**************************************************************");
                    System.out.println("No se pudo encontrar el grupo a partir del codigo ::" + keyCodigoGrupo);
                    System.out.println("**************************************************************");
                    continue;
                }

                //NEW CODE
                boolean existGrupoConFechaEnGeneral = false;
                GeneralResponse generalResponseFromDB = generalDao.getGeneralByFechaAndGrupoId(grupoResponse.getGrupo().getId(), fecha);
                //verificar si el grupo esta ya registrado en la tabla ban_general para una fecha Dada
                if (generalResponseFromDB.getMsgEnum().getCode() == 1) {
                    existGrupoConFechaEnGeneral = true;
                    System.out.println("*****************");
                    System.out.println("El Grupo :[" + grupoResponse.getGrupo().getId() + "] EXISTE en la tabla GENERAL para la fecha [" + fecha + "]");
                    System.out.println("*****************");
                } else {
                    System.out.println("*****************");
                    System.out.println("El Grupo :[" + grupoResponse.getGrupo().getId() + "] NO EXISTE en la tabla GENERAL para la fecha [" + fecha + "]");
                    System.out.println("*****************");
                }

                //END NEW CODE
                //Crear el entity General y setear sus atributos si el grupo no existe en la DB para euna fecha dada. 
                if (existGrupoConFechaEnGeneral == false) {
                    generalEntity.setFecha(listPlanos.get(0).getFecha());
                    generalEntity.setFechaCreacion(new Date());
                    generalEntity.setGrupoId(grupoResponse.getGrupo());
                } else {
                    generalEntity = generalResponseFromDB.getGeneral();
                }
                System.out.println("Key Grupo ---->" + keyCodigoGrupo);
                System.out.println("Key Grupo ---->" + keyCodigoGrupo);

                //Crear la lista de Detalle General asociados al General de arriba
                List<DetalleGeneral> detalleGeneralList = new ArrayList<DetalleGeneral>();

                for (int i = 0; i < listPlanos.size(); i++) {

                    DetalleGeneral detalleGeneral = (DetalleGeneral) EntityContext.getEntity("DetalleGeneral");

                    //NEW CODE
                    boolean wasAChange = false;

                    if (existeFechaEnDB) {

                        List<String> key = new ArrayList<String>();
                        key.add(listPlanos.get(i).getBanca());
                        key.add(listPlanos.get(i).getLoteria());
                        key.add(keyCodigoGrupo);
                        DetalleGeneral detalleGeneralFromDB = mapFromDB.get(key);

                        if (detalleGeneralFromDB != null) {
                            System.out.println("Valor Total :::" + detalleGeneralFromDB.getValorVentaTotal());
                            System.out.println("Valor Tota from archivo ::: " + listPlanos.get(i).getTotalVenta());
                            if (!detalleGeneralFromDB.getValorVentaTotal().equals(ManageUtils.convertToFloat(listPlanos.get(i).getTotalVenta()))
                                    || !detalleGeneralFromDB.getJuego1Venta().equals(ManageUtils.convertToFloat(listPlanos.get(i).getVquiniela()))
                                    || !detalleGeneralFromDB.getJuego2Venta().equals(ManageUtils.convertToFloat(listPlanos.get(i).getVpale()))
                                    || !detalleGeneralFromDB.getJuego3Venta().equals(ManageUtils.convertToFloat(listPlanos.get(i).getVtripleta()))
                                    || !detalleGeneralFromDB.getJuego4Venta().equals(ManageUtils.convertToFloat(listPlanos.get(i).getVpega3()))
                                    || !detalleGeneralFromDB.getValorPremioTotal().equals(ManageUtils.convertToFloat(listPlanos.get(i).getTotalPremio()))
                                    || !detalleGeneralFromDB.getJuego1Premio().equals(ManageUtils.convertToFloat(listPlanos.get(i).getPquiniela()))
                                    || !detalleGeneralFromDB.getJuego2Premio().equals(ManageUtils.convertToFloat(listPlanos.get(i).getPpale()))
                                    || !detalleGeneralFromDB.getJuego3Premio().equals(ManageUtils.convertToFloat(listPlanos.get(i).getPtripleta()))
                                    || !detalleGeneralFromDB.getJuego4Premio().equals(ManageUtils.convertToFloat(listPlanos.get(i).getPpega3()))) {

                                wasAChange = true;
                                // HUBO UN CAMBIO EN ESTE ROW
                                detalleGeneral = mapFromDB.get(key);
                                System.out.println("");
                                System.out.println("*****************************************");
                                System.out.println("HUBO UN CAMBIO PARA EL ROW CON LOS CODIGOS");
                                System.out.println("GRUPO ::" + listPlanos.get(i).getGrupo());
                                System.out.println("BANCA ::" + listPlanos.get(i).getBanca());
                                System.out.println("LOTERIA ::" + listPlanos.get(i).getLoteria());
                                System.out.println("******************************************");
                                System.out.println("");

                            } else {
                                //NO HUBO NINGUN CAMBIO EN ESTE ROW
                                System.out.println("");
                                System.out.println("*****************************************");
                                System.out.println("NO HUBO NINGUN CAMBIO PARA EL ROW CON LOS CODIGOS");
                                System.out.println("GRUPO ::" + listPlanos.get(i).getGrupo());
                                System.out.println("BANCA ::" + listPlanos.get(i).getBanca());
                                System.out.println("LOTERIA ::" + listPlanos.get(i).getLoteria());
                                System.out.println("******************************************");
                                System.out.println("");
                                continue;
                            }
                        }

                    }

                    //END NEW CODE
                    //Buscar la Banca en la base de dato utilizando el codigo de la banca y el ID del Grupo.
                    BancaResponse bancaResponse = bancaDao.getBancaByCodigoAndGrupoId(listPlanos.get(i).getBanca(), grupoResponse.getGrupo().getId());

                    if (bancaResponse.getMsgEnum().getCode() == 0) {

                        System.out.println("**************************************************************");
                        System.out.println("No se pudo encontrar la Banca a partir del codigoBanca y Grupo ID :");
                        System.out.println("");
                        System.out.println("Codigo Banca ::" + listPlanos.get(i).getBanca());
                        System.out.println("Grupo ID ::" + grupoResponse.getGrupo().getId());
                        System.out.println("**************************************************************");
                        continue;
                    }
                    //Buscar la Loteria en la base de dato utilizando de codigo de la Loteria.
                    LoteriaResponse loteriaResponse = loteriaDao.getLoteriaByCodigo(listPlanos.get(i).getLoteria());
                    if (loteriaResponse.getMsgEnum().getCode() == 0) {
                        System.out.println("**************************************************************");
                        System.out.println("No se pudo encontrar la Loteria a partir del codigo_loteria ::" + listPlanos.get(i).getLoteria());
                        System.out.println("**************************************************************");
                        continue;
                    }
                    //Busca la BancaLoteria en la base de datos usando el ID de la Banca y el ID del Loteria
                    BancaLoteriaResponse bancaLoteriaResponse = bancaLoteriaDao.getBancaLoteriaById(bancaResponse.getBanca().getId(), loteriaResponse.getLoteria().getId());

                    detalleGeneral.setBancaLoteria(bancaLoteriaResponse.getBancaLoteria());
                    detalleGeneral.setFechaActualizacion(new Date());
                    detalleGeneral.setFechaCreacion(new Date());
                    detalleGeneral.setFechaEfectiva(ManageUtils.parseToDate(fecha));
                    detalleGeneral.setGeneralId(generalEntity);
                    Math.round(3.5);

                    //Seteando los premios de cada juego.
                    detalleGeneral.setJuego1Premio(ManageUtils.redondearFloat(ManageUtils.convertToFloat(listPlanos.get(i).getPquiniela())));
                    detalleGeneral.setJuego2Premio(ManageUtils.redondearFloat(ManageUtils.convertToFloat(listPlanos.get(i).getPpale())));
                    detalleGeneral.setJuego3Premio(ManageUtils.redondearFloat(ManageUtils.convertToFloat(listPlanos.get(i).getPtripleta())));
                    detalleGeneral.setJuego4Premio(ManageUtils.redondearFloat(ManageUtils.convertToFloat(listPlanos.get(i).getVpega3())));

                    //Seteando las ventas de Cada Juego
                    detalleGeneral.setJuego1Venta(ManageUtils.redondearFloat(ManageUtils.convertToFloat(listPlanos.get(i).getVquiniela())));
                    detalleGeneral.setJuego2Venta(ManageUtils.redondearFloat(ManageUtils.convertToFloat(listPlanos.get(i).getVpale())));
                    detalleGeneral.setJuego3Venta(ManageUtils.redondearFloat(ManageUtils.convertToFloat(listPlanos.get(i).getVtripleta())));
                    detalleGeneral.setJuego4Venta(ManageUtils.redondearFloat(ManageUtils.convertToFloat(listPlanos.get(i).getVpega3())));

                    //Setando los valores totales
                    detalleGeneral.setValorVentaTotal(ManageUtils.redondearFloat(ManageUtils.convertToFloat(listPlanos.get(i).getTotalVenta())));
                    detalleGeneral.setValorPremioTotal(ManageUtils.redondearFloat(ManageUtils.convertToFloat(listPlanos.get(i).getTotalPremio())));
                    detalleGeneral.setStatus("A");

                    //Obtener los Juegos a partir de la loteria ID
                    JuegoResponse juegoResponse = loteriaJuegoDao.getJuegosByLoteriaId(loteriaResponse.getLoteria().getId(), "A");

                    if (juegoResponse.getMsgEnum().getCode() == 0) {
                        System.out.println("************************************************************");
                        System.out.println("No se Encontro Juegos Disponible para la Loteria ID ::" + loteriaResponse.getLoteria().getId());
                        System.out.println("************************************************************");
                        continue;
                    }

                    List<Juego> juegos = juegoResponse.getJuegos();
                    System.out.println("************************************************************");
                    System.out.println("JuegoResponse List ::" + juegoResponse.getJuegos());
                    System.out.println("************************************************************");
                    for (Juego juego : juegos) {

                        //Buscar la BancaLoteriaJuego para buscar el porcentaje de un juego a partir de Banca ID, Loteria ID, Juego ID.
                        BancaLoteriaJuegoResponse bancaLoteriaJuegoResponse = bancaLoteriaJuegoDao.getBancaLoteriaJuegoById(bancaResponse.getBanca().getId(), loteriaResponse.getLoteria().getId(), juego.getId());

                        if (bancaLoteriaJuegoResponse.getMsgEnum().getCode() == 0) {
                            System.out.println("************************************************************");
                            System.out.println("No se Encontro relacion entre banca, Juego y Loteria Para :");
                            System.out.println("");
                            System.out.println("Banca :: " + bancaResponse.getBanca().getId());
                            System.out.println("Loteria :: " + loteriaResponse.getLoteria().getId());
                            System.out.println("Juego :: " + juego.getId());
                            System.out.println("************************************************************");
                            continue;
                        }

                        Double porcientoJuego = Double.parseDouble(String.valueOf(bancaLoteriaJuegoResponse.getBancaLoteriaJuego().getPorcentajeCobro()));
                        Double montoPagarPorJuego = 0.0d;
                        if (juego.getCodigo().equalsIgnoreCase("Q")) {
                            montoPagarPorJuego = ((ManageUtils.convertToFloat(listPlanos.get(i).getVquiniela()) * porcientoJuego) / 100);
                            detalleGeneral.setJuego1PorcientoPagar(porcientoJuego.floatValue());
                            detalleGeneral.setJuego1MontoPagar(ManageUtils.redondearFloat(montoPagarPorJuego.floatValue()));

                        } else if (juego.getCodigo().equalsIgnoreCase("P")) {

                            montoPagarPorJuego = ((ManageUtils.convertToFloat(listPlanos.get(i).getVpale()) * porcientoJuego) / 100);
                            detalleGeneral.setJuego2PorcientoPagar(porcientoJuego.floatValue());
                            detalleGeneral.setJuego2MontoPagar(ManageUtils.redondearFloat(montoPagarPorJuego.floatValue()));

                        } else if (juego.getCodigo().equalsIgnoreCase("T")) {
                            montoPagarPorJuego = ((ManageUtils.convertToFloat(listPlanos.get(i).getVtripleta()) * porcientoJuego) / 100);
                            detalleGeneral.setJuego3PorcientoPagar(porcientoJuego.floatValue());
                            detalleGeneral.setJuego3MontoPagar(ManageUtils.redondearFloat(montoPagarPorJuego.floatValue()));

                        } else if (juego.getCodigo().equalsIgnoreCase("P3")) {
                            montoPagarPorJuego = ((ManageUtils.convertToFloat(listPlanos.get(i).getVpega3()) * porcientoJuego) / 100);
                            detalleGeneral.setJuego4PorcientoPagar(porcientoJuego.floatValue());
                            detalleGeneral.setJuego4MontoPagar(ManageUtils.redondearFloat(montoPagarPorJuego.floatValue()));
                        }

                    }

                    detalleGeneral.setTotalPagar(ManageUtils.redondearFloat((detalleGeneral.getJuego1MontoPagar() + detalleGeneral.getJuego2MontoPagar() + detalleGeneral.getJuego3MontoPagar() + detalleGeneral.getJuego4MontoPagar())));
                    detalleGeneral.setMontoFinalOperaciones(ManageUtils.redondearFloat((detalleGeneral.getValorVentaTotal() - detalleGeneral.getValorPremioTotal() - detalleGeneral.getTotalPagar())));
                    detalleGeneral.setPendienteCobrar(ManageUtils.redondearFloat(detalleGeneral.getMontoFinalOperaciones()));

                    //Status Detalle  D-Perdidas  P-Pendiente  C-Cobrado
                    if (detalleGeneral.getMontoFinalOperaciones() <= 0) {
                        detalleGeneral.setStatusDetalle("D");
                    } else {
                        detalleGeneral.setStatusDetalle("P");

                    }

                    //START
                    //Verificar si hay algun cobro realizado para algun row del archivo, para restarselo al poendiente a cobrar
                    boolean existeCobro = false;
                    CobroHistorialResponse cobroHistorialResponse = null;
                    if (existGrupoConFechaEnGeneral == true) {
                        cobroHistorialResponse = cobroHistorialDao.getCobroHistorialListByDetalleGeneralId(detalleGeneral.getId(), "V");
                        if (cobroHistorialResponse.getMsgEnum().getCode() == 1) {
                            if (cobroHistorialResponse.getCobrosHistorialList() != null && !cobroHistorialResponse.getCobrosHistorialList().isEmpty()) {
                                System.out.println("CobroHistorialList Size Found :: " + cobroHistorialResponse.getCobrosHistorialList().size());
                                existeCobro = true;
                            }
                        } else {
                            System.out.println("");
                            System.out.println("*********************[ERROR]******************************");
                            System.out.println("HUBO UN PROBLEMA TRATANDO DE OBTENER LOS COBROS ASOCIADO A UN DETALLE GENERAL");
                            System.out.println("");
                        }

                        //Si el Monto final operaciones es igual o menor que cero y hay algun cobro realizado todo eso cobros se Anulan
                        if (detalleGeneral.getMontoFinalOperaciones() <= 0 && existeCobro == true) {
                            System.out.println("************************");
                            System.out.println("Preparado para Anular los cobros asociado al detalle_general_id :: " + detalleGeneral.getId());
                            System.out.println("Monto Final Operaciones :: " + detalleGeneral.getMontoFinalOperaciones());
                            for (CobroHistorial cobroHistorial : cobroHistorialResponse.getCobrosHistorialList()) {
                                cobroHistorial.setStatusCobroHistorial("A");
                                System.out.println("");
                                System.out.println("Anulando Cobro_Historial id ::" + cobroHistorial.getId());
                                cobroHistorialDao.updateCobroHistorial(cobroHistorial);
                            }

                        }

                        if (detalleGeneral.getMontoFinalOperaciones() > 0 && existeCobro == true) {
                            Float montoCobrado = 0.0f;
                            for (CobroHistorial cobroHistorial : cobroHistorialResponse.getCobrosHistorialList()) {
                                System.out.println("");
                                montoCobrado += cobroHistorial.getMontoCobrado();
                            }

                            System.out.println("Para el Detalle General con  ID '" + detalleGeneral.getId() + "' tiene un Monto total Cobrado de " + montoCobrado);
                            System.out.println("");
                            System.out.println("Monto Pendient Cobrar ::" + detalleGeneral.getPendienteCobrar());
                            System.out.println("Monto Cobrado ::" + montoCobrado);
                            Float monto = ManageUtils.redondearFloat((detalleGeneral.getPendienteCobrar() - montoCobrado));

                            System.out.println("Monto resultante depues de restar el pendiente_cobrar menos el monto Cobrado");
                            System.out.println("Monto ::" + monto);
                            
                            if (monto == 0) {
                                detalleGeneral.setStatusDetalle("C");
                                detalleGeneral.setPendienteCobrar(monto);
                            } else if (monto > 0) {
                                detalleGeneral.setPendienteCobrar(monto);
                            } else if (monto < 0) {
                                detalleGeneral.setStatusDetalle("C");
                                detalleGeneral.setPendienteCobrar(0f);
                                Cobro cobro = new Cobro();
                                cobro.setComentario("La sumatoria de monto cobrado supera el pendiente a cobrar del detalle_general_id : " + detalleGeneral.getId() + ". El exedente es de " + (monto * -1));
                                cobro.setFechaCobro(new Date());
                                cobro.setMontoCobrado((monto * -1));
                                cobro.setStatusCobro("E");
                                CobroHistorial cobroHistorial = new CobroHistorial();
                                cobroHistorial.setMontoCobrado((monto * -1));
                                cobroHistorial.setDetalleGeneralId(detalleGeneral);
                                cobroHistorial.setStatusCobroHistorial("E");
                                List<CobroHistorial> list = new ArrayList<CobroHistorial>();
                                list.add(cobroHistorial);
                                cobro.setCobroHistorialList(list);
                                CobroResponse cobroResponse = cobroDao.crearCobro(cobro);
                                if (cobroResponse.getMsgEnum().getCode() == 1) {
                                    System.out.println("");
                                    System.out.println("El Cobro con el Excedente fue agragado exitosamente");

                                } else if (cobroResponse.getMsgEnum().getCode() == 0) {
                                    System.out.println("");
                                    System.out.println("El Cobro con el Excedente NO FUE agragado exitosamente");
                                }
                            }

                        }

                    }

                    //Termina la verificacion de Cobro para un detallege general determinado.
                    //END
                    detalleGeneralList.add(detalleGeneral);

                }

                System.out.println("Codigo Grupo -->" + keyCodigoGrupo);
                System.out.println("Cantidad de Rows Para el Grupo  --->" + detalleGeneralList.size());
                generalEntity.setStatus("A");
                generalEntity.setDetalleGeneralList(detalleGeneralList);

                GeneralResponse generalResponse = null;
                System.out.println("");
                System.out.println("existGrupoConFechaEnGeneral  --->" + existGrupoConFechaEnGeneral);
                System.out.println("");
                if (existGrupoConFechaEnGeneral == true) {

                    if (!detalleGeneralList.isEmpty()) { //verifica que hay al menos un detAlle general para actualizar
                        generalResponse = generalDao.editarGeneral(generalEntity);
                    }
                } else {
                    generalResponse = generalDao.crearGeneral(generalEntity);
                }

                if (generalResponse != null) {
                    if (generalResponse.getMsgEnum().getCode() == 1) {
                        System.out.println("General Added SuccessFully");
                        System.out.println("General ID ::" + generalResponse.getGeneral().getId());
                    } else if (generalResponse.getMsgEnum().getCode() == 0) {
                        System.out.println("**********************************************************************");
                        System.out.println("");
                        System.out.println("No se pudo insertar en la base de datos el General ni el detalle general ::");
                        System.out.println("Grupo ID ::" + keyCodigoGrupo);
                        System.out.println("Detalle General List Size ::" + detalleGeneralList.size());
                        System.out.println("**********************************************************************");
                    }

                }
            } // end
                
                
                // >>>>>>>> CODIGO NUEVO <<<<<<<<<<<<
                 
            
//                System.out.println(message);
////                //******************************************************************
//                 
//                 Context cxt=new InitialContext();
//                 ConnectionFactory factory=(ConnectionFactory) cxt.lookup("jms/sibFactory");
//                 Queue queue =(Queue) cxt.lookup("jms/sibQueue");
//                 
//                 con=factory.createConnection();
//                 session=con.createSession(false, Session.AUTO_ACKNOWLEDGE);
//                 MessageProducer producer=session.createProducer(queue);
//                 
//                 TextMessage msg=session.createTextMessage();
//                 msg.setText(message);
//                 
//                 producer.send(msg);
            
                
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }finally {
                try {
                    if (con!=null) {
                        con.close();
                    }
                    if (session!=null) {
                        session.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            
           
            
            
        }
    
    }
    
}