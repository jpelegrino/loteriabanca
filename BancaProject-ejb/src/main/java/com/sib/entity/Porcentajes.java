/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

/**
 *
 * @author julio
 */
@Entity
@SqlResultSetMapping(name="montosPorcentajes",
        entities = {
            @EntityResult(entityClass =Porcentajes.class,
                    fields = {@FieldResult(name="banca",column = "codigo_banca"),
                              @FieldResult(name="fecha",column = "fecha_efectiva"),
                              @FieldResult(name="totalBanca",column = "total_banca"),
                              @FieldResult(name="totalJuego1",column = "total_juego1"),
                              @FieldResult(name="totalJuego2",column = "total_juego2"),
                              @FieldResult(name="totalJuego3",column = "total_juego3")
                            })
        })
public class Porcentajes implements Serializable {
    @Id
    private String banca;
    private String fecha;
    private double totalBanca;
    private double totalJuego1;
    private double totalJuego2;
    private double totalJuego3;

    public String getBanca() {
        return banca;
    }

    public void setBanca(String banca) {
        this.banca = banca;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public double getTotalBanca() {
        return totalBanca;
    }

    public void setTotalBanca(double totalBanca) {
        this.totalBanca = totalBanca;
    }

    public double getTotalJuego1() {
        return totalJuego1;
    }

    public void setTotalJuego1(double totalJuego1) {
        this.totalJuego1 = totalJuego1;
    }

    public double getTotalJuego2() {
        return totalJuego2;
    }

    public void setTotalJuego2(double totalJuego2) {
        this.totalJuego2 = totalJuego2;
    }

    public double getTotalJuego3() {
        return totalJuego3;
    }

    public void setTotalJuego3(double totalJuego3) {
        this.totalJuego3 = totalJuego3;
    }
    
    
}
