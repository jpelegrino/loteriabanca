/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "COBRO")
@NamedQueries({
    @NamedQuery(name = "Cobro.findAll", query = "SELECT c FROM Cobro c"),
    @NamedQuery(name = "Cobro.findById", query = "SELECT c FROM Cobro c WHERE c.id = :id"),
    @NamedQuery(name = "Cobro.findByComentario", query = "SELECT c FROM Cobro c WHERE c.comentario = :comentario"),
    @NamedQuery(name = "Cobro.findByFechaCobro", query = "SELECT c FROM Cobro c WHERE c.fechaCobro = :fechaCobro"),
    @NamedQuery(name = "Cobro.findByStatusCobro", query = "SELECT c FROM Cobro c WHERE c.statusCobro = :statusCobro")})
public class Cobro implements Serializable, Entidad {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "comentario")
    private String comentario;
    @Column(name = "fecha_efectiva")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCobro;
    @Column(name = "fecha_creacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Column(name = "fecha_modificacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    @Size(max = 1)
    @Column(name = "status_cobro")
    private String statusCobro;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cobroId")
    private List<CobroHistorial> cobroHistorialList;
    @Column(name = "monto_total_cobrado")
    private Float montoCobrado;

    public Cobro() {
    }

    public Cobro(Long id) {
        this.id = id;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Date getFechaCobro() {
        return fechaCobro;
    }

    public void setFechaCobro(Date fechaCobro) {
        this.fechaCobro = fechaCobro;
    }

    public String getStatusCobro() {
        return statusCobro;
    }

    public void setStatusCobro(String statusCobro) {
        this.statusCobro = statusCobro;
    }

    public List<CobroHistorial> getCobroHistorialList() {
        return cobroHistorialList;
    }

    public void setCobroHistorialList(List<CobroHistorial> cobroHistorialList) {
        this.cobroHistorialList = cobroHistorialList;
    }

    public Float getMontoCobrado() {
        return montoCobrado;
    }

    public void setMontoCobrado(Float montoCobrado) {
        this.montoCobrado = montoCobrado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cobro)) {
            return false;
        }
        Cobro other = (Cobro) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.Cobro[ id=" + id + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "Cobro";
    }

}
