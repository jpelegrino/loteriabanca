/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "BANCA")
@NamedQueries({
    @NamedQuery(name = "Banca.findAll", query = "SELECT b FROM Banca b"),
    @NamedQuery(name = "Banca.findAll2", query = "SELECT b FROM Banca b WHERE b.status = :status"),
    @NamedQuery(name = "Banca.findById", query = "SELECT b FROM Banca b WHERE b.id = :id"),
    @NamedQuery(name = "Banca.findByCodigo", query = "SELECT b FROM Banca b WHERE b.codigo = :codigo"),
    @NamedQuery(name = "Banca.findByFechaCreacion", query = "SELECT b FROM Banca b WHERE b.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "Banca.findByStatus", query = "SELECT b FROM Banca b WHERE b.status = :status"),
    @NamedQuery(name = "Banca.findByFechaActualizacion", query = "SELECT b FROM Banca b WHERE b.fechaActualizacion = :fechaActualizacion"),
    @NamedQuery(name = "Banca.findByGrupoBanca", query = "SELECT b FROM Banca b WHERE b.grupoId.id = :grupoId and b.codigo = :codigo"),
    @NamedQuery(name = "Banca.findByGrupo", query = "SELECT b FROM Banca b WHERE b.grupoId.id = :grupoId")})
public class Banca implements Serializable,Entidad {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "codigo")
    private String codigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_creacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "status")
    private String status;
    @Column(name = "fecha_actualizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaActualizacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "banca")
    private List<BancaLoteria> bancaLoteriaList;
    @JoinColumn(name = "grupo_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Grupo grupoId;

    public Banca() {
    }

    public Banca(Integer id) {
        this.id = id;
    }

    public Banca(Integer id, String codigo, Date fechaCreacion, String status) {
        this.id = id;
        this.codigo = codigo;
        this.fechaCreacion = fechaCreacion;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public List<BancaLoteria> getBancaLoteriaList() {
        return bancaLoteriaList;
    }

    public void setBancaLoteriaList(List<BancaLoteria> bancaLoteriaList) {
        this.bancaLoteriaList = bancaLoteriaList;
    }

    public Grupo getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(Grupo grupoId) {
        this.grupoId = grupoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Banca)) {
            return false;
        }
        Banca other = (Banca) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.Banca[ id=" + id + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "Banca";
    }

    }