/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "JUEGO")
@NamedQueries({
    @NamedQuery(name = "Juego.findAll", query = "SELECT j FROM Juego j"),
    @NamedQuery(name = "Juego.findAll2", query = "SELECT j FROM Juego j WHERE j.status = :status"),
    @NamedQuery(name = "Juego.findById", query = "SELECT j FROM Juego j WHERE j.id = :id"),
    @NamedQuery(name = "Juego.findByCodigo", query = "SELECT j FROM Juego j WHERE j.codigo = :codigo"),
    @NamedQuery(name = "Juego.findByStatus", query = "SELECT j FROM Juego j WHERE j.status = :status"),
    @NamedQuery(name = "Juego.findByNombre", query = "SELECT j FROM Juego j WHERE j.nombre = :nombre"),
    @NamedQuery(name = "Juego.findByFechaCreacion", query = "SELECT j FROM Juego j WHERE j.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "Juego.findByFechaActualizacion", query = "SELECT j FROM Juego j WHERE j.fechaActualizacion = :fechaActualizacion"),
    @NamedQuery(name = "Juego.validarExistenciaJuego", query = "SELECT j FROM Juego j WHERE j.codigo = :codigo"),
    @NamedQuery(name = "Juego.validarExistenciaJuego2", query = "SELECT j FROM Juego j WHERE j.id = :id"),
    @NamedQuery(name = "Juego.findAllJuegoActivo", query = "SELECT j FROM Juego j WHERE j.status ='A'")})
public class Juego implements Serializable,Entidad {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "codigo")
    private String codigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "status")
    private String status;
    @Size(max = 45)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_creacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Column(name = "fecha_actualizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaActualizacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "juego")
    private List<LoteriaJuego> loteriaJuegoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "juego")
    private List<BancaLoteriaJuego> bancaLoteriaJuegoList;

    public Juego() {
    }

    public Juego(Integer id) {
        this.id = id;
    }

    public Juego(Integer id, String codigo, String status, Date fechaCreacion) {
        this.id = id;
        this.codigo = codigo;
        this.status = status;
        this.fechaCreacion = fechaCreacion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public List<LoteriaJuego> getLoteriaJuegoList() {
        return loteriaJuegoList;
    }

    public void setLoteriaJuegoList(List<LoteriaJuego> loteriaJuegoList) {
        this.loteriaJuegoList = loteriaJuegoList;
    }

    public List<BancaLoteriaJuego> getBancaLoteriaJuegoList() {
        return bancaLoteriaJuegoList;
    }

    public void setBancaLoteriaJuegoList(List<BancaLoteriaJuego> bancaLoteriaJuegoList) {
        this.bancaLoteriaJuegoList = bancaLoteriaJuegoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Juego)) {
            return false;
        }
        Juego other = (Juego) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.Juego[ id=" + id + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "Juego";
    }
    
}
