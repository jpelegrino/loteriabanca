/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "LOTERIA_has_JUEGO")
@NamedQueries({
    @NamedQuery(name = "LoteriaJuego.findAll", query = "SELECT l FROM LoteriaJuego l"),
    @NamedQuery(name = "LoteriaJuego.findAllJuegosActivos", query = "SELECT l FROM LoteriaJuego l WHERE l.loteriaJuegoPK.loteriaId = :loteriaId AND l.status='A'"),
    @NamedQuery(name = "LoteriaJuego.findByLoteriaId", query = "SELECT l FROM LoteriaJuego l WHERE l.loteriaJuegoPK.loteriaId = :loteriaId and l.status = :status"),
    @NamedQuery(name = "LoteriaJuego.findByJuegoId", query = "SELECT l FROM LoteriaJuego l WHERE l.loteriaJuegoPK.juegoId = :juegoId"),
    @NamedQuery(name = "LoteriaJuego.findByFechaCreacion", query = "SELECT l FROM LoteriaJuego l WHERE l.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "LoteriaJuego.findByStatus", query = "SELECT l FROM LoteriaJuego l WHERE l.status = :status"),
    @NamedQuery(name = "LoteriaJuego.findByFechaActualizacion", query = "SELECT l FROM LoteriaJuego l WHERE l.fechaActualizacion = :fechaActualizacion")})
public class LoteriaJuego implements Serializable,Entidad {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected LoteriaJuegoPK loteriaJuegoPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_creacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "status")
    private String status;
    @Column(name = "fecha_actualizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaActualizacion;
    @JoinColumn(name = "juego_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Juego juego;
    @JoinColumn(name = "loteria_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Loteria loteria;

    public LoteriaJuego() {
    }

    public LoteriaJuego(LoteriaJuegoPK loteriaJuegoPK) {
        this.loteriaJuegoPK = loteriaJuegoPK;
    }

    public LoteriaJuego(LoteriaJuegoPK loteriaJuegoPK, Date fechaCreacion, String status) {
        this.loteriaJuegoPK = loteriaJuegoPK;
        this.fechaCreacion = fechaCreacion;
        this.status = status;
    }

    public LoteriaJuego(int loteriaId, int juegoId) {
        this.loteriaJuegoPK = new LoteriaJuegoPK(loteriaId, juegoId);
    }

    public LoteriaJuegoPK getLoteriaJuegoPK() {
        return loteriaJuegoPK;
    }

    public void setLoteriaJuegoPK(LoteriaJuegoPK loteriaJuegoPK) {
        this.loteriaJuegoPK = loteriaJuegoPK;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public Juego getJuego() {
        return juego;
    }

    public void setJuego(Juego juego) {
        this.juego = juego;
    }

    public Loteria getLoteria() {
        return loteria;
    }

    public void setLoteria(Loteria loteria) {
        this.loteria = loteria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (loteriaJuegoPK != null ? loteriaJuegoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LoteriaJuego)) {
            return false;
        }
        LoteriaJuego other = (LoteriaJuego) object;
        if ((this.loteriaJuegoPK == null && other.loteriaJuegoPK != null) || (this.loteriaJuegoPK != null && !this.loteriaJuegoPK.equals(other.loteriaJuegoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.LoteriaJuego[ loteriaJuegoPK=" + loteriaJuegoPK + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "LoteriaJuego";
    }
    
}
