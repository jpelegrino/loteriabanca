/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author julio
 */
@Table(name = "GASTO")
@Entity
public class Gasto implements Serializable,Entidad {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "monto")
    private double monto;
    @Column(name = "fecha_efectiva")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date fecha;
    @Column(name = "fecha_creacion")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date fecha_creacion;
    @Column(name = "status")
    private String status;
    @JoinColumn(name = "id_tipo_gasto", referencedColumnName = "id")
    @ManyToOne(optional = false)   
    private TipoGasto tipoGasto;
    @JoinColumn(name = "id_banca", referencedColumnName = "id")
    @ManyToOne(optional = false)   
    private Banca banca;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }
    
    

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public TipoGasto getTipoGasto() {
        return tipoGasto;
    }

    public void setTipoGasto(TipoGasto tipoGasto) {
        this.tipoGasto = tipoGasto;
    }

    public Banca getBanca() {
        return banca;
    }

    public void setBanca(Banca banca) {
        this.banca = banca;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + this.id;
        hash = 17 * hash + (int) (Double.doubleToLongBits(this.monto) ^ (Double.doubleToLongBits(this.monto) >>> 32));
        hash = 17 * hash + (this.fecha != null ? this.fecha.hashCode() : 0);
        hash = 17 * hash + (this.status != null ? this.status.hashCode() : 0);
        hash = 17 * hash + (this.tipoGasto != null ? this.tipoGasto.hashCode() : 0);
        hash = 17 * hash + (this.banca != null ? this.banca.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Gasto other = (Gasto) obj;
        if (this.id != other.id) {
            return false;
        }
        if (Double.doubleToLongBits(this.monto) != Double.doubleToLongBits(other.monto)) {
            return false;
        }
        if (this.fecha != other.fecha && (this.fecha == null || !this.fecha.equals(other.fecha))) {
            return false;
        }
        if ((this.status == null) ? (other.status != null) : !this.status.equals(other.status)) {
            return false;
        }
        if (this.tipoGasto != other.tipoGasto && (this.tipoGasto == null || !this.tipoGasto.equals(other.tipoGasto))) {
            return false;
        }
        if (this.banca != other.banca && (this.banca == null || !this.banca.equals(other.banca))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Gasto{" + "id=" + id + ", monto=" + monto + ", fecha=" + fecha + ", status=" + status + ", tipoGasto=" + tipoGasto + ", banca=" + banca + '}';
    }
    
   
    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "Gasto";
    }
    
}