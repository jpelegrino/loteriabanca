/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author julio
 */
@Embeddable
public class BancaLoteriaJuegoPK implements Serializable,Entidad {
    @Basic(optional = false)
    @NotNull
    @Column(name = "loteria_id")
    private int loteriaId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "banca_id")
    private int bancaId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "juego_id")
    private int juegoId;

    public BancaLoteriaJuegoPK() {
    }

    public BancaLoteriaJuegoPK(int loteriaId, int bancaId, int juegoId) {
        this.loteriaId = loteriaId;
        this.bancaId = bancaId;
        this.juegoId = juegoId;
    }

    public int getLoteriaId() {
        return loteriaId;
    }

    public void setLoteriaId(int loteriaId) {
        this.loteriaId = loteriaId;
    }

    public int getBancaId() {
        return bancaId;
    }

    public void setBancaId(int bancaId) {
        this.bancaId = bancaId;
    }

    public int getJuegoId() {
        return juegoId;
    }

    public void setJuegoId(int juegoId) {
        this.juegoId = juegoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) loteriaId;
        hash += (int) bancaId;
        hash += (int) juegoId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BancaLoteriaJuegoPK)) {
            return false;
        }
        BancaLoteriaJuegoPK other = (BancaLoteriaJuegoPK) object;
        if (this.loteriaId != other.loteriaId) {
            return false;
        }
        if (this.bancaId != other.bancaId) {
            return false;
        }
        if (this.juegoId != other.juegoId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.BancaLoteriaJuegoPK[ loteriaId=" + loteriaId + ", bancaId=" + bancaId + ", juegoId=" + juegoId + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "BancaLoteriaJuegoPK";
    }
    
}
