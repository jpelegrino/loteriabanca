package com.sib.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;

/**
 *
 * @author Snailin Inoa
 */
@Entity
@SqlResultSetMappings({
    @SqlResultSetMapping(name = "bancaGrupoCobro",
            entities = {
                @EntityResult(entityClass = BancaGrupoCobro.class, fields = {
                    @FieldResult(name = "idGrupo", column = "GRUPO_ID"),
                    @FieldResult(name = "idBanca", column = "BANCA_ID"),
                    @FieldResult(name = "deuda", column = "DEUDA"),
                    @FieldResult(name = "codigoBanca", column = "CODIGO_BANCA"),
                    @FieldResult(name = "codigoGrupo", column = "CODIGO_GRUPO")

                })
            })
})
public class BancaGrupoCobro implements Serializable, Entidad {

    @Id
    private int idBanca;
    private int idGrupo;
    private Float deuda;
    private String codigoBanca;
    private String codigoGrupo;

    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    public int getIdBanca() {
        return idBanca;
    }

    public void setIdBanca(int idBanca) {
        this.idBanca = idBanca;
    }

    public Float getDeuda() {
        return deuda;
    }

    public void setDeuda(Float deuda) {
        this.deuda = deuda;
    }

    public String getCodigoBanca() {
        return codigoBanca;
    }

    public void setCodigoBanca(String codigoBanca) {
        this.codigoBanca = codigoBanca;
    }

    public String getCodigoGrupo() {
        return codigoGrupo;
    }

    public void setCodigoGrupo(String codigoGrupo) {
        this.codigoGrupo = codigoGrupo;
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "BancaGrupoCobro";
    }

}
