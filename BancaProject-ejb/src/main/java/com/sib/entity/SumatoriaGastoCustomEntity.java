/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

/**
 *
 * @author julio
 */
@Entity
@SqlResultSetMapping(name="gastoTotal",
        entities = {
            @EntityResult(entityClass =SumatoriaGastoCustomEntity.class,
                    fields = {@FieldResult(name="id",column = "id"),
                              @FieldResult(name="montoTotal",column = "monto_total"),                            
                            })
        })
public class SumatoriaGastoCustomEntity implements Serializable {
    @Id
    private int id;
    private double montoTotal;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(double montoTotal) {
        this.montoTotal = montoTotal;
    }
    
    
}
