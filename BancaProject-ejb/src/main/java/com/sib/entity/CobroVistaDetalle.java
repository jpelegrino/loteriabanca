package com.sib.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Snailin Inoa
 */
@Entity
@SqlResultSetMappings({
    @SqlResultSetMapping(name = "cobroVistaDetalle",
            entities = {
                @EntityResult(entityClass = CobroVistaDetalle.class, fields = {
                    @FieldResult(name = "cobroHistorialId", column = "COBRO_HISTORIAL_ID"),
                    @FieldResult(name = "cobroId", column = "COBRO_ID"),
                    @FieldResult(name = "fechaCobro", column = "FECHA_COBRO"),
                    @FieldResult(name = "montoCobrado", column = "MONTO_COBRADO"),
                    @FieldResult(name = "statusCobroHistorial", column = "STATUS_COBRO_HISTORIAL"),
                    @FieldResult(name = "loteriaId", column = "LOTERIA_ID"),
                    @FieldResult(name = "detalleGeneralId", column = "DETALLE_GENERAL_ID"),
                    @FieldResult(name = "codigoLoteria", column = "CODIGO_LOTERIA"),
                    @FieldResult(name = "bancaId", column = "BANCA_ID"),
                    @FieldResult(name = "codigoBanca", column = "CODIGO_BANCA"),
                    @FieldResult(name = "grupoId", column = "GRUPO_ID"),
                    @FieldResult(name = "codigoGrupo", column = "CODIGO_GRUPO")

                })
            })
})
public class CobroVistaDetalle implements Serializable, Entidad {

    @Id
    private Long cobroHistorialId;
    private Long cobroId;
    private Long detalleGeneralId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCobro;
    private Float montoCobrado;
    private String statusCobroHistorial;
    private Long loteriaId;
    private String codigoLoteria;
    private Long bancaId;
    private String codigoBanca;
    private Long grupoId;
    private String codigoGrupo;

    public Long getCobroHistorialId() {
        return cobroHistorialId;
    }

    public void setCobroHistorialId(Long cobroHistorialId) {
        this.cobroHistorialId = cobroHistorialId;
    }

    public Long getCobroId() {
        return cobroId;
    }

    public void setCobroId(Long cobroId) {
        this.cobroId = cobroId;
    }

    public Date getFechaCobro() {
        return fechaCobro;
    }

    public void setFechaCobro(Date fechaCobro) {
        this.fechaCobro = fechaCobro;
    }

    public Float getMontoCobrado() {
        return montoCobrado;
    }

    public void setMontoCobrado(Float montoCobrado) {
        this.montoCobrado = montoCobrado;
    }

    public String getStatusCobroHistorial() {
        return statusCobroHistorial;
    }

    public void setStatusCobroHistorial(String statusCobroHistorial) {
        this.statusCobroHistorial = statusCobroHistorial;
    }

    public Long getLoteriaId() {
        return loteriaId;
    }

    public void setLoteriaId(Long loteriaId) {
        this.loteriaId = loteriaId;
    }

    public String getCodigoLoteria() {
        return codigoLoteria;
    }

    public void setCodigoLoteria(String codigoLoteria) {
        this.codigoLoteria = codigoLoteria;
    }

    public Long getBancaId() {
        return bancaId;
    }

    public void setBancaId(Long bancaId) {
        this.bancaId = bancaId;
    }

    public String getCodigoBanca() {
        return codigoBanca;
    }

    public void setCodigoBanca(String codigoBanca) {
        this.codigoBanca = codigoBanca;
    }

    public Long getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(Long grupoId) {
        this.grupoId = grupoId;
    }

    public String getCodigoGrupo() {
        return codigoGrupo;
    }

    public void setCodigoGrupo(String codigoGrupo) {
        this.codigoGrupo = codigoGrupo;
    }

    public Long getDetalleGeneralId() {
        return detalleGeneralId;
    }

    public void setDetalleGeneralId(Long detalleGeneralId) {
        this.detalleGeneralId = detalleGeneralId;
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "CobroVistaDetalle";
    }

}
