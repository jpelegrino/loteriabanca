/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "COBRO_HISTORIAL")
@NamedQueries({
    @NamedQuery(name = "CobroHistorial.findAll", query = "SELECT c FROM CobroHistorial c"),
    @NamedQuery(name = "CobroHistorial.findById", query = "SELECT c FROM CobroHistorial c WHERE c.id = :id"),
    @NamedQuery(name = "CobroHistorial.findByMontoCobrado", query = "SELECT c FROM CobroHistorial c WHERE c.montoCobrado = :montoCobrado"),
    @NamedQuery(name = "CobroHistorial.findByStatusCobroHistorial", query = "SELECT c FROM CobroHistorial c WHERE c.statusCobroHistorial = :statusCobroHistorial")})
public class CobroHistorial implements Serializable, Entidad {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "monto_cobrado")
    private Float montoCobrado;
    @Size(max = 1)
    @Column(name = "status_cobro_historial")
    private String statusCobroHistorial;
    @JoinColumn(name = "cobro_id", referencedColumnName = "id")
    @ManyToOne(optional = false, cascade = CascadeType.MERGE)
    private Cobro cobroId;
    @JoinColumn(name = "detalle_general_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private DetalleGeneral detalleGeneralId;

    public CobroHistorial() {
    }

    public CobroHistorial(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getMontoCobrado() {
        return montoCobrado;
    }

    public void setMontoCobrado(Float montoCobrado) {
        this.montoCobrado = montoCobrado;
    }

    public String getStatusCobroHistorial() {
        return statusCobroHistorial;
    }

    public void setStatusCobroHistorial(String statusCobroHistorial) {
        this.statusCobroHistorial = statusCobroHistorial;
    }

    public Cobro getCobroId() {
        return cobroId;
    }

    public void setCobroId(Cobro cobroId) {
        this.cobroId = cobroId;
    }

    public DetalleGeneral getDetalleGeneralId() {
        return detalleGeneralId;
    }

    public void setDetalleGeneralId(DetalleGeneral detalleGeneralId) {
        this.detalleGeneralId = detalleGeneralId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CobroHistorial)) {
            return false;
        }
        CobroHistorial other = (CobroHistorial) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.CobroHistorial[ id=" + id + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "CobroHistorial";
    }

}
