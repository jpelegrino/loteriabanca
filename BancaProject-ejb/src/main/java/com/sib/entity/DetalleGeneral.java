/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "DETALLE_GENERAL")
@NamedQueries({
    @NamedQuery(name = "DetalleGeneral.findAll", query = "SELECT d FROM DetalleGeneral d"),
    @NamedQuery(name = "DetalleGeneral.findById", query = "SELECT d FROM DetalleGeneral d WHERE d.id = :id"),
    @NamedQuery(name = "DetalleGeneral.findByValorVentaTotal", query = "SELECT d FROM DetalleGeneral d WHERE d.valorVentaTotal = :valorVentaTotal"),
    @NamedQuery(name = "DetalleGeneral.findByValorPremioTotal", query = "SELECT d FROM DetalleGeneral d WHERE d.valorPremioTotal = :valorPremioTotal"),
    @NamedQuery(name = "DetalleGeneral.findByTotalPagar", query = "SELECT d FROM DetalleGeneral d WHERE d.totalPagar = :totalPagar"),
    @NamedQuery(name = "DetalleGeneral.findByMontoFinalOperaciones", query = "SELECT d FROM DetalleGeneral d WHERE d.montoFinalOperaciones = :montoFinalOperaciones"),
    @NamedQuery(name = "DetalleGeneral.findByPendienteCobrar", query = "SELECT d FROM DetalleGeneral d WHERE d.pendienteCobrar = :pendienteCobrar"),
    @NamedQuery(name = "DetalleGeneral.findByStatusDetalle", query = "SELECT d FROM DetalleGeneral d WHERE d.statusDetalle = :statusDetalle"),
    @NamedQuery(name = "DetalleGeneral.validarExistenciabyBancaLoteria", query = "SELECT d FROM DetalleGeneral d WHERE d.bancaLoteria.banca.id = :bancaId and d.bancaLoteria.loteria.id = :loteriaId"),
    @NamedQuery(name = "DetalleGeneral.validarExistenciaBanca", query = "SELECT d FROM DetalleGeneral d WHERE d.bancaLoteria.banca.id = :bancaId")})
public class DetalleGeneral implements Serializable, Entidad {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor_venta_total")
    private Float valorVentaTotal;
    @Column(name = "valor_premio_total")
    private Float valorPremioTotal;
    @Column(name = "total_pagar")
    private Float totalPagar;
    @Column(name = "monto_final_operaciones")
    private Float montoFinalOperaciones;
    @Column(name = "pendiente_cobrar")
    private Float pendienteCobrar;
    @Size(max = 1)
    @Column(name = "status_detalle")
    private String statusDetalle;
    @Size(max = 1)
    @Column(name = "status")
    private String status;
    @Column(name = "juego1_venta")
    private Float juego1Venta;
    @Column(name = "juego1_premio")
    private Float juego1Premio;
    @Column(name = "juego1_porciento_pagar")
    private Float juego1PorcientoPagar;
    @Column(name = "juego1_monto_pagar")
    private Float juego1MontoPagar;
    @Column(name = "juego2_venta")
    private Float juego2Venta;
    @Column(name = "juego2_premio")
    private Float juego2Premio;
    @Column(name = "juego2_porciento_pagar")
    private Float juego2PorcientoPagar;
    @Column(name = "juego2_monto_pagar")
    private Float juego2MontoPagar;
    @Column(name = "juego3_venta")
    private Float juego3Venta;
    @Column(name = "juego3_premio")
    private Float juego3Premio;
    @Column(name = "juego3_porciento_pagar")
    private Float juego3PorcientoPagar;
    @Column(name = "juego3_monto_pagar")
    private Float juego3MontoPagar;
    @Column(name = "juego4_venta")
    private Float juego4Venta;
    @Column(name = "juego4_premio")
    private Float juego4Premio;
    @Column(name = "juego4_porciento_pagar")
    private Float juego4PorcientoPagar;
    @Column(name = "juego4_monto_pagar")
    private Float juego4MontoPagar;
    @Column(name = "fecha_actualizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaActualizacion;
    @Column(name = "fecha_efectiva")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaEfectiva;
    @Column(name = "fecha_creacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @JoinColumns({
        @JoinColumn(name = "banca_id", referencedColumnName = "banca_id"),
        @JoinColumn(name = "loteria_id", referencedColumnName = "loteria_id")})
    @ManyToOne(optional = false)
    private BancaLoteria bancaLoteria;
    @JoinColumn(name = "general_id", referencedColumnName = "id")
    @ManyToOne(optional = false, cascade = CascadeType.MERGE)
    private General generalId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detalleGeneralId")
    private List<CobroHistorial> cobroHistorialList;

    public DetalleGeneral() {
    }

    public DetalleGeneral(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getValorVentaTotal() {
        return valorVentaTotal;
    }

    public void setValorVentaTotal(Float valorVentaTotal) {
        this.valorVentaTotal = valorVentaTotal;
    }

    public Float getValorPremioTotal() {
        return valorPremioTotal;
    }

    public void setValorPremioTotal(Float valorPremioTotal) {
        this.valorPremioTotal = valorPremioTotal;
    }

    public Float getTotalPagar() {
        return totalPagar;
    }

    public void setTotalPagar(Float totalPagar) {
        this.totalPagar = totalPagar;
    }

    public Float getMontoFinalOperaciones() {
        return montoFinalOperaciones;
    }

    public void setMontoFinalOperaciones(Float montoFinalOperaciones) {
        this.montoFinalOperaciones = montoFinalOperaciones;
    }

    public Float getPendienteCobrar() {
        return pendienteCobrar;
    }

    public void setPendienteCobrar(Float pendienteCobrar) {
        this.pendienteCobrar = pendienteCobrar;
    }

    public String getStatusDetalle() {
        return statusDetalle;
    }

    public void setStatusDetalle(String statusDetalle) {
        this.statusDetalle = statusDetalle;
    }

    public Float getJuego1Venta() {
        return juego1Venta;
    }

    public void setJuego1Venta(Float juego1Venta) {
        this.juego1Venta = juego1Venta;
    }

    public Float getJuego1Premio() {
        return juego1Premio;
    }

    public void setJuego1Premio(Float juego1Premio) {
        this.juego1Premio = juego1Premio;
    }

    public Float getJuego1PorcientoPagar() {
        return juego1PorcientoPagar;
    }

    public void setJuego1PorcientoPagar(Float juego1PorcientoPagar) {
        this.juego1PorcientoPagar = juego1PorcientoPagar;
    }

    public Float getJuego1MontoPagar() {
        return juego1MontoPagar;
    }

    public void setJuego1MontoPagar(Float juego1MontoPagar) {
        this.juego1MontoPagar = juego1MontoPagar;
    }

    public Float getJuego2Venta() {
        return juego2Venta;
    }

    public void setJuego2Venta(Float juego2Venta) {
        this.juego2Venta = juego2Venta;
    }

    public Float getJuego2Premio() {
        return juego2Premio;
    }

    public void setJuego2Premio(Float juego2Premio) {
        this.juego2Premio = juego2Premio;
    }

    public Float getJuego2PorcientoPagar() {
        return juego2PorcientoPagar;
    }

    public void setJuego2PorcientoPagar(Float juego2PorcientoPagar) {
        this.juego2PorcientoPagar = juego2PorcientoPagar;
    }

    public Float getJuego2MontoPagar() {
        return juego2MontoPagar;
    }

    public void setJuego2MontoPagar(Float juego2MontoPagar) {
        this.juego2MontoPagar = juego2MontoPagar;
    }

    public Float getJuego3Venta() {
        return juego3Venta;
    }

    public void setJuego3Venta(Float juego3Venta) {
        this.juego3Venta = juego3Venta;
    }

    public Float getJuego3Premio() {
        return juego3Premio;
    }

    public void setJuego3Premio(Float juego3Premio) {
        this.juego3Premio = juego3Premio;
    }

    public Float getJuego3PorcientoPagar() {
        return juego3PorcientoPagar;
    }

    public void setJuego3PorcientoPagar(Float juego3PorcientoPagar) {
        this.juego3PorcientoPagar = juego3PorcientoPagar;
    }

    public Float getJuego3MontoPagar() {
        return juego3MontoPagar;
    }

    public void setJuego3MontoPagar(Float juego3MontoPagar) {
        this.juego3MontoPagar = juego3MontoPagar;
    }

    public Float getJuego4Venta() {
        return juego4Venta;
    }

    public void setJuego4Venta(Float juego4Venta) {
        this.juego4Venta = juego4Venta;
    }

    public Float getJuego4Premio() {
        return juego4Premio;
    }

    public void setJuego4Premio(Float juego4Premio) {
        this.juego4Premio = juego4Premio;
    }

    public Float getJuego4PorcientoPagar() {
        return juego4PorcientoPagar;
    }

    public void setJuego4PorcientoPagar(Float juego4PorcientoPagar) {
        this.juego4PorcientoPagar = juego4PorcientoPagar;
    }

    public Float getJuego4MontoPagar() {
        return juego4MontoPagar;
    }

    public void setJuego4MontoPagar(Float juego4MontoPagar) {
        this.juego4MontoPagar = juego4MontoPagar;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public BancaLoteria getBancaLoteria() {
        return bancaLoteria;
    }

    public void setBancaLoteria(BancaLoteria bancaLoteria) {
        this.bancaLoteria = bancaLoteria;
    }

    public General getGeneralId() {
        return generalId;
    }

    public void setGeneralId(General generalId) {
        this.generalId = generalId;
    }

    public List<CobroHistorial> getCobroHistorialList() {
        return cobroHistorialList;
    }

    public void setCobroHistorialList(List<CobroHistorial> cobroHistorialList) {
        this.cobroHistorialList = cobroHistorialList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleGeneral)) {
            return false;
        }
        DetalleGeneral other = (DetalleGeneral) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "com.sib.entity.DetalleGeneral[ id=" + id + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "DetalleGeneral";
    }

    public Date getFechaEfectiva() {
        return fechaEfectiva;
    }

    public void setFechaEfectiva(Date fechaEfectiva) {
        this.fechaEfectiva = fechaEfectiva;
    }
    
    

}