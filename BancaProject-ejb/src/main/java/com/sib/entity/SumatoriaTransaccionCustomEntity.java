/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

/**
 *
 * @author julio
 */
@Entity
@SqlResultSetMapping(name="transaccionTotal",
        entities = {
            @EntityResult(entityClass =SumatoriaTransaccionCustomEntity.class,
                    fields = {@FieldResult(name="id",column = "id"),
                              @FieldResult(name="ventaTotal",column = "venta"),
                              @FieldResult(name="premioTotal",column = "premio"),
                              @FieldResult(name="juego1Venta",column = "juego1_venta"),
                              @FieldResult(name="juego1Premio",column = "juego1_premio"),
                              @FieldResult(name="juego2Venta",column = "juego2_venta"),
                              @FieldResult(name="juego2Premio",column = "juego2_premio"),
                              @FieldResult(name="juego3Venta",column = "juego3_venta"),
                              @FieldResult(name="juego3Premio",column = "juego3_premio"),
                              @FieldResult(name="juego4Venta",column = "juego4_venta"),
                              @FieldResult(name="juego4Premio",column = "juego4_premio"),
                              
                              
                            })
        })
public class SumatoriaTransaccionCustomEntity implements Serializable {
    @Id
    private int id;
    private double ventaTotal;
    private double premioTotal;
    private double juego1Venta;
    private double juego1Premio;
    private double juego2Venta;
    private double juego2Premio;
    private double juego3Venta;
    private double juego3Premio;
    private double juego4Venta;
    private double juego4Premio;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getVentaTotal() {
        return ventaTotal;
    }

    public void setVentaTotal(double ventaTotal) {
        this.ventaTotal = ventaTotal;
    }

    public double getPremioTotal() {
        return premioTotal;
    }

    public void setPremioTotal(double premioTotal) {
        this.premioTotal = premioTotal;
    }

    public double getJuego1Venta() {
        return juego1Venta;
    }

    public void setJuego1Venta(double juego1Venta) {
        this.juego1Venta = juego1Venta;
    }

    public double getJuego1Premio() {
        return juego1Premio;
    }

    public void setJuego1Premio(double juego1Premio) {
        this.juego1Premio = juego1Premio;
    }

    public double getJuego2Venta() {
        return juego2Venta;
    }

    public void setJuego2Venta(double juego2Venta) {
        this.juego2Venta = juego2Venta;
    }

    public double getJuego2Premio() {
        return juego2Premio;
    }

    public void setJuego2Premio(double juego2Premio) {
        this.juego2Premio = juego2Premio;
    }

    public double getJuego3Venta() {
        return juego3Venta;
    }

    public void setJuego3Venta(double juego3Venta) {
        this.juego3Venta = juego3Venta;
    }

    public double getJuego3Premio() {
        return juego3Premio;
    }

    public void setJuego3Premio(double juego3Premio) {
        this.juego3Premio = juego3Premio;
    }

    public double getJuego4Venta() {
        return juego4Venta;
    }

    public void setJuego4Venta(double juego4Venta) {
        this.juego4Venta = juego4Venta;
    }

    public double getJuego4Premio() {
        return juego4Premio;
    }

    public void setJuego4Premio(double juego4Premio) {
        this.juego4Premio = juego4Premio;
    }

    
    
    
}
