/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.validation.constraints.Size;

/**
 *
 * @author julio
 */
@Entity
@SqlResultSetMapping(name="juegobljResults",
        entities = {
            @EntityResult(entityClass =JuegoAndBLJ.class,
                    fields = {@FieldResult(name="codigo",column = "codigo"),
                              @FieldResult(name="nombre",column = "nombre"),
                            @FieldResult(name="porcentajeCobro",column = "porcentaje_cobro")})
        })
public class JuegoAndBLJ implements Entidad, Serializable {
    @Id   
    private Integer id;   
    private String codigo;    
    private String nombre;
    private int porcentajeCobro;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPorcentajeCobro() {
        return porcentajeCobro;
    }

    public void setPorcentajeCobro(int porcentajeCobro) {
        this.porcentajeCobro = porcentajeCobro;
    }

   
    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
    return "JuegoAndBLJ";
    }
    
    
}
