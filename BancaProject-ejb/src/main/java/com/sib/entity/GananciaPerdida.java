/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

/**
 *
 * @author julio
 */
@Entity
@SqlResultSetMapping(name="gananciasYperdidas",
        entities = {
            @EntityResult(entityClass =GananciaPerdida.class,
                    fields = {@FieldResult(name="banca",column = "codigo_banca"),
                              @FieldResult(name="fecha",column = "fecha_efectiva"),
                              @FieldResult(name="ganancia",column = "ganancia"),
                              @FieldResult(name="perdida",column = "perdida")
                            })
        })
public class GananciaPerdida implements Serializable {
    @Id    
    private String banca;
    private String fecha;
    private double ganancia;
    private double perdida;
    

    public String getBanca() {
        return banca;
    }

    public void setBanca(String banca) {
        this.banca = banca;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public double getGanancia() {
        return ganancia;
    }

    public void setGanancia(double ganancia) {
        this.ganancia = ganancia;
    }

    public double getPerdida() {
        return perdida;
    }

    public void setPerdida(double perdida) {
        this.perdida = perdida;
    }
    
    
    
}
