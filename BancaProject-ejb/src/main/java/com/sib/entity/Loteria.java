/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "LOTERIA")
@NamedQueries({
    @NamedQuery(name = "Loteria.findAll", query = "SELECT l FROM Loteria l"),
    @NamedQuery(name = "Loteria.findAll2", query = "SELECT l FROM Loteria l WHERE l.status = :status"),
    @NamedQuery(name = "Loteria.findById", query = "SELECT l FROM Loteria l WHERE l.id = :id"),
    @NamedQuery(name = "Loteria.findByCodigo", query = "SELECT l FROM Loteria l WHERE l.codigo = :codigo"),
    @NamedQuery(name = "Loteria.findByFechaCreacion", query = "SELECT l FROM Loteria l WHERE l.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "Loteria.findByStatus", query = "SELECT l FROM Loteria l WHERE l.status = :status"),
    @NamedQuery(name = "Loteria.findByFechaActualizacion", query = "SELECT l FROM Loteria l WHERE l.fechaActualizacion = :fechaActualizacion")})
public class Loteria implements Serializable,Entidad {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "codigo")
    private String codigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_creacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "status")
    private String status;
    @Column(name = "fecha_actualizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaActualizacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "loteria")
    private List<BancaLoteria> bancaLoteriaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "loteria")
    private List<LoteriaJuego> loteriaJuegoList;

    public Loteria() {
    }

    public Loteria(Integer id) {
        this.id = id;
    }

    public Loteria(Integer id, String codigo, Date fechaCreacion, String status) {
        this.id = id;
        this.codigo = codigo;
        this.fechaCreacion = fechaCreacion;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public List<BancaLoteria> getBancaLoteriaList() {
        return bancaLoteriaList;
    }

    public void setBancaLoteriaList(List<BancaLoteria> bancaLoteriaList) {
        this.bancaLoteriaList = bancaLoteriaList;
    }

    public List<LoteriaJuego> getLoteriaJuegoList() {
        return loteriaJuegoList;
    }

    public void setLoteriaJuegoList(List<LoteriaJuego> loteriaJuegoList) {
        this.loteriaJuegoList = loteriaJuegoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Loteria)) {
            return false;
        }
        Loteria other = (Loteria) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.Loteria[ id=" + id + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "Loteria";
    }
    
}
