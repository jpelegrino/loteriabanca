 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "BANCA_has_LOTERIA_has_JUEGO")
@NamedQueries({
    @NamedQuery(name = "BancaLoteriaJuego.findAll", query = "SELECT b FROM BancaLoteriaJuego b"),
    @NamedQuery(name = "BancaLoteriaJuego.findByLoteriaId", query = "SELECT b FROM BancaLoteriaJuego b WHERE b.bancaLoteriaJuegoPK.loteriaId = :loteriaId"),
    @NamedQuery(name = "BancaLoteriaJuego.validarExisLoteriaJuego", query = "SELECT b FROM BancaLoteriaJuego b WHERE b.bancaLoteriaJuegoPK.loteriaId = :loteriaId and b.bancaLoteriaJuegoPK.juegoId = :juegoId"),
    @NamedQuery(name = "BancaLoteriaJuego.findByBancaId", query = "SELECT b FROM BancaLoteriaJuego b WHERE b.bancaLoteriaJuegoPK.bancaId = :bancaId"),
    @NamedQuery(name = "BancaLoteriaJuego.findByJuegoId", query = "SELECT b FROM BancaLoteriaJuego b WHERE b.bancaLoteriaJuegoPK.juegoId = :juegoId"),
    @NamedQuery(name = "BancaLoteriaJuego.findByPorcentajeCobro", query = "SELECT b FROM BancaLoteriaJuego b WHERE b.porcentajeCobro = :porcentajeCobro"),
    @NamedQuery(name = "BancaLoteriaJuego.findByFechaCreacion", query = "SELECT b FROM BancaLoteriaJuego b WHERE b.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "BancaLoteriaJuego.findByStatus", query = "SELECT b FROM BancaLoteriaJuego b WHERE b.status = :status"),
    @NamedQuery(name = "BancaLoteriaJuego.findByFechaActualizacion", query = "SELECT b FROM BancaLoteriaJuego b WHERE b.fechaActualizacion = :fechaActualizacion"),
    @NamedQuery(name = "BancaLoteriaJuego.findByBancaLoteriaJuego", query = "SELECT b FROM BancaLoteriaJuego b WHERE b.bancaLoteriaJuegoPK.bancaId = :bancaId and b.bancaLoteriaJuegoPK.loteriaId = :loteriaId and b.bancaLoteriaJuegoPK.bancaId = :bancaId")})
public class BancaLoteriaJuego implements Serializable,Entidad {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BancaLoteriaJuegoPK bancaLoteriaJuegoPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "porcentaje_cobro")
    private int porcentajeCobro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_creacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "status")
    private String status;
    @Column(name = "fecha_actualizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaActualizacion;
    @JoinColumn(name = "juego_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Juego juego;
    @JoinColumns({
        @JoinColumn(name = "banca_id", referencedColumnName = "banca_id", insertable = false, updatable = false),
        @JoinColumn(name = "loteria_id", referencedColumnName = "loteria_id", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private BancaLoteria bancaLoteria;

    public BancaLoteriaJuego() {
    }

    public BancaLoteriaJuego(BancaLoteriaJuegoPK bancaLoteriaJuegoPK) {
        this.bancaLoteriaJuegoPK = bancaLoteriaJuegoPK;
    }

    public BancaLoteriaJuego(BancaLoteriaJuegoPK bancaLoteriaJuegoPK, int porcentajeCobro, Date fechaCreacion, String status) {
        this.bancaLoteriaJuegoPK = bancaLoteriaJuegoPK;
        this.porcentajeCobro = porcentajeCobro;
        this.fechaCreacion = fechaCreacion;
        this.status = status;
    }

    public BancaLoteriaJuego(int loteriaId, int bancaId, int juegoId) {
        this.bancaLoteriaJuegoPK = new BancaLoteriaJuegoPK(loteriaId, bancaId, juegoId);
    }

    public BancaLoteriaJuegoPK getBancaLoteriaJuegoPK() {
        return bancaLoteriaJuegoPK;
    }

    public void setBancaLoteriaJuegoPK(BancaLoteriaJuegoPK bancaLoteriaJuegoPK) {
        this.bancaLoteriaJuegoPK = bancaLoteriaJuegoPK;
    }

    public int getPorcentajeCobro() {
        return porcentajeCobro;
    }

    public void setPorcentajeCobro(int porcentajeCobro) {
        this.porcentajeCobro = porcentajeCobro;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public Juego getJuego() {
        return juego;
    }

    public void setJuego(Juego juego) {
        this.juego = juego;
    }

    public BancaLoteria getBancaLoteria() {
        return bancaLoteria;
    }

    public void setBancaLoteria(BancaLoteria bancaLoteria) {
        this.bancaLoteria = bancaLoteria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bancaLoteriaJuegoPK != null ? bancaLoteriaJuegoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BancaLoteriaJuego)) {
            return false;
        }
        BancaLoteriaJuego other = (BancaLoteriaJuego) object;
        if ((this.bancaLoteriaJuegoPK == null && other.bancaLoteriaJuegoPK != null) || (this.bancaLoteriaJuegoPK != null && !this.bancaLoteriaJuegoPK.equals(other.bancaLoteriaJuegoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.BancaLoteriaJuego[ bancaLoteriaJuegoPK=" + bancaLoteriaJuegoPK + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "BancaLoteriaJuego";
    }
    
}
