package com.sib.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Temporal;

/**
 *
 * @author Snailin Inoa
 */
@Entity
@SqlResultSetMappings({
    @SqlResultSetMapping(name = "cobroCustomEntity",
            entities = {
                @EntityResult(entityClass = CobroCustomEntity.class, fields = {
                    @FieldResult(name = "cobroId", column = "COBRO_ID"),
                    @FieldResult(name = "fechaEfectiva", column = "FECHA_EFECTIVA"),
                    @FieldResult(name = "comentario", column = "COMENTARIO"),
                    @FieldResult(name = "montoTotalCobrado", column = "MONTO_TOTAL_COBRADO"),

                    @FieldResult(name = "montoTotal", column = "MONTO_TOTAL"),
                    @FieldResult(name = "cobroHistorialId", column = "COBRO_HISTORIAL_ID"),
                    @FieldResult(name = "detalleGeneralId", column = "DETALLE_GENERAL_ID"),
                    @FieldResult(name = "bancaId", column = "BANCA_ID"),

                    @FieldResult(name = "codigoBanca", column = "CODIGO_BANCA"),
                    @FieldResult(name = "grupoId", column = "GRUPO_ID"),
                    @FieldResult(name = "codigoGrupo", column = "CODIGO_GRUPO"),
                    @FieldResult(name = "cobroStatus", column = "STATUS_COBRO")

                })
            })
})
public class CobroCustomEntity implements Serializable, Entidad {

    @Id
    private Long cobroId;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaEfectiva;
    private String comentario;
    private Long montoTotalCobrado;
    private Long montoTotal;
    private Long cobroHistorialId;
    private Long detalleGeneralId;
    private Long bancaId;
    private String codigoBanca;
    private Long grupoId;
    private String codigoGrupo;
    private String cobroStatus;

    public Long getCobroId() {
        return cobroId;
    }

    public void setCobroId(Long cobroId) {
        this.cobroId = cobroId;
    }

    public Date getFechaEfectiva() {
        return fechaEfectiva;
    }

    public void setFechaEfectiva(Date fechaEfectiva) {
        this.fechaEfectiva = fechaEfectiva;
    }

   

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Long getMontoTotalCobrado() {
        return montoTotalCobrado;
    }

    public void setMontoTotalCobrado(Long montoTotalCobrado) {
        this.montoTotalCobrado = montoTotalCobrado;
    }

    public Long getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(Long montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Long getCobroHistorialId() {
        return cobroHistorialId;
    }

    public void setCobroHistorialId(Long cobroHistorialId) {
        this.cobroHistorialId = cobroHistorialId;
    }

    public Long getDetalleGeneralId() {
        return detalleGeneralId;
    }

    public void setDetalleGeneralId(Long detalleGeneralId) {
        this.detalleGeneralId = detalleGeneralId;
    }

    public Long getBancaId() {
        return bancaId;
    }

    public void setBancaId(Long bancaId) {
        this.bancaId = bancaId;
    }

    public String getCodigoBanca() {
        return codigoBanca;
    }

    public void setCodigoBanca(String codigoBanca) {
        this.codigoBanca = codigoBanca;
    }

    public Long getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(Long grupoId) {
        this.grupoId = grupoId;
    }

    public String getCodigoGrupo() {
        return codigoGrupo;
    }

    public void setCodigoGrupo(String codigoGrupo) {
        this.codigoGrupo = codigoGrupo;
    }

    public String getCobroStatus() {
        return cobroStatus;
    }

    public void setCobroStatus(String cobroStatus) {
        this.cobroStatus = cobroStatus;
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "CobroCustomEntity";
    }

}
