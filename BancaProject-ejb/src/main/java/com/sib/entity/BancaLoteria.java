/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author julio
 */
@Entity
@Table(name = "BANCA_has_LOTERIA")
@NamedQueries({
    @NamedQuery(name = "BancaLoteria.findAll", query = "SELECT b FROM BancaLoteria b"),
    @NamedQuery(name = "BancaLoteria.findByBancaId", query = "SELECT b FROM BancaLoteria b WHERE b.bancaLoteriaPK.bancaId = :bancaId"),
    @NamedQuery(name = "BancaLoteria.findByLoteriaId", query = "SELECT b FROM BancaLoteria b WHERE b.bancaLoteriaPK.loteriaId = :loteriaId"),
    @NamedQuery(name = "BancaLoteria.findByFechaCreacion", query = "SELECT b FROM BancaLoteria b WHERE b.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "BancaLoteria.findByBancaLoteria", query = "SELECT b FROM BancaLoteria b WHERE b.bancaLoteriaPK.bancaId = :bancaId and b.bancaLoteriaPK.loteriaId = :loteriaId")})
public class BancaLoteria implements Serializable,Entidad {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BancaLoteriaPK bancaLoteriaPK;
    @Column(name = "fecha_creacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bancaLoteria")
    private List<DetalleGeneral> detalleGeneralList;
    @JoinColumn(name = "loteria_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Loteria loteria;
    @JoinColumn(name = "banca_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Banca banca;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bancaLoteria")
    private List<BancaLoteriaJuego> bancaLoteriaJuegoList;

    public BancaLoteria() {
    }

    public BancaLoteria(BancaLoteriaPK bancaLoteriaPK) {
        this.bancaLoteriaPK = bancaLoteriaPK;
    }

    public BancaLoteria(int bancaId, int loteriaId) {
        this.bancaLoteriaPK = new BancaLoteriaPK(bancaId, loteriaId);
    }

    public BancaLoteriaPK getBancaLoteriaPK() {
        return bancaLoteriaPK;
    }

    public void setBancaLoteriaPK(BancaLoteriaPK bancaLoteriaPK) {
        this.bancaLoteriaPK = bancaLoteriaPK;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public List<DetalleGeneral> getDetalleGeneralList() {
        return detalleGeneralList;
    }

    public void setDetalleGeneralList(List<DetalleGeneral> detalleGeneralList) {
        this.detalleGeneralList = detalleGeneralList;
    }

    public Loteria getLoteria() {
        return loteria;
    }

    public void setLoteria(Loteria loteria) {
        this.loteria = loteria;
    }

    public Banca getBanca() {
        return banca;
    }

    public void setBanca(Banca banca) {
        this.banca = banca;
    }

    public List<BancaLoteriaJuego> getBancaLoteriaJuegoList() {
        return bancaLoteriaJuegoList;
    }

    public void setBancaLoteriaJuegoList(List<BancaLoteriaJuego> bancaLoteriaJuegoList) {
        this.bancaLoteriaJuegoList = bancaLoteriaJuegoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bancaLoteriaPK != null ? bancaLoteriaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BancaLoteria)) {
            return false;
        }
        BancaLoteria other = (BancaLoteria) object;
        if ((this.bancaLoteriaPK == null && other.bancaLoteriaPK != null) || (this.bancaLoteriaPK != null && !this.bancaLoteriaPK.equals(other.bancaLoteriaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.BancaLoteria[ bancaLoteriaPK=" + bancaLoteriaPK + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "BancaLoteria";
    }
    
}
