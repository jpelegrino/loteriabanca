/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author julio
 */
@Embeddable
public class BancaLoteriaPK implements Serializable,Entidad {
    @Basic(optional = false)
    @NotNull
    @Column(name = "banca_id")
    private int bancaId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "loteria_id")
    private int loteriaId;

    public BancaLoteriaPK() {
    }

    public BancaLoteriaPK(int bancaId, int loteriaId) {
        this.bancaId = bancaId;
        this.loteriaId = loteriaId;
    }

    public int getBancaId() {
        return bancaId;
    }

    public void setBancaId(int bancaId) {
        this.bancaId = bancaId;
    }

    public int getLoteriaId() {
        return loteriaId;
    }

    public void setLoteriaId(int loteriaId) {
        this.loteriaId = loteriaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) bancaId;
        hash += (int) loteriaId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BancaLoteriaPK)) {
            return false;
        }
        BancaLoteriaPK other = (BancaLoteriaPK) object;
        if (this.bancaId != other.bancaId) {
            return false;
        }
        if (this.loteriaId != other.loteriaId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sib.entity.BancaLoteriaPK[ bancaId=" + bancaId + ", loteriaId=" + loteriaId + " ]";
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return "BancaLoteriaPK";
    }
    
}
