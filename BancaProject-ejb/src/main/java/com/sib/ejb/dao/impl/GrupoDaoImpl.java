/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.ejb.dao.impl;

import com.sib.bs.responses.GrupoResponse;
import com.sib.bs.responses.MessageEnum;
import com.sib.ejb.dao.BancaDao;
import com.sib.ejb.dao.GrupoDao;
import com.sib.entity.Banca;
import com.sib.entity.Grupo;
import com.sib.util.ConnectionDB;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author julio
 */
@Stateless
public class GrupoDaoImpl implements GrupoDao {

    @PersistenceContext(unitName = "sibPU")
    private EntityManager em;
    private Query query;
    @EJB
    private BancaDao bdao;
    
    @Override
    public GrupoResponse crearGrupo(Grupo grupo) {
        GrupoResponse response=new GrupoResponse();
        
        try {
            
            boolean existe=validarExistenciaGrupo(grupo.getCodigo());
            if (existe) {
                response.setMsgEnum(MessageEnum.VALIDATION);               
            }else {
                grupo.setFechaCreacion(new Date());                
                em.persist(grupo);
                response.setGrupo(grupo);
                response.setMsgEnum(MessageEnum.SUCCESS);

            }
            
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        return response;
        
        
    }

    @Override
    public GrupoResponse getGrupos(String status) {
        GrupoResponse response=new GrupoResponse();
         List<Grupo> list=null;
         
         if (status==null || status.isEmpty()) {
         
             query=em.createNamedQuery("Grupo.findAll");
             try {
                 list=query.getResultList();
                 response.setGrupos(list);
                 response.setMsgEnum(MessageEnum.SUCCESS);
             } catch (Exception e) {
                 response.setMsgEnum(MessageEnum.FAIL);
                 e.printStackTrace();
             }
         }else {
              query=em.createNamedQuery("Grupo.findAll2");
              query.setParameter("status", status);       
        
            try {
                list=query.getResultList();
                response.setGrupos(list);
                response.setMsgEnum(MessageEnum.SUCCESS);
            } catch (Exception e) {
               response.setMsgEnum(MessageEnum.FAIL);
               e.printStackTrace();
            }
             
         }
       
        
        return response;
    }
    
    @Override
    public GrupoResponse getGrupos(String status,int start,int max) {
        GrupoResponse response=new GrupoResponse();
         List<Grupo> list=null;
         
         if (status==null || status.isEmpty()) {
         
             query=em.createNamedQuery("Grupo.findAll");
             query.setFirstResult(start);
             query.setMaxResults(max);
             try {
                 list=query.getResultList();
                 response.setGrupos(list);
                 response.setMsgEnum(MessageEnum.SUCCESS);
             } catch (Exception e) {
                 response.setMsgEnum(MessageEnum.FAIL);
                 e.printStackTrace();
             }
         }else {
              query=em.createNamedQuery("Grupo.findAll2");
              query.setParameter("status", status);
              query.setFirstResult(start);
              query.setMaxResults(max);
        
            try {
                list=query.getResultList();
                response.setGrupos(list);
                response.setMsgEnum(MessageEnum.SUCCESS);
            } catch (Exception e) {
               response.setMsgEnum(MessageEnum.FAIL);
               e.printStackTrace();
            }
             
         }
       
        
        return response;
    }

    @Override
    public GrupoResponse findGrupoByCodigo(String codigo) {
        GrupoResponse response=new GrupoResponse();
        
            query=em.createNamedQuery("Grupo.findByCodigo");
            query.setParameter("codigo", codigo);
            Grupo grupo=null;
            try {
               grupo=(Grupo)query.getSingleResult();
               response.setGrupo(grupo);
               response.setMsgEnum(MessageEnum.SUCCESS);
            } catch (Exception e) {
                response.setMsgEnum(MessageEnum.FAIL);
                e.printStackTrace();
            }

        
        return response;
    
    }

    @Override
    public GrupoResponse updateGrupo(Grupo grupo) {
        GrupoResponse response=new GrupoResponse();
        
        try {
            em.merge(grupo);
            response.setGrupo(grupo);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        return response;    
 
    }

    @Override
    public GrupoResponse getGrupoById(int id) {
        GrupoResponse response=new GrupoResponse();
        Grupo grupo=null;
        try {
            grupo=(Grupo) em.find(Grupo.class, id);
            response.setGrupo(grupo);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
       return response;
    
    }

    @Override
    public String getName() {
        return "GrupoDaoImpl";
    }

    @Override
    public boolean validarExistenciaGrupo(String codigo) {
        query=em.createNamedQuery("Grupo.findByCodigo");
        query.setParameter("codigo", codigo);
        boolean existe=false;
        try {
            Grupo g=(Grupo) query.getSingleResult();
            existe=(g!=null);
        } catch (Exception e) {
            existe=false;
            e.printStackTrace();
        }
        return existe;
    }

    @Override
    public GrupoResponse deleteGrupo(Grupo grupo) {
        GrupoResponse response=new GrupoResponse();
        boolean existe=bdao.validarExistenciaGrupoInBancas(grupo.getId());
        
        try {
            if (existe) {
            response.setMsgEnum(MessageEnum.VALIDATION);
            }else {            
                em.remove(em.merge(grupo));
                response.setMsgEnum(MessageEnum.SUCCESS);
            }
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        return response;
        
        
        
    }

  
    
}