/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.ejb.dao;

import com.sib.bs.responses.JuegoResponse;
import com.sib.entity.Juego;
import javax.ejb.Local;
import javax.ejb.Remote;

/**
 *
 * @author julio
 */
@Local
@Remote
public interface JuegoDao {
    
    public JuegoResponse crearJuego(Juego juego);
    public JuegoResponse editarJuego(Juego juego);
    public JuegoResponse deleteJuego(Juego juego);
    public JuegoResponse getJuegos(String status);
    public JuegoResponse getJuegoById(int id);
    public boolean validarExistenciaJuego(String codigo);
    public boolean validarExistenciaJuego(int id);
    public JuegoResponse getJuegosActivos();
    public JuegoResponse getJuegosNotRelatedInLJ();
    public JuegoResponse getJuegosNOtRelatedInLJByLoteria(int loteria);
    public JuegoResponse getJuegosByLoteriaNotRInBLJ(int loteria,int banca);
    
}
