/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.ejb.dao.impl;

import com.sib.bs.factory.EntityContext;
import com.sib.bs.responses.BancaLoteriaJuegoResponse;
import com.sib.bs.responses.JuegoBLJResponse;
import com.sib.bs.responses.JuegoResponse;
import com.sib.bs.responses.MessageEnum;
import com.sib.ejb.dao.BancaLoteriaJuegoDao;
import com.sib.ejb.dao.DetalleGeneralDao;
import com.sib.entity.BancaLoteriaJuego;
import com.sib.entity.BancaLoteriaJuegoPK;
import com.sib.entity.Juego;
import com.sib.entity.JuegoAndBLJ;
import com.sib.util.ConnectionDB;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author julio
 */
@Stateless
public class BancaLoteriaJuegoDaoImpl 
implements BancaLoteriaJuegoDao{
    
    @PersistenceContext(unitName = "sibPU")
    private EntityManager em;
    Query query;
    @EJB
    private DetalleGeneralDao dgdao;

    @Override
    public BancaLoteriaJuegoResponse crearBancaLoteriaJuego(int banca,int loteria,int juego,int porc) {
        BancaLoteriaJuegoResponse response=new BancaLoteriaJuegoResponse();
        
        try {
            
           
            BancaLoteriaJuegoPK pk=(BancaLoteriaJuegoPK) EntityContext.getEntity("BancaLoteriaJuegoPK");
            pk.setBancaId(banca);
            pk.setLoteriaId(loteria);
            pk.setJuegoId(juego);

            BancaLoteriaJuego bancaLoteriaJuego=(BancaLoteriaJuego) EntityContext.getEntity("BancaLoteriaJuego");
            bancaLoteriaJuego.setBancaLoteriaJuegoPK(pk);
            bancaLoteriaJuego.setFechaCreacion(new Date());
            bancaLoteriaJuego.setFechaActualizacion(new Date());
            bancaLoteriaJuego.setStatus("A");
            bancaLoteriaJuego.setPorcentajeCobro(porc);

            em.persist(bancaLoteriaJuego);
            response.setBancaLoteriaJuego(bancaLoteriaJuego);
            response.setMsgEnum(MessageEnum.SUCCESS);
        
        
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        return response;
       
    
    }

    @Override
    public BancaLoteriaJuegoResponse getBancaLoteriaJuegoById(int banca,int loteria,int juego) {
        BancaLoteriaJuegoResponse response=new BancaLoteriaJuegoResponse();
        
       
        BancaLoteriaJuegoPK pk=(BancaLoteriaJuegoPK) EntityContext.getEntity("BancaLoteriaJuegoPK");
        pk.setBancaId(banca);
        pk.setLoteriaId(loteria);
        pk.setJuegoId(juego);
        
        BancaLoteriaJuego blj=null;
        try {
            blj=(BancaLoteriaJuego) em.find(BancaLoteriaJuego.class, pk);
            response.setBancaLoteriaJuego(blj);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        return response;
    
    }

    @Override
    public String getName() {

    return "BancaLoteriaJuegoDaoImpl";
    }

    @Override
    public BancaLoteriaJuegoResponse editarBancaLoteriaJuego(BancaLoteriaJuego bancaLoteriaJuego) {
        BancaLoteriaJuegoResponse response=new BancaLoteriaJuegoResponse();
        
        try {
            em.merge(bancaLoteriaJuego);
            response.setBancaLoteriaJuego(bancaLoteriaJuego);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        return response;
              
            
       
    }

    @Override
    public BancaLoteriaJuegoResponse deleteBancaLoteriaJuego(BancaLoteriaJuego bancaLoteriaJuego) {
        BancaLoteriaJuegoResponse response=new BancaLoteriaJuegoResponse();
        
        try {
            boolean existe=dgdao.validarExistenciaDGByBancaLoteria(bancaLoteriaJuego.getBancaLoteriaJuegoPK().getBancaId(),
                    bancaLoteriaJuego.getBancaLoteriaJuegoPK().getLoteriaId());

            if (existe) {
                response.setMsgEnum(MessageEnum.VALIDATION);
            }else {            
                em.remove(em.merge(bancaLoteriaJuego)); 
                response.setMsgEnum(MessageEnum.SUCCESS);
            }
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        return response;
        
        
    
    }

    @Override
    public BancaLoteriaJuegoResponse aplicarPorcientoByGrupo(int id,int porciento) {
        BancaLoteriaJuegoResponse response=new BancaLoteriaJuegoResponse();

        query=em.createNativeQuery("SELECT bb.* FROM BANCA_has_LOTERIA_has_JUEGO bb "
                + "join BANCA b ON (bb.banca_id=b.id) "
                + "join GRUPO g ON (b.grupo_id=g.id) WHERE g.id=?", BancaLoteriaJuego.class);
        query.setParameter(1, id);
        List<BancaLoteriaJuego> list=null;
        
        
        try {            
            list=query.getResultList();
            System.out.println("list: "+ list.size());
            
            for(BancaLoteriaJuego b : list) {
                b.setPorcentajeCobro(porciento);
                b.setFechaActualizacion(new Date());
                em.merge(b);               
        
            }
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
           response.setMsgEnum(MessageEnum.FAIL);
           e.printStackTrace();
        }
        
        return response;
        
    }

    @Override
    public BancaLoteriaJuegoResponse aplicarPorcientoByBanca(int id, int porciento) {
        BancaLoteriaJuegoResponse response=new BancaLoteriaJuegoResponse();

        query=em.createNativeQuery("SELECT bb.* FROM BANCA_has_LOTERIA_has_JUEGO bb "
                + "join BANCA b ON (bb.banca_id=b.id) "
                + "WHERE b.id=?", BancaLoteriaJuego.class);
        query.setParameter(1, id);
        List<BancaLoteriaJuego> list=null;
        
        
        try {            
            list=query.getResultList();
            System.out.println("list: "+ list.size());
            
            for(BancaLoteriaJuego b : list) {
                b.setPorcentajeCobro(porciento);
                b.setFechaActualizacion(new Date());
                em.merge(b);
                System.out.println("PORCIENTO: "+ b.getPorcentajeCobro());
        
            }
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
           response.setMsgEnum(MessageEnum.FAIL);
           e.printStackTrace();
        }
        
        return response;
    
    }

    @Override
    public BancaLoteriaJuegoResponse aplicarPorcientoByJuego(int banca, int loteria, int juego, int porciento) {
        BancaLoteriaJuegoResponse response=new BancaLoteriaJuegoResponse();
        
         query=em.createNativeQuery("SELECT * FROM BANCA_has_LOTERIA_has_JUEGO WHERE banca_id=? and loteria_id=? and juego_id=?", BancaLoteriaJuego.class);
         query.setParameter(1, banca);
         query.setParameter(2, loteria);
         query.setParameter(3, juego);
         
         try {
           BancaLoteriaJuego b=(BancaLoteriaJuego) query.getSingleResult();
           b.setPorcentajeCobro(porciento);
           b.setFechaActualizacion(new Date());
           System.out.println("PORCIENTO: "+ b.getPorcentajeCobro());
           em.merge(b);
           response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
         
         return response;
        
    }

    @Override
    public JuegoResponse getJuegosInBLJ() {
        JuegoResponse response=new JuegoResponse();
        List<Juego> list=null;
        query=em.createNativeQuery("SELECT * FROM JUEGO j WHERE id IN (SELECT juego_id FROM BANCA_has_LOTERIA_has_JUEGO)", Juego.class);
        
        try {
            list=query.getResultList();
            response.setJuegos(list);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        return response;
    
    }

    @Override
    public JuegoResponse getJuegosByBancaAndLoteria(int banca, int loteria) {
        JuegoResponse response=new JuegoResponse();
        List<Juego> list=null;
        query=em.createNativeQuery("SELECT * FROM JUEGO j WHERE id IN (SELECT juego_id FROM BANCA_has_LOTERIA_has_JUEGO WHERE banca_id=? and loteria_id=?)", Juego.class);
        query.setParameter(1, banca);
        query.setParameter(2, loteria);
        
        try {
            list=query.getResultList();
            response.setJuegos(list);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        return response;
    
    }

    @Override
    public JuegoBLJResponse getJuegosAndPorcByBanAndJue(int banca, int loteria) {
        JuegoBLJResponse response=new JuegoBLJResponse();
        
        String sql="select j.id,j.codigo,j.nombre,b.porcentaje_cobro "
                + "from JUEGO j join BANCA_has_LOTERIA_has_JUEGO b "
                + "on(j.id=b.juego_id) WHERE banca_id=? and loteria_id=?";
        
        query=em.createNativeQuery(sql, "juegobljResults");
        query.setParameter(1, banca);
        query.setParameter(2, loteria);
        
        List<JuegoAndBLJ> list=null;        
        try {
            list=query.getResultList();
            System.out.println("list -->>>>>> "+ list);
            response.setJuegosAndBLJ(list);
            response.setMsgEnum(MessageEnum.SUCCESS);
           
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            System.out.println("Msg error:  "+ e);
        }
        
        return response;
        
    
    }
    
}
