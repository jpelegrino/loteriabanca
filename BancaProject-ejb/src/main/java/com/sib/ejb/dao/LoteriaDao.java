/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.ejb.dao;

import com.sib.bs.responses.LoteriaResponse;
import com.sib.entity.Loteria;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;

/**
 *
 * @author julio
 */
@Local
@Remote
public interface LoteriaDao extends ServiceDao{
    
    public LoteriaResponse crearLoteria(Loteria loteria);
    public LoteriaResponse editarLoteria(Loteria loteria);
    public LoteriaResponse getLoterias(String status);
    public LoteriaResponse getLoteriaByCodigo(String codigo);
    public LoteriaResponse getLoteriaById(int id);
    public boolean validarExistenciaLoteria(String codigo);
    public LoteriaResponse deleteLoteria(Loteria loteria);
    
}
