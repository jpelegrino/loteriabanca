/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.ejb.dao.impl;

import com.sib.ejb.dao.CorreoSibDao;
import java.util.Properties;
import javax.ejb.Stateless;
import javax.jms.MessageEOFException;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.log4j.Logger;

/**
 *
 * @author julio
 */
@Stateless
public class CorreoSibImpl implements CorreoSibDao{     
    public static Logger LOG=Logger.getLogger(CorreoSibImpl.class);
    

    @Override
    public void sendCorreo(String correo, String asunto, String msg) {
        final String username = "julio.torah@gmail.com";
        final String password = "jlpmibebe";
        
        // Gmail properties
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        
        
        LOG.info("Authenticating to Gmail");
    
        // Gmail - Authentication on the sender
        Session session=Session.getInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        
        LOG.info("Authentication Complete");
        
        // Gmail - Sending the message
        
        try {
            
            LOG.info("Sending the message");
            Message message=new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(correo));
            message.setSubject(asunto);
            message.setText(msg);
            
            Transport.send(message);
            LOG.info("Message Send!!");
        } catch (MessagingException e) {
        }
        
    }

    @Override
    public String getName() {
        return "CorreoSibImpl";
    }
    
}
