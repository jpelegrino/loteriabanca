/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.ejb.dao.impl;

import com.sib.bs.factory.EntityContext;
import com.sib.bs.responses.BancaLoteriaResponse;
import com.sib.bs.responses.LoteriaResponse;
import com.sib.bs.responses.MessageEnum;
import com.sib.ejb.dao.BancaLoteriaDao;
import com.sib.entity.BancaLoteria;
import com.sib.entity.BancaLoteriaPK;
import com.sib.entity.DetalleGeneral;
import com.sib.entity.Loteria;
import com.sib.util.ConnectionDB;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author julio
 */
@Stateless
public class BancaLoteriaDaoImpl implements BancaLoteriaDao {

    @PersistenceContext(unitName = "sibPU")
    private EntityManager em;
    private Query query;
    
    private static Logger LOG=Logger.getLogger(BancaLoteriaDaoImpl.class);
    
    @Override
    public BancaLoteriaResponse crearBancaLoteria(int banca,int loteria) {
        
        BancaLoteriaResponse response=new BancaLoteriaResponse();
        
        try {
            boolean existe=validarExistenciaBL(banca,loteria);
            
            if (existe) {
             response.setMsgEnum(MessageEnum.VALIDATION);
            System.out.println("IMPLEMENTACION DE MENSAJERIA");
                
            }else {
                BancaLoteriaPK pk=(BancaLoteriaPK) EntityContext.getEntity("BancaLoteriaPK");
                pk.setBancaId(banca);
                pk.setLoteriaId(loteria);

                BancaLoteria bancaLoteria=(BancaLoteria) EntityContext.getEntity("BancaLoteria");
                bancaLoteria.setBancaLoteriaPK(pk);
                bancaLoteria.setFechaCreacion(new Date());
                em.persist(bancaLoteria);
                response.setBancaLoteria(bancaLoteria);
                response.setMsgEnum(MessageEnum.SUCCESS);
               
        }
            
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        return response;   

    
    }

    @Override
    public BancaLoteriaResponse getBancaLoteriaById(int bancaId,int loteriaId) {
        BancaLoteriaResponse response=new BancaLoteriaResponse();
        
        query=em.createNamedQuery("BancaLoteria.findByBancaLoteria");
        query.setParameter("bancaId", bancaId);
        query.setParameter("loteriaId", loteriaId);
        
        BancaLoteria bl=null;
        
        try {
            bl=(BancaLoteria) query.getSingleResult();
            response.setBancaLoteria(bl);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
             e.printStackTrace();
        }
        
        return response;
    }

    @Override
    public boolean validarLoteriaInBanca(int id) {
        query=em.createNamedQuery("BancaLoteria.findByLoteriaId");
        query.setParameter("loteriaId", id);
        boolean existe=true;
        try {
            List<BancaLoteria> list=query.getResultList();
            existe=list.isEmpty();
        } catch (Exception e) {
            existe=true;
            e.printStackTrace();
        }
        
        return existe;
    
    }

    @Override
    public String getName() {
        return "BancaLoteriaDaoImpl";
        
    }

    @Override
    public LoteriaResponse getLoteriasByBanca(int banca) {
        LoteriaResponse response=new LoteriaResponse();
        List<Loteria> loterias=null;
        List<BancaLoteria> list=null;
        
        query=em.createNativeQuery("select l.* from LOTERIA l join BANCA_has_LOTERIA b on(l.id=b.loteria_id) WHERE b.banca_id=? and l.status='A'", Loteria.class);
        query.setParameter(1, banca);
                
        try {
            loterias=query.getResultList();
            response.setLoterias(loterias);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        return response;
    
        
    }

    @Override
    public LoteriaResponse getLoteriasNotInBanca(int banca) {
        LoteriaResponse response=new LoteriaResponse();
        List<Loteria> loterias=null;
        query=em.createNativeQuery("select * from LOTERIA WHERE id not in (select loteria_id FROM BANCA_has_LOTERIA WHERE banca_id=?) and status='A'", Loteria.class);
        query.setParameter(1, banca);
        
        try {
            loterias=query.getResultList();
            response.setLoterias(loterias);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        return response;
    
    }

    @Override
    public boolean validarExistenciaBL(int banca, int loteria) {

            query=em.createNamedQuery("BancaLoteria.findByBancaLoteria");
            query.setParameter("bancaId", banca);
            query.setParameter("loteriaId", loteria);
            BancaLoteria bl=null;
            boolean existe=false;
            
            try {
            bl=(BancaLoteria) query.getSingleResult();
                System.out.println("bl ["+bl+"]");
            existe=(bl!=null);
        } catch (Exception e) {
            existe=false;
            e.printStackTrace();
        }
            
        return existe;
    
    }

    @Override
    public BancaLoteriaResponse deleteBancaLoteria(BancaLoteria bancaLoteria) {
        BancaLoteriaResponse response=new BancaLoteriaResponse();
        
        try {
            BancaLoteriaPK pk=bancaLoteria.getBancaLoteriaPK();
            int banca=pk.getBancaId();
            int loteria=pk.getLoteriaId();
            
            boolean existe=validarExistenciaBLInDetalleGeneral(banca,loteria);
            
            if(existe) {
                response.setMsgEnum(MessageEnum.VALIDATION);
            }else {
            
            em.remove(em.merge(bancaLoteria));
            response.setMsgEnum(MessageEnum.SUCCESS);
            }
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        return response;
    
    }

    @Override
    public boolean validarExistenciaBLInDetalleGeneral(int banca, int loteria) {

        boolean existe=false;
        try {
            String sqlQuery="SELECT * FROM DETALLE_GENERAL"
                    + " WHERE banca_id=? AND loteria_id=? ";
            query=em.createNativeQuery(sqlQuery, DetalleGeneral.class);
            query.setParameter(1, banca);
            query.setParameter(2, loteria);
            
            LOG.info("sqlQuery --->>> "+ sqlQuery);
            
            DetalleGeneral dg=(DetalleGeneral) query.getSingleResult();
            LOG.info("Detalle General ::: "+ dg);
            
            if (dg!=null && !dg.isNull()) {
                existe=true;
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return existe;
    
    }
    
}
