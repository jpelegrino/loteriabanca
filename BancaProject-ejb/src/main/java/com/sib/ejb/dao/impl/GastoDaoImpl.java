package com.sib.ejb.dao.impl;

import com.sib.bs.factory.EntityContext;
import com.sib.bs.responses.GastoMontoTotalResponse;
import com.sib.bs.responses.GastoResponse;
import com.sib.bs.responses.MessageEnum;
import com.sib.ejb.dao.BancaDao;
import com.sib.ejb.dao.GastoDao;
import com.sib.ejb.dao.TipoGastoDao;
import com.sib.entity.Banca;
import com.sib.entity.Gasto;
import com.sib.entity.SumatoriaGastoCustomEntity;
import com.sib.entity.TipoGasto;
import com.sib.util.ManageUtils;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author julio
 */
@Stateless
public class GastoDaoImpl implements GastoDao {

    private static final Logger LOG = Logger.getLogger(GastoDaoImpl.class);

    @PersistenceContext(unitName = "sibPU")
    private EntityManager em;
    private Query query;
    @EJB
    private BancaDao bancaDao;
    @EJB
    private TipoGastoDao tipoGastoDao;

    @Override
    public String getName() {
        return "GastoDaoImpl";

    }

    @Override
    public GastoResponse crearGasto(int bancaId, int tipoGastoId, double monto, String fecha_efectiva) {
        GastoResponse response = new GastoResponse();
        LOG.info(" -- Registrando un gasto --");
        try {
            Gasto gasto = (Gasto) EntityContext.getEntity("Gasto");
            Banca banca = bancaDao.getBancaById(bancaId).getBanca();
            TipoGasto tipoGasto = tipoGastoDao.getTipoGastoById(tipoGastoId).getTipoGasto();

            Date fe = ManageUtils.convertToDate(fecha_efectiva);

            if (fecha_efectiva == null || fecha_efectiva.isEmpty()) {
                gasto.setFecha(new Date());
                LOG.info("-- El usuario no especifico ninguna fecha --");
            } else {
                gasto.setFecha(fe);
                LOG.info("-- El usuario especifico una fecha --");
            }

            gasto.setFecha_creacion(new Date());
            gasto.setBanca(banca);
            gasto.setTipoGasto(tipoGasto);
            gasto.setMonto(monto);
            gasto.setStatus("V");
            em.persist(gasto);
            response.setGasto(gasto);

            response.setMsgEnum(MessageEnum.SUCCESS);

        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);

            e.printStackTrace();
        }

        return response;

    }

    @Override
    public GastoResponse deleteGasto(Gasto gasto) {
        GastoResponse response = new GastoResponse();

        LOG.info("-- Eliminando un gasto --");
        try {
            em.remove(em.merge(gasto));
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        return response;

    }

    @Override
    public GastoResponse getGastos(String startDate, String endDate, int grupo, 
            int banca, long tipoGasto,String sortField, String orderType, 
            int start, int max) {
        GastoResponse response = new GastoResponse();
        LOG.info("-- Obteniendo los gastos segun los parametros dado en esta solicitud ---");
        try {
            String nquery = this.buildGetGastoQuery(startDate, endDate, grupo,
                    banca, tipoGasto, sortField, orderType);

            query = em.createNativeQuery(nquery, Gasto.class);

            query.setFirstResult(start);
            query.setMaxResults(max);

            List<Gasto> list = query.getResultList();
            response.setGastos(list);
            response.setCuenta(list.size());
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }

        return response;

    }

    @Override
    public GastoResponse getGastoById(int id) {
        Gasto gasto=null;
        GastoResponse response=new GastoResponse();
        try {
            gasto=(Gasto) em.find(Gasto.class, id);
            response.setGasto(gasto);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        return response;
    
    }    
    
    @Override
    public int getGastoTotalRecords(String startDate, String endDate, int grupo, 
            int banca, long tipoGasto, String sortField, String orderType) {
        int totalRecord = 0;
        try {
            String nquery = this.buildGetGastoQuery(startDate, endDate, grupo,
                    banca, tipoGasto, sortField, orderType);
           
            query = em.createNativeQuery(nquery, Gasto.class);

            List<Gasto> list = query.getResultList();
            totalRecord = list.size();

        } catch (Exception e) {
            LOG.error("EXCEPTION:" + e.getMessage(), e);
        }

        return totalRecord;

    }

    private String buildGetGastoQuery(String startDate, String endDate, int grupo,
            int banca, long tipoGasto, String sortField, String orderType) {
        boolean addANDStatement = false;
        StringBuilder sqlQuery = new StringBuilder();

        sqlQuery.append("select g.* from GASTO g");

        sqlQuery.append(" join BANCA b on (g.id_banca=b.id)");
        sqlQuery.append(" join GRUPO gr on (b.grupo_id=gr.id)");

        sqlQuery.append(" join TIPO_GASTO tg on (g.id_tipo_gasto=tg.id)");

        if (grupo > 0 || banca > 0 || tipoGasto > 0
                || (startDate != null && !startDate.isEmpty())) {
            sqlQuery.append(" WHERE");
        }

        if (grupo > 0) {
            sqlQuery.append(" gr.id=");
            sqlQuery.append(String.valueOf(grupo));
            addANDStatement = true;
        }

        if (tipoGasto > 0) {
            sqlQuery.append(addANDStatement ? " AND" : "");
            sqlQuery.append(" tg.id=");
            sqlQuery.append(String.valueOf(tipoGasto));
            addANDStatement = true;
        }        
        
        /*
        * -2 is a default value for supporting UI autocomplete filter.
        */
        if (banca > 0 || banca == -2) {
            sqlQuery.append(addANDStatement ? " AND" : "");
            sqlQuery.append(" b.id=");
            sqlQuery.append(String.valueOf(banca));
            addANDStatement = true;
        }

        if (startDate != null && !startDate.isEmpty()
                && endDate != null && !endDate.isEmpty()) {
            sqlQuery.append(addANDStatement ? " AND" : "");
            sqlQuery.append(" g.fecha_efectiva between '");
            sqlQuery.append(startDate);
            sqlQuery.append("' and '");
            sqlQuery.append(endDate);
            sqlQuery.append("'");
        } else if (startDate != null && !startDate.isEmpty()) {
            sqlQuery.append(addANDStatement ? " AND" : "");
            sqlQuery.append(" fecha_efectiva= '");
            sqlQuery.append(startDate);
            sqlQuery.append("'");
        }

        if ((sortField == null || sortField.isEmpty())
                || (orderType == null || orderType.isEmpty())) {
            sqlQuery.append("  ORDER BY id DESC");
        } else {
            sqlQuery.append("  ORDER BY");
            sqlQuery.append(" ");
            sqlQuery.append(sortField);
            sqlQuery.append(" ");
            sqlQuery.append(orderType);
        }

        return sqlQuery.toString();
    }

    @Override
    public GastoMontoTotalResponse getTotalMontoGastos(String startDate, String endDate,
            int grupo, int banca,
            long tipoGasto, String sortField, String orderType) {
        
        GastoMontoTotalResponse response=new GastoMontoTotalResponse();
        String sql=buildTotalMontoGastos(startDate,endDate,grupo,banca,tipoGasto,sortField,orderType);
        
        try {
            query=em.createNativeQuery(sql, "gastoTotal");
            SumatoriaGastoCustomEntity sumatoria=(SumatoriaGastoCustomEntity) query.getSingleResult();
            response.setSumatoriaGasto(sumatoria);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        return response;
    
    }
    
    
    private String buildTotalMontoGastos(String startDate, String endDate,
            int grupo, int banca,
            long tipoGasto, String sortField, String orderType) {
    
        boolean addANDStatement = false;
        StringBuilder sqlQuery = new StringBuilder();

        sqlQuery.append("select g.id,SUM(g.monto) as monto_total from GASTO g join BANCA b ");

        sqlQuery.append(" on (g.id_banca=b.id) join GRUPO gr ");
        sqlQuery.append(" on (b.grupo_id=gr.id) join TIPO_GASTO tg on (g.id_tipo_gasto=tg.id) ");
       

        if (grupo > 0 || banca > 0 || tipoGasto > 0
                || (startDate != null && !startDate.isEmpty())) {
            sqlQuery.append(" WHERE");
        }

        if (grupo > 0) {
            sqlQuery.append(" gr.id=");
            sqlQuery.append(String.valueOf(grupo));
            addANDStatement = true;
        }

        if (tipoGasto > 0) {
            sqlQuery.append(addANDStatement ? " AND" : "");
            sqlQuery.append(" tg.id=");
            sqlQuery.append(String.valueOf(tipoGasto));
            addANDStatement = true;
        }        
        
        /*
        * -2 is a default value for supporting UI autocomplete filter.
        */
        if (banca > 0 || banca == -2) {
            sqlQuery.append(addANDStatement ? " AND" : "");
            sqlQuery.append(" b.id=");
            sqlQuery.append(String.valueOf(banca));
            addANDStatement = true;
        }

        if (startDate != null && !startDate.isEmpty()
                && endDate != null && !endDate.isEmpty()) {
            sqlQuery.append(addANDStatement ? " AND" : "");
            sqlQuery.append(" g.fecha_efectiva between '");
            sqlQuery.append(startDate);
            sqlQuery.append("' and '");
            sqlQuery.append(endDate);
            sqlQuery.append("'");
        } else if (startDate != null && !startDate.isEmpty()) {
            sqlQuery.append(addANDStatement ? " AND" : "");
            sqlQuery.append(" fecha_efectiva= '");
            sqlQuery.append(startDate);
            sqlQuery.append("'");
        }

        if ((sortField == null || sortField.isEmpty())
                || (orderType == null || orderType.isEmpty())) {
            sqlQuery.append("  ORDER BY g.id DESC");
        } else {
            sqlQuery.append("  ORDER BY");
            sqlQuery.append(" ");
            sqlQuery.append(sortField);
            sqlQuery.append(" ");
            sqlQuery.append(orderType);
        }

        return sqlQuery.toString();
    
    }

}
