/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.ejb.dao;

import com.sib.bs.responses.DetalleGeneralResponse;
import com.sib.bs.responses.GananciaPerdidaResponse;
import com.sib.bs.responses.PorcentajeResponse;
import com.sib.bs.responses.TransaccionMontoTotalResponse;
import com.sib.entity.DetalleGeneral;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;

/**
 *
 * @author julio
 */
@Local
@Remote
public interface DetalleGeneralDao extends ServiceDao{
    
    public DetalleGeneralResponse crearDetalleGeneral(DetalleGeneral detalleGeneral);
    public DetalleGeneralResponse getDetalleGenerals();
    public DetalleGeneralResponse getDetalleGeneralById(Long id);
    public DetalleGeneralResponse editarDetalleGeneral(DetalleGeneral detalleGeneral);
    public boolean validarExistenciaDGByBancaLoteria(int banca,int loteria);
    public boolean validarExistenciaBancaInDG(int banca);
    public DetalleGeneralResponse getDetallesGeneralesListByGeneralIDs(List<Long> generalIDs);
    public DetalleGeneralResponse getDetalleGeneralListByGrupo(int grupo);
    public DetalleGeneralResponse getDetalleGeneralListByGrupo(int grupo,int banca,String startDate,String endDate,
                         String sortField,String orderType,int start,int max);
    
    public GananciaPerdidaResponse getGananciasAndPerdidasByGrupo(int grupo,String fecha1,String fecha2 );
    public GananciaPerdidaResponse getGananciasAndPerdidasByGrupo(int grupo,String startDate,
            String endDate,String sortField,String orderType );
    
    public GananciaPerdidaResponse getGananciasAndPerdidasByGrupo(int grupo);
    public PorcentajeResponse getMontoPorcentajeByGrupo(int grupo);
    public PorcentajeResponse getMontoPorcentajeByGrupo(int grupo,String startDate,String endDate,
            String sortField,String orderType);
    public int getTransactionRecords(int grupo,int banca,String startDate,String endDate);
    public TransaccionMontoTotalResponse getSumatoriaTransaccion(int grupo,int banca,String startDate,String endDate); 
    
}