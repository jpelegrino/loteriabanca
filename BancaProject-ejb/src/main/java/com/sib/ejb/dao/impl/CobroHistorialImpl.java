package com.sib.ejb.dao.impl;

import com.sib.bs.responses.CobroHistorialResponse;
import com.sib.bs.responses.MessageEnum;
import com.sib.ejb.dao.CobroHistorialDao;
import com.sib.entity.CobroHistorial;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author Snailin Inoa
 */
@Stateless
public class CobroHistorialImpl implements CobroHistorialDao {

    @PersistenceContext(unitName = "sibPU")
    private EntityManager em;
    private Query query;
    private static final Logger LOGGER = Logger.getLogger(CobroHistorialImpl.class);

    @Override
    public CobroHistorialResponse crearCobroHistorial(CobroHistorial cobroHistorial) {
        LOGGER.info("crearCobro :: START");
        CobroHistorialResponse cobroHistorialResponse = new CobroHistorialResponse();

        try {
            em.persist(cobroHistorial);
            cobroHistorialResponse.setCobroHistorial(cobroHistorial);
            cobroHistorialResponse.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            cobroHistorialResponse.setMsgEnum(MessageEnum.FAIL);
            LOGGER.error("ERROR  creando una cobro historial  :: " + e);
        }
        LOGGER.info("crearCobro :: END");
        return cobroHistorialResponse;
    }

    @Override
    public CobroHistorialResponse updateCobroHistorial(CobroHistorial cobroHistorial) {
        LOGGER.info("updateCobroHistorial() :: START");
        CobroHistorialResponse cobroHistorialResponse = new CobroHistorialResponse();
        try {

            em.merge(cobroHistorial);
            cobroHistorialResponse.setCobroHistorial(cobroHistorial);
        } catch (Exception e) {
            cobroHistorialResponse.setMsgEnum(MessageEnum.FAIL);
            LOGGER.error("ERROR  actualizando una cobro historial  :: " + e);
        }
        LOGGER.info("updateCobroHistorial() :: END");
        return cobroHistorialResponse;
    }

    @Override
    public CobroHistorialResponse getCobrosHistorial(String status) {
        LOGGER.info("getCobrosHistorial() :: START");
        LOGGER.info("*************** INPUT **************");
        LOGGER.info("status ::" + status);
        CobroHistorialResponse cobroHistorialResponse = new CobroHistorialResponse();
        try {
            String sql = "select * from cobro_historial";
            if (status != null && !status.isEmpty()) {
                sql = sql + "  where status_cobro_historial=" + "'" + status + "'";
            }
            query = em.createNativeQuery(sql, CobroHistorial.class);
            List<CobroHistorial> list = query.getResultList();
            cobroHistorialResponse.setCobrosHistorialList(list);
            cobroHistorialResponse.setMsgEnum(MessageEnum.SUCCESS);

        } catch (Exception e) {
            cobroHistorialResponse.setMsgEnum(MessageEnum.FAIL);
            LOGGER.info("Error buscando una lista de COBRO HISTORIAL :: " + e);
        }
        LOGGER.info("getCobrosHistorial() :: END");

        return cobroHistorialResponse;

    }

    @Override
    public CobroHistorialResponse getCobroHistorialById(int cobroHistorialId) {
        LOGGER.info("getCobroHistorialById() :: START");
        LOGGER.info("*************** INPUT **************");
        LOGGER.info("cobroHistorialId ::" + cobroHistorialId);
        CobroHistorialResponse cobroHistorialResponse = new CobroHistorialResponse();
        CobroHistorial cobroHistorial = null;
        try {
            cobroHistorial = (CobroHistorial) em.find(CobroHistorial.class, cobroHistorialId);
            cobroHistorialResponse.setCobroHistorial(cobroHistorial);
            cobroHistorialResponse.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            cobroHistorialResponse.setMsgEnum(MessageEnum.FAIL);
            LOGGER.info("Error buscando un cobro historial a partir de un ID() :: " + e);
        }
        LOGGER.info("getCobroHistorialById() :: END");
        return cobroHistorialResponse;
    }

    @Override
    public CobroHistorialResponse getCobroHistorialListByCobroID(Long cobroId, String status) {
        LOGGER.info("getCobroHistorialListByCobroID() :: START");
        LOGGER.info("*************** INPUT **************");
        LOGGER.info("cobroId ::" + cobroId);
        LOGGER.info("status ::" + status);
        CobroHistorialResponse cobroHistorialResponse = new CobroHistorialResponse();
        List<CobroHistorial> cobroHistorialList = null;
        try {
            boolean addStatusToQuery = false;
            String sql = "SELECT * FROM COBRO_HISTORIAL WHERE COBRO_ID=?1 ";

            if (status != null && !status.isEmpty()) {
                sql += "AND STATUS_COBRO_HISTORIAL=?2";
                addStatusToQuery = true;

            }

            LOGGER.info("SQL :: " + sql);
            query = em.createNativeQuery(sql, CobroHistorial.class);

            query.setParameter(1, cobroId);
            if (addStatusToQuery) {
                query.setParameter(2, status);
            }
            cobroHistorialList = (List<CobroHistorial>) query.getResultList();
            cobroHistorialResponse.setCobrosHistorialList(cobroHistorialList);
            cobroHistorialResponse.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            cobroHistorialResponse.setMsgEnum(MessageEnum.FAIL);
            LOGGER.info("Error buscando todos los cobros historiales a partir del cobro id y el status :: " + e);
        }
        LOGGER.info("getCobroHistorialListByCobroID() :: END");
        return cobroHistorialResponse;
    }

    @Override
    public String getName() {
        return "CobroHistorialImpl";
    }

    @Override
    public CobroHistorialResponse getCobroHistorialListByDetalleGeneralId(Long detalleGeneralId, String status) {
        LOGGER.info("getCobroHistorialListByDetalleGeneralId() :: START");
        LOGGER.info("*************** INPUT **************");
        LOGGER.info("detalleGeneralId ::" + detalleGeneralId);
        LOGGER.info("status ::" + status);

        CobroHistorialResponse cobroHistorialResponse = new CobroHistorialResponse();
        List<CobroHistorial> cobroHistorialList=null;
        boolean addStatusToQuery = false;
        try {
            String sql = "SELECT * FROM COBRO_HISTORIAL WHERE DETALLE_GENERAL_ID =?1 ";
            if (status != null && !status.isEmpty()) {
                sql += " AND STATUS_COBRO_HISTORIAL=?2";
                addStatusToQuery = true;
            }
            LOGGER.info("SQL :: " + sql);
            query = em.createNativeQuery(sql, CobroHistorial.class);

            query.setParameter(1, detalleGeneralId);
            if (addStatusToQuery) {
                query.setParameter(2, status);
            }
            cobroHistorialList = (List<CobroHistorial>) query.getResultList();
            cobroHistorialResponse.setCobrosHistorialList(cobroHistorialList);
            cobroHistorialResponse.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            cobroHistorialResponse.setMsgEnum(MessageEnum.FAIL);
            LOGGER.info("Error buscando todos los cobros historiales a partir del DETALLE_GENERAL_ID y el STATUS :: " + e);
        }
        LOGGER.info("getCobroHistorialListByDetalleGeneralId() :: END");
        return cobroHistorialResponse;

    }

}
