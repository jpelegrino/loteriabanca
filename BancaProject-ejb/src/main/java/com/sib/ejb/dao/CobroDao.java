package com.sib.ejb.dao;

import com.sib.bs.responses.CobroCustomEntityResponse;
import com.sib.bs.responses.CobroResponse;
import com.sib.bs.responses.CobroVistaDetalleResponse;
import com.sib.entity.Cobro;
import javax.ejb.Local;

/**
 *
 * @author Snailin Inoa
 */
@Local
public interface CobroDao extends ServiceDao {

    public CobroResponse crearCobro(Cobro cobro);

    public CobroResponse updateCobro(Cobro cobro);

    public CobroResponse getCobros(String status);

    public CobroResponse getCobroById(Long cobroId);

    public CobroVistaDetalleResponse getCobroVistaDetalleList(Long cobroId, String status);

    public CobroCustomEntityResponse getCobroCustomEntityLazyList(String startDate, String endDate, Integer grupoId,
            Integer bancaId, String sortField, String orderType, int start, int max, String status);

    public int getCobroCustomEntityTotalRecords(String startDate, String endDate, Integer grupoId,
            Integer bancaId, String sortField, String orderType, String status);

}
