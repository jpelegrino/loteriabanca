/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.ejb.dao;

import java.io.InputStream;
import javax.ejb.Local;

/**
 *
 * @author julio
 */
@Local
public interface PlanoDao extends ServiceDao {
    public void send(InputStream is);
}