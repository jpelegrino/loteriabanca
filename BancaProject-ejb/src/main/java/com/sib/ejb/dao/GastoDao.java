/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.ejb.dao;

import com.sib.bs.responses.GastoMontoTotalResponse;
import com.sib.bs.responses.GastoResponse;
import com.sib.entity.Gasto;
import javax.ejb.Local;
import javax.ejb.Remote;

/**
 *
 * @author julio
 */
@Local
@Remote
public interface GastoDao extends ServiceDao {
    
    public GastoResponse crearGasto(int bancaId, int tipoGastoId,double monto,String fecha_efectiva);
    public GastoResponse deleteGasto(Gasto gasto);
    public GastoResponse getGastoById(int id);
    public GastoResponse getGastos(String startDate,String endDate,int grupo,
            int banca, long tipoGasto, String sortField,String orderType,
            int start,int max);
    public int getGastoTotalRecords(String startDate,String endDate,int grupo,
            int banca, long tipoGasto, String sortField,String orderType);
    public GastoMontoTotalResponse getTotalMontoGastos(String startDate,String endDate,int grupo,
            int banca, long tipoGasto, String sortField,String orderType);
    
}
