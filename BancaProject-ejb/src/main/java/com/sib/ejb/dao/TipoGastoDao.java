/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.ejb.dao;

import com.sib.bs.responses.TipoGastoResponse;
import com.sib.entity.TipoGasto;
import javax.ejb.Local;
import javax.ejb.Remote;

/**
 *
 * @author julio
 */
@Local
@Remote
public interface TipoGastoDao extends ServiceDao {
    
    public TipoGastoResponse crearTipoGasto(TipoGasto tipoGasto);
    public TipoGastoResponse getTipoGastos();
    public TipoGastoResponse getTipoGastoById(long id);
    public TipoGastoResponse deleteTipoGasto(TipoGasto tipoGasto);
    public boolean verificarExistenciaInGasto(int tipoGasto);
    
}
