package com.sib.ejb.dao.impl;

import com.sib.bs.factory.EntityContext;
import com.sib.bs.responses.BancaResponse;
import com.sib.bs.responses.JuegoResponse;
import com.sib.bs.responses.LoteriaResponse;
import com.sib.bs.responses.MessageEnum;
import com.sib.ejb.dao.BancaDao;
import com.sib.ejb.dao.BancaLoteriaDao;
import com.sib.ejb.dao.BancaLoteriaJuegoDao;
import com.sib.ejb.dao.DetalleGeneralDao;
import com.sib.ejb.dao.JuegoDao;
import com.sib.ejb.dao.LoteriaJuegoDao;
import com.sib.entity.Banca;
import com.sib.entity.BancaLoteriaJuego;
import com.sib.entity.BancaLoteriaJuegoPK;
import com.sib.entity.Juego;
import com.sib.entity.Loteria;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author julio
 */
@Stateless
public class BancaDaoImpl implements BancaDao {
    private static Logger LOG=Logger.getLogger(BancaDaoImpl.class);

    @PersistenceContext(unitName = "sibPU")
    private EntityManager em;
    private Query query;
    @EJB
    private DetalleGeneralDao dgdao;
    @EJB
    private BancaLoteriaDao bancaLoteriaDao;
    @EJB
    private JuegoDao juegoDao;
    @EJB
    private BancaLoteriaJuegoDao bancaLoteriaJuegoDao;
    @EJB
    private LoteriaJuegoDao loteriaJuegoDao;

    @Override
    public BancaResponse crearBanca(Banca banca) {
        BancaResponse response = new BancaResponse();
        
        /*
        Este es el codigo que debe ir, el codigo de abajo se cambiara en un futuro
         banca.setFechaCreacion(new Date());                
                banca.setStatus("A");
                em.persist(banca);
                em.flush();
                response.setBanca(banca);
                response.setMsgEnum(MessageEnum.SUCCESS);
        */
        

        try {
            boolean validar = validarGrupoBanca(banca.getGrupoId().getId(), banca.getCodigo());
            if (validar) {
                response.setBanca(banca);
                response.setMsgEnum(MessageEnum.VALIDATION);
                System.out.println("SE IMPLEMTARA MENSAJERIA");
            } else {
                banca.setFechaCreacion(new Date());                
                banca.setStatus("A");
                em.persist(banca);
                em.flush();
                response.setBanca(banca);
                response.setMsgEnum(MessageEnum.SUCCESS);
                
                int bancaId=banca.getId();
                
                LoteriaResponse lout=bancaLoteriaDao.getLoteriasNotInBanca(bancaId);
                
                if (lout!=null && lout.getMsgEnum().getCode()==1) {
                    for (Loteria l : lout.getLoterias()) {
                        bancaLoteriaDao.crearBancaLoteria(bancaId, l.getId());
                        JuegoResponse jout=loteriaJuegoDao.getJuegosByLoteriaId(l.getId(),"A");
                        List<Juego> juegoList=jout.getJuegos();
                        for (Juego j : juegoList) {                           
                            
                            bancaLoteriaJuegoDao.crearBancaLoteriaJuego(bancaId, l.getId(), j.getId(), 20);
                            
                        }
                    }
                } 
                

            }
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }

        return response;
    }

    @Override
    public BancaResponse getBancas(String status) {
        BancaResponse response = new BancaResponse();
        List<Banca> list = null;

        if (status == null || status.isEmpty()) {
            query = em.createNamedQuery("Banca.findAll");

            try {
                list = query.getResultList();
                response.setBancas(list);
                response.setMsgEnum(MessageEnum.SUCCESS);
            } catch (Exception e) {
                response.setMsgEnum(MessageEnum.FAIL);
                e.printStackTrace();
            }

        } else {
            query = em.createNamedQuery("Banca.findAll2");
            query.setParameter("status", status);

            try {
                list = query.getResultList();
                response.setBancas(list);
                response.setMsgEnum(MessageEnum.SUCCESS);
            } catch (Exception e) {
                response.setMsgEnum(MessageEnum.FAIL);
                e.printStackTrace();
            }
        }

        return response;

    }

    @Override
    public BancaResponse updateBanca(Banca banca) {
        BancaResponse response = new BancaResponse();

        try {
            banca.setFechaActualizacion(new Date());
            banca.setFechaCreacion(new Date());
            em.merge(banca);
            response.setBanca(banca);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }

        return response;

    }

    @Override
    public boolean validarGrupoBanca(int grupo, String banca) {
        query = em.createNamedQuery("Banca.findByGrupoBanca");
        query.setParameter("grupoId", grupo);
        query.setParameter("codigo", banca);
        Banca ban = null;
        boolean existe=false;
        try {
            ban = (Banca) query.getSingleResult();
            existe=(ban != null);
        } catch (Exception e) {
            existe=false;
            e.printStackTrace();
        }

        return existe;

    }

    @Override
    public String getName() {
        return "BancaDaoImpl";

    }

    @Override
    public BancaResponse getBancaByCodigoAndGrupoId(String codigo, int grupoId) {
        BancaResponse response = new BancaResponse();
        query = em.createNamedQuery("Banca.findByGrupoBanca");
        query.setParameter("grupoId", grupoId);
        query.setParameter("codigo", codigo);
        Banca banca = null;
        try {
            banca = (Banca) query.getSingleResult();
            response.setBanca(banca);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }

        return response;

    }

    @Override
    public BancaResponse getBancaById(int id) {
        BancaResponse response = new BancaResponse();
        Banca banca = null;
        try {
            banca = (Banca) em.find(Banca.class, id);
            response.setBanca(banca);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        return response;

    }

    @Override
    public boolean validarExistenciaGrupoInBancas(int grupo) {

        query = em.createNamedQuery("Banca.findByGrupo");
        query.setParameter("grupoId", grupo);

        boolean existe = false;
        try {
            List<Banca> list = query.getResultList();
            existe = list.size() > 0;
        } catch (Exception e) {
            existe=false;
            e.printStackTrace();
        }

        return existe;

    }

    @Override
    public BancaResponse deleteBanca(Banca banca) {
        BancaResponse response = new BancaResponse();

        try {
            boolean existe = dgdao.validarExistenciaBancaInDG(banca.getId());
            if (existe) {
                response.setMsgEnum(MessageEnum.VALIDATION);
            } else {
                em.remove(em.merge(banca));
                response.setMsgEnum(MessageEnum.SUCCESS);
            }

        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }

        return response;

    }

    @Override
    public BancaResponse getBancaByGrupoId(int grupoId) {
        BancaResponse response = new BancaResponse();
        List<Banca> list = null;

        query = em.createNamedQuery("Banca.findByGrupo");
        query.setParameter("grupoId", grupoId);

        try {
            list = query.getResultList();
            response.setBancas(list);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }

        return response;
    }

    @Override
    public BancaResponse getAllBancas() {
        BancaResponse response=new BancaResponse();
        query=em.createNamedQuery("Banca.findAll");
        List<Banca> list=null;
        
        try {
            list=query.getResultList();
            response.setBancas(list);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
        }
        
        return response;
    
    }

    @Override
    public BancaResponse getBancaByGrupoId(int grupoId, int start, int max) {
         BancaResponse response = new BancaResponse();
        List<Banca> list = null;

        query = em.createNamedQuery("Banca.findByGrupo");
        query.setParameter("grupoId", grupoId);
        query.setFirstResult(start);
        query.setMaxResults(max);

        try {
            list = query.getResultList();
            response.setBancas(list);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }

        return response;
    
    }

}