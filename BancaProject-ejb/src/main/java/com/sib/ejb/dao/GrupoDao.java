/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.ejb.dao;


import com.sib.bs.responses.GrupoResponse;
import com.sib.entity.Grupo;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;

/**
 *
 * @author julio
 */
@Local
public interface GrupoDao extends ServiceDao{
    
    public GrupoResponse crearGrupo(Grupo grupo);
    public GrupoResponse getGrupos(String status);
    public GrupoResponse getGrupos(String status,int start,int max);
    public GrupoResponse findGrupoByCodigo(String codigo);
    public GrupoResponse updateGrupo(Grupo grupo);
    public GrupoResponse getGrupoById(int id);
    public boolean validarExistenciaGrupo(String codigo);
    public GrupoResponse deleteGrupo(Grupo grupo);
    
}