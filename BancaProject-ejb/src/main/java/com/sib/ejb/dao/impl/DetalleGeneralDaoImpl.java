/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.ejb.dao.impl;

import com.sib.bs.responses.DetalleGeneralResponse;
import com.sib.bs.responses.GananciaPerdidaResponse;
import com.sib.bs.responses.MessageEnum;
import com.sib.bs.responses.PorcentajeResponse;
import com.sib.bs.responses.TransaccionMontoTotalResponse;
import com.sib.ejb.dao.DetalleGeneralDao;
import com.sib.entity.Banca;
import com.sib.entity.DetalleGeneral;
import com.sib.entity.GananciaPerdida;
import com.sib.entity.Gasto;
import com.sib.entity.Porcentajes;
import com.sib.entity.SumatoriaTransaccionCustomEntity;
import com.sib.util.ConnectionDB;
import com.sib.util.ManageUtils;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author julio
 */
@Stateless
public class DetalleGeneralDaoImpl
        implements DetalleGeneralDao {
    
    private static Logger LOG=Logger.getLogger(DetalleGeneralDaoImpl.class);

    @PersistenceContext(unitName = "sibPU")
    private EntityManager em;
    private Query query;

    @Override
    public DetalleGeneralResponse crearDetalleGeneral(DetalleGeneral detalleGeneral) {
        DetalleGeneralResponse response = new DetalleGeneralResponse();

        try {
            em.merge(detalleGeneral);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }

        return response;

    }

    @Override
    public DetalleGeneralResponse getDetalleGenerals() {
        DetalleGeneralResponse response = new DetalleGeneralResponse();

        query = em.createNamedQuery("DetalleGeneral.findAll");
        List<DetalleGeneral> list = null;
        try {
            list = query.getResultList();
            response.setDetalleGenerals(list);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }

        return response;

    }

    @Override
    public DetalleGeneralResponse getDetalleGeneralById(Long id) {
        DetalleGeneralResponse response = new DetalleGeneralResponse();
        DetalleGeneral dg = null;
        try {
            dg = (DetalleGeneral) em.find(DetalleGeneral.class, id);
            response.setDetalleGeneral(dg);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }

        return response;

    }

    @Override
    public String getName() {

        return "DetalleGeneralDaoImpl";
    }

    @Override
    public boolean validarExistenciaDGByBancaLoteria(int banca, int loteria) {

        query = em.createNamedQuery("DetalleGeneral.validarExistenciabyBancaLoteria");
        query.setParameter("bancaId", banca);
        query.setParameter("loteriaId", loteria);

        boolean existe = false;
        try {
            DetalleGeneral dg = (DetalleGeneral) query.getSingleResult();
            existe = (dg != null);
        } catch (Exception e) {
            existe = false;
            e.printStackTrace();
        }

        return existe;

    }

    @Override
    public boolean validarExistenciaBancaInDG(int banca) {
        query = em.createNamedQuery("DetalleGeneral.validarExistenciaBanca");
        query.setParameter("bancaId", banca);
        boolean existe = false;

        try {
            List<DetalleGeneral> list = query.getResultList();
            existe = (list.size() > 0);
        } catch (Exception e) {
            existe = false;
            e.printStackTrace();
        }

        return existe;

    }

    @Override
    public DetalleGeneralResponse editarDetalleGeneral(DetalleGeneral detalleGeneral) {
        DetalleGeneralResponse response = new DetalleGeneralResponse();

        try {
            em.merge(detalleGeneral);
            response.setDetalleGeneral(detalleGeneral);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }

        return response;

    }

    @Override
    public DetalleGeneralResponse getDetallesGeneralesListByGeneralIDs(List<Long> generalIDs) {
        DetalleGeneralResponse response = new DetalleGeneralResponse();

        try {
            if (generalIDs != null && !generalIDs.isEmpty()) {
                String sql = "SELECT * FROM DETALLE_GENERAL WHERE GENERAL_ID IN (";
                for (int i = 0; i < generalIDs.size(); i++) {

                    if ((i + 1) != generalIDs.size()) {
                        sql = sql + generalIDs.get(i) + ",";
                    } else {
                        sql = sql + generalIDs.get(i) + ")";
                    }

                }
                System.out.println("**********");
                System.out.println("SQL --->" + sql);
                System.out.println("**********");
                query = em.createNativeQuery(sql, DetalleGeneral.class);
                List<DetalleGeneral> list = query.getResultList();
                response.setDetalleGenerals(list);
                response.setMsgEnum(MessageEnum.SUCCESS);
            }
        } catch (Exception e) {
            System.out.println("Exception e -->" + e);
            response.setMsgEnum(MessageEnum.FAIL);
        }

        return response;
    }

   @Deprecated
    public DetalleGeneralResponse getDetalleGeneralListByGrupo(int grupo) {

        DetalleGeneralResponse response=new DetalleGeneralResponse();
        
        String sql="select d.* "
                + "from DETALLE_GENERAL d join BAN_GENERAL b"
                + " on(d.general_id=b.id)"
                + " where b.grupo_id=?";
        
        LOG.info("SQL QUERY -->> "+ sql);
        query=em.createNativeQuery(sql, DetalleGeneral.class);
        query.setParameter(1, grupo);
        List<DetalleGeneral> list=null;       
        
        try {
            list=query.getResultList();
            LOG.info("QUERY Result-->> "+ list);
            response.setDetalleGenerals(list);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            LOG.error("DETALLE_GENERAL error ", e);
        }
        
        return response;
    
    }
 
    @Override
    public DetalleGeneralResponse getDetalleGeneralListByGrupo(int grupo,int banca, String startDate,
            String endDate,String sortField,String orderType, int start, int max) {

         DetalleGeneralResponse response=new DetalleGeneralResponse();         
        String sql=buildGetTransactionsQuery(grupo,banca, startDate, endDate,sortField,orderType);        

        LOG.info("SQL QUERY -->> "+ sql);
       
        query=em.createNativeQuery(sql, DetalleGeneral.class);        
        query.setFirstResult(start);
        query.setMaxResults(max);
        
        List<DetalleGeneral> list=null;       
        
        try {
            list=query.getResultList();
            LOG.info("QUERY Result-->> "+ list);
            response.setDetalleGenerals(list);
            response.setCounter(list.size());
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            LOG.error("DETALLE_GENERAL error ", e);
        }
        
        return response;
    
    }
    
    
    private String buildGetTransactionsQuery(int grupo,int banca,
            String startDate, String endDate,String sortField,String orderType) {
        
    StringBuilder sqlQuery=new StringBuilder();
    boolean addANDStatement = false;
    
    sqlQuery.append("SELECT * FROM DETALLE_GENERAL d ");
    sqlQuery.append("join BAN_GENERAL b ");
    sqlQuery.append("on(d.general_id=b.id) ");
    
    
    if (grupo>0 || banca>0 
            || (startDate!=null && !startDate.isEmpty())) {
        sqlQuery.append("WHERE ");
        
    }
    
     if (grupo > 0) {
            sqlQuery.append(" b.grupo_id=");
            sqlQuery.append(String.valueOf(grupo));
            addANDStatement = true;
        }
     
     if (banca > 0) {
            sqlQuery.append(addANDStatement ? " AND" : "");
            sqlQuery.append(" d.banca_id=");
            sqlQuery.append(String.valueOf(banca));
            addANDStatement = true;
        }
     
     
     if (startDate != null && !startDate.isEmpty()
                && endDate != null && !endDate.isEmpty()) {
            sqlQuery.append(addANDStatement ? " AND" : "");
            sqlQuery.append(" d.fecha_efectiva between '");
            sqlQuery.append(startDate);
            sqlQuery.append("' and '");
            sqlQuery.append(endDate);
            sqlQuery.append("'");
        } else if (startDate != null && !startDate.isEmpty()) {
            sqlQuery.append(addANDStatement ? " AND" : "");
            sqlQuery.append(" d.fecha_efectiva= '");
            sqlQuery.append(startDate);
            sqlQuery.append("'");
        }
     
     if ((sortField == null || sortField.isEmpty())
                || (orderType == null || orderType.isEmpty())) {
            sqlQuery.append("  ORDER BY d.id DESC");
        } else {
            sqlQuery.append("  ORDER BY");
            sqlQuery.append(" ");
            sqlQuery.append(sortField);
            sqlQuery.append(" ");
            sqlQuery.append(orderType);
        }
    
    return sqlQuery.toString();
    
    }


    @Deprecated
    public GananciaPerdidaResponse getGananciasAndPerdidasByGrupo(int grupo) {
        GananciaPerdidaResponse response=new GananciaPerdidaResponse();
        
        String sql="select bg.fecha,b.codigo as codigo_banca,"
                + "sum(d.valor_venta_total) as ganancia,"
                + "sum(d.valor_premio_total) as perdida "
                + "from DETALLE_GENERAL d join BAN_GENERAL bg on(d.general_id=bg.id) "
                + "join BANCA b on(d.banca_id=b.id) "
                + "WHERE bg.grupo_id=? group by d.banca_id";
        
        query=em.createNativeQuery(sql, "gananciasYperdidas");
        query.setParameter(1, grupo);
        
        List<GananciaPerdida> list=null;
        
        try {
            list=query.getResultList();
            response.setGananciasPerdidas(list);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
        }
        
        return response;
        
    
    }
    
    /*
    Nueva implementacion de reporte de ganancia y perdida
    */
    
    @Override
    public GananciaPerdidaResponse getGananciasAndPerdidasByGrupo(int grupo,String f1,String f2) {
        GananciaPerdidaResponse response=new GananciaPerdidaResponse();
        boolean result=false;
        
        try {
            Query q=em.createNativeQuery("SELECT * FROM GASTO g  "
                    + "WHERE g.fecha_efectiva between ? and ?", Gasto.class);
            q.setParameter(1, f1);
            q.setParameter(2, f2);
        
            List<Gasto> gastos=q.getResultList();
            LOG.info("Gasto LIST ::: "+ gastos);
            if (gastos!=null && !gastos.isEmpty()) {
                result=true;
            }
            
        } catch (Exception e) {
            result=false;
        }
        
        LOG.info("RESULT ---->>> "+ result);
        
         String sql=null;
        
        if (result) {
         
        sql="SELECT bg.fecha,b.codigo as codigo_banca,"
                + "ROUND(SUM(dg.monto_final_operaciones)-(SELECT SUM(MONTO) FROM GASTO g WHERE g.fecha_efectiva between ? and ? and id_banca=b.id GROUP BY id_banca)) AS ganancia,"
                + "ROUND(((SELECT SUM(MONTO)"
                + " FROM GASTO g WHERE g.fecha_efectiva between ? and ? and id_banca=b.id GROUP BY id_banca)+SUM(dg.total_pagar)+SUM(dg.valor_premio_total))) as perdida,"
                + "SUM(dg.valor_venta_total) AS venta,"
                + "SUM(dg.valor_premio_total) as premio,"
                + "(SELECT SUM(MONTO) FROM GASTO g WHERE g.fecha_efectiva between ? and ? and id_banca=b.id GROUP BY id_banca) AS gasto,"
                + "ROUND(SUM(dg.total_pagar)) as porcentaje "
                + "FROM DETALLE_GENERAL dg INNER JOIN BAN_GENERAL bg "
                + "on(dg.general_id=bg.id) INNER JOIN BANCA b "
                + "ON dg.banca_id=b.id INNER JOIN GRUPO g "
                + "ON g.id=b.grupo_id "
                + "WHERE bg.grupo_id=? GROUP BY b.id";
        
        }else {
           
            sql="SELECT bg.fecha,b.codigo as codigo_banca,"
                + "ROUND(SUM(dg.monto_final_operaciones)-(0)) AS ganancia,"
                + "ROUND(((0"
                + ")+SUM(dg.valor_premio_total))) as perdida,"
                + "SUM(dg.valor_venta_total) AS venta,"
                + "SUM(dg.valor_premio_total) as premio,"
                + "(0) AS gasto,"
                + "ROUND(SUM(dg.total_pagar)) as porcentaje "
                + "FROM DETALLE_GENERAL dg INNER JOIN BAN_GENERAL bg "
                + "on(dg.general_id=bg.id) INNER JOIN BANCA b "
                + "ON dg.banca_id=b.id INNER JOIN GRUPO g "
                + "ON g.id=b.grupo_id "
                + "WHERE bg.grupo_id=? GROUP BY b.id";
        }
        
       
        
        query=em.createNativeQuery(sql, "gananciasYperdidas");
        if (result) {
        query.setParameter(1, f1);
        query.setParameter(2, f2);
        query.setParameter(3, f1);
        query.setParameter(4, f2);
        query.setParameter(5, f1);
        query.setParameter(6, f2);
        query.setParameter(7, grupo);
        }
        if(result==false) {
        query.setParameter(1, grupo);
        }
        
        List<GananciaPerdida> list=null;
        
        try {
            list=query.getResultList();
            response.setGananciasPerdidas(list);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
        }
        
        return response;
        
    
    }
    
    
     @Override
    public GananciaPerdidaResponse getGananciasAndPerdidasByGrupo(int grupo,String startDate,
            String endDate,String sortField,String orderType) {
        GananciaPerdidaResponse response=new GananciaPerdidaResponse();
        
        String sql=buildGananciasAndPerdidasByGrupo(grupo,startDate,endDate,sortField,orderType);
        
        query=em.createNativeQuery(sql, "gananciasYperdidas");
        query.setParameter(1, startDate);
        query.setParameter(2, endDate);
        query.setParameter(3, startDate);
        query.setParameter(4, endDate);
        query.setParameter(5, startDate);
        query.setParameter(6, endDate);
        
        
        List<GananciaPerdida> list=null;
        
        try {
            list=query.getResultList();
            response.setGananciasPerdidas(list);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
        }
        
        return response;
        
    
    }
    
    
    private String buildGananciasAndPerdidasByGrupo(int grupo,
            String startDate,String endDate,String sortField,String orderType) {
        
       
        
    StringBuilder sqlQuery=new StringBuilder();
    boolean addANDStatement = false;
    
    sqlQuery.append("SELECT dg.fecha_efectiva,b.codigo as codigo_banca, ");
    sqlQuery.append("ROUND(SUM(dg.monto_final_operaciones)-(SELECT SUM(MONTO) FROM GASTO g WHERE g.fecha_efectiva between ? and ? and id_banca=b.id GROUP BY id_banca)) AS ganancia, ");
    sqlQuery.append("ROUND(((SELECT SUM(MONTO) FROM GASTO g WHERE g.fecha_efectiva between ? and ? and id_banca=b.id GROUP BY id_banca)+SUM(dg.total_pagar)+SUM(dg.valor_premio_total))) as perdida, ");
    sqlQuery.append("SUM(dg.valor_venta_total) AS venta, ");
    sqlQuery.append("SUM(dg.valor_premio_total) as premio, ");
    sqlQuery.append("(SELECT SUM(MONTO) FROM GASTO g WHERE g.fecha_efectiva between ? and ? and id_banca=b.id GROUP BY id_banca) AS gasto, ");
    sqlQuery.append("ROUND(SUM(dg.total_pagar)) as porcentaje ");
    sqlQuery.append("FROM DETALLE_GENERAL dg INNER JOIN BAN_GENERAL bg ");
    sqlQuery.append("on(dg.general_id=bg.id) INNER JOIN BANCA b ");
    sqlQuery.append("ON dg.banca_id=b.id INNER JOIN GRUPO g ");
    sqlQuery.append("ON g.id=b.grupo_id ");
    
    
    if (grupo>0  || (startDate!=null && !startDate.isEmpty())) {
        sqlQuery.append("WHERE ");
        
    }
    
     if (grupo > 0) {
            sqlQuery.append(" bg.grupo_id=");
            sqlQuery.append(String.valueOf(grupo));
            addANDStatement = true;
        }
     
//     if (banca > 0) {
//            sqlQuery.append(addANDStatement ? " AND" : "");
//            sqlQuery.append(" d.banca_id=");
//            sqlQuery.append(String.valueOf(banca));
//            addANDStatement = true;
//        }
     
     
     if (startDate != null && !startDate.isEmpty()
                && endDate != null && !endDate.isEmpty()) {
            sqlQuery.append(addANDStatement ? " AND" : "");
            sqlQuery.append(" dg.fecha_efectiva between '");
            sqlQuery.append(startDate);
            sqlQuery.append("' and '");
            sqlQuery.append(endDate);
            sqlQuery.append("'");
        } else if (startDate != null && !startDate.isEmpty()) {
            sqlQuery.append(addANDStatement ? " AND" : "");
            sqlQuery.append(" dg.fecha_efectiva= '");
            sqlQuery.append(startDate);
            sqlQuery.append("'");
        }
     
     if ((sortField == null || sortField.isEmpty())
                || (orderType == null || orderType.isEmpty())) {
         sqlQuery.append(" GROUP BY b.id ");
            sqlQuery.append("  ORDER BY dg.id DESC ");
        } else {
            sqlQuery.append(" GROUP BY b.id ");
            sqlQuery.append("  ORDER BY");
            sqlQuery.append(" ");
            sqlQuery.append(sortField);
            sqlQuery.append(" ");
            sqlQuery.append(orderType);
        }
    
    return sqlQuery.toString();
    
    }

    

    @Override
    public PorcentajeResponse getMontoPorcentajeByGrupo(int grupo) {
        PorcentajeResponse response=new PorcentajeResponse();
        
        String sql="select bg.fecha,b.codigo as codigo_banca,"
                + "ROUND(sum(d.total_pagar),2) as total_banca,"
                + " ROUND(sum(d.juego1_monto_pagar),2) as total_juego1,"
                + "ROUND(sum(d.juego2_monto_pagar),2) as total_juego2,"
                + "ROUND(sum(d.juego3_monto_pagar),2) as total_juego3 "
                + "from DETALLE_GENERAL d join BAN_GENERAL bg "
                + "on(d.general_id=bg.id) join BANCA b on(d.banca_id=b.id) "
                + "WHERE bg.grupo_id=? GROUP BY d.banca_id";
        
        query=em.createNativeQuery(sql, "montosPorcentajes");
        query.setParameter(1, grupo);
        List<Porcentajes> list=null;
        
        
        try {
            list=query.getResultList();
            response.setPorcentajes(list);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
        }
        
        return response;
    
    }

    @Override
    public PorcentajeResponse getMontoPorcentajeByGrupo(int grupo,String startDate,String endDate,
            String sortField,String orderType) {

        PorcentajeResponse response=new PorcentajeResponse();
        
        String sql=buildMontoPorcentajeByGrupo(grupo,startDate,endDate,sortField,orderType);
        
        query=em.createNativeQuery(sql, "montosPorcentajes");        
       
        List<Porcentajes> list=null;
        
        
        try {
            list=query.getResultList();
            response.setPorcentajes(list);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
        }
        
        return response;
    
    }
    
    
    private String buildMontoPorcentajeByGrupo(int grupo,String startDate,String endDate,
            String sortField,String orderType) {
        
        StringBuilder sqlQuery=new StringBuilder();
    boolean addANDStatement = false;
    
    sqlQuery.append("SELECT d.fecha_efectiva,b.codigo as codigo_banca, ");
    sqlQuery.append("ROUND(sum(d.total_pagar),2) as total_banca, ");
    sqlQuery.append("ROUND(sum(d.juego1_monto_pagar),2) as total_juego1, ");
    sqlQuery.append("ROUND(sum(d.juego2_monto_pagar),2) as total_juego2, ");
    sqlQuery.append("ROUND(sum(d.juego3_monto_pagar),2) as total_juego3 ");
    sqlQuery.append("from DETALLE_GENERAL d join BAN_GENERAL bg ");
    sqlQuery.append("on(d.general_id=bg.id) join BANCA b on(d.banca_id=b.id) ");
    
    
    
    if (grupo>0 || (startDate!=null && !startDate.isEmpty())) {
        sqlQuery.append("WHERE ");
        
    }
    
     if (grupo > 0) {
            sqlQuery.append(" bg.grupo_id=");
            sqlQuery.append(String.valueOf(grupo));
            addANDStatement = true;
        }
     
//     if (banca > 0) {
//            sqlQuery.append(addANDStatement ? " AND" : "");
//            sqlQuery.append(" d.banca_id=");
//            sqlQuery.append(String.valueOf(banca));
//            addANDStatement = true;
//        }
     
     
     if (startDate != null && !startDate.isEmpty()
                && endDate != null && !endDate.isEmpty()) {
            sqlQuery.append(addANDStatement ? " AND" : "");
            sqlQuery.append(" d.fecha_efectiva between '");
            sqlQuery.append(startDate);
            sqlQuery.append("' and '");
            sqlQuery.append(endDate);
            sqlQuery.append("'");
        } else if (startDate != null && !startDate.isEmpty()) {
            sqlQuery.append(addANDStatement ? " AND" : "");
            sqlQuery.append(" d.fecha_efectiva= '");
            sqlQuery.append(startDate);
            sqlQuery.append("'");
        }
     
     if ((sortField == null || sortField.isEmpty())
                || (orderType == null || orderType.isEmpty())) {
            sqlQuery.append(" GROUP BY banca_id ");
            sqlQuery.append("  ORDER BY d.id DESC");
        } else {
            sqlQuery.append(" GROUP BY banca_id ");
            sqlQuery.append("  ORDER BY");
            sqlQuery.append(" ");
            sqlQuery.append(sortField);
            sqlQuery.append(" ");
            sqlQuery.append(orderType);
        }
    
    return sqlQuery.toString();
    
    
    }

    @Override
    public int getTransactionRecords(int grupo,int banca, String startDate, String endDate) {
        int totalRecord=0;
        
        String sql=buildGetTransactionsQuery(grupo,banca, startDate, endDate,null,null);  
        
        LOG.info("SQL QUERY -->> "+ sql);
        query=em.createNativeQuery(sql, DetalleGeneral.class);
        
        List<DetalleGeneral> list=null;       
        
        try {
            list=query.getResultList();
            totalRecord=list.size();
            LOG.info("QUERY Result-->> "+ list);
            
        } catch (Exception e) {
            
            LOG.error("DETALLE_GENERAL error ", e);
        }
        
        return totalRecord;
    
    }

    @Override
    public TransaccionMontoTotalResponse getSumatoriaTransaccion(int grupo, int banca,
            String startDate, String endDate) {
        TransaccionMontoTotalResponse response=new TransaccionMontoTotalResponse();
        String sql=buildSumatoriaTransaccion(grupo,banca,startDate,endDate);
        
        try {
            query=em.createNativeQuery(sql, "transaccionTotal");
            SumatoriaTransaccionCustomEntity sumatoria=
                    (SumatoriaTransaccionCustomEntity) query.getSingleResult();
            response.setSumatoriaTransaccion(sumatoria);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            LOG.error("Problema al tratar de obtener la sumatoria de transacciones "+ e);
            e.printStackTrace();
        }
        
        
        return response;
    
    }
    
    
    public String buildSumatoriaTransaccion(int grupo, int banca, 
            String startDate, String endDate) {

        StringBuilder sqlQuery=new StringBuilder();
        boolean addANDStatement = false;

        sqlQuery.append("select d.id,SUM(d.valor_venta_total) as venta, ");
        sqlQuery.append("SUM(d.valor_premio_total) as premio,SUM(d.juego1_venta) as juego1_venta, ");
        sqlQuery.append("SUM(d.juego1_premio) as juego1_premio,SUM(d.juego2_venta) as juego2_venta, ");
        sqlQuery.append("SUM(d.juego2_premio) as juego2_premio,SUM(d.juego3_venta) as juego3_venta, ");
        sqlQuery.append("SUM(d.juego3_premio) as juego3_premio,SUM(d.juego4_venta) as juego4_venta, ");
        sqlQuery.append("SUM(d.juego4_premio) as juego4_premio  from DETALLE_GENERAL d ");
        sqlQuery.append("join BAN_GENERAL b on(d.general_id=b.id) ");
        

        if (grupo>0 || banca>0 
                || (startDate!=null && !startDate.isEmpty())) {
            sqlQuery.append("WHERE ");

        }

         if (grupo > 0) {
                sqlQuery.append(" b.grupo_id=");
                sqlQuery.append(String.valueOf(grupo));
                addANDStatement = true;
            }

         if (banca > 0) {
                sqlQuery.append(addANDStatement ? " AND" : "");
                sqlQuery.append(" d.banca_id=");
                sqlQuery.append(String.valueOf(banca));
                addANDStatement = true;
            }


         if (startDate != null && !startDate.isEmpty()
                    && endDate != null && !endDate.isEmpty()) {
                sqlQuery.append(addANDStatement ? " AND" : "");
                sqlQuery.append(" d.fecha_efectiva between '");
                sqlQuery.append(startDate);
                sqlQuery.append("' and '");
                sqlQuery.append(endDate);
                sqlQuery.append("'");
            } else if (startDate != null && !startDate.isEmpty()) {
                sqlQuery.append(addANDStatement ? " AND" : "");
                sqlQuery.append(" d.fecha_efectiva= '");
                sqlQuery.append(startDate);
                sqlQuery.append("'");
            }

        
    return sqlQuery.toString();
    
    
    }
    
    
    

    
}