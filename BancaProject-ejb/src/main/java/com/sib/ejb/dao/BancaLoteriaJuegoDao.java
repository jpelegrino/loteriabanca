/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.ejb.dao;

import com.sib.bs.responses.BancaLoteriaJuegoResponse;
import com.sib.bs.responses.JuegoBLJResponse;
import com.sib.bs.responses.JuegoResponse;
import com.sib.entity.BancaLoteriaJuego;
import com.sib.entity.BancaLoteriaJuegoPK;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;

/**
 *
 * @author julio
 */
@Local
public interface BancaLoteriaJuegoDao extends ServiceDao {
    
    public BancaLoteriaJuegoResponse crearBancaLoteriaJuego(int banca,int loteria,int juego,int porc);
    public BancaLoteriaJuegoResponse getBancaLoteriaJuegoById(int banca,int loteria,int juego);
    public BancaLoteriaJuegoResponse editarBancaLoteriaJuego(BancaLoteriaJuego bancaLoteriaJuego);
    public BancaLoteriaJuegoResponse deleteBancaLoteriaJuego(BancaLoteriaJuego bancaLoteriaJuego);
    public BancaLoteriaJuegoResponse aplicarPorcientoByGrupo(int id,int porciento);
    public BancaLoteriaJuegoResponse aplicarPorcientoByBanca(int id,int porciento);
    public BancaLoteriaJuegoResponse aplicarPorcientoByJuego(int banca,int loteria,int juego,int porciento);
    public JuegoResponse getJuegosInBLJ();
    public JuegoResponse getJuegosByBancaAndLoteria(int banca,int loteria);
    public JuegoBLJResponse getJuegosAndPorcByBanAndJue(int banca,int loteria);
    
}
