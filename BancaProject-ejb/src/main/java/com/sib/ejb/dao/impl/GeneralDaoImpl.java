/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.ejb.dao.impl;

import com.sib.bs.responses.GeneralResponse;
import com.sib.bs.responses.GrupoResponse;
import com.sib.bs.responses.MessageEnum;
import com.sib.ejb.dao.GeneralDao;
import com.sib.ejb.dao.GrupoDao;
import com.sib.entity.General;
import com.sib.entity.Grupo;
import com.sib.util.ConnectionDB;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author julio
 */
@Stateless
public class GeneralDaoImpl implements GeneralDao {

    @PersistenceContext(unitName = "sibPU")
    private EntityManager em;
    private Query query;
    @EJB
    private GrupoDao gdao;

    @Override
    public GeneralResponse crearGeneral(General general) {
        GeneralResponse response = new GeneralResponse();

        try {
            general.setStatus("A");
            general.setStatusGeneral("P");
            em.merge(general);
            response.setGeneral(general);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }

        return response;

    }

    @Override
    public GeneralResponse getGeneralById(int id) {
        GeneralResponse response = new GeneralResponse();
        General general = null;
        try {
            general = (General) em.find(General.class, id);
            response.setGeneral(general);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }

        return response;

    }

    @Override
    public String getName() {

        return "GeneralDaoImpl";
    }

    @Override
    public boolean validarGrupoInGeneral(int grupo) {
        boolean existe = false;
        try {
            GrupoResponse resp = gdao.getGrupoById(grupo);
            Grupo grp = resp.getGrupo();
            existe = (grp != null);
            System.out.println("existente :::::::::: " + existe);

        } catch (Exception e) {
            existe = false;
            e.printStackTrace();
        }
        return existe;

    }

    @Override
    public boolean validarExistenciaFecha(String fecha) {

        query = em.createNamedQuery("General.findByFecha");
        query.setParameter("fecha", fecha);
        List<General> list = null;
        boolean existe = false;

        try {
            list = query.getResultList();
            existe = list.size() > 0;
        } catch (Exception e) {
            existe = false;
            e.printStackTrace();
        }

        return existe;

    }

    @Override
    public GeneralResponse getGeneralListByFecha(String fecha) {
        GeneralResponse response = new GeneralResponse();
        List<General> list = null;
        try {
            query = em.createNamedQuery("General.findByFecha");
            query.setParameter("fecha", fecha);
            list = query.getResultList();
            response.setGenerals(list);
            response.setMsgEnum(MessageEnum.SUCCESS);

        } catch (Exception e) {

            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public GeneralResponse getGeneralByFechaAndGrupoId(int id, String fecha) {

        GeneralResponse response = new GeneralResponse();
        General general = null;
        String sql = "Select * from BAN_GENERAL where fecha='" + fecha + "'  and grupo_id=" + id + ";";
        System.out.println("SQL --->" + sql);
        try {
            query = em.createNativeQuery(sql, General.class);
            general = (General) query.getSingleResult();
            response.setGeneral(general);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }

        return response;

    }

    @Override
    public GeneralResponse editarGeneral(General general) {
        GeneralResponse response = new GeneralResponse();

        try {
            em.merge(general);
            response.setGeneral(general);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        return response;
    }

}
