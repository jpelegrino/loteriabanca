/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.ejb.dao;

import com.sib.bs.responses.GeneralResponse;
import com.sib.entity.General;
import javax.ejb.Local;
import javax.ejb.Remote;

/**
 *
 * @author julio
 */
@Local
@Remote
public interface GeneralDao extends ServiceDao {
    
    public GeneralResponse crearGeneral(General general);
    public GeneralResponse getGeneralById(int id);
    public boolean validarGrupoInGeneral(int grupo);
    public boolean validarExistenciaFecha(String fecha);
    public GeneralResponse getGeneralListByFecha(String fecha);
    public GeneralResponse getGeneralByFechaAndGrupoId(int id , String fecha);
    public GeneralResponse editarGeneral(General general);
    
}
