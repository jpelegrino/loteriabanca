/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.ejb.dao.impl;

import com.sib.bs.responses.MessageEnum;
import com.sib.bs.responses.TipoGastoResponse;
import com.sib.ejb.dao.TipoGastoDao;
import com.sib.entity.Gasto;
import com.sib.entity.TipoGasto;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author julio
 */
@Stateless
public class TipoGastoDaoImpl implements TipoGastoDao{
    
    private static Logger LOG=Logger.getLogger(TipoGastoDaoImpl.class);

    @PersistenceContext(unitName = "sibPU")
    private EntityManager em;
    private Query query;
    
    

    @Override
    public String getName() {
        return "TipoGastoDaoImpl";
    
    }

    @Override
    public TipoGastoResponse crearTipoGasto(TipoGasto tipoGasto) {
        TipoGastoResponse response=new TipoGastoResponse();
        
        LOG.info("-- Agregando un tipo de gasto --");
        
        try {
            em.persist(tipoGasto);
            response.setTipoGasto(tipoGasto);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        return response;
    
    }

    @Override
    public TipoGastoResponse getTipoGastos() {
        TipoGastoResponse response=new TipoGastoResponse();
        
        LOG.info("-- Obteniendo la lista de gastos disponible --");
        
        try {
            query=em.createNamedQuery("TipoGasto.findAll");
            List<TipoGasto> list=query.getResultList();
            response.setTipoGastos(list);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        return response;
        
    
    }

    @Override
    public TipoGastoResponse getTipoGastoById(long id) {
        TipoGastoResponse response =new TipoGastoResponse();
        
        try {
            query=em.createNamedQuery("TipoGasto.findById");
            query.setParameter("id", id);
            TipoGasto tipoGasto=(TipoGasto) query.getSingleResult();
            response.setTipoGasto(tipoGasto);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.SUCCESS);
            e.printStackTrace();
        }
        
        return response;
    
    }

    @Override
    public TipoGastoResponse deleteTipoGasto(TipoGasto tipoGasto) {
        TipoGastoResponse response=new TipoGastoResponse();
        boolean existe=verificarExistenciaInGasto(tipoGasto.getId().intValue());
        
        try {
            if(existe){
                response.setMsgEnum(MessageEnum.VALIDATION);
            }else {
                em.remove(em.merge(tipoGasto));
                response.setMsgEnum(MessageEnum.SUCCESS);
            }
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        return response;
    
    }

    @Override
    public boolean verificarExistenciaInGasto(int tipoGasto) {
        boolean existe=false;
        try {
            String sql="SELECT * FROM GASTO WHERE id_tipo_gasto=?";
            LOG.info("SQL QUERY ::: "+sql);
            query=em.createNativeQuery(sql, Gasto.class);
            query.setParameter(1, tipoGasto);
            
            existe=(query.getResultList().size()>=1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return existe;
    }
    
}
