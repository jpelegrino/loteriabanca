/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.ejb.dao;

import com.sib.bs.responses.JuegoResponse;
import com.sib.bs.responses.LoteriaJuegoResponse;
import com.sib.entity.Juego;
import com.sib.entity.Loteria;
import com.sib.entity.LoteriaJuego;
import com.sib.entity.LoteriaJuegoPK;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;

/**
 *
 * @author julio
 */
@Local
@Remote
public interface LoteriaJuegoDao extends ServiceDao{
    
    public LoteriaJuegoResponse addJuegoToLoteria(int juego,int loteria);
    public LoteriaJuegoResponse getLoteriaJuego(int juego,int loteria);
    public boolean validarExistenciaJuegoInLJ(int juegoId);
    public JuegoResponse getJuegosByLoteriaId(int loteriaId,String status);
    public LoteriaJuegoResponse editarJuegoByLoteria(LoteriaJuego loteriaJuego);
    public LoteriaJuegoResponse deleteLoteriaJuego(LoteriaJuego loteriaJuego);
    public boolean validarExistenciaLoteriaJuego(int juego,int loteria);
    public boolean validarExistenciaLoteriaJuegoInBLJ(int juego,int loteria);
    public JuegoResponse getJuegosActivosByLoteria(int loteria);
    
    
}
