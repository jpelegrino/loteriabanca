/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.ejb.dao;


import com.sib.bs.responses.BancaResponse;
import com.sib.entity.Banca;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;

/**
 *
 * @author julio
 */
@Local
public interface BancaDao extends ServiceDao{
    public BancaResponse crearBanca(Banca banca);
    public BancaResponse getBancas(String status);    
    public BancaResponse updateBanca(Banca banca);
    public boolean validarGrupoBanca(int grupo,String banca);
    public BancaResponse getBancaByCodigoAndGrupoId(String codigo,int grupoId);
    public BancaResponse getBancaById(int id);
    public BancaResponse getBancaByGrupoId(int grupoId);
    public BancaResponse getBancaByGrupoId(int grupoId,int start,int max);
    public boolean validarExistenciaGrupoInBancas(int grupo);
    public BancaResponse deleteBanca(Banca banca);
    public BancaResponse getAllBancas();
    
}