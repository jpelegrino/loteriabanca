/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.ejb.dao.impl;

import com.sib.bs.responses.BancaResponse;
import com.sib.bs.responses.LoteriaResponse;
import com.sib.bs.responses.MessageEnum;
import com.sib.ejb.dao.BancaDao;
import com.sib.ejb.dao.BancaLoteriaDao;
import com.sib.ejb.dao.LoteriaDao;
import com.sib.entity.Banca;
import com.sib.entity.Loteria;
import com.sib.util.ConnectionDB;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author julio
 */
@Stateless
public class LoteriaDaoImpl implements LoteriaDao {
    
    @PersistenceContext(unitName = "sibPU")
    private EntityManager em;
    private Query query;
    @EJB
    private BancaLoteriaDao bancaLoteriaDao;
    @EJB
    private BancaDao bancaDao;

    @Override
    public LoteriaResponse crearLoteria(Loteria loteria) {
        LoteriaResponse response=new LoteriaResponse();
        
        /*
         Este es el codigo que debe ir, el codigo de abajo se cambiara en un futuro
         em.persist(loteria);
        response.setMsgEnum(MessageEnum.SUCCESS);
        response.setLoteria(loteria);
        */
        
        try {
            boolean existe=validarExistenciaLoteria(loteria.getCodigo());
            if (existe) {
                response.setMsgEnum(MessageEnum.VALIDATION);           
        }else {
            loteria.setFechaCreacion(new Date());            
            loteria.setStatus("A");
       
        em.persist(loteria);
        em.flush();
        response.setMsgEnum(MessageEnum.SUCCESS);
        response.setLoteria(loteria);
        
        int loteriaId=loteria.getId();
        BancaResponse resp=bancaDao.getAllBancas();
        if (resp != null && resp.getMsgEnum().getCode()==1) {
            for (Banca b : resp.getBancas()){
                bancaLoteriaDao.crearBancaLoteria(b.getId(), loteriaId);        
            }
        
        }
        
        }
            
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        return response;
        
        
    }

    @Override
    public LoteriaResponse editarLoteria(Loteria loteria) {
        LoteriaResponse response=new LoteriaResponse();
        
        try {
                em.merge(loteria);
                response.setLoteria(loteria);
                response.setMsgEnum(MessageEnum.SUCCESS);
           
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        return response;
        
        
    }

    @Override
    public LoteriaResponse getLoterias(String status) {
        LoteriaResponse response=new LoteriaResponse();
        
       
        
        List<Loteria> list=null;
        
        if (status==null || status.isEmpty()) {
        
            query=em.createNamedQuery("Loteria.findAll");
            try {
                list=query.getResultList();
                response.setLoterias(list);
                response.setMsgEnum(MessageEnum.SUCCESS);
            } catch (Exception e) {
                response.setMsgEnum(MessageEnum.FAIL);
                e.printStackTrace();
            }
        }else {
            query=em.createNamedQuery("Loteria.findAll2");
            query.setParameter("status", status);            
            try {

                list=query.getResultList();
                response.setLoterias(list);
                response.setMsgEnum(MessageEnum.SUCCESS);
            } catch (Exception e) {
                response.setMsgEnum(MessageEnum.FAIL);
                e.printStackTrace();
            }
        
        }
        
        
        
           
        return response;
    
    }

    @Override
    public LoteriaResponse getLoteriaByCodigo(String codigo) {
        LoteriaResponse response=new LoteriaResponse();
        
        Loteria loteria=null;
         query=em.createNamedQuery("Loteria.findByCodigo");
         query.setParameter("codigo", codigo);
        try {
           loteria=(Loteria) query.getSingleResult();
           response.setLoteria(loteria);
           response.setMsgEnum(MessageEnum.SUCCESS);
            
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        
        return response;
    
    }

    @Override
    public LoteriaResponse getLoteriaById(int id) {
        LoteriaResponse response=new LoteriaResponse();
        Loteria loteria=null;
        try {
            loteria=(Loteria) em.find(Loteria.class, id);
            response.setLoteria(loteria);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
           response.setMsgEnum(MessageEnum.FAIL);
           e.printStackTrace();
        }
        return response;
    
    }

    @Override
    public boolean validarExistenciaLoteria(String codigo) {
        query=em.createNamedQuery("Loteria.findByCodigo");
        query.setParameter("codigo", codigo);
        boolean existe=false;
        try {
            Loteria loteria=(Loteria) query.getSingleResult();
            existe=(loteria!=null);
        } catch (Exception e) {
            existe=false;
            e.printStackTrace();
        }
        
        return existe;
    
    }

    @Override
    public LoteriaResponse deleteLoteria(Loteria loteria) {
        LoteriaResponse response=new LoteriaResponse();
        
        try {
            boolean noExiste=bancaLoteriaDao.validarLoteriaInBanca(loteria.getId());
            if (noExiste) {
            
                em.remove(em.merge(loteria));
                response.setMsgEnum(MessageEnum.SUCCESS);
            }else {
                response.setMsgEnum(MessageEnum.VALIDATION);
            }
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        return response;
        
        
    }

    @Override
    public String getName() {

        return "LoteriaDaoImpl";
    }
    
}
