/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.ejb.dao.impl;

import com.sib.bs.responses.JuegoResponse;
import com.sib.bs.responses.MessageEnum;
import com.sib.ejb.dao.JuegoDao;
import com.sib.ejb.dao.LoteriaJuegoDao;
import com.sib.entity.Juego;
import com.sib.util.ConnectionDB;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author julio
 */
@Stateless
public class JuegoDaoImpl implements JuegoDao {
    
    private static Logger LOG=Logger.getLogger(JuegoDaoImpl.class);

    @PersistenceContext(unitName = "sibPU")
    private EntityManager em;
    private Query query;
    @EJB
    private LoteriaJuegoDao loteriaJuegoDao;
    
    @Override
    public JuegoResponse crearJuego(Juego juego) {
        JuegoResponse response=new JuegoResponse();
        
        try {
            
            boolean validar=validarExistenciaJuego(juego.getCodigo());
        
        if (validar) {
            response.setMsgEnum(MessageEnum.VALIDATION);        
            
        }else {
            juego.setFechaCreacion(new Date());            
            juego.setStatus("A");
            em.persist(juego);
            
            response.setJuego(juego);
            response.setMsgEnum(MessageEnum.SUCCESS);
       
        }
            
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        
        
       
            
        
        
        return response;
    }

    @Override
    public JuegoResponse editarJuego(Juego juego) {
        JuegoResponse response=new JuegoResponse();        
        
            try {

                juego.setFechaActualizacion(new Date());

                em.merge(juego);
                response.setJuego(juego);
                response.setMsgEnum(MessageEnum.SUCCESS);

               
            } catch (Exception e) {
                response.setMsgEnum(MessageEnum.FAIL);
                e.printStackTrace();
            }
            
       
        return response;
    }

    @Override
    public JuegoResponse getJuegos(String status) {
        List<Juego> list=null;
        
        JuegoResponse response=new JuegoResponse();
        
        if (status==null || status.isEmpty()) {
            query=em.createNamedQuery("Juego.findAll");
            try {
                list=query.getResultList();
                response.setJuegos(list);
                response.setMsgEnum(MessageEnum.SUCCESS);
            } catch (Exception e) {
                response.setMsgEnum(MessageEnum.FAIL);
                e.printStackTrace();
            }
            
        }else {        
            query=em.createNamedQuery("Juego.findAll2");
            query.setParameter("status", status);        

            try {
                list=query.getResultList();
                response.setJuegos(list);
                response.setMsgEnum(MessageEnum.SUCCESS);
            } catch (Exception e) {
                response.setMsgEnum(MessageEnum.FAIL);
                e.printStackTrace();
            }
        
        }
        return response;
    
    }

    @Override
    public JuegoResponse getJuegoById(int id) {
        Juego juego=null;
        JuegoResponse response=new JuegoResponse();
        try {
            juego=(Juego) em.find(Juego.class, id);
            response.setJuego(juego);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        return response;
    
    }

    @Override
    public boolean validarExistenciaJuego(String codigo) {
        query=em.createNamedQuery("Juego.validarExistenciaJuego");
        query.setParameter("codigo", codigo);
     
        
        boolean exists=false;
        try {
            Juego juego=(Juego) query.getSingleResult();
            exists=(juego!=null);
        } catch (Exception e) {
           exists=false;
           e.printStackTrace();
        }
        
        return exists;
    
    
    }

    @Override
    public JuegoResponse deleteJuego(Juego juego) {
        JuegoResponse response=new JuegoResponse();
        
        try {
            boolean existe=loteriaJuegoDao.validarExistenciaJuegoInLJ(juego.getId());
        System.out.println("existe: ::::::::::::::::::: "+ existe);
        if (existe) {
            
            response.setMsgEnum(MessageEnum.VALIDATION);
             System.out.println("SE IMPLEMENTARA UNA MENSAJERIA PARA ESTE FIN");          
            
        }else {
            em.remove(em.merge(juego));
            response.setMsgEnum(MessageEnum.SUCCESS);
             
        }
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        
        
        
        return response;
    }

    @Override
    public JuegoResponse getJuegosActivos() {
        JuegoResponse response=new JuegoResponse();
        query=em.createNamedQuery("Juego.findAllJuegoActivo");
        List<Juego> list=null;
        try {
            list=query.getResultList();
            response.setJuegos(list);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
           e.printStackTrace();
        }
        
        return response;
    
    }

    @Override
    public JuegoResponse getJuegosNotRelatedInLJ() {
        JuegoResponse response=new JuegoResponse();
        query=em.createNativeQuery("select * from   JUEGO WHERE id not in (select juego_id from LOTERIA_has_JUEGO) and status='A'", Juego.class);
        List<Juego> list=null;
        
        try {
            list=query.getResultList();
            response.setJuegos(list);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        
        return response;
    
    }

    @Override
    public JuegoResponse getJuegosNOtRelatedInLJByLoteria(int loteriaId) {
        
        JuegoResponse response=new JuegoResponse();
        query=em.createNativeQuery("select * from   JUEGO  WHERE id not in (select juego_id from LOTERIA_has_JUEGO WHERE loteria_id=?) and status='A'", Juego.class);
        query.setParameter(1, loteriaId);
        List<Juego> list=null;
        
        try {
            list=query.getResultList();
            response.setJuegos(list);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        
        return response;
    }

    @Override
    public boolean validarExistenciaJuego(int id) {

         query=em.createNamedQuery("Juego.validarExistenciaJuego2");
         query.setParameter("id", id);
     
        
        boolean exists=false;
        try {
            Juego juego=(Juego) query.getSingleResult();
            exists=(juego!=null);
        } catch (Exception e) {
            return false;
        }
        
        return exists;
    }

    @Override
    public JuegoResponse getJuegosByLoteriaNotRInBLJ(int loteria,int banca) {
        JuegoResponse response=new JuegoResponse();
        
        String sql="select * "
                + "from JUEGO "
                + "WHERE id in (select juego_id "
                + "from LOTERIA_has_JUEGO  "
                + "WHERE loteria_id=? and juego_id NOT IN (select juego_id "
                + "from BANCA_has_LOTERIA_has_JUEGO WHERE banca_id=? and loteria_id=?)) AND status='A'";
        
        LOG.info("SQL QUERY ::::: "+ sql);
        
        query=em.createNativeQuery(sql, Juego.class);
        query.setParameter(1, loteria);
        query.setParameter(2, banca);
        query.setParameter(3, loteria);
        List<Juego> list=null;
        
        try {
            list=query.getResultList();
            LOG.info("QUERY RESULT :::: "+ list);
            response.setJuegos(list);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            LOG.error("Error ", e);
        }
        
        return response;
        
    }

   
    
}
