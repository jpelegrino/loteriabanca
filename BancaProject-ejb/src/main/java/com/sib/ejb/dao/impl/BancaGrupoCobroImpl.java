package com.sib.ejb.dao.impl;

import com.sib.bs.responses.BancaGrupoCobroResponse;
import com.sib.bs.responses.MessageEnum;
import com.sib.ejb.dao.BancaGrupoCobroDao;
import com.sib.entity.BancaGrupoCobro;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author Snailin Inoa
 */
@Stateless
public class BancaGrupoCobroImpl implements BancaGrupoCobroDao {

    @PersistenceContext(unitName = "sibPU")
    private EntityManager em;
    private Query query;
    private static final Logger LOGGER = Logger.getLogger(BancaGrupoCobroImpl.class);

    @Override
    public BancaGrupoCobroResponse getBancaGrupoCobroListByGroupId(int idGrupo) {
        LOGGER.info("getBancaGrupoCobroListByGroupId() :: START");
        BancaGrupoCobroResponse bancaGrupoCobroResponse = new BancaGrupoCobroResponse();
        try {
            String sql = "SELECT SUM(d.pendiente_cobrar) AS DEUDA, bc.codigo AS CODIGO_BANCA,\n"
                    + "g.codigo AS  CODIGO_GRUPO ,g.id AS GRUPO_ID, d.banca_id AS BANCA_ID, b.status_general, d.status_detalle\n"
                    + "FROM DETALLE_GENERAL d JOIN BAN_GENERAL b ON (d.general_id=b.id)\n"
                    + "JOIN BANCA bc ON (bc.id=d.banca_id)\n"
                    + "JOIN GRUPO g ON (g.id=b.grupo_id)\n"
                    + "WHERE b.grupo_id IN (SELECT grupo_id FROM BAN_GENERAL WHERE grupo_id=?1)\n"
                    + "AND d.status_detalle='P'\n"
                    + "GROUP BY d.banca_id";

            query = em.createNativeQuery(sql, "bancaGrupoCobro");
            query.setParameter(1, idGrupo);
            List<BancaGrupoCobro> list = query.getResultList();

            bancaGrupoCobroResponse.setBancaGrupoCobro(list);
            bancaGrupoCobroResponse.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            LOGGER.error("ERROR tratando de obtener la lista de deudas de bancas para un grupo Especifico ::" + e);
            bancaGrupoCobroResponse.setMsgEnum(MessageEnum.FAIL);

        }
        LOGGER.info("getBancaGrupoCobroListByGroupId() :: END");
        return bancaGrupoCobroResponse;
    }

    @Override
    public String getName() {
        return "BancaGrupoCobroImpl";
    }

    @Override
    public BancaGrupoCobroResponse getAllDetallesIDRelatedToBancaAndGrupo(int idBanca, int idGrupo) {
        LOGGER.info("getAllDetallesIDRelatedToBancaAndGrupo() :: START");
        BancaGrupoCobroResponse response = new BancaGrupoCobroResponse();
        try {
            if (idBanca <= 0 || idGrupo <= 0) {
                LOGGER.error("ID_BANCA o ID_GRUPO ES MENOR O IGUAL A CERO");
                LOGGER.error("ID_BANCA ::" + idBanca);
                LOGGER.error("ID GRUPO ::" + idGrupo);
                response.setMsgEnum(MessageEnum.FAIL);
                return response;
            }

            String sql = "SELECT d.id AS DETALLE_ID\n"
                    + "FROM DETALLE_GENERAL d JOIN BAN_GENERAL b ON (d.general_id=b.id)\n"
                    + "JOIN BANCA bc ON (d.banca_id=?1)\n"
                    + "JOIN GRUPO g ON (g.id=b.grupo_id)\n"
                    + "WHERE b.grupo_id IN (SELECT grupo_id FROM BAN_GENERAL WHERE grupo_id=?2)\n"
                    + "AND d.status_detalle='P'\n"
                    + "GROUP BY d.id"
                    +" ORDER BY d.id ASC";

            query = em.createNativeQuery(sql);
            query.setParameter(1, idBanca);
            query.setParameter(2, idGrupo);
            List<Long> list = (List<Long>) query.getResultList();
            response.setDetallesGeneralesID(list);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            LOGGER.error("Error tratando de obtener la lista de ID de la tabla DETALLE_GENERAL ::" + e);
        }
        LOGGER.info("getAllDetallesIDRelatedToBancaAndGrupo() :: END");
        return response;
    }

}
