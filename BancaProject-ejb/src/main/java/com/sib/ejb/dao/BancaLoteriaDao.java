/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.ejb.dao;

import com.sib.bs.responses.BancaLoteriaResponse;
import com.sib.bs.responses.LoteriaResponse;
import com.sib.entity.BancaLoteria;
import com.sib.entity.BancaLoteriaPK;
import javax.ejb.Local;
import javax.ejb.Remote;

/**
 *
 * @author julio
 */
@Local
public interface BancaLoteriaDao extends ServiceDao{
    public BancaLoteriaResponse crearBancaLoteria(int banca,int loteria);
    public BancaLoteriaResponse getBancaLoteriaById(int bancaId,int loteriaId);
    public boolean validarLoteriaInBanca(int id);
    public boolean validarExistenciaBL(int banca,int loteria);
    public LoteriaResponse getLoteriasByBanca(int banca);
    public LoteriaResponse getLoteriasNotInBanca(int banca);
    public BancaLoteriaResponse deleteBancaLoteria(BancaLoteria bancaLoteria);
    public boolean validarExistenciaBLInDetalleGeneral(int banca,int loteria);
    
}
