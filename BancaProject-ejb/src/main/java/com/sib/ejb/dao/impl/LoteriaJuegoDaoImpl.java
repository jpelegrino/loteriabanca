/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.ejb.dao.impl;

import com.sib.bs.factory.EntityContext;
import com.sib.bs.responses.JuegoResponse;
import com.sib.bs.responses.LoteriaJuegoResponse;
import com.sib.bs.responses.MessageEnum;
import com.sib.ejb.dao.JuegoDao;
import com.sib.ejb.dao.LoteriaJuegoDao;
import com.sib.entity.BancaLoteriaJuego;
import com.sib.entity.Juego;
import com.sib.entity.LoteriaJuego;
import com.sib.entity.LoteriaJuegoPK;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author julio
 */
@Stateless
public class LoteriaJuegoDaoImpl implements LoteriaJuegoDao{
    
    public static final Logger LOG
            = Logger.getLogger(LoteriaJuegoDaoImpl.class.getName());

    @PersistenceContext(unitName = "sibPU")
    private EntityManager em;
    private Query query;
    
    @EJB
    JuegoDao juegoDao;
    
    @Override
    public LoteriaJuegoResponse addJuegoToLoteria(int juego,int loteria) {
        LoteriaJuegoResponse response=new LoteriaJuegoResponse();

        try {
             boolean existe=validarExistenciaLoteriaJuego(juego,loteria);

            if (existe) {
                response.setMsgEnum(MessageEnum.VALIDATION);
            }else {
                boolean exist=juegoDao.validarExistenciaJuego(juego);

                if (exist) {
                LoteriaJuegoPK pk=(LoteriaJuegoPK) EntityContext.getEntity("LoteriaJuegoPK");
                LoteriaJuego loteriaJuego=(LoteriaJuego) EntityContext.getEntity("LoteriaJuego");

                pk.setJuegoId(juego);
                pk.setLoteriaId(loteria);
                loteriaJuego.setLoteriaJuegoPK(pk);
                loteriaJuego.setStatus("A");
                loteriaJuego.setFechaCreacion(new Date());
                loteriaJuego.setFechaActualizacion(new Date());       

                em.persist(loteriaJuego);
                response.setLoteriaJuego(loteriaJuego);
                response.setMsgEnum(MessageEnum.SUCCESS);

                }else {
                    response.setMsgEnum(MessageEnum.FAIL);
                }
            }
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            LOG.error("Falla tratando de agregar un Juego a una loteria: "+ e.getMessage(),e);
        }

        return response;
       
    }

    @Override
    public LoteriaJuegoResponse getLoteriaJuego(int juego,int loteria) {
        LoteriaJuegoResponse response=new LoteriaJuegoResponse();
        
        LoteriaJuegoPK pk=(LoteriaJuegoPK) EntityContext.getEntity("LoteriaJuegoPK");
        pk.setJuegoId(juego);
        pk.setLoteriaId(loteria);
        LoteriaJuego lj=null;
        
        try {
            lj=(LoteriaJuego) em.find(LoteriaJuego.class, pk);
            response.setLoteriaJuego(lj);
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
        }
        
        return response;
    
    }

   
    @Override
    public JuegoResponse getJuegosByLoteriaId(int loteriaId,String status) {
        JuegoResponse response=new JuegoResponse();
        
        query=em.createNativeQuery("select * from JUEGO WHERE id in (SELECT juego_id FROM LOTERIA_has_JUEGO WHERE loteria_id=? and status=?)", Juego.class);
        query.setParameter(1, loteriaId);
        query.setParameter(2, status);
        
        
        List<Juego> list=null;
        
        try {
             list=query.getResultList();
            
             response.setJuegos(list);
             response.setMsgEnum(MessageEnum.SUCCESS);

            
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
       
        
        return response;
    
    }

    @Override
    public LoteriaJuegoResponse editarJuegoByLoteria(LoteriaJuego loteriaJuego) {
        LoteriaJuegoResponse response=new LoteriaJuegoResponse();
        
        try {
             
            boolean existe=validarExistenciaLoteriaJuego(loteriaJuego.getLoteriaJuegoPK().getJuegoId(),
                    loteriaJuego.getLoteriaJuegoPK().getLoteriaId());

            if(existe) {
             loteriaJuego.setFechaActualizacion(new Date());
             em.merge(loteriaJuego);
             response.setLoteriaJuego(loteriaJuego);
             response.setMsgEnum(MessageEnum.SUCCESS);

            }else {
                response.setMsgEnum(MessageEnum.VALIDATION);
            }
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
        }
        
        return response;
       
    }

   

    @Override
    public LoteriaJuegoResponse deleteLoteriaJuego(LoteriaJuego loteriaJuego) {
        LoteriaJuegoResponse response=new LoteriaJuegoResponse();
        
        try {
            boolean existe=validarExistenciaLoteriaJuegoInBLJ(loteriaJuego.getLoteriaJuegoPK().getJuegoId(),loteriaJuego.getLoteriaJuegoPK().getLoteriaId());
            System.out.println("EXISTE ::::::::::::::::::::::::::::: "+ existe);
            if (existe) {
                response.setMsgEnum(MessageEnum.VALIDATION);
            }else  {
                em.remove(em.merge(loteriaJuego));
                response.setMsgEnum(MessageEnum.SUCCESS);
            }
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
            e.printStackTrace();
        }
        
        return response;
        
        
    }

    @Override
    public boolean validarExistenciaLoteriaJuego(int juego, int loteria) {

        query=em.createNativeQuery("select * from LOTERIA_has_JUEGO WHERE loteria_id=? and juego_id=?",LoteriaJuego.class);
        query.setParameter(1, loteria);
        query.setParameter(2, juego);
        boolean existe=false;
                
        try {
            List<LoteriaJuego> list=query.getResultList();
            existe=(list.size()>0);
        } catch (Exception e) {
            return false;
        }
        
        return existe;
        
    
    }
    
    
    @Override
    public boolean validarExistenciaLoteriaJuegoInBLJ(int juego, int loteria) {
        query=em.createNativeQuery("select * from BANCA_has_LOTERIA_has_JUEGO WHERE juego_id=? and loteria_id=?",BancaLoteriaJuego.class);
        query.setParameter(1, juego);
        query.setParameter(2, loteria);
        boolean existe=false;
                
        try {
            BancaLoteriaJuego blj=(BancaLoteriaJuego)query.getSingleResult();
            existe=(blj!=null);
        } catch (Exception e) {
           existe=false;
           e.printStackTrace();
        }
        
        return existe;
        
    }

    @Override
    public JuegoResponse getJuegosActivosByLoteria(int loteria) {
        JuegoResponse response=new JuegoResponse();

        query=em.createNamedQuery("LoteriaJuego.findAllJuegosActivos");
        query.setParameter("loteriaId", loteria);
        
        List<LoteriaJuego> list=null;
        List<Juego> listJuego=null;
        
        try {
            listJuego=new LinkedList<Juego>();
            list=query.getResultList();
            
            for (LoteriaJuego lj : list) {
                listJuego.add(lj.getJuego());
            }
            
            response.setJuegos(listJuego);
            
            response.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            response.setMsgEnum(MessageEnum.FAIL);
        }
        
        return response;
        
        
        
    
    }
    
     @Override
    public String getName() {

    return "LoteriaJuegoDaoImpl";
    }

    @Override
    public boolean validarExistenciaJuegoInLJ(int juegoId) {
        query=em.createNamedQuery("LoteriaJuego.findByJuegoId");
        query.setParameter("juegoId", juegoId);
        
        boolean existe=false;
        List<LoteriaJuego> list=null;
        
        try {
            list=query.getResultList();
            existe=list.size()>0;
        } catch (Exception e) {
            existe=false;
            e.printStackTrace();
        }
        
        return existe;
        
        
    
    
    }

    

   
    
}
