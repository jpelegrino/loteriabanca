package com.sib.ejb.dao;

import com.sib.bs.responses.CobroHistorialResponse;
import com.sib.entity.CobroHistorial;
import javax.ejb.Local;

/**
 *
 * @author Snailin Inoa
 */
@Local
public interface CobroHistorialDao extends ServiceDao {

    public CobroHistorialResponse crearCobroHistorial(CobroHistorial cobroHistorial);

    public CobroHistorialResponse updateCobroHistorial(CobroHistorial cobroHistorial);

    public CobroHistorialResponse getCobrosHistorial(String status);

    public CobroHistorialResponse getCobroHistorialById(int cobroHistorialId);

    public CobroHistorialResponse getCobroHistorialListByCobroID(Long cobroId, String status);

    public CobroHistorialResponse getCobroHistorialListByDetalleGeneralId(Long detalleGeneralId, String status);

}
