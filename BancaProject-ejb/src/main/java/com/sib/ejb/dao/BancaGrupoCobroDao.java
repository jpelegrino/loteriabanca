package com.sib.ejb.dao;

import com.sib.bs.responses.BancaGrupoCobroResponse;
import java.util.List;
import javax.ejb.Local;


/**
 *
 * @author Snailin Inoa
 */
@Local
public interface BancaGrupoCobroDao extends ServiceDao {

    public BancaGrupoCobroResponse getBancaGrupoCobroListByGroupId(int idGrupo);
    public BancaGrupoCobroResponse getAllDetallesIDRelatedToBancaAndGrupo(int idBanca, int idGrupo);

}
