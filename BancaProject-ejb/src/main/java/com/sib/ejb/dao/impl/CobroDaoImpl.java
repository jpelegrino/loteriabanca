package com.sib.ejb.dao.impl;

import com.sib.bs.responses.CobroCustomEntityResponse;
import com.sib.bs.responses.CobroResponse;
import com.sib.bs.responses.CobroVistaDetalleResponse;
import com.sib.bs.responses.MessageEnum;
import com.sib.ejb.dao.CobroDao;
import com.sib.entity.BancaGrupoCobro;
import com.sib.entity.Cobro;
import com.sib.entity.CobroCustomEntity;
import com.sib.entity.CobroVistaDetalle;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author Snailin Inoa
 */
@Stateless
public class CobroDaoImpl implements CobroDao {

    @PersistenceContext(unitName = "sibPU")
    private EntityManager em;
    private Query query;
    private static final Logger LOGGER = Logger.getLogger(CobroDaoImpl.class);

    @Override
    public CobroResponse crearCobro(Cobro cobro) {
        LOGGER.info("crearCobro() :: START");
        CobroResponse cobroResponse = new CobroResponse();
        try {

            em.persist(cobro);
            cobroResponse.setCobro(cobro);
            cobroResponse.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            cobroResponse.setMsgEnum(MessageEnum.FAIL);
            LOGGER.error("ERROR creando un COBRO :: " + e);
        }
        LOGGER.info("crearCobro() :: END");
        return cobroResponse;
    }

    @Override
    public CobroResponse updateCobro(Cobro cobro) {
        LOGGER.info("updateCobro() :: START");
        CobroResponse cobroResponse = new CobroResponse();
        try {
            em.merge(cobro);
            cobroResponse.setCobro(cobro);
            cobroResponse.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            cobroResponse.setMsgEnum(MessageEnum.FAIL);
            LOGGER.error("ERROR actualizando un COBRO :: " + e);
        }
        LOGGER.info("updateCobro() :: END");
        return cobroResponse;
    }

    @Override
    public CobroResponse getCobros(String status) {
        LOGGER.info("getCobros() :: START");
        CobroResponse cobroResponse = new CobroResponse();
        try {
            String sql = "SELECT * FROM COBRO";
            if (status != null && !status.isEmpty()) {
                sql = sql + "  WHERE STATUS_COBRO=" + "'" + status + "'";
            }

            sql += " ORDER BY ID DESC";
            LOGGER.info("SQL :: " + sql);
            query = em.createNativeQuery(sql, Cobro.class);
            List<Cobro> list = query.getResultList();
            cobroResponse.setCobros(list);
            cobroResponse.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            cobroResponse.setMsgEnum(MessageEnum.FAIL);
            LOGGER.error("ERROR tratando de obtener la lista de COBRO :: " + e);
        }
        LOGGER.info("getCobros() :: END");
        return cobroResponse;
    }

    @Override
    public CobroResponse getCobroById(Long cobroId) {
        LOGGER.info("getCobroById() :: START");
        CobroResponse cobroResponse = new CobroResponse();

        try {
            Cobro cobro = (Cobro) em.find(Cobro.class, cobroId);
            cobroResponse.setCobro(cobro);
            cobroResponse.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            cobroResponse.setMsgEnum(MessageEnum.FAIL);
            LOGGER.error("ERROR tratando de obtener un COBRO a partir del cobro-ID :: " + e);
        }
        LOGGER.info("getCobroById() :: END");
        return cobroResponse;

    }

    @Override
    public String getName() {
        return "CobroDaoImpl";
    }

    @Override
    public CobroVistaDetalleResponse getCobroVistaDetalleList(Long cobroId, String status) {
        LOGGER.info("getCobroVistaDetalleList () :: START");
        CobroVistaDetalleResponse cobroVistaDetalleResponse = new CobroVistaDetalleResponse();

        try {
            String sql = "SELECT c.id AS COBRO_ID,c.fecha_efectiva,ch.id AS COBRO_HISTORIAL_ID, ch.monto_cobrado,\n "
                    + "ch.status_cobro_historial,dg.loteria_id,l.codigo AS CODIGO_LOTERIA ,\n"
                    + "b.codigo AS CODIGO_BANCA,\n"
                    + " dg.id AS DETALLE_GENERAL_ID, "
                    + "g.codigo AS CODIGO_GRUPO, b.id AS BANCA_ID,g.id AS GRUPO_ID\n"
                    + "FROM COBRO c\n"
                    + "INNER JOIN COBRO_HISTORIAL ch ON c.id=ch.COBRO_ID AND c.id=?1 \n"
                    + "INNER JOIN DETALLE_GENERAL dg ON ch.DETALLE_GENERAL_ID=dg.ID\n"
                    + "INNER JOIN LOTERIA l ON dg.LOTERIA_ID=l.ID\n"
                    + "INNER JOIN BANCA b ON dg.BANCA_ID= b.ID\n"
                    + "INNER JOIN GRUPO g ON b.GRUPO_ID = g.ID\n ";

            if (status != null && !status.isEmpty()) {
                sql += " AND status_cobro_historial=" + status;
            }
            
            sql+=" ORDER BY dg.id ASC ";

            LOGGER.info("COBRO ID ::" + cobroId);
            LOGGER.info("STATUS ::" + status);

            LOGGER.info("SQL :: " + sql);

            query = em.createNativeQuery(sql, "cobroVistaDetalle");
            query.setParameter(1, cobroId);
            List<CobroVistaDetalle> cobroVistaDetalleList = query.getResultList();

            System.out.println("");
            System.out.println("");
            System.out.println("cobroVistaDetalleList :: " + cobroVistaDetalleList);
            System.out.println("cobroVistaDetalleList  SIZE:: " + cobroVistaDetalleList.size());
            System.out.println("");
            System.out.println("");

            cobroVistaDetalleResponse.setCobroVistaDetalleList(cobroVistaDetalleList);
            cobroVistaDetalleResponse.setMsgEnum(MessageEnum.SUCCESS);

        } catch (Exception e) {
            cobroVistaDetalleResponse.setMsgEnum(MessageEnum.FAIL);
            LOGGER.error("Error tratando de obtener los detalles asiciado a un cobro. ::" + e);
        }

        LOGGER.info("getCobroVistaDetalleList () :: END");

        return cobroVistaDetalleResponse;
    }

    @Override
    public CobroCustomEntityResponse getCobroCustomEntityLazyList(String startDate, String endDate, Integer grupoId, Integer bancaId, String sortField, String orderType, int start, int max, String status) {
        LOGGER.info("getCobroCustomEntityLazyList() :: START");
        CobroCustomEntityResponse cobroCustomEntityResponse = new CobroCustomEntityResponse();
        try {
            String sql = "SELECT c.ID AS COBRO_ID, c.FECHA_EFECTIVA, c.COMENTARIO,c.MONTO_TOTAL_COBRADO,c.STATUS_COBRO ,SUM(ch.MONTO_COBRADO) AS MONTO_TOTAL, ch.ID AS COBRO_HISTORIAL_ID, dg.ID AS DETALLE_GENERAL_ID,dg.BANCA_ID,\n"
                    + " b.CODIGO AS CODIGO_BANCA, g.ID AS GRUPO_ID,g.CODIGO AS CODIGO_GRUPO \n"
                    + " FROM COBRO c INNER JOIN COBRO_HISTORIAL ch ON c.id=ch.cobro_id \n"
                    + " INNER JOIN DETALLE_GENERAL dg ON ch.detalle_general_id=dg.id\n"
                    + " INNER JOIN BANCA b ON b.id=dg.banca_id\n"
                    + " INNER JOIN GRUPO g ON b.grupo_id=g.id\n";

            if (grupoId != null && grupoId > 0) {
                sql += " AND g.id=" + grupoId + " \n";

            }

            if (bancaId != null && bancaId > 0) {
                sql += " AND b.id=" + bancaId + " \n";

            }

            if (startDate != null && !startDate.isEmpty() && endDate != null && !endDate.isEmpty()) {
                sql += " AND c.fecha_efectiva between '" + startDate + "' AND  '" + endDate + "' \n";
            }

            if (status != null && !status.isEmpty()) {
                sql += " AND c.status_cobro='" + status + "' \n";

            }
            sql += " GROUP BY c.id";
            sql += " ORDER BY COBRO_ID DESC";

            LOGGER.info("SQL :: " + sql);
            query = em.createNativeQuery(sql, "cobroCustomEntity");
            query.setFirstResult(start);
            query.setMaxResults(max);
            List<CobroCustomEntity> cobroCustomEntitys = query.getResultList();
            cobroCustomEntityResponse.setCobroCustomEntityList(cobroCustomEntitys);
            cobroCustomEntityResponse.setMsgEnum(MessageEnum.SUCCESS);
        } catch (Exception e) {
            cobroCustomEntityResponse.setMsgEnum(MessageEnum.FAIL);
            LOGGER.info("Error tratando de obtener Cobro Custom Entity :: " + e);
        }
        LOGGER.info("getCobroCustomEntityLazyList() :: END");
        return cobroCustomEntityResponse;

    }

    @Override
    public int getCobroCustomEntityTotalRecords(String startDate, String endDate, Integer grupoId, Integer bancaId, String sortField, String orderType, String status) {

        int result = 0;
        try {
            String sql = "SELECT c.ID AS COBRO_ID, c.FECHA_EFECTIVA, c.COMENTARIO,c.MONTO_TOTAL_COBRADO,c.STATUS_COBRO ,SUM(ch.MONTO_COBRADO) AS MONTO_TOTAL, ch.ID AS COBRO_HISTORIAL_ID, dg.ID AS DETALLE_GENERAL_ID,dg.BANCA_ID,\n"
                    + " b.CODIGO AS CODIGO_BANCA, g.ID AS GRUPO_ID,g.CODIGO AS CODIGO_GRUPO \n"
                    + " FROM COBRO c INNER JOIN COBRO_HISTORIAL ch ON c.id=ch.cobro_id \n"
                    + " INNER JOIN DETALLE_GENERAL dg ON ch.detalle_general_id=dg.id\n"
                    + " INNER JOIN BANCA b ON b.id=dg.banca_id\n"
                    + " INNER JOIN GRUPO g ON b.grupo_id=g.id\n";

            if (grupoId != null && grupoId > 0) {
                sql += " AND g.id=" + grupoId + " \n";

            }

            if (bancaId != null && bancaId > 0) {
                sql += " AND b.id=" + bancaId + " \n";

            }

            if (startDate != null && !startDate.isEmpty() && endDate != null && !endDate.isEmpty()) {
                sql += " AND c.fecha_efectiva between '" + startDate + "' AND  '" + endDate + "' \n";
            }

            if (status != null && !status.isEmpty()) {
                sql += " AND c.status_cobro='" + status + "' \n";

            }
            sql += " GROUP BY c.id";
            sql += " ORDER BY c.id DESC";

            LOGGER.info("SQL :: " + sql);
            query = em.createNativeQuery(sql, "cobroCustomEntity");

            result = query.getResultList().size();
        } catch (Exception e) {
            LOGGER.info("Error tratando de obtener la cantidad total de records :: " + e);
        }
        return result;
    }

}
