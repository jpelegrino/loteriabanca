/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sib.ejb.message;


import com.sib.bs.responses.GeneralResponse;
import com.sib.bs.services.LocateService;
import com.sib.ejb.dao.CorreoSibDao;
import com.sib.ejb.dao.GeneralDao;
import com.sib.entity.DetalleGeneral;
import com.sib.entity.General;
import com.sib.util.Plano;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;

/**
 *
 * @author julio
 */
@MessageDriven(mappedName = "jms/sibQueue",activationConfig = {
    @ActivationConfigProperty(propertyName = "acknowledgeMode",
        propertyValue = "Auto-acknowledge")
})
public class PlanoMDB implements MessageListener{

    @EJB
    private CorreoSibDao correoSibDao;
    
    @Override
    public void onMessage(Message message) {
        
         try {
            TextMessage tm=(TextMessage) message;
            // Se implementara el envio de correo por esta via.
            
            String msg=(String) tm.getText();
            //correoSibDao.sendCorreo("jpelegrino@gmail.com", "Estatus del parseado", msg);
             System.out.println("JULIO ---->>> "+ msg);
           
            
        } catch (Exception e) {
             System.out.println("MDB: "+ e.getMessage());
        }
        
    }
    
}