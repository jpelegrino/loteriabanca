/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import com.sib.ui.models.CompaniaModel;
import com.sib.ui.models.ConsorcioModel;

/**
 *
 * @author Snailin Inoa
 */
public class Test {

    public static void main(String[] args) {
        // Esto es un comentario
        // Algo mas...

        CompaniaModel companiaModel = new CompaniaModel();
        companiaModel.setId(1);
        companiaModel.setCompaniaText("Compania Text");
        ConsorcioModel consorcioModel = new ConsorcioModel();
        consorcioModel.setId(1);
        consorcioModel.setStatus('A');
        consorcioModel.setConsorcioText("Description for Concorcio");
        consorcioModel.setCompaniaModel(companiaModel);

        System.out.println("consorcioModel ==>" + consorcioModel);
        //   Connection
    }
}
