package com.sib.interfaces;

import javax.faces.event.ActionEvent;

/**
 *
 * @author Snailin Inoa
 */
public interface IBean {

    public void resetBean();

    public String getFileName();
    
    public void navigate(ActionEvent event);
}
