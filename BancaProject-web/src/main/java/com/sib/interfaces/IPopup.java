package com.sib.interfaces;

import javax.faces.event.ActionEvent;

/**
 *
 * @author Snailin Inoa
 */
public interface IPopup {

    public void execute(ActionEvent event);

    public void cancelProcess(ActionEvent event);

    public void reset();

    public boolean validateFields();

    public String getFileName();
    
    public String getPopupTitle();

}
