package com.sib.gasto.agregar;

import com.sib.gasto.GastoConstantes;
import com.sib.gasto.GastoModelBean;
import com.sib.interfaces.IPopup;
import com.sib.ui.enums.UIOperationResultEnum;
import com.sib.ui.models.BancaModel;
import com.sib.ui.models.GastoModel;
import com.sib.ui.models.ResponseModel;
import com.sib.util.DateFormatUtil;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "gastoAgregarPopupBean")
@ViewScoped
public class GastoAgregarPopup implements IPopup, Serializable {

    public static final Logger LOG = Logger.getLogger(GastoAgregarPopup.class.getName());

    private Date placeholderDate;
    private Long placeholderGasto;
    private String placeholderMonto;

    private BancaModel[] selectedBancas;

    @ManagedProperty(value = "#{gastoModelBean}")
    private GastoModelBean modelBean;

    private GastoModel model;

    public GastoAgregarPopup() {
    }

    public Date getPlaceholderDate() {
        return placeholderDate;
    }

    public void setPlaceholderDate(Date placeholderDate) {
        this.placeholderDate = placeholderDate;
    }

    public Long getPlaceholderGasto() {
        return placeholderGasto;
    }

    public void setPlaceholderGasto(Long placeholderGasto) {
        this.placeholderGasto = placeholderGasto;
    }

    public String getPlaceholderMonto() {
        return placeholderMonto;
    }

    public void setPlaceholderMonto(String placeholderMonto) {
        this.placeholderMonto = placeholderMonto;
    }

    @Override
    public void execute(ActionEvent event) {
        if (validateFields()) {
            try {

                String dateInString;

                if (placeholderDate != null) {
                    dateInString
                            = DateFormatUtil.
                                    convertDateToStringWithStandardFormat(
                                            this.placeholderDate);
                } else {
                    dateInString 
                            = DateFormatUtil.
                                    convertDateToStringWithStandardFormat(
                                            new Date());
                }

                boolean finalResult = false;

                for (BancaModel bm : this.selectedBancas) {
                    ResponseModel response = this.modelBean.agregarGasto(bm.getId(),
                            Integer.parseInt(String.valueOf(placeholderGasto)),
                            Double.valueOf(placeholderMonto), dateInString);
                    finalResult = (response != null && response.getWasSuccessful());
                }

                if (finalResult) {
                    prepararStatusOperacion(UIOperationResultEnum.SUCCESS);
                    MessageDisplayUtils.INSTANCE.printInfoMessageFromBundle(
                            GastoConstantes.AGREGAR_TABLA_MESSAGE_CLIENT_ID,
                            "sab.gastos.agregar.gasto.success");
                } else {
                    prepararStatusOperacion(UIOperationResultEnum.FAILURE);
                    MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(
                            GastoConstantes.POPUP_MESSAGE_CLIENT_ID,
                            "sab.gastos.agregar.gasto.error");
                }

            } catch (Exception ex) {
                LOG.error("Error creando gastos."
                        + ex.getMessage(), ex);
            }
        }
    }

    @Override
    public boolean validateFields() {
        boolean result = true;

        if (placeholderGasto == null || placeholderGasto <= 0) {
            MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(
                    GastoConstantes.POPUP_MESSAGE_CLIENT_ID,
                    "sab.error.campo.vacio");
            result = false;
        } else if (!String.valueOf(placeholderMonto).matches("\\d+\\.\\d{1,2}?")
                && !String.valueOf(placeholderMonto).matches("^[0-9]+$")) {
            MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(
                    GastoConstantes.POPUP_MESSAGE_CLIENT_ID,
                    "sab.gasto.monto");
            result = false;
        }
        return result;
    }

    public GastoModel getModel() {
        return model;
    }

    public void setModel(GastoModel model) {
        this.model = model;
    }

    public BancaModel[] getSelectedBancas() {
        return selectedBancas;
    }

    public void setSelectedBancas(BancaModel[] selectedBancas) {
        this.selectedBancas = selectedBancas;
    }

    public GastoModelBean getModelBean() {
        return modelBean;
    }

    public void setModelBean(GastoModelBean modelBean) {
        this.modelBean = modelBean;
    }

    @Override
    public String getFileName() {
        return "gastoagregarpopup";
    }

    @Override
    public String getPopupTitle() {
        return MessageDisplayUtils.INSTANCE.
                getMessageFromBundle("sab.agregar.gasto");
    }

    public Logger getLogger() {
        return LOG;
    }

    protected void prepararStatusOperacion(UIOperationResultEnum operationStatus) {
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("operationStatus", operationStatus.getStatusLiteral());
    }

    @Override
    public void cancelProcess(ActionEvent event) {
        reset();
    }

    @Override
    public void reset() {
        this.setPlaceholderGasto(0L);
        this.setPlaceholderMonto("");
        this.setPlaceholderDate(null);
    }
}
