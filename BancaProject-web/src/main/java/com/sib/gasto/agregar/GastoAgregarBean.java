package com.sib.gasto.agregar;

import com.sib.gasto.AbstractGastoBean;
import com.sib.gasto.GastoConstantes;
import com.sib.interfaces.IPopup;
import com.sib.ui.models.BancaModel;
import com.sib.util.MessageDisplayUtils;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.ToggleSelectEvent;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "gastoAgregarBean")
@ViewScoped
public class GastoAgregarBean extends AbstractGastoBean {

    public static final Logger LOG = Logger.getLogger(GastoAgregarBean.class.getName());

    private IPopup popupHandler;
    private List<BancaModel> bancasGrupoSeleccionado;

    @ManagedProperty(value = "#{gastoAgregarPopupBean}")
    private IPopup createPopup;

    private BancaModel[] selectedBancas;

    public GastoAgregarBean() {

    }

    public IPopup getPopupHandler() {
        if (this.popupHandler == null) {
            this.popupHandler = createPopup;
        }
        return popupHandler;
    }

    public BancaModel[] getSelectedBancas() {
        return selectedBancas;
    }

    public void setSelectedBancas(BancaModel[] selectedBancas) {
        this.selectedBancas = selectedBancas;
    }

    public void setPopupHandler(IPopup popupHandler) {
        this.popupHandler = popupHandler;
    }

    public IPopup getCreatePopup() {
        return createPopup;
    }

    public void setCreatePopup(IPopup createPopup) {
        this.createPopup = createPopup;
    }

    public void onGrupoSelectionChange() {
        this.bancasGrupoSeleccionado = null;
    }

    public void openCreatePopup(ActionEvent event) {

        LOG.info("/****************/");
        LOG.info("Selected Bancas:" + this.selectedBancas);
        LOG.info("/****************/");
        
        if (this.selectedBancas == null || this.selectedBancas.length <= 0) {
            MessageDisplayUtils.INSTANCE.
                    printErrorMessageFromBundle(GastoConstantes.AGREGAR_TABLA_MESSAGE_CLIENT_ID,
                            "sab.gasto.agregar.seleccione.banca");
        } else {
            this.setPopupHandler(this.createPopup);

            GastoAgregarPopup gastoAgrePopup = (GastoAgregarPopup) this.createPopup;
            gastoAgrePopup.setSelectedBancas(selectedBancas);
            gastoAgrePopup.reset();

            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("operationStatus", "000");
        }
    }

    public List<BancaModel> getBancasGrupoSeleccionado() {

        if (bancasGrupoSeleccionado == null) {
            int grupoId = this.getModelBean().getInputBean().getGrupoId();
            bancasGrupoSeleccionado
                    = this.getBancaModelBean().getBancasDeGrupo(grupoId);
        }

        return bancasGrupoSeleccionado;
    }

    public void toggleSelection(ToggleSelectEvent toggleEvent) {
        System.out.println("/***************************/");
        System.out.println("/***************************/");
        System.out.println("TOGGLE SELECT EVENT :: ");
        System.out.println("/***************************/");
        System.out.println("/***************************/");
    }
}
