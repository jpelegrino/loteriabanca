package com.sib.gasto;

import com.sib.entity.Gasto;
import com.sib.ui.models.GastoModel;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
/**
 * This is the Gasto <-> GastoModel objects converter helper class.
 *
 * @author Omar Moronta
 */
public class GastoUtil implements Serializable{
    
    public static final Logger LOG = Logger.getLogger(GastoUtil.class.getName()); 
    
    public GastoModel crearGastoModelDeGastoModel(GastoModel gm){
        GastoModel ngm=null;
    
        if(gm!=null){
            byte[] bytesArr = SerializationUtils.serialize(gm);
            ngm = (GastoModel)SerializationUtils.deserialize(bytesArr);
        }
        
        return ngm;
    }
    
    public GastoModel convertirGastoAGastoModel(Gasto gasto){
        GastoModel gm = null;
        
        if(gasto!=null){
            byte[] bytesArr = SerializationUtils.serialize(gasto);
            Gasto gp = (Gasto)SerializationUtils.deserialize(bytesArr);
            
            if(gp!=null){
                try {
                    gm = new GastoModel();
                    BeanUtils.copyProperties(gm, gp);
                } catch (IllegalAccessException ex) {
                    LOG.error(ex.getMessage(), ex);
                } catch (InvocationTargetException ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }
        
        return gm;
    }
    
    public Gasto convertirGastoModelAGasto(GastoModel gm){
        Gasto gp = null;
        
        if(gm!=null){
            byte[] bytesArr = SerializationUtils.serialize(gm);
            GastoModel ngm = (GastoModel)SerializationUtils.deserialize(bytesArr);
            
            if(ngm!=null){
                try {
                    gp = new Gasto();
                    BeanUtils.copyProperties(gp, ngm);
                } catch (IllegalAccessException ex) {
                    LOG.error(ex.getMessage(), ex);
                } catch (InvocationTargetException ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }
        
        return gp;
    }    
}
