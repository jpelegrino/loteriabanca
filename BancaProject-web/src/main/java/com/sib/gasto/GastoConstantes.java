package com.sib.gasto;

import java.io.Serializable;

/**
 *
 * @author Omar Moronta
 */
public class GastoConstantes implements Serializable {
    public static final String TABLA_MESSAGE_CLIENT_ID = "gastomantmsg";
    public static final String POPUP_MESSAGE_CLIENT_ID = "gastoagrdlg";
    public static final String AGREGAR_TABLA_MESSAGE_CLIENT_ID = "gastoagremantmsg";    
}
