package com.sib.gasto;

import com.sib.util.DateFormatUtil;
import java.io.Serializable;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "gastoInputBean")
@ViewScoped
public class GastoInputBean implements Serializable {

    private int grupoId;
    private long tipoGastoId;
    private int bancaId;
    private String codigoBanca;
    private Date initDate;
    private Date finalDate;

    public GastoInputBean() {

    }

    public int getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(int grupoId) {
        this.grupoId = grupoId;
    }

    public long getTipoGastoId() {
        return tipoGastoId;
    }

    public void setTipoGastoId(long tipoGastoId) {
        this.tipoGastoId = tipoGastoId;
    }

    public String getCodigoBanca() {
        return codigoBanca;
    }

    public void setCodigoBanca(String codigoBanca) {
        this.codigoBanca = codigoBanca;
    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public String getFormattedInitDate() {
        String result = "";
        if (initDate != null) {
            result = DateFormatUtil.
                    convertDateToStringWithStandardFormat(
                            this.getInitDate());
        }

        return result;
    }

    public Date getFinalDate() {
        return finalDate;
    }

    public void setFinalDate(Date finalDate) {
        this.finalDate = finalDate;
    }

    public int getBancaId() {
        return bancaId;
    }

    public void setBancaId(int bancaId) {
        this.bancaId = bancaId;
    }   
    
    public String getFormattedFinalDate() {
        String result = "";
        if (finalDate != null) {
            result = DateFormatUtil.
                    convertDateToStringWithStandardFormat(
                            this.getFinalDate());
        }

        return result;
    }

    public void reset() {
        this.setGrupoId(0);
        this.setBancaId(0);
        this.setCodigoBanca("");
        this.setInitDate(null);
        this.setFinalDate(null);
        this.setTipoGastoId(0L);
    }
}
