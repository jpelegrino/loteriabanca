package com.sib.gasto;

import com.sib.bs.responses.GastoResponse;
import com.sib.bs.responses.MessageEnum;
import com.sib.ejb.dao.GastoDao;
import com.sib.entity.Gasto;
import com.sib.ui.models.GastoModel;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.util.CollectionUtils;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "gastoModelBean")
@ViewScoped
public class GastoModelBean extends LazyDataModel implements Serializable {

    public static final Logger LOG
            = Logger.getLogger(GastoModelBean.class.getName());

    private boolean isCountVerified;
    private List<GastoModel> result;

    @EJB
    private GastoDao gastoDao;

    @ManagedProperty(value = "#{gastoInputBean}")
    private GastoInputBean inputBean;

    private final GastoUtil gastoUtil;

    public GastoModelBean() {
        gastoUtil = new GastoUtil();
    }

    public GastoInputBean getInputBean() {
        return inputBean;
    }

    public void setInputBean(GastoInputBean inputBean) {
        this.inputBean = inputBean;
    }

    @Override
    public List<GastoModel> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map filters) {

        int datasetSize = this.getDatasetSizeGastos();

        if (datasetSize > 0) {

            GastoResponse out
                    = this.gastoDao.getGastos(inputBean.getFormattedInitDate(),
                            inputBean.getFormattedFinalDate(),
                            inputBean.getGrupoId(), inputBean.getBancaId(),
                            inputBean.getTipoGastoId(),
                            StringUtils.isBlank(sortField) ? "id" : sortField,
                            SortOrder.ASCENDING.equals(sortOrder) ? "ASC" : "DESC",
                            first, pageSize);

            if (out != null && !CollectionUtils.isEmpty(out.getGastos())) {
                result = new ArrayList<GastoModel>();

                this.setRowCount(datasetSize);

                GastoModel gm;

                for (Gasto g : out.getGastos()) {
                    gm = gastoUtil.convertirGastoAGastoModel(g);
                    result.add(gm);
                }
            } else {
                result = new ArrayList<GastoModel>();
                this.setRowCount(0);
            }
        } else {
            result = new ArrayList<GastoModel>();
            this.setRowCount(0);
        }

        return result;
    }

    private int getDatasetSizeGastos() {
        int datasetSize = 0;

        if (!isCountVerified && datasetSize == 0) {
            try {

                LOG.info("GRUPO ID::" + inputBean.getGrupoId());
                LOG.info("Tipo Gasto::" + inputBean.getTipoGastoId());
                LOG.info("Banca::" + inputBean.getCodigoBanca());
                LOG.info("Start Date::" + inputBean.getFormattedInitDate());
                LOG.info("End Date::" + inputBean.getFormattedFinalDate());

                datasetSize = this.gastoDao.getGastoTotalRecords(
                        inputBean.getFormattedInitDate(),
                        inputBean.getFormattedFinalDate(),
                        inputBean.getGrupoId(), inputBean.getBancaId(),
                        inputBean.getTipoGastoId(),
                        "", "");
            } catch (Exception ex) {
                LOG.error("Error retrieving Gastos datasetsize. Error: "
                        + ex.getMessage(), ex);
            }
        }

        return datasetSize;
    }

    public ResponseModel agregarGasto(int bancaId, int tipoGastoId, double monto,
            String fechaEfectiva) {
        ResponseModel resp = new ResponseModel();

        try {
            GastoResponse out = this.gastoDao.crearGasto(bancaId, tipoGastoId, monto, fechaEfectiva);

            if (out != null) {
                if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                    resp.setWasSuccessful(true);
                    resp.setMessage(this.getMessageFromBundle("sab.gastos.agregar.gasto.success"));
                } else {
                    resp.setWasSuccessful(false);
                    resp.setMessage(this.getMessageFromBundle("sab.gastos.agregar.gasto.error"));
                    LOG.error("Ha ocurrido un error creando gasto. "
                            + "Message Enum equals:>" + out.getMsgEnum());
                }
            }
        } catch (Exception ex) {
            resp.setWasSuccessful(false);
            resp.setMessage(this.getMessageFromBundle("sab.gastos.agregar.gasto.error"));
            LOG.error("Ha ocurrido un error creando gasto."
                    + ex.getMessage(), ex);
        }
        return resp;
    }

    public ResponseModel eliminarGasto(int gastoId) {
        ResponseModel resp = new ResponseModel();

        try {

            GastoResponse gastoResp = this.gastoDao.getGastoById(gastoId);

            if (gastoResp != null && gastoResp.getMsgEnum().equals(MessageEnum.SUCCESS)) {

                GastoResponse out = this.gastoDao.deleteGasto(gastoResp.getGasto());

                if (out != null) {
                    if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                        resp.setWasSuccessful(true);
                        resp.setMessage(this.getMessageFromBundle("sab.gastos.eliminar.gasto.success"));
                    } else {
                        resp.setWasSuccessful(false);
                        resp.setMessage(this.getMessageFromBundle("sab.gastos.eliminar.gasto.error"));
                        LOG.error("Ha ocurrido un error eliminando gasto. "
                                + "Message Enum equals:>" + out.getMsgEnum());
                    }
                }
            } else {
                resp.setWasSuccessful(false);
                resp.setMessage(this.getMessageFromBundle("sab.gastos.eliminar.gasto.error"));
                LOG.error("Ha ocurrido un error eliminando gasto. "
                        + "Input Gasto is null.");
            }
        } catch (Exception ex) {
            resp.setWasSuccessful(false);
            resp.setMessage(this.getMessageFromBundle("sab.gastos.eliminar.gasto.error"));
            LOG.error("Ha ocurrido un error eliminando gasto."
                    + ex.getMessage(), ex);
        }
        return resp;
    }

    protected String getMessageFromBundle(String msgproperty, Object... params) {
        return MessageDisplayUtils.INSTANCE.
                getMessageFromBundle(msgproperty, params);
    }

    public void reset() {
        isCountVerified = false;
        result = null;
    }
}
