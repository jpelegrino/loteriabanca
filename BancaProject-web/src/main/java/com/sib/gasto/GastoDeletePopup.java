package com.sib.gasto;

import com.sib.interfaces.IPopup;
import com.sib.ui.enums.UIOperationResultEnum;
import com.sib.ui.models.GastoModel;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "gastoDeletePopup")
@ViewScoped
public class GastoDeletePopup implements IPopup, Serializable {

    public static final Logger LOG = Logger.getLogger(GastoDeletePopup.class.getName());

    private GastoModel selectedGasto;

    @ManagedProperty(value = "#{gastoModelBean}")
    private GastoModelBean modelBean;

    public GastoModelBean getModelBean() {
        return modelBean;
    }

    public void setModelBean(GastoModelBean modelBean) {
        this.modelBean = modelBean;
    }

    public GastoModel getSelectedGasto() {
        return selectedGasto;
    }

    public void setSelectedGasto(GastoModel selectedGasto) {
        this.selectedGasto = selectedGasto;
    }

    @Override
    public void execute(ActionEvent event) {
        try {
            ResponseModel response
                    = this.modelBean.eliminarGasto(this.selectedGasto.getId());

            if (response != null && response.getWasSuccessful()) {
                prepararStatusOperacion(UIOperationResultEnum.SUCCESS);
                this.modelBean.reset();
                MessageDisplayUtils.INSTANCE.printInfoMessage(
                        null, response.getMessage());
            } else if(response!=null) {
                prepararStatusOperacion(UIOperationResultEnum.FAILURE);
                MessageDisplayUtils.INSTANCE.printErrorMessage(
                        GastoConstantes.POPUP_MESSAGE_CLIENT_ID,
                        response.getMessage());
            }
        } catch (Exception ex) {
            LOG.error("Error creando gastos."
                    + ex.getMessage(), ex);
        }
    }

    protected void prepararStatusOperacion(UIOperationResultEnum operationStatus) {
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("operationStatus", operationStatus.getStatusLiteral());
    }

    @Override
    public void cancelProcess(ActionEvent event) {
        reset();
    }

    @Override
    public boolean validateFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getFileName() {
        return "gastoeliminarpopup";
    }

    @Override
    public String getPopupTitle() {
        return MessageDisplayUtils.INSTANCE.
                getMessageFromBundle("sab.eliminar.gasto");
    }

    @Override
    public void reset() {

    }

}
