package com.sib.gasto;

import com.sib.banca.BancaModelBean;
import com.sib.grupo.GrupoModelBean;
import com.sib.tipo.gasto.TipoGastoModelBean;
import com.sib.ui.enums.StatusEnum;
import com.sib.ui.models.GrupoModel;
import com.sib.ui.models.TipoGastoModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedProperty;
import javax.faces.model.SelectItem;

/**
 *
 * @author Omar Moronta
 */
public class AbstractGastoBean implements Serializable {

    private List<SelectItem> gruposSI;
    private List<SelectItem> tipoGastosSI;   
    
    @ManagedProperty(value = "#{gastoModelBean}")
    private GastoModelBean modelBean;

    @ManagedProperty(value = "#{grupoModelBean}")
    private GrupoModelBean grupoModelBean;

    @ManagedProperty(value = "#{bancaModelBean}")
    private BancaModelBean bancaModelBean;

    @ManagedProperty(value = "#{tipoGastoModelBean}")
    private TipoGastoModelBean tipoGastoModelBean;    
    
    public GastoModelBean getModelBean() {
        return modelBean;
    }

    public void setModelBean(GastoModelBean modelBean) {
        this.modelBean = modelBean;
    }

    public GrupoModelBean getGrupoModelBean() {
        return grupoModelBean;
    }

    public void setGrupoModelBean(GrupoModelBean grupoModelBean) {
        this.grupoModelBean = grupoModelBean;
    }

    public BancaModelBean getBancaModelBean() {
        return bancaModelBean;
    }

    public void setBancaModelBean(BancaModelBean bancaModelBean) {
        this.bancaModelBean = bancaModelBean;
    }

    public TipoGastoModelBean getTipoGastoModelBean() {
        return tipoGastoModelBean;
    }

    public void setTipoGastoModelBean(TipoGastoModelBean tipoGastoModelBean) {
        this.tipoGastoModelBean = tipoGastoModelBean;
    }

    public List<SelectItem> getTipoGastosSI() {
        if(tipoGastosSI==null){
            List<TipoGastoModel> tipoGastos = 
                    this.tipoGastoModelBean.getResult();
            
            tipoGastosSI = new ArrayList<SelectItem>();
            
            if(tipoGastos!=null){
                for(TipoGastoModel tgm : tipoGastos){
                    tipoGastosSI.add(new SelectItem(tgm.getId(), tgm.getDescripcion()));
                }
            }
        }
        return tipoGastosSI;
    }

    public void setTipoGastosSI(List<SelectItem> tipoGastosSI) {
        this.tipoGastosSI = tipoGastosSI;
    }

    public List<SelectItem> getGruposSI() {
        if (gruposSI == null) {
            List<GrupoModel> gruposActivos
                    = this.grupoModelBean.getGruposByStatus(StatusEnum.ACTIVO.getStatusLiteral());
            gruposSI = new ArrayList<SelectItem>();

            if (gruposActivos != null) {
                for (GrupoModel gm : gruposActivos) {
                    gruposSI.add(new SelectItem(gm.getId(), gm.getCodigo()));
                }
            }
        }
        return gruposSI;
    }

    public void setGruposSI(List<SelectItem> gruposSI) {
        this.gruposSI = gruposSI;
    }
}
