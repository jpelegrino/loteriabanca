package com.sib.gasto;

import com.sib.interfaces.IPopup;
import com.sib.ui.models.BancaModel;
import com.sib.ui.models.GastoModel;
import com.sib.util.MessageDisplayUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "gastoBean")
@ViewScoped
public class GastoBean extends AbstractGastoBean {

    public static final Logger LOG = Logger.getLogger(GastoBean.class.getName());

    private IPopup popupHandler;

    @ManagedProperty(value = "#{gastoDeletePopup}")
    private IPopup deletePopup;

    private List<String> bancasSI;

    public List<String> bancaAutocomplete(String query) {
        List<String> results = new ArrayList<String>();
        this.bancasSI = null;

        for (String s : this.getBancasSI()) {
            if (s.startsWith(query)) {
                results.add(s);
            }
        }

        return results;
    }

    public List<String> getBancasSI() {
        int grupoId = this.getModelBean().getInputBean().getGrupoId();
        List<BancaModel> bancasDeGrupo
                = this.getBancaModelBean().getBancasDeGrupo(grupoId);
        bancasSI = new ArrayList<String>();

        if (bancasDeGrupo != null) {
            for (BancaModel gm : bancasDeGrupo) {
                bancasSI.add(gm.getCodigo());
            }
        }
        return bancasSI;
    }

    public IPopup getPopupHandler() {
        if (popupHandler == null) {
            popupHandler = deletePopup;
        }
        return popupHandler;
    }

    public void setPopupHandler(IPopup popupHandler) {
        this.popupHandler = popupHandler;
    }

    public IPopup getDeletePopup() {
        return deletePopup;
    }

    public void setDeletePopup(IPopup deletePopup) {
        this.deletePopup = deletePopup;
    }

    public void setBancasSI(List<String> bancasSI) {
        this.bancasSI = bancasSI;
    }

    public void executeSearch(ActionEvent event) {

        String codigoBanca = this.getModelBean().
                getInputBean().getCodigoBanca();

        if (this.getModelBean().getInputBean().getInitDate() != null
                && this.getModelBean().getInputBean().getFinalDate() != null
                && this.getModelBean().getInputBean().getFinalDate().
                        before(this.getModelBean().getInputBean().getInitDate())) {
            MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(null,"sab.reporte.error.date");
        } else {
            if (!StringUtils.isBlank(codigoBanca)) {
                int grupoId = this.getModelBean().getInputBean().getGrupoId();
                boolean found = false;

                List<BancaModel> bancasDeGrupo
                        = this.getBancaModelBean().getBancasDeGrupo(grupoId);
                bancasSI = new ArrayList<String>();

                if (bancasDeGrupo != null) {
                    for (BancaModel gm : bancasDeGrupo) {
                        if (gm.getCodigo().equals(codigoBanca)) {
                            this.getModelBean().getInputBean().setBancaId(gm.getId());
                            found = true;
                            break;
                        }
                    }

                    if (!found) {
                        this.getModelBean().getInputBean().setBancaId(-2);
                    }
                }
            } else {
                this.getModelBean().getInputBean().setBancaId(0);
            }

            this.getModelBean().reset();

        }
    }

    public void limpiarFiltro(ActionEvent event) {
        this.getModelBean().reset();
        this.getModelBean().getInputBean().reset();
    }

    public void openDeleteGasto(GastoModel gm) {
        this.popupHandler = this.deletePopup;
        ((GastoDeletePopup) this.deletePopup).setSelectedGasto(gm);
    }
}
