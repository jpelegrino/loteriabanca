package com.sib.reportes.bygrupo.common;

import com.sib.grupo.GrupoModelBean;
import com.sib.ui.enums.StatusEnum;
import com.sib.ui.models.GrupoModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedProperty;
import javax.faces.model.SelectItem;

/**
 *
 * @author Omar Moronta
 */
public abstract class GrupoIdBasedAbstractBean implements Serializable {

    int selectedGrupo;
    private List<SelectItem> gruposSI;

    @ManagedProperty(value = "#{grupoModelBean}")
    private GrupoModelBean grupoModelBean;

    public int getSelectedGrupo() {
        return selectedGrupo;
    }

    public void setSelectedGrupo(int selectedGrupo) {
        this.selectedGrupo = selectedGrupo;
    }

    public GrupoModelBean getGrupoModelBean() {
        return grupoModelBean;
    }

    public void setGrupoModelBean(GrupoModelBean grupoModelBean) {
        this.grupoModelBean = grupoModelBean;
    }

    public List<SelectItem> getGruposSI() {
        if (gruposSI == null) {
            List<GrupoModel> gruposActivos
                    = this.grupoModelBean.getGruposByStatus(StatusEnum.ACTIVO.getStatusLiteral());
            gruposSI = new ArrayList<SelectItem>();

            if (gruposActivos != null) {
                for (GrupoModel gm : gruposActivos) {
                    gruposSI.add(new SelectItem(gm.getId(), gm.getCodigo()));
                }
            }
        }
        return gruposSI;
    }

    public void setGruposSI(List<SelectItem> gruposSI) {
        this.gruposSI = gruposSI;
    }

    public abstract void onGrupoSelectionChange();
}
