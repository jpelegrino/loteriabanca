package com.sib.reportes.bygrupo.montoporcentaje;

import com.sib.reportes.bygrupo.gananciaperdida.*;
import com.sib.bs.responses.GananciaPerdidaResponse;
import com.sib.bs.responses.MessageEnum;
import com.sib.bs.responses.PorcentajeResponse;
import com.sib.ejb.dao.DetalleGeneralDao;
import com.sib.entity.GananciaPerdida;
import com.sib.entity.Porcentajes;
import com.sib.interfaces.IModelBean;
import com.sib.ui.models.GananciaPerdidaModel;
import com.sib.ui.models.PorcentajesModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "montoPorcentajeModelBean")
@ViewScoped
public class MontoPorcentajeModelBean implements IModelBean, Serializable {

    private List<PorcentajesModel> montoPorcentajeList;

    @EJB
    private DetalleGeneralDao detalleGeneralDao;

    public static final Logger LOG
            = Logger.getLogger(RepGananciaPerdidaModelBean.class.getName());

    @ManagedProperty(value = "#{montoPorcentajeInputBean}")
    private MontoPorcentajeInputBean inputBean;

    public MontoPorcentajeInputBean getInputBean() {
        return inputBean;
    }

    public void setInputBean(MontoPorcentajeInputBean inputBean) {
        this.inputBean = inputBean;
    }

    public List<PorcentajesModel> getMontoPorcentajeList() {
        if (montoPorcentajeList == null) {
            init();
        }
        return montoPorcentajeList;
    }

    private void init() {
        Integer grupoId = this.inputBean.getGrupoId();
        
        PorcentajeResponse out
                = detalleGeneralDao.getMontoPorcentajeByGrupo(grupoId == null ? 0 : grupoId.intValue());

        montoPorcentajeList = new ArrayList<PorcentajesModel>();

        if (out != null) {

            if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                List<Porcentajes> orig = out.getPorcentajes();

                if (orig != null) {
                    PorcentajesUtil porcUtil = new PorcentajesUtil();
                    for (Porcentajes j : orig) {
                        PorcentajesModel cur = porcUtil.convertirPorcAPorcModel(j);
                        montoPorcentajeList.add(cur);
                    }
                }
            } else {
                this.displayErrorMessageFromBundle("sab.reportes.bygrupo.porc.lbl",
                        RepGananciaPerdidaConstantes.TABLA_MESSAGE_CLIENT_ID);
            }
        }

    }

    @Override
    public void resetModelBean() {
        LOG.info("Reseting modelbean ");
        this.montoPorcentajeList = null;
    }

    protected void displayErrorMessageFromBundle(String msgproperty, String msgClientId) {
        MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(msgClientId,
                msgproperty);
    }
}
