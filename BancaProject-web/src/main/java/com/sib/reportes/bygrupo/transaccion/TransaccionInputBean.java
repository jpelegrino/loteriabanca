package com.sib.reportes.bygrupo.transaccion;

import com.sib.reportes.bygrupo.common.GrupoIdBasedAbstractInputBean;
import com.sib.util.DateFormatUtil;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "transaccionInputBean")
@ViewScoped
public class TransaccionInputBean extends GrupoIdBasedAbstractInputBean {

    private Date initDate;
    private Date finalDate;
    private int bancaId;
    private String codigoBanca;

    public TransaccionInputBean() {
    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public Date getFinalDate() {
        return finalDate;
    }

    public void setFinalDate(Date finalDate) {
        this.finalDate = finalDate;
    }

    public int getBancaId() {
        return bancaId;
    }

    public void setBancaId(int bancaId) {
        this.bancaId = bancaId;
    }

    public String getCodigoBanca() {
        return codigoBanca;
    }

    public void setCodigoBanca(String codigoBanca) {
        this.codigoBanca = codigoBanca;
    }

    public String getFormattedInitDate() {
        String result = "";
        if (initDate != null) {
            result = DateFormatUtil.
                    convertDateToStringWithStandardFormat(
                            this.getInitDate());
        }

        return result;
    }

    public String getFormattedFinalDate() {
        String result = "";
        if (finalDate != null) {
            result = DateFormatUtil.
                    convertDateToStringWithStandardFormat(
                            this.getFinalDate());
        }

        return result;
    }

    public void reset() {
        this.setGrupoId(0);
        this.setBancaId(0);
        this.setCodigoBanca("");
        this.setInitDate(null);
        this.setFinalDate(null);
    }
}
