package com.sib.reportes.bygrupo.transaccion;

import com.sib.banca.BancaModelBean;
import com.sib.reportes.bygrupo.common.GrupoIdBasedAbstractBean;
import com.sib.ui.models.BancaModel;
import com.sib.util.MessageDisplayUtils;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "transaccionBean")
@ViewScoped
public class TransaccionBean extends GrupoIdBasedAbstractBean {

    public static final Logger LOG = Logger.getLogger(TransaccionBean.class.getName());

    @ManagedProperty(value = "#{transaccionModelBean}")
    private TransaccionModelBean modelBean;

    @ManagedProperty(value = "#{bancaModelBean}")
    private BancaModelBean bancaModelBean;

    private List<String> bancasSI;

    public TransaccionBean() {

    }

    public BancaModelBean getBancaModelBean() {
        return bancaModelBean;
    }

    public void setBancaModelBean(BancaModelBean bancaModelBean) {
        this.bancaModelBean = bancaModelBean;
    }

    public List<String> bancaAutocomplete(String query) {
        List<String> results = new ArrayList<String>();
        this.bancasSI = null;

        for (String s : this.getBancasSI()) {
            if (s.startsWith(query)) {
                results.add(s);
            }
        }

        return results;
    }

    public TransaccionModelBean getModelBean() {
        return modelBean;
    }

    public void setModelBean(TransaccionModelBean modelBean) {
        this.modelBean = modelBean;
    }

    public List<String> getBancasSI() {
        int grupoId = this.getModelBean().getInputBean().getGrupoId();
        List<BancaModel> bancasDeGrupo
                = this.getBancaModelBean().getBancasDeGrupo(grupoId);
        bancasSI = new ArrayList<String>();

        if (bancasDeGrupo != null) {
            for (BancaModel gm : bancasDeGrupo) {
                bancasSI.add(gm.getCodigo());
            }
        }
        return bancasSI;
    }

    public void executeSearch(ActionEvent event) {

        String codigoBanca = this.getModelBean().
                getInputBean().getCodigoBanca();

        if (this.getModelBean().getInputBean().getInitDate() != null
                && this.getModelBean().getInputBean().getFinalDate() != null
                && this.getModelBean().getInputBean().getFinalDate().
                before(this.getModelBean().getInputBean().getInitDate())) {
            MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(null,"sab.reporte.error.date");
        } else {

            if (!StringUtils.isBlank(codigoBanca)) {
                int grupoId = this.getModelBean().getInputBean().getGrupoId();
                boolean found = false;

                List<BancaModel> bancasDeGrupo
                        = this.getBancaModelBean().getBancasDeGrupo(grupoId);
                bancasSI = new ArrayList<String>();

                if (bancasDeGrupo != null) {
                    for (BancaModel gm : bancasDeGrupo) {
                        if (gm.getCodigo().equals(codigoBanca)) {
                            this.getModelBean().getInputBean().setBancaId(gm.getId());
                            found = true;
                            break;
                        }
                    }

                    if (!found) {
                        this.getModelBean().getInputBean().setBancaId(-2);
                    }
                }
            } else {
                this.getModelBean().getInputBean().setBancaId(0);
            }

            this.getModelBean().resetModelBean();
        }
    }

    public void limpiarFiltro(ActionEvent event) {
        this.getModelBean().resetModelBean();
        this.getModelBean().getInputBean().reset();
    }

    @Override
    public void onGrupoSelectionChange() {
        this.modelBean.resetModelBean();
    }
}
