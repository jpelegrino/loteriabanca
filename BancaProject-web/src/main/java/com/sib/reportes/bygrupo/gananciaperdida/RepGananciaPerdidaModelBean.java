package com.sib.reportes.bygrupo.gananciaperdida;

import com.sib.bs.responses.GananciaPerdidaResponse;
import com.sib.bs.responses.MessageEnum;
import com.sib.ejb.dao.DetalleGeneralDao;
import com.sib.entity.GananciaPerdida;
import com.sib.interfaces.IModelBean;
import com.sib.ui.models.GananciaPerdidaModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "repGananciaPerdidaModelBean")
@ViewScoped
public class RepGananciaPerdidaModelBean implements IModelBean, Serializable {

    private List<GananciaPerdidaModel> gananciaPerdidaList;

    @EJB
    private DetalleGeneralDao detalleGeneralDao;

    public static final Logger LOG
            = Logger.getLogger(RepGananciaPerdidaModelBean.class.getName());

    @ManagedProperty(value = "#{repGananciaPerdidaInputBean}")
    private RepGananciaPerdidaInputBean inputBean;

    public RepGananciaPerdidaInputBean getInputBean() {
        return inputBean;
    }

    public void setInputBean(RepGananciaPerdidaInputBean inputBean) {
        this.inputBean = inputBean;
    }

    public List<GananciaPerdidaModel> getGananciaPerdidaList() {
        if (gananciaPerdidaList == null) {
            init();
        }
        return gananciaPerdidaList;
    }

    private void init() {
        Integer grupoId = this.inputBean.getGrupoId();
        
        GananciaPerdidaResponse out
                = detalleGeneralDao.getGananciasAndPerdidasByGrupo(grupoId == null ? 0 : grupoId.intValue());

        gananciaPerdidaList = new ArrayList<GananciaPerdidaModel>();

        if (out != null) {

            if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                List<GananciaPerdida> orig = out.getGananciasPerdidas();
                
                if (orig != null) {
                   
                    GananciaPerdidaUtil dgUtil = new GananciaPerdidaUtil();
                    for (GananciaPerdida j : orig) {
                        
                        
                        GananciaPerdidaModel cur = dgUtil.convertirGanPerdAGanPerdModel(j);
                        gananciaPerdidaList.add(cur);
                    }
                    
                }
            } else {
                this.displayErrorMessageFromBundle("sab.reportes.bygrupo.ganper.error.lbl",
                        RepGananciaPerdidaConstantes.TABLA_MESSAGE_CLIENT_ID);
            }
        }

    }

    @Override
    public void resetModelBean() {
        LOG.info("Reseting modelbean ");
        this.gananciaPerdidaList = null;
    }

    protected void displayErrorMessageFromBundle(String msgproperty, String msgClientId) {
        MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(msgClientId,
                msgproperty);
    }
}
