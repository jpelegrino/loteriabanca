package com.sib.reportes.bygrupo.transaccion;

import com.sib.entity.SumatoriaTransaccionCustomEntity;
import com.sib.ui.models.SumatoriaTransaccionCustomEntityModel;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;

/**
 * This is the SumatoriaTransaccion object helper class.
 *
 * @author Omar Moronta
 */
public class SumatoriaTransaccionUtil implements Serializable {
    
    public static final Logger LOG 
            = Logger.getLogger(SumatoriaTransaccionUtil.class.getName());   

    public SumatoriaTransaccionUtil() {

    }
    
    public SumatoriaTransaccionCustomEntityModel convertirSumTransaccionASumTransaccionModel
            (SumatoriaTransaccionCustomEntity j) {
        SumatoriaTransaccionCustomEntityModel cur = null;

        if (j != null) {
            byte[] bytesArr = SerializationUtils.serialize(j);
            SumatoriaTransaccionCustomEntity dg 
                    = (SumatoriaTransaccionCustomEntity)SerializationUtils.
                            deserialize(bytesArr);
            
            if(dg!=null){
                try {
                    cur = new SumatoriaTransaccionCustomEntityModel();
                    BeanUtils.copyProperties(cur, dg);
                } catch (IllegalAccessException ex) {
                    LOG.error(ex.getMessage(), ex);
                } catch (InvocationTargetException ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }

        return cur;
    }
}