package com.sib.reportes.bygrupo.montoporcentaje;

import com.sib.reportes.bygrupo.common.GrupoIdBasedAbstractInputBean;
import com.sib.util.DateFormatUtil;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "montoPorcentajeInputBean")
@ViewScoped
public class MontoPorcentajeInputBean extends GrupoIdBasedAbstractInputBean {

    private Date initDate;
    private Date finalDate;

    public MontoPorcentajeInputBean() {
    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public Date getFinalDate() {
        return finalDate;
    }

    public void setFinalDate(Date finalDate) {
        this.finalDate = finalDate;
    }

    public String getFormattedInitDate() {
        String result = "";
        if (initDate != null) {
            result = DateFormatUtil.
                    convertDateToStringWithStandardFormat(
                            this.getInitDate());
        }

        return result;
    }

    public String getFormattedFinalDate() {
        String result = "";
        if (finalDate != null) {
            result = DateFormatUtil.
                    convertDateToStringWithStandardFormat(
                            this.getFinalDate());
        }

        return result;
    }

    public void reset() {
        this.setGrupoId(0);
        this.setInitDate(null);
        this.setFinalDate(null);
    }
}
