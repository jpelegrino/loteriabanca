package com.sib.reportes.bygrupo.gananciaperdida;

import com.sib.reportes.bygrupo.common.GrupoIdBasedAbstractBean;
import com.sib.ui.models.GananciaPerdidaModel;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.component.datatable.DataTable;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "repGananciaPerdidaBean")
@ViewScoped
public class RepGananciaPerdidaBean extends GrupoIdBasedAbstractBean {

    @ManagedProperty(value = "#{repGananciaPerdidaModelBean}")
    private RepGananciaPerdidaModelBean modelBean;

    private DataTable dt;

    List<GananciaPerdidaModel> filteredList;

    /*
     * Asumiendo que el filtro es fechaCreacionFormateada.
     * Pendiente implementar Lazy.
     */
    private static final String FILTRO_LABEL = "fechaCreacionFormateada";

    public RepGananciaPerdidaModelBean getModelBean() {
        return modelBean;
    }

    public DataTable getDt() {
        return dt;
    }

    public void setDt(DataTable dt) {
        this.dt = dt;
    }

    public List<GananciaPerdidaModel> getFilteredList() {
        return filteredList;
    }

    public void setFilteredList(List<GananciaPerdidaModel> filteredList) {
        this.filteredList = filteredList;
    }

    public void setModelBean(RepGananciaPerdidaModelBean modelBean) {
        this.modelBean = modelBean;
    }

    public double getSummaryGanancias() {
        double result = 0;

        if (this.dt.getFilters() != null && !this.dt.getFilters().isEmpty()) {
            String valorFiltrado=this.dt.getFilters().get(FILTRO_LABEL);
            for (GananciaPerdidaModel gpm : this.modelBean.getGananciaPerdidaList()) {
                if (gpm.getFechaCreacionFormateada() != null 
                        && gpm.getFechaCreacionFormateada().startsWith(valorFiltrado)) {
                    result += gpm.getGanancia();
                }
            }
        } else if (this.modelBean.getGananciaPerdidaList() != null
                && !this.modelBean.getGananciaPerdidaList().isEmpty()) {
            for (GananciaPerdidaModel gpm : this.modelBean.getGananciaPerdidaList()) {
                result += gpm.getGanancia();
            }
        }

        return result;
    }

    public double getSummaryPerdidas() {
        double result = 0;
        if (this.dt.getFilters() != null && !this.dt.getFilters().isEmpty()) {
            String valorFiltrado=this.dt.getFilters().get(FILTRO_LABEL);
            for (GananciaPerdidaModel gpm : this.getModelBean().getGananciaPerdidaList()) {
                if (gpm.getFechaCreacionFormateada() != null 
                        && gpm.getFechaCreacionFormateada().startsWith(valorFiltrado)) {
                    result += gpm.getPerdida();
                }
            }
        } else if (this.modelBean.getGananciaPerdidaList() != null
                && !this.modelBean.getGananciaPerdidaList().isEmpty()) {
            for (GananciaPerdidaModel gpm : this.modelBean.getGananciaPerdidaList()) {
                result += gpm.getPerdida();
            }
        }

        return result;
    }

    @Override
    public void onGrupoSelectionChange() {
        this.modelBean.resetModelBean();
    }

}
