package com.sib.reportes.bygrupo.transaccion;

import com.sib.entity.DetalleGeneral;
import com.sib.ui.models.DetalleGeneralModel;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;

/**
 * This is the DetalleGeneral object helper class.
 *
 * @author Omar Moronta
 */
public class DetalleGeneralUtil implements Serializable {
    
    public static final Logger LOG = Logger.getLogger(DetalleGeneralUtil.class.getName());   

    public DetalleGeneralUtil() {

    }

    public DetalleGeneralModel crearDetGenModelDeDetGenModel(DetalleGeneralModel orig) {
        DetalleGeneralModel dest = new DetalleGeneralModel();

        if (orig != null) {
            byte[] bytesArr = SerializationUtils.serialize(orig);
            dest = (DetalleGeneralModel)SerializationUtils.deserialize(bytesArr);
        }

        return dest;
    }
    
    public DetalleGeneralModel convertirDetGenADetGenModel(DetalleGeneral j) {
        DetalleGeneralModel cur = null;

        if (j != null) {
            byte[] bytesArr = SerializationUtils.serialize(j);
            DetalleGeneral dg = (DetalleGeneral)SerializationUtils.deserialize(bytesArr);
            
            if(dg!=null){
                try {
                    cur = new DetalleGeneralModel();
                    BeanUtils.copyProperties(cur, dg);
                } catch (IllegalAccessException ex) {
                    LOG.error(ex.getMessage(), ex);
                } catch (InvocationTargetException ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }

        return cur;
    }
}