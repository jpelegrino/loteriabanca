package com.sib.reportes.bygrupo.montoporcentaje;

import com.sib.reportes.bygrupo.common.GrupoIdBasedAbstractBean;
import com.sib.ui.models.PorcentajesModel;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.component.datatable.DataTable;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "montoPorcentajeBean")
@ViewScoped
public class MontoPorcentajeBean extends GrupoIdBasedAbstractBean {

    @ManagedProperty(value = "#{montoPorcentajeModelBean}")
    private MontoPorcentajeModelBean modelBean;

    private DataTable dt;

    List<PorcentajesModel> filteredList;

    /*
     * Asumiendo que el filtro es fechaCreacionFormateada.
     * Pendiente implementar Lazy.
     */
    private static final String FILTRO_LABEL = "fechaCreacionFormateada";

    public MontoPorcentajeModelBean getModelBean() {
        return modelBean;
    }

    public void setModelBean(MontoPorcentajeModelBean modelBean) {
        this.modelBean = modelBean;
    }

    public List<PorcentajesModel> getFilteredList() {
        return filteredList;
    }

    public void setFilteredList(List<PorcentajesModel> filteredList) {
        this.filteredList = filteredList;
    }

    public DataTable getDt() {
        return dt;
    }

    public void setDt(DataTable dt) {
        this.dt = dt;
    }

    public double getSummaryTotalBancas() {
        double result = 0;

        if (this.dt.getFilters() != null && !this.dt.getFilters().isEmpty()) {
            String valorFiltrado = this.dt.getFilters().get(FILTRO_LABEL);
            for (PorcentajesModel gpm : this.modelBean.getMontoPorcentajeList()) {
                if (gpm.getFechaCreacionFormateada() != null
                        && gpm.getFechaCreacionFormateada().startsWith(valorFiltrado)) {
                    result += gpm.getTotalBanca();
                }
            }
        } else if (this.modelBean.getMontoPorcentajeList() != null
                && !this.modelBean.getMontoPorcentajeList().isEmpty()) {
            for (PorcentajesModel gpm : this.modelBean.getMontoPorcentajeList()) {
                result += gpm.getTotalBanca();
            }
        }

        return result;
    }

    public double getSummaryTotalJuego1() {
        double result = 0;

        if (this.dt.getFilters() != null && !this.dt.getFilters().isEmpty()) {
            String valorFiltrado = this.dt.getFilters().get(FILTRO_LABEL);
            for (PorcentajesModel gpm : this.modelBean.getMontoPorcentajeList()) {
                if (gpm.getFechaCreacionFormateada() != null
                        && gpm.getFechaCreacionFormateada().startsWith(valorFiltrado)) {
                    result += gpm.getTotalJuego1();
                }
            }
        } else if (this.modelBean.getMontoPorcentajeList() != null
                && !this.modelBean.getMontoPorcentajeList().isEmpty()) {
            for (PorcentajesModel gpm : this.modelBean.getMontoPorcentajeList()) {
                result += gpm.getTotalJuego1();
            }
        }

        return result;
    }

    public double getSummaryTotalJuego2() {
        double result = 0;

        if (this.dt.getFilters() != null && !this.dt.getFilters().isEmpty()) {
            String valorFiltrado = this.dt.getFilters().get(FILTRO_LABEL);
            for (PorcentajesModel gpm : this.modelBean.getMontoPorcentajeList()) {
                if (gpm.getFechaCreacionFormateada() != null
                        && gpm.getFechaCreacionFormateada().startsWith(valorFiltrado)) {
                    result += gpm.getTotalJuego2();
                }
            }
        } else if (this.modelBean.getMontoPorcentajeList() != null
                && !this.modelBean.getMontoPorcentajeList().isEmpty()) {
            for (PorcentajesModel gpm : this.modelBean.getMontoPorcentajeList()) {
                result += gpm.getTotalJuego2();
            }
        }

        return result;
    }

    public double getSummaryTotalJuego3() {
        double result = 0;

        if (this.dt.getFilters() != null && !this.dt.getFilters().isEmpty()) {
            String valorFiltrado = this.dt.getFilters().get(FILTRO_LABEL);
            for (PorcentajesModel gpm : this.modelBean.getMontoPorcentajeList()) {
                if (gpm.getFechaCreacionFormateada() != null
                        && gpm.getFechaCreacionFormateada().startsWith(valorFiltrado)) {
                    result += gpm.getTotalJuego3();
                }
            }
        } else if (this.modelBean.getMontoPorcentajeList() != null
                && !this.modelBean.getMontoPorcentajeList().isEmpty()) {
            for (PorcentajesModel gpm : this.modelBean.getMontoPorcentajeList()) {
                result += gpm.getTotalJuego3();
            }
        }

        return result;
    }

    @Override
    public void onGrupoSelectionChange() {
        this.modelBean.resetModelBean();
    }

}
