package com.sib.reportes.bygrupo.common;

import java.io.Serializable;

/**
 *
 * @author Omar Moronta
 */
public class GrupoIdBasedAbstractInputBean implements Serializable {

    private int grupoId;

    public int getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(int grupoId) {
        this.grupoId = grupoId;
    }
}
