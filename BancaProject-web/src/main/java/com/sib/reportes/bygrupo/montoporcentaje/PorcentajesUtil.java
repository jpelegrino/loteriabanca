package com.sib.reportes.bygrupo.montoporcentaje;

import com.sib.entity.Porcentajes;
import com.sib.ui.models.PorcentajesModel;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;

/**
 * This is the Porcentajes object helper class.
 *
 * @author Omar Moronta
 */
public class PorcentajesUtil implements Serializable {
    
    public static final Logger LOG = Logger.getLogger(PorcentajesUtil.class.getName());   

    public PorcentajesUtil() {

    }

    public PorcentajesModel crearPorcModelDePorcModel(PorcentajesModel orig) {
        PorcentajesModel dest = new PorcentajesModel();

        if (orig != null) {
            byte[] bytesArr = SerializationUtils.serialize(orig);
            dest = (PorcentajesModel)SerializationUtils.deserialize(bytesArr);
        }

        return dest;
    }
    
    public PorcentajesModel convertirPorcAPorcModel(Porcentajes j) {
        PorcentajesModel cur = null;

        if (j != null) {
            byte[] bytesArr = SerializationUtils.serialize(j);
            Porcentajes dg = (Porcentajes)SerializationUtils.deserialize(bytesArr);
            
            if(dg!=null){
                try {
                    cur = new PorcentajesModel();
                    BeanUtils.copyProperties(cur, dg);
                } catch (IllegalAccessException ex) {
                    LOG.error(ex.getMessage(), ex);
                } catch (InvocationTargetException ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }

        return cur;
    }
}