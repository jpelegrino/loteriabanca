package com.sib.reportes.bygrupo.gananciaperdida;

import com.sib.reportes.bygrupo.common.GrupoIdBasedAbstractInputBean;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "repGananciaPerdidaInputBean")
@ViewScoped
public class RepGananciaPerdidaInputBean extends GrupoIdBasedAbstractInputBean{

    public RepGananciaPerdidaInputBean() {
    }
}