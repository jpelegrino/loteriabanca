package com.sib.reportes.bygrupo.gananciaperdida;

import com.sib.entity.GananciaPerdida;
import com.sib.ui.models.GananciaPerdidaModel;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;

/**
 * This is the GananciaPerdida object helper class.
 *
 * @author Omar Moronta
 */
public class GananciaPerdidaUtil implements Serializable {
    
    public static final Logger LOG = Logger.getLogger(GananciaPerdidaUtil.class.getName());   

    public GananciaPerdidaUtil() {

    }

    public GananciaPerdidaModel crearGanPerdModelDeGanPerdModel(GananciaPerdidaModel orig) {
        GananciaPerdidaModel dest = new GananciaPerdidaModel();

        if (orig != null) {
            byte[] bytesArr = SerializationUtils.serialize(orig);
            dest = (GananciaPerdidaModel)SerializationUtils.deserialize(bytesArr);
        }

        return dest;
    }
    
    public GananciaPerdidaModel convertirGanPerdAGanPerdModel(GananciaPerdida j) {
        GananciaPerdidaModel cur = null;

        if (j != null) {
            byte[] bytesArr = SerializationUtils.serialize(j);
            GananciaPerdida dg = (GananciaPerdida)SerializationUtils.deserialize(bytesArr);
            
            if(dg!=null){
                try {
                    cur = new GananciaPerdidaModel();
                    BeanUtils.copyProperties(cur, dg);
                } catch (IllegalAccessException ex) {
                    LOG.error(ex.getMessage(), ex);
                } catch (InvocationTargetException ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }

        return cur;
    }
}