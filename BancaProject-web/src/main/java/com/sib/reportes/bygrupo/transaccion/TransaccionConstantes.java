package com.sib.reportes.bygrupo.transaccion;

import java.io.Serializable;

/**
 *
 * @author Omar Moronta
 */
public class TransaccionConstantes implements Serializable{
    public static final String TABLA_MESSAGE_CLIENT_ID="transaccionmantmsg";
    public static final String POPUP_MESSAGE_CLIENT_ID = "transaccdlg";
}
