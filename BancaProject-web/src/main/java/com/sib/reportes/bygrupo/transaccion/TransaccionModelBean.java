package com.sib.reportes.bygrupo.transaccion;

import com.sib.bs.responses.DetalleGeneralResponse;
import com.sib.bs.responses.TransaccionMontoTotalResponse;
import com.sib.ejb.dao.DetalleGeneralDao;
import com.sib.entity.DetalleGeneral;
import com.sib.interfaces.IModelBean;
import com.sib.ui.models.DetalleGeneralModel;
import com.sib.ui.models.SumatoriaTransaccionCustomEntityModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.util.CollectionUtils;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "transaccionModelBean")
@ViewScoped
public class TransaccionModelBean extends LazyDataModel
        implements IModelBean, Serializable {

    public static final Logger LOG
            = Logger.getLogger(TransaccionModelBean.class.getName());

    private boolean isCountVerified;
    private List<DetalleGeneralModel> result;
    private SumatoriaTransaccionCustomEntityModel sumatoriaTransaccion;

    @EJB
    private DetalleGeneralDao detalleGeneralDao;

    @ManagedProperty(value = "#{transaccionInputBean}")
    private TransaccionInputBean inputBean;

    public TransaccionInputBean getInputBean() {
        return inputBean;
    }

    public void setInputBean(TransaccionInputBean inputBean) {
        this.inputBean = inputBean;
    }

    public List<DetalleGeneralModel> getResult() {
        return result;
    }

    public void setResult(List<DetalleGeneralModel> result) {
        this.result = result;
    }

    public SumatoriaTransaccionCustomEntityModel getSumatoriaTransaccion() {
        if(sumatoriaTransaccion==null){
            TransaccionMontoTotalResponse out = this.detalleGeneralDao.
                    getSumatoriaTransaccion(this.inputBean.getGrupoId(), 
                    this.inputBean.getBancaId(), 
                    this.inputBean.getFormattedInitDate(), 
                    this.inputBean.getFormattedFinalDate());
            
            if(out!=null && out.getSumatoriaTransaccion()!=null){
                SumatoriaTransaccionUtil sumTransaUtil 
                        = new SumatoriaTransaccionUtil();
                sumatoriaTransaccion = sumTransaUtil.
                        convertirSumTransaccionASumTransaccionModel
                            (out.getSumatoriaTransaccion());
                
            }
        }
        return sumatoriaTransaccion;
    }

    public void setSumatoriaTransaccion(SumatoriaTransaccionCustomEntityModel sumatoriaTransaccion) {
        this.sumatoriaTransaccion = sumatoriaTransaccion;
    }    

    @Override
    public List<DetalleGeneralModel> load(int first, int pageSize, String sortField,
            SortOrder sortOrder, Map filters) {

        int datasetSize = this.getDatasetSize();

        if (datasetSize > 0) {
            LOG.info("/*******************************/:::" + sortField);
            DetalleGeneralResponse out
                    = this.detalleGeneralDao.getDetalleGeneralListByGrupo(
                            inputBean.getGrupoId(), inputBean.getBancaId(),
                            inputBean.getFormattedInitDate(),
                            inputBean.getFormattedFinalDate(),
                            StringUtils.isBlank(sortField) ? "d.id" : sortField,
                            SortOrder.ASCENDING.equals(sortOrder) ? "ASC" : "DESC",
                            first, pageSize);

            if (out != null && !CollectionUtils.isEmpty(out.getDetalleGenerals())) {
                List<DetalleGeneral> outList = out.getDetalleGenerals();
                result = new ArrayList<DetalleGeneralModel>();

                this.setRowCount(datasetSize);

                DetalleGeneralUtil dgUtil = new DetalleGeneralUtil();
                for (DetalleGeneral j : outList) {
                    DetalleGeneralModel cur = dgUtil.convertirDetGenADetGenModel(j);
                    result.add(cur);
                }
            } else {
                result = new ArrayList<DetalleGeneralModel>();
                this.setRowCount(0);
            }

        } else {
            result = new ArrayList<DetalleGeneralModel>();
            this.setRowCount(0);
        }

        return result;
    }

    private int getDatasetSize() {
        int datasetSize = 0;
        LOG.info("DATASETSIZE==" + inputBean.getGrupoId());
        if (!isCountVerified && datasetSize == 0) {
            try {
                datasetSize
                        = this.detalleGeneralDao.getTransactionRecords(
                                inputBean.getGrupoId(), inputBean.getBancaId(),
                                inputBean.getFormattedInitDate(),
                                inputBean.getFormattedFinalDate());
            } catch (Exception ex) {
                LOG.error("Error retrieving Transacciones datasetsize. Error: "
                        + ex.getMessage(), ex);
            }
        }

        return datasetSize;
    }

    @Override
    public void resetModelBean() {
        LOG.info("Reseting modelbean ");
        this.result = null;
        this.sumatoriaTransaccion=null;
        this.isCountVerified = false;
    }

    protected void displayErrorMessageFromBundle(String msgproperty, String msgClientId) {
        MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(msgClientId,
                msgproperty);
    }
}
