package com.sib.consorcio;

import com.sib.interfaces.IPopup;
import com.sib.ui.models.ConsorcioModel;
import java.io.Serializable;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "consorcioCreatePopup")
@ViewScoped
public class ConsorcioCreatePopup implements IPopup, Serializable {

    public static Logger LOG = Logger.getLogger(ConsorcioCreatePopup.class.getName());
    private ConsorcioModel consorcioModel = null;

    public ConsorcioModel getConsorcioModel() {
        return consorcioModel;
    }

    public void setConsorcioModel(ConsorcioModel ConsorcioModel) {
        this.consorcioModel = ConsorcioModel;
    }

    @Override
    public void reset() {
        LOG.info("reset() :: START");

        LOG.info("reset() :: END");

    }

    @Override
    public boolean validateFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getFileName() {
        return "consorcioCreatePopup";
    }

    @Override
    public void execute(ActionEvent event) {
        LOG.info("execute() :: START");

        LOG.info("execute() :: END");
    }

    @Override
    public void cancelProcess(ActionEvent event) {
        LOG.info("cancelProcess() :: START");

        LOG.info("cancelProcess() :: END");
    }

    @Override
    public String getPopupTitle() {
        return "";
    }

}
