package com.sib.consorcio;

import com.sib.interfaces.IPopup;
import com.sib.ui.models.ConsorcioModel;
import java.io.Serializable;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "consorcioViewPopup")
@ViewScoped
public class ConsorcioViewPopup implements IPopup, Serializable {

    public static Logger LOG = Logger.getLogger(ConsorcioViewPopup.class.getName());
    private ConsorcioModel consorcioModel = null;

    public ConsorcioModel getConsorcioModel() {
        return consorcioModel;
    }

    public void setConsorcioModel(ConsorcioModel consorcioModel) {
        this.consorcioModel = consorcioModel;
    }

    @Override
    public void execute(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancelProcess(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void reset() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean validateFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getFileName() {
        return "consorcioViewPopup";
    }

    @Override
    public String getPopupTitle() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
