package com.sib.consorcio;

import static com.sib.banca.BancaBean.LOG;
import com.sib.interfaces.IBean;
import com.sib.interfaces.IPopup;
import com.sib.navigation.NavigationBean;
import com.sib.ui.models.ConsorcioModel;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "consorcioBean")
@ViewScoped
public class ConsorcioBean implements IBean, Serializable {

    public static Logger LOG = Logger.getLogger(ConsorcioBean.class.getName());

    @ManagedProperty(value = "#{consorcioModelBean}")
    private ConsorcioModelBean modelBean;

    @ManagedProperty(value = "#{consorcioCreatePopup}")
    private IPopup consorcioCreatePopup;
    @ManagedProperty(value = "#{consorcioUpdatePopup}")
    private IPopup consorcioUpdatePopup;
    @ManagedProperty(value = "#{consorcioViewPopup}")
    private IPopup consorcioViewPopup;
    private IPopup popupHandler;
    @ManagedProperty(value = "#{navigationBean}")
    private NavigationBean navigationBean;

//
//    @PostConstruct
//    public void initialize() {
//        FacesContext.getCurrentInstance().getExternalContext().getSession(true);
//    }
    public ConsorcioModelBean getModelBean() {
        return modelBean;
    }

    public void setModelBean(ConsorcioModelBean modelBean) {
        this.modelBean = modelBean;
    }

    public IPopup getConsorcioCreatePopup() {
        return consorcioCreatePopup;
    }

    public void setConsorcioCreatePopup(IPopup consorcioCreatePopup) {
        this.consorcioCreatePopup = consorcioCreatePopup;
    }

    public IPopup getConsorcioUpdatePopup() {
        return consorcioUpdatePopup;
    }

    public void setConsorcioUpdatePopup(IPopup consorcioUpdatePopup) {
        this.consorcioUpdatePopup = consorcioUpdatePopup;
    }

    public IPopup getConsorcioViewPopup() {
        return consorcioViewPopup;
    }

    public void setConsorcioViewPopup(IPopup consorcioViewPopup) {
        this.consorcioViewPopup = consorcioViewPopup;
    }

    public IPopup getPopupHandler() {
        if (this.popupHandler == null) {
            this.popupHandler = consorcioCreatePopup;
        }
        return popupHandler;
    }

    public void setPopupHandler(IPopup popupHandler) {
        this.popupHandler = popupHandler;
    }

    public NavigationBean getNavigationBean() {
        return navigationBean;
    }

    public void setNavigationBean(NavigationBean navigationBean) {
        this.navigationBean = navigationBean;
    }

    public void openCreatePopup(ActionEvent event) {
        LOG.info("openCreatePopup() :: START");
        this.setPopupHandler(consorcioCreatePopup);
        ((ConsorcioCreatePopup) this.popupHandler).setConsorcioModel(new ConsorcioModel());
        RequestContext.getCurrentInstance().execute("PF('consorcioDialog').show()");
        LOG.info("openCreatePopup() :: END");
    }

    public void openUpdatePopup(ActionEvent event) {
        LOG.info("openUpdatePopup() :: START");
        this.setPopupHandler(this.consorcioUpdatePopup);
        ((ConsorcioUpdatePopup) this.popupHandler).setConsorcioModel(new ConsorcioModel());
        RequestContext.getCurrentInstance().execute("PF('consorcioDialog').show()");
        LOG.info("openUpdatePopup() :: START");
    }

    public void openViewPopup(ActionEvent event) {
        LOG.info("openUpdatePopup() :: START");
        this.setPopupHandler(this.consorcioViewPopup);
        ((ConsorcioViewPopup) this.popupHandler).setConsorcioModel(new ConsorcioModel());
        RequestContext.getCurrentInstance().execute("PF('consorcioDialog').show()");
        LOG.info("openUpdatePopup() :: START");
    }

    @Override
    public void resetBean() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getFileName() {
        return "ui.consorcio/consorcio";
    }

    @Override
    public void navigate(ActionEvent event) {
        LOG.log(Level.INFO, "{0} setNavigation() :: START", this.getClass().toString());
        navigationBean.setNavigationFileName(getFileName());
        LOG.log(Level.INFO, "{0} setNavigation() :: END", this.getClass().toString());
    }

}
