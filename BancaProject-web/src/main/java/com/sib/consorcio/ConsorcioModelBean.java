package com.sib.consorcio;

import com.sib.bs.factory.EntityContext;


import com.sib.interfaces.IModelBean;
import com.sib.ui.models.CompaniaModel;
import com.sib.ui.models.ConsorcioModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "consorcioModelBean")
@ViewScoped
public class ConsorcioModelBean implements  Serializable {

//    public static Logger LOG = Logger.getLogger(ConsorcioModelBean.class.getName());
//    @EJB
//    ConsorcioDao consorcioDao;
//    CompaniaDao companiaDao;

    private List<ConsorcioModel> consorciosList;
    
    

    public void crearConsorcio(ConsorcioModel consorcioModel) {
      //  LOG.info("crearConsorcio() :: START");

//        try {
//            Consorcio consorcio = (Consorcio) EntityContext.getEntity("consorcio");
//            Compania compania = companiaDao.getCompaniaById(1);
//            //El default de la compania es 1, solo habra un registro en la tabla sys_compania for now
//            consorcio.setCompania(compania);
//            consorcio.setConsorcioText(consorcioModel.getConsorcioText());
//            consorcio.setStatus(true);
//            consorcioDao.crearConsorcio(consorcio);
//        } catch (Exception e) {
//            LOG.error("Error creaando un Consorcio :: " + e);
//        }
      //  LOG.info("crearConsorcio() :: END");
    }

    public List<ConsorcioModel> getConsorcios() {
       // LOG.info("getConsorcios() :: START");

        try {
            if (consorciosList == null) {
                consorciosList = new ArrayList<ConsorcioModel>();
                for (int i = 0; i < 15; i++) {
                    ConsorcioModel model = new ConsorcioModel();
                    model.setId(i + 1);
                    model.setConsorcioText("Consorcio Descrption #" + (i + 1));
                    model.setStatus('A');

                    CompaniaModel companiaModel = new CompaniaModel();
                    companiaModel.setCompaniaText("Text for Compañia");
                    companiaModel.setId(i + 1);
                    model.setCompaniaModel(companiaModel);

                    consorciosList.add(model);
                }
            }
//            List<Consorcio> consorcios = consorcioDao.getConsorcios();
//
//            if (consorcios != null && !consorcios.isEmpty()) {
//                listConsorcioModel = new ArrayList<ConsorcioModel>();
//                for (Consorcio consorcio : consorcios) {
//
//                    ConsorcioModel consorcioModel = new ConsorcioModel();
//
//                    consorcioModel.setId(consorcio.getId());
//                    consorcioModel.setConsorcioText(consorcio.getConsorcioText());
//                    consorcioModel.setStatus(consorcio.isStatus());
//
//                    CompaniaModel companiaModel = new CompaniaModel();
//
//                    companiaModel.setCompaniaText(consorcio.getCompania().getCompaniaText());
//                    companiaModel.setId(consorcio.getCompania().getId());
//                    consorcioModel.setCompaniaModel(companiaModel);
//
//                    listConsorcioModel.add(consorcioModel);
//                }
//            }
        } catch (Exception e) {
      //      LOG.log(Level.SEVERE, "Error obteniendo la lista de Consorcios :: ".concat(e.toString()));
        }
     //   LOG.info("getConsorcios() :: EMD");

        return consorciosList;
    }

//    @Override
//    public void resetModelBean() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

}
