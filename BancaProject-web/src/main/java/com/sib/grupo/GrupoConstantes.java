package com.sib.grupo;

import java.io.Serializable;

/**
 *
 * @author Omar Moronta
 */
public class GrupoConstantes implements Serializable{
    public static final String TABLA_MESSAGE_CLIENT_ID="grupomantmsg";
    public static final String POPUP_MESSAGE_CLIENT_ID = "grupdlg";
}
