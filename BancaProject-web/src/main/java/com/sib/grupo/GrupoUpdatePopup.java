package com.sib.grupo;

import com.sib.ui.enums.UIOperationResultEnum;
import com.sib.ui.models.GrupoModel;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "grupoUpdatePopup")
@ViewScoped
public class GrupoUpdatePopup extends GrupoAbstractPopup 
    implements Serializable {

    public static final Logger LOG
            = Logger.getLogger(GrupoUpdatePopup.class.getName());

    private GrupoModel untouchedModel;

    public GrupoUpdatePopup() {
    }

    public GrupoModel getUntouchedModel() {
        return untouchedModel;
    }

    public void setUntouchedModel(GrupoModel untouchedModel) {
        this.untouchedModel = untouchedModel;
    }

    @Override
    public void execute(ActionEvent event) {
        if (validateFields()) {
            try {
                LOG.info("Actualizando grupo: " + this.getModel());
                prepararStatusOperacion(UIOperationResultEnum.SUCCESS);
                ResponseModel response = getModelBean().actualizarGrupo(this.getModel());
                if (response.getWasSuccessful()) {
                    prepararStatusOperacion(UIOperationResultEnum.SUCCESS);
                    this.displayInfoMessage(response.getMessage(),
                            GrupoConstantes.TABLA_MESSAGE_CLIENT_ID);
                } else {
                    LOG.error("Error actualizando Grupo: " + response.getMessage());
                    this.displayErrorMessage(response.getMessage(),
                            GrupoConstantes.POPUP_MESSAGE_CLIENT_ID);
                    prepararStatusOperacion(UIOperationResultEnum.FAILURE);
                }
            } catch (Exception ex) {
                LOG.error("Error actualizando Grupo. Exception: " 
                        + ex.getMessage(), ex);
                this.displayErrorMessageFromBundle("sab.grupo.actualizar.error",
                        GrupoConstantes.POPUP_MESSAGE_CLIENT_ID);
                prepararStatusOperacion(UIOperationResultEnum.FAILURE);
            }
        } else {
            prepararStatusOperacion(UIOperationResultEnum.FAILURE);
            LOG.info("Fields Validation Failed..");
        }
    }

    @Override
    public void cancelProcess(ActionEvent event) {
        LOG.info("Cancelling Process");
    }

    @Override
    public void reset() {
        this.untouchedModel=null;
        super.reset();
    }

    @Override
    public boolean validateFields() {
        
        boolean result = true;

        if ((this.getModel().getCodigo() == null
                || this.getModel().getCodigo().isEmpty())) {
            MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(
                    GrupoConstantes.POPUP_MESSAGE_CLIENT_ID, "sab.error.campo.vacio");
            result = false;
        }

        if(result){
            if((this.getModel().getCodigo()!=null
                    && this.getModel().getCodigo().equals(this.untouchedModel.getCodigo()))
                 && (this.getModel().getStatus()!=null && 
                        this.getModel().getStatus().equals(this.untouchedModel.getStatus()))){
                MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(
                        GrupoConstantes.POPUP_MESSAGE_CLIENT_ID, "sab.error.no.cambios");
                result = false;                
            }
        }
        return result;
    }

    @Override
    public String getFileName() {
        return "grupoupdatepopup";
    }

    @Override
    public String getPopupTitle() {
        return MessageDisplayUtils.INSTANCE.getMessageFromBundle("sab.grupo.actualizar.lbl");
    }

    @Override
    public Logger getLogger() {
        return LOG;
    }
}
