package com.sib.grupo;

import com.sib.interfaces.IPopup;
import com.sib.ui.enums.UIOperationResultEnum;
import com.sib.ui.models.GrupoModel;
import com.sib.util.MessageDisplayUtils;
import javax.faces.bean.ManagedProperty;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Omar Moronta
 */
public abstract class GrupoAbstractPopup implements IPopup {

    private GrupoModel model;

    @ManagedProperty(value = "#{grupoModelBean}")
    private GrupoModelBean modelBean;

    public GrupoModel getModel() {
        return model;
    }

    public void setModel(GrupoModel model) {
        this.model = model;
    }

    public GrupoModelBean getModelBean() {
        return modelBean;
    }

    public void setModelBean(GrupoModelBean modelBean) {
        this.modelBean = modelBean;
    }

    @Override
    public void cancelProcess(ActionEvent event) {
        getLogger().info("Cancelling Process");
    }

    @Override
    public void reset() {
        this.model = null;
    }

    protected void prepararStatusOperacion(UIOperationResultEnum operationStatus) {
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("operationStatus", operationStatus.getStatusLiteral());
    }

    protected void displayErrorMessageFromBundle(String msgproperty, String msgClientId) {
        MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(msgClientId,
                msgproperty);
    }

    protected void displayErrorMessage(String message, String msgClientId) {
        MessageDisplayUtils.INSTANCE.printErrorMessage(msgClientId,
                message);
    }

    protected void displayInfoMessage(String message, String msgClientId) {
        MessageDisplayUtils.INSTANCE.printInfoMessage(msgClientId,
                message);
    }

    public abstract Logger getLogger();

}
