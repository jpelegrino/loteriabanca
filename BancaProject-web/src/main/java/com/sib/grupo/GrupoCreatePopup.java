package com.sib.grupo;

import com.sib.ui.enums.StatusEnum;
import com.sib.ui.enums.UIOperationResultEnum;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "grupoCreatePopup")
@ViewScoped
public class GrupoCreatePopup extends GrupoAbstractPopup 
    implements Serializable {

    public static final Logger LOG = Logger.getLogger(GrupoCreatePopup.class.getName());

    public GrupoCreatePopup() {
    }

    @Override
    public void execute(ActionEvent event) {
        if (validateFields()) {
            try {
                LOG.info("Creando grupo con CODIGO: " + this.getModel().getCodigo());
                this.getModel().setStatus(StatusEnum.ACTIVO.getStatusLiteral());                
                ResponseModel response = getModelBean().crearGrupo(this.getModel());
                
                if(response.getWasSuccessful()){
                    prepararStatusOperacion(UIOperationResultEnum.SUCCESS);
                    this.displayInfoMessage(response.getMessage(),
                        GrupoConstantes.TABLA_MESSAGE_CLIENT_ID);                
                }else{
                    LOG.error("Error creando grupo: " + response.getMessage());
                    this.displayErrorMessage(response.getMessage(),
                        GrupoConstantes.POPUP_MESSAGE_CLIENT_ID);
                    prepararStatusOperacion(UIOperationResultEnum.FAILURE);                
                }
            } catch (Exception ex) {
                LOG.error("Error creando grupo. Exception: " + ex.getMessage(), ex);
                this.displayErrorMessageFromBundle("sab.grupo.agregar.error",
                        GrupoConstantes.POPUP_MESSAGE_CLIENT_ID);
                prepararStatusOperacion(UIOperationResultEnum.FAILURE);
            }
        } else {
            prepararStatusOperacion(UIOperationResultEnum.FAILURE);
            LOG.info("Fields Validation Failed..");
        }
    }

    @Override
    public void cancelProcess(ActionEvent event) {
        LOG.info("Cancelling Process");
    }

    @Override
    public void reset() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean validateFields() {
        boolean result = true;

        if (this.getModel().getCodigo() == null
                || this.getModel().getCodigo().isEmpty()) {
            this.displayErrorMessageFromBundle("sab.error.campo.vacio", 
                    GrupoConstantes.POPUP_MESSAGE_CLIENT_ID);
            result = false;
        }else if(!this.getModel().getCodigo().matches("[0-9][0-9]{2}")) {
            MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(
                    GrupoConstantes.POPUP_MESSAGE_CLIENT_ID,
                    "sab.grupo.agregar.error.match");
            result = false;            
        }

        return result;
    }

    @Override
    public String getFileName() {
        return "grupocreatepopup";
    }

    @Override
    public String getPopupTitle() {
        return MessageDisplayUtils.INSTANCE.getMessageFromBundle("sab.grupo.agregar.lbl");
    }

    @Override
    public Logger getLogger() {
        return LOG;
    }
}