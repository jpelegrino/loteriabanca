package com.sib.grupo;

import com.sib.banca.BancaBean;
import com.sib.interfaces.IPopup;
import com.sib.ui.models.GrupoModel;
import java.io.Serializable;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "grupoBean")
@ViewScoped
public class GrupoBean implements Serializable {

    public static final Logger LOG = Logger.getLogger(GrupoBean.class.getName());

    @ManagedProperty(value = "#{grupoModelBean}")
    private GrupoModelBean modelBean;

    private IPopup popupHandler;

    @ManagedProperty(value = "#{grupoCreatePopup}")
    private IPopup createPopup;
    @ManagedProperty(value = "#{grupoUpdatePopup}")
    private IPopup updatePopup;
    @ManagedProperty(value = "#{grupoDeletePopup}")
    private IPopup deletePopup;

    public IPopup getPopupHandler() {
        if (this.popupHandler == null) {
            this.popupHandler = this.createPopup;
        }
        return popupHandler;
    }

    public void setPopupHandler(IPopup popupHandler) {
        this.popupHandler = popupHandler;
    }
    
    public GrupoModelBean getModelBean() {
        return modelBean;
    }    

    public void setModelBean(GrupoModelBean modelBean) {
        this.modelBean = modelBean;
    }

    public IPopup getCreatePopup() {
        return createPopup;
    }

    public void setCreatePopup(IPopup createPopup) {
        this.createPopup = createPopup;
    }

    public IPopup getUpdatePopup() {
        return updatePopup;
    }

    public void setUpdatePopup(IPopup updatePopup) {
        this.updatePopup = updatePopup;
    }

    public IPopup getDeletePopup() {
        return deletePopup;
    }

    public void setDeletePopup(IPopup deletePopup) {
        this.deletePopup = deletePopup;
    }

    public void openCreatePopup(ActionEvent event) {
        ((GrupoCreatePopup)createPopup).setModel(new GrupoModel());
        this.setPopupHandler(this.createPopup);
    }

    public void openUpdatePopup(ActionEvent event) {
        
        GrupoModel selectedJuego = getSelectedGrupoModelFromRequest();
        
        ((GrupoUpdatePopup)updatePopup).setUntouchedModel(selectedJuego);

        GrupoUtil gutil = new GrupoUtil();
        GrupoModel selectedModel = 
                gutil.crearGrupoModelDeGrupoModel(selectedJuego);        
        ((GrupoUpdatePopup)updatePopup).setModel(selectedModel);
        
        this.setPopupHandler(this.updatePopup);
    }

    public void openDeletePopup(ActionEvent event) {
        GrupoModel selected = getSelectedGrupoModelFromRequest();
        ((GrupoDeletePopup)deletePopup).setModel(selected);
        this.setPopupHandler(this.deletePopup);
    }
    
    private GrupoModel getSelectedGrupoModelFromRequest(){
        GrupoModel selectedJuego = new GrupoModel();
        
        Map<String,String> params = 
                FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        
        String grupoId = params.get("grupoId");
        int id = Integer.parseInt(grupoId);
        
        GrupoModelBean gmb = (GrupoModelBean)this.getModelBean();
        
        for(GrupoModel gm : gmb.getResult()){
            if(gm.getId()==id){
                selectedJuego=gm;
            }
        }
    
        return selectedJuego;
    }    

}