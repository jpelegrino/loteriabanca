package com.sib.grupo;

import com.sib.bs.responses.GrupoResponse;
import com.sib.bs.responses.MessageEnum;
import com.sib.ejb.dao.GrupoDao;
import com.sib.entity.Grupo;
import com.sib.interfaces.IModelBean;
import com.sib.ui.models.GrupoModel;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "grupoModelBean")
@ViewScoped
public class GrupoModelBean implements IModelBean, Serializable {

    public static final Logger LOG
            = Logger.getLogger(GrupoModelBean.class.getName());

    private List<GrupoModel> result;

    @EJB
    GrupoDao grupoDao;

    public GrupoModelBean() {

    }

    public List<GrupoModel> getResult() {
        if (result == null) {
            init();
        }
        return result;
    }

    public void setResult(List<GrupoModel> result) {
        this.result = result;
    }

    private void init() {
        GrupoResponse out = grupoDao.getGrupos(null);

        result = new ArrayList<GrupoModel>();

        if (out != null) {
            if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                List<Grupo> gruposOrig = out.getGrupos();

                if (gruposOrig != null) {
                    GrupoUtil gUtil = new GrupoUtil();
                    for (Grupo g : gruposOrig) {
                        GrupoModel cur = gUtil.convertirGrupoAGrupoModel(g);
                        result.add(cur);
                    }
                }
            } else {
                this.displayErrorMessageFromBundle("sab.juego.error.lbl",
                        GrupoConstantes.TABLA_MESSAGE_CLIENT_ID);
            }
        }

    }

    @Override
    public void resetModelBean() {
        result = null;
    }

    public ResponseModel crearGrupo(GrupoModel grupoModel) {
        ResponseModel resp = new ResponseModel();

        try {
            GrupoUtil gutil = new GrupoUtil();
            Grupo grupo = gutil.convertirGrupoModelAGrupo(grupoModel);
            LOG.info("Inicio crear Grupo..");
            GrupoResponse out = this.grupoDao.crearGrupo(grupo);
            resp = processResponse(out, "sab.grupo.agregar.success",
                    "sab.grupo.agregar.error", "sab.grupo.agregar.val.error");
            LOG.info("Fin crear Grupo..");
        } catch (Exception ex) {
            resp.setWasSuccessful(false);
            resp.setMessage(this.getMessageFromBundle("sab.grupo.agregar.error"));            
            LOG.error("Error invocando a GrupoDao para crear grupo."
                    + ex.getMessage(), ex);
        }

        return resp;
    }

    public ResponseModel eliminarGrupo(GrupoModel grupoModel) {

        ResponseModel resp = new ResponseModel();

        try {
            GrupoUtil gutil = new GrupoUtil();
            Grupo grupo = gutil.convertirGrupoModelAGrupo(grupoModel);
            LOG.info("Inicio grupo Juego..");
            GrupoResponse out = this.grupoDao.deleteGrupo(grupo);
            resp = processResponse(out, "sab.grupo.eliminar.success",
                    "sab.grupo.eliminar.error", "sab.grupo.agregar.val.error");
            LOG.info("Fin grupo Juego..");
        } catch (Exception ex) {
            resp.setWasSuccessful(false);
            resp.setMessage(this.getMessageFromBundle("sab.grupo.eliminar.error"));            
            LOG.error("Error invocando a GrupoDao para eliminar grupo."
                    + ex.getMessage(), ex);
        }

        return resp;
    }

    public ResponseModel actualizarGrupo(GrupoModel grupoModel) {

        ResponseModel resp = new ResponseModel();

        try {
            GrupoUtil gutil = new GrupoUtil();
            Grupo grupo = gutil.convertirGrupoModelAGrupo(grupoModel);
            LOG.info("Inicio actualizar grupo..");
            LOG.info("Actualizando grupo con Id: " + grupo.getId());
            GrupoResponse out = this.grupoDao.updateGrupo(grupo);
            resp = processResponse(out, "sab.grupo.actualizar.success",
                    "sab.grupo.actualizar.error", "sab.grupo.actualizar.val.error");
            LOG.info("Fin actualizar rupo..");
        } catch (Exception ex) {
            resp.setWasSuccessful(false);
            resp.setMessage(this.getMessageFromBundle("sab.grupo.actualizar.error"));            
            LOG.error("Error invocando a GrupoDao para actualizar grupo."
                    + ex.getMessage(), ex);
        }

        return resp;
    }

    protected void displayErrorMessageFromBundle(String msgproperty, String msgClientId) {
        MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(msgClientId,
                msgproperty);
    }
    
    public List<GrupoModel> getGruposByStatus(String status){
        List<GrupoModel> results = null;
        
        GrupoResponse out = grupoDao.getGrupos(status);

        if (out != null) {
            if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                results = new ArrayList<GrupoModel>();
                List<Grupo> gruposOrig = out.getGrupos();

                if (gruposOrig != null) {
                    GrupoUtil gUtil = new GrupoUtil();
                    for (Grupo g : gruposOrig) {
                        GrupoModel cur = gUtil.convertirGrupoAGrupoModel(g);
                        results.add(cur);
                    }
                }
            } else {
                LOG.error("Ha ocurrido un error obteniendo grupo por status. "
                        + "Message Enum equals:>"+out.getMsgEnum());
            }
        }
        
        return results;
    }

    protected String getMessageFromBundle(String msgproperty, Object... params) {
        return MessageDisplayUtils.INSTANCE.
                getMessageFromBundle(msgproperty, params);
    }

    private ResponseModel processResponse(GrupoResponse response,
            String succesMessage, String failureMessage, String validationFailMessage) {

        ResponseModel resp = new ResponseModel();

        if (response != null) {
            /**
             * In this switch the FAIL enum will be treated as default for
             * displaying general message.
             */
            switch (response.getMsgEnum()) {

                case SUCCESS:
                    resp.setWasSuccessful(true);
                    resp.setMessage(
                            getMessageFromBundle(succesMessage));
                    resetModelBean();
                    break;

                case VALIDATION:
                    resp.setWasSuccessful(false);
                    resp.setMessage(getMessageFromBundle(validationFailMessage));
                    break;

                default:
                    resp.setWasSuccessful(false);
                    resp.setMessage(
                            getMessageFromBundle(failureMessage));
                    break;
            }
        } else {
            resp.setWasSuccessful(false);
            resp.setMessage(
                    getMessageFromBundle(failureMessage));

        }

        return resp;
    }

}
