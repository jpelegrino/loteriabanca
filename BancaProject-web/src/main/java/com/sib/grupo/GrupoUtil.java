package com.sib.grupo;

import com.sib.entity.Grupo;
import com.sib.ui.models.GrupoModel;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;

/**
 * This is the Grupo <-> GrupoModel objects converter helper class.
 *
 * @author Omar Moronta
 */
public class GrupoUtil implements Serializable{
    public static final Logger LOG = Logger.getLogger(GrupoUtil.class.getName()); 
    
    public GrupoModel crearGrupoModelDeGrupoModel(GrupoModel gm){
        GrupoModel ngm=null;
    
        if(gm!=null){
            byte[] bytesArr = SerializationUtils.serialize(gm);
            ngm = (GrupoModel)SerializationUtils.deserialize(bytesArr);
        }
        
        return ngm;
    }
    
    public GrupoModel convertirGrupoAGrupoModel(Grupo grupo){
        GrupoModel gm = null;
        
        if(grupo!=null){
            byte[] bytesArr = SerializationUtils.serialize(grupo);
            Grupo gp = (Grupo)SerializationUtils.deserialize(bytesArr);
            
            if(gp!=null){
                try {
                    gm = new GrupoModel();
                    BeanUtils.copyProperties(gm, gp);
                } catch (IllegalAccessException ex) {
                    LOG.error(ex.getMessage(), ex);
                } catch (InvocationTargetException ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }
        
        return gm;
    }
    
    public Grupo convertirGrupoModelAGrupo(GrupoModel gm){
        Grupo gp = null;
        
        if(gm!=null){
            byte[] bytesArr = SerializationUtils.serialize(gm);
            GrupoModel ngm = (GrupoModel)SerializationUtils.deserialize(bytesArr);
            
            if(ngm!=null){
                try {
                    gp = new Grupo();
                    BeanUtils.copyProperties(gp, ngm);
                } catch (IllegalAccessException ex) {
                    LOG.error(ex.getMessage(), ex);
                } catch (InvocationTargetException ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }
        
        return gp;
    }    
}
