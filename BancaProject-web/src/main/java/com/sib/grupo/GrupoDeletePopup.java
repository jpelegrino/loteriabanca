package com.sib.grupo;

import com.sib.ui.enums.UIOperationResultEnum;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "grupoDeletePopup")
@ViewScoped
public class GrupoDeletePopup extends GrupoAbstractPopup 
    implements Serializable{

    public static final Logger LOG
            = Logger.getLogger(GrupoDeletePopup.class.getName());

    public GrupoDeletePopup() {
    }

    @Override
    public void execute(ActionEvent event) {
        try {
            LOG.info("Eliminando Grupo: " + this.getModel().getCodigo());
            ResponseModel response = getModelBean().eliminarGrupo(this.getModel());

            if (response.getWasSuccessful()) {
                prepararStatusOperacion(UIOperationResultEnum.SUCCESS);
                this.displayInfoMessage(response.getMessage(),
                        GrupoConstantes.TABLA_MESSAGE_CLIENT_ID);
            } else {
                LOG.error("Error eliminando Grupo: " + response.getMessage());
                this.displayErrorMessage(response.getMessage(),
                        GrupoConstantes.POPUP_MESSAGE_CLIENT_ID);
                prepararStatusOperacion(UIOperationResultEnum.FAILURE);
            }
        } catch (Exception ex) {
            LOG.error("Error eliminando Grupo. Exception: " 
                    + ex.getMessage(), ex);
            this.displayErrorMessageFromBundle("sab.grupo.eliminar.error",
                    GrupoConstantes.POPUP_MESSAGE_CLIENT_ID);
            prepararStatusOperacion(UIOperationResultEnum.FAILURE);
        }
    }

    @Override
    public boolean validateFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getFileName() {
        return "grupodeletepopup";
    }

    @Override
    public String getPopupTitle() {
        return MessageDisplayUtils.INSTANCE.getMessageFromBundle("sab.grupo.eliminar.lbl");
    }

    @Override
    public Logger getLogger() {
        return LOG;
    }
}