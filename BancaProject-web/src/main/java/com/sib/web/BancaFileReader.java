/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.web;

import com.sib.bs.services.LocateService;
import com.sib.ejb.dao.PlanoDao;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.WebApplicationInitializer;

/**
 *
 * @author julio
 */
public class BancaFileReader implements WebApplicationInitializer{
    
    private static Logger LOG=Logger.getLogger(BancaFileReader.class);
    
    private final static String ABSOLUTE_PATH="/home/julio/Desktop/Downloads/mfarplano";
    
    @Scheduled(fixedDelay = 8000)
    public void startReading() {
//        PlanoDao planoDao=(PlanoDao) LocateService.getService("PlanoDaoImpl");
        
//        File file=new File(ABSOLUTE_PATH);
//        InputStream is=null;
//        
//        try {
//           
//            if (file.exists()) {
//                LOG.info("File exists ---> "+ file.exists());
//                is=new FileInputStream(file); 
//                planoDao.send(is);
//                LOG.info("The text file has been converted to bytes successfuly ");
//                
//            
//            }else {
//                LOG.debug("** NO SE ENCONTRO ARCHIVO EN LA RUTA ESPECIFICADA **");
//            }
//            
//        } catch (FileNotFoundException e) {
//            LOG.error("File ::: ", e);
//        }
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        ApplicationContext context=new ClassPathXmlApplicationContext("spring-module.xml");       
        LOG.info("Text file start reading ....");
    
    }
    
}
