package com.sib.cobro;

import com.sib.bs.responses.BancaGrupoCobroResponse;
import com.sib.bs.responses.CobroResponse;
import com.sib.bs.responses.DetalleGeneralResponse;
import com.sib.bs.responses.MessageEnum;
import com.sib.ejb.dao.BancaGrupoCobroDao;
import com.sib.ejb.dao.CobroDao;
import com.sib.ejb.dao.DetalleGeneralDao;
import com.sib.ejb.dao.GeneralDao;
import com.sib.entity.BancaGrupoCobro;
import com.sib.entity.Cobro;
import com.sib.entity.CobroHistorial;
import com.sib.entity.DetalleGeneral;
import com.sib.interfaces.IModelBean;
import com.sib.ui.models.BancaGrupoCobroModel;
import com.sib.util.NumberFormatUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "bancaGrupoCobroModelBean")
@ViewScoped
public class BancaGrupoCobroModelBean implements IModelBean, Serializable {
    
    public static final Logger LOGGER = Logger.getLogger(BancaGrupoCobroModelBean.class);
    @EJB
    BancaGrupoCobroDao bancaGrupoCobroDao;
    
    @EJB
    DetalleGeneralDao detalleGeneralDao;
    
    @EJB
    GeneralDao generalDao;
    
    @EJB
    CobroDao cobroDao;
    private Float deudaTotalGrupo;

    //this Field es usado para mostarl el total de la deuda formateada sin decimales.
    private String deudaTotalGrupoUI;
    
    private boolean showCheckBox = false;
    
    public boolean isShowCheckBox() {
        return showCheckBox;
    }
    
    public void setShowCheckBox(boolean showCheckBox) {
        this.showCheckBox = showCheckBox;
    }
    
    @ManagedProperty(value = "#{cobroInputBean}")
    private CobroInputBean inputBean;
    private List<BancaGrupoCobroModel> bancaGrupoCobroModelList;
    
    public List<BancaGrupoCobroModel> getBancaGrupoCobroModelList() {
        if (bancaGrupoCobroModelList == null || bancaGrupoCobroModelList.isEmpty()) {
            init();
        }
        return bancaGrupoCobroModelList;
    }
    
    public void setBancaGrupoCobroModelList(List<BancaGrupoCobroModel> bancaGrupoCobroModelList) {
        this.bancaGrupoCobroModelList = bancaGrupoCobroModelList;
    }
    
    public CobroInputBean getInputBean() {
        return inputBean;
    }
    
    public void setInputBean(CobroInputBean inputBean) {
        this.inputBean = inputBean;
    }
    
    @Override
    public void resetModelBean() {
        bancaGrupoCobroModelList = null;
        this.deudaTotalGrupo = 0f;
        this.showCheckBox = false;
    }
    
    public void init() {
        LOGGER.info("init() :: START");
        bancaGrupoCobroModelList = new ArrayList<BancaGrupoCobroModel>();
        try {
            if (inputBean.getGrupoId() == null || inputBean.getGrupoId() <= 0) {
                LOGGER.error("El ID del Grupo es Null or menor o igual a 0");
                LOGGER.error("ID GRUPO :: " + inputBean.getGrupoId());
                return;
            }
            BancaGrupoCobroResponse response = bancaGrupoCobroDao.getBancaGrupoCobroListByGroupId(inputBean.getGrupoId());
            
            if (response.getBancaGrupoCobro() != null && !response.getBancaGrupoCobro().isEmpty()) {
                showCheckBox = true;
                LOGGER.info("Size :: " + response.getBancaGrupoCobro().size());
                CobroUtil cobroUtil = new CobroUtil();

//                for (int i = 0; i < 40; i++) {
//                    BancaGrupoCobroModel model = cobroUtil.convertirBancaGrupoCobroABancaGrupoCobroModel(response.getBancaGrupoCobro().get(0));
//                    bancaGrupoCobroModelList.add(model);
//                }
                for (BancaGrupoCobro bancaGrupoCobro : response.getBancaGrupoCobro()) {
                    deudaTotalGrupo = deudaTotalGrupo + bancaGrupoCobro.getDeuda();
                    System.out.println("");
                    System.out.println("eNTITY -->" + bancaGrupoCobro.getDeuda());
                    BancaGrupoCobroModel model = cobroUtil.convertirBancaGrupoCobroABancaGrupoCobroModel(bancaGrupoCobro);
                    System.out.println("");
                    System.out.println("");
                    System.out.println("ID GRUPO :: " + model.getIdGrupo());
                    System.out.println("ID BANCA :: " + model.getIdBanca());
                    System.out.println("DEUDA :: " + model.getDeuda());
                    System.out.println("CODIGO_BANCA :: " + model.getCodigoBanca());
                    System.out.println("CODIGO_GRUPO :: " + model.getCodigoGrupo());
                    System.out.println("");
                    bancaGrupoCobroModelList.add(model);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error tratando de obtener la lista de deuda para aplicar cobros ::" + e);
        }
        
        LOGGER.info("init() :: END");
    }
    
    public Float getDeudaTotalGrupo() {
        return deudaTotalGrupo;
    }
    
    public void setDeudaTotalGrupo(Float deudaTotalGrupo) {
        this.deudaTotalGrupo = deudaTotalGrupo;
    }
    
    public String getDeudaTotalGrupoUI() {
        this.deudaTotalGrupoUI = NumberFormatUtil.formatWithoutDecimal(this.deudaTotalGrupo);
        return deudaTotalGrupoUI;
    }
    
    public void setDeudaTotalGrupoUI(String deudaTotalGrupoUI) {
        this.deudaTotalGrupoUI = deudaTotalGrupoUI;
    }
    
    public Integer getSize() {
        Integer result = 0;
        if (bancaGrupoCobroModelList != null && !bancaGrupoCobroModelList.isEmpty()) {
            result = bancaGrupoCobroModelList.size();
        }
        return result;
    }
    
    public CobroResponse crearCobro(List<BancaGrupoCobroModel> bancaGrupoCobroModels, Float totalACobrar, String commentario, Date fechaEfectiva) {
        LOGGER.info("crearCobro() :: START");
        CobroResponse cobroResponse = new CobroResponse();
        try {
            if (bancaGrupoCobroModels != null && !bancaGrupoCobroModels.isEmpty()) {
                
                Cobro cobro = new Cobro();
                Float montoTotalCobrado = 0f;
                List<CobroHistorial> cobroHistorialList = new ArrayList<CobroHistorial>();
                
                for (BancaGrupoCobroModel model : bancaGrupoCobroModels) {
                    LOGGER.info("idGrupo :: " + model.getIdGrupo());
                    LOGGER.info("idBanca :: " + model.getIdBanca());
                    LOGGER.info("totalAPagar ::" + totalACobrar);
                    BancaGrupoCobroResponse response = bancaGrupoCobroDao.getAllDetallesIDRelatedToBancaAndGrupo(model.getIdBanca(), model.getIdGrupo());
                    
                    if (response.getMsgEnum().getCode() == 1 && response.getDetallesGeneralesID() != null && !response.getDetallesGeneralesID().isEmpty()) {
                        
                        System.out.println("Size --->" + response.getDetallesGeneralesID().size());
                        
                        for (int i = 0; i < response.getDetallesGeneralesID().size(); i++) {
                            DetalleGeneralResponse detalleGeneralResponse = detalleGeneralDao.getDetalleGeneralById(response.getDetallesGeneralesID().get(i));
                            System.out.println("Detalle_ID --->" + response.getDetallesGeneralesID().get(i));
                            if (detalleGeneralResponse.getMsgEnum().getCode() == 1 && detalleGeneralResponse.getDetalleGeneral() != null) {
                                
                                DetalleGeneral detalleGeneral = detalleGeneralResponse.getDetalleGeneral();
                                
                                CobroHistorial cobroHistorial = new CobroHistorial();
                                
                                Float nuevoPendiente = 0f;
                                
                                System.out.println("/***************************/");
                                System.out.println("TotalACobrar -->" + totalACobrar);
                                System.out.println("Pendiente a Cobrar --->" + detalleGeneral.getPendienteCobrar());
                                if (totalACobrar >= detalleGeneral.getPendienteCobrar()) {
                                    totalACobrar = totalACobrar - detalleGeneral.getPendienteCobrar();
                                    detalleGeneral.setStatusDetalle("C");
                                    cobroHistorial.setMontoCobrado(detalleGeneral.getPendienteCobrar());
                                    detalleGeneral.setPendienteCobrar(0f);
                                    
                                } else {
                                    nuevoPendiente = detalleGeneral.getPendienteCobrar() - totalACobrar;
                                    cobroHistorial.setMontoCobrado(totalACobrar);
                                    totalACobrar = 0f;
                                    detalleGeneral.setPendienteCobrar(nuevoPendiente);
                                }
                                System.out.println("Nuevo Pendiente ::" + nuevoPendiente);
                                montoTotalCobrado = montoTotalCobrado + cobroHistorial.getMontoCobrado();
                                
                                DetalleGeneralResponse detalleAEditar = detalleGeneralDao.editarDetalleGeneral(detalleGeneral);
                                if (detalleAEditar.getMsgEnum().getCode() != 1) {
                                    LOGGER.info("ERROR EDITANDO EL DETALLE GENERAL PARA APLICAR COBRO");
                                }
                                System.out.println("Detalle General ID :: " + detalleAEditar.getDetalleGeneral().getId());
                                System.out.println("/***************************/");
                                
                                cobroHistorial.setStatusCobroHistorial("V");
                                
                                cobroHistorial.setDetalleGeneralId(detalleGeneral);
                                cobroHistorial.setCobroId(cobro);
                                cobroHistorialList.add(cobroHistorial);
                                
                                if (totalACobrar == 0f) {
                                    break;
                                }
                                
                            } else {
                                LOGGER.error("Error trantando de obtener el detalle_general record from DB");
                                cobroResponse.setMsgEnum(MessageEnum.FAIL);
                                //     FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Atencion", "Contacte al Admnistrador."));
                                return cobroResponse;
                            }
                            
                        }
                        
                    } else {
                        LOGGER.error("Error trantando de crear cobro... En la DB no se consiguio la lista de DETALLE_GENERAL_IDs");
                        // FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Atencion", "Contacte al Admnistrador."));
                        cobroResponse.setMsgEnum(MessageEnum.FAIL);
                        return cobroResponse;
                        
                    }
                    
                    if (totalACobrar == 0f) {
                        break;
                    }
                }
                
                System.out.println("TOTAL COBRAR AND THE END ::" + totalACobrar);
                
                if (!cobroHistorialList.isEmpty()) {
                    
                    cobro.setFechaCobro(fechaEfectiva);
                    cobro.setFechaCreacion(new Date());
                    cobro.setCobroHistorialList(cobroHistorialList);
                    cobro.setStatusCobro("V");
                    cobro.setMontoCobrado(montoTotalCobrado);
                    cobro.setComentario(commentario);
                    cobroResponse = cobroDao.crearCobro(cobro);
                    System.out.println("");
                    System.out.println("Cantidad de RECORDS Cobro Historial agregados a la DB--->" + cobroHistorialList.size());
                    if (cobroResponse.getMsgEnum().getCode() == 1) {
                        
                        LOGGER.info("cOBRO FUE AGREGADO A LA DB");
                    } else {
                        LOGGER.error("Error agregando cobro en la DB");
                    }
                }
                
            }
            
        } catch (Exception e) {
            LOGGER.error("Error tratando de Crear un CObro ::" + e);
            
        }
        
        return cobroResponse;
    }
    
}
