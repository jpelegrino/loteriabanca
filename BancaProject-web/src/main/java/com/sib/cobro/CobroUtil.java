package com.sib.cobro;

import com.sib.entity.BancaGrupoCobro;
import com.sib.entity.Cobro;
import com.sib.entity.CobroCustomEntity;
import com.sib.entity.CobroVistaDetalle;
import com.sib.ui.models.BancaGrupoCobroModel;
import com.sib.ui.models.CobroCustomEntityModel;
import com.sib.ui.models.CobroModel;
import com.sib.ui.models.CobroVistaDetalleModel;
import java.lang.reflect.InvocationTargetException;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author Snailin Inoa
 */
public class CobroUtil {

    public static final Logger LOGGER = Logger.getLogger(CobroUtil.class);

    public CobroModel convertirCobroACobroModel(Cobro cobro, Integer bancaID, Integer grupoID, String codigoBanca, String codigoGrupo) {
        CobroModel cobroModel = null;
        if (cobro != null) {
            cobroModel = new CobroModel();
            cobroModel.setId(cobro.getId());
            cobroModel.setComentario(cobro.getComentario());
            cobroModel.setFechaCobro(cobro.getFechaCobro());
            cobroModel.setMontoCobrado(cobro.getMontoCobrado());
            cobroModel.setStatusCobro(cobro.getStatusCobro());
            cobroModel.setIdBanca(bancaID);
            cobroModel.setIdGrupo(grupoID);
            cobroModel.setCodigoBanca(codigoBanca);
            cobroModel.setCodigoGrupo(codigoGrupo);

        }

        return cobroModel;
    }

    public CobroCustomEntityModel convertirCobroCustomEntityACobroCustomEntityModel(CobroCustomEntity cobroCustomEntity) {
        CobroCustomEntityModel cobroCustomEntityModel = null;

        if (cobroCustomEntity != null) {
            cobroCustomEntityModel = new CobroCustomEntityModel();
            cobroCustomEntityModel.setBancaId(cobroCustomEntity.getBancaId());
            cobroCustomEntityModel.setCobroHistorialId(cobroCustomEntity.getCobroHistorialId());
            cobroCustomEntityModel.setCobroId(cobroCustomEntity.getCobroId());
            cobroCustomEntityModel.setCobroStatus(cobroCustomEntity.getCobroStatus());
            cobroCustomEntityModel.setCodigoBanca(cobroCustomEntity.getCodigoBanca());
            cobroCustomEntityModel.setCodigoGrupo(cobroCustomEntity.getCodigoGrupo());
            cobroCustomEntityModel.setComentario(cobroCustomEntity.getComentario());
            cobroCustomEntityModel.setDetalleGeneralId(cobroCustomEntity.getDetalleGeneralId());
            cobroCustomEntityModel.setFechaEfectiva(cobroCustomEntity.getFechaEfectiva());

            cobroCustomEntityModel.setGrupoId(cobroCustomEntity.getGrupoId());
            cobroCustomEntityModel.setMontoTotal(cobroCustomEntity.getMontoTotal());
            cobroCustomEntityModel.setMontoTotalCobrado(cobroCustomEntity.getMontoTotalCobrado());

        }
        return cobroCustomEntityModel;

    }

//    public CobroHistorialModel convertirCobroHistorialACobroHistorialModel(CobroHistorial cobroHistorial) {
//        CobroHistorialModel cobroHistorialModel = null;
//
//        if (cobroHistorial != null) {
//            cobroHistorialModel = new CobroHistorialModel();
//
//            cobroHistorial.setId(cobroHistorial.getId());
//            CobroModel cobroModel = convertirCobroACobroModel(cobroHistorial.getCobroId());
//            cobroHistorialModel.setCobroId(cobroModel);
//            cobroHistorialModel.setMontoCobrado(cobroHistorial.getMontoCobrado());
//            cobroHistorialModel.setStatusCobroHistorial(cobroHistorial.getStatusCobroHistorial());
//            //      cobroHistorialModel.se
//
//        }
//        return cobroHistorialModel;
//    }
    public BancaGrupoCobroModel convertirBancaGrupoCobroABancaGrupoCobroModel(BancaGrupoCobro bancaGrupoCobro) {
        BancaGrupoCobroModel cur = null;

        if (bancaGrupoCobro != null) {
            byte[] bytesArr = SerializationUtils.serialize(bancaGrupoCobro);
            BancaGrupoCobro cobro = (BancaGrupoCobro) SerializationUtils.deserialize(bytesArr);

            if (cobro != null) {
                try {
                    cur = new BancaGrupoCobroModel();
                    BeanUtils.copyProperties(cur, cobro);
                } catch (IllegalAccessException ex) {
                    LOGGER.error(ex.getMessage(), ex);
                } catch (InvocationTargetException ex) {
                    LOGGER.error(ex.getMessage(), ex);
                }
            }
        }

        return cur;

    }

    public CobroVistaDetalleModel convertirCobroVistaDetalleACobroVistaDetalleModel(CobroVistaDetalle cobroVistaDetalle) {
        CobroVistaDetalleModel cur = null;

        if (cobroVistaDetalle != null) {
            byte[] bytesArr = SerializationUtils.serialize(cobroVistaDetalle);
            CobroVistaDetalle cobro = (CobroVistaDetalle) SerializationUtils.deserialize(bytesArr);

            if (cobro != null) {
                try {
                    cur = new CobroVistaDetalleModel();
                    BeanUtils.copyProperties(cur, cobro);
                } catch (IllegalAccessException ex) {
                    LOGGER.error(ex.getMessage(), ex);
                } catch (InvocationTargetException ex) {
                    LOGGER.error(ex.getMessage(), ex);
                }
            }
        }

        return cur;

    }

}
