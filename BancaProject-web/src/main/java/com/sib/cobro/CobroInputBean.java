package com.sib.cobro;

import com.sib.ui.enums.CobroStatusEnum;
import com.sib.util.DateFormatUtil;
import java.io.Serializable;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "cobroInputBean")
@ViewScoped
public class CobroInputBean implements Serializable {

    private Integer grupoId;
    private Integer grupoIdForFilter;
    private Integer bancaId;
    private Date fechaInicial;
    private Date fechaFinal;
    private String status = CobroStatusEnum.V.getCodigo();

    public Integer getGrupoIdForFilter() {
        return grupoIdForFilter;
    }

    public void setGrupoIdForFilter(Integer grupoIdForFilter) {
        this.grupoIdForFilter = grupoIdForFilter;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public CobroInputBean() {
    }

    public Integer getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(Integer grupoId) {
        this.grupoId = grupoId;
    }

    public Integer getBancaId() {
        return bancaId;
    }

    public void setBancaId(Integer bancaId) {
        this.bancaId = bancaId;
    }

    public String getFormattedInitDate() {
        String result = "";
        if (fechaInicial != null) {
            result = DateFormatUtil.
                    convertDateToStringWithStandardFormat(
                            this.getFechaInicial());
        }

        return result;
    }

    public String getFormattedFinalDate() {
        String result = "";
        if (fechaFinal != null) {
            result = DateFormatUtil.
                    convertDateToStringWithStandardFormat(
                            this.getFechaFinal());

        }

        return result;
    }

    public void cleanFields() {
        grupoId = null;
        grupoId = null;
        fechaInicial = null;
        fechaFinal = null;
        grupoIdForFilter = null;
        status = CobroStatusEnum.V.getCodigo();

    }
}
