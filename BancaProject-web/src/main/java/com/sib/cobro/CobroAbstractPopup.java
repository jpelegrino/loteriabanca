package com.sib.cobro;

import com.sib.interfaces.IPopup;
import com.sib.ui.models.CobroModel;
import javax.faces.bean.ManagedProperty;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Snailin Inoa
 */
public abstract class CobroAbstractPopup implements IPopup {
    

    private CobroModel model;

    @ManagedProperty(value = "#{cobroModelBean}")
    private CobroModelBean modelBean;

    @ManagedProperty(value = "#{bancaGrupoCobroModelBean}")
    private BancaGrupoCobroModelBean bancaGrupoCobroModelBean;

    public CobroModel getModel() {
        return model;
    }

    public void setModel(CobroModel model) {
        this.model = model;
    }

    public CobroModelBean getModelBean() {
        return modelBean;
    }

    public void setModelBean(CobroModelBean modelBean) {
        this.modelBean = modelBean;
    }

    public BancaGrupoCobroModelBean getBancaGrupoCobroModelBean() {
        return bancaGrupoCobroModelBean;
    }

    public void setBancaGrupoCobroModelBean(BancaGrupoCobroModelBean bancaGrupoCobroModelBean) {
        this.bancaGrupoCobroModelBean = bancaGrupoCobroModelBean;
    }

    
    

}
