package com.sib.cobro;

import static com.sib.cobro.CobroViewPopupBean.LOGGER;
import com.sib.ui.models.CobroCustomEntityModel;
import com.sib.ui.models.CobroModel;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "cobroViewSingleDetailPopup")
@ViewScoped
public class CobroViewSingleDetailPopup extends CobroAbstractPopup implements Serializable {

    private CobroCustomEntityModel cobroCustomEntityModel;

    public CobroCustomEntityModel getCobroCustomEntityModel() {
        return cobroCustomEntityModel;
    }

    public void setCobroCustomEntityModel(CobroCustomEntityModel cobroCustomEntityModel) {
        this.cobroCustomEntityModel = cobroCustomEntityModel;
    }

    @Override
    public void execute(ActionEvent event) {

    }

    @Override
    public void cancelProcess(ActionEvent event) {
        this.reset();
    }

    @Override
    public void reset() {
        cobroCustomEntityModel = null;
    }

    @Override
    public boolean validateFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getFileName() {
        return "cobroviewsingledetailpopup";
    }

    @Override
    public String getPopupTitle() {
        return "Cobro Historial";
    }

    public void initialize(ActionEvent event) {
        LOGGER.info("initialize() :: START");
        this.reset();
        CobroCustomEntityModel cobro = (CobroCustomEntityModel) event.getComponent().getAttributes().get("cobro");
        LOGGER.info("Cobro :: " + cobro);
        if (cobro != null) {
            this.cobroCustomEntityModel = cobro;
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("operationStatus", "000");
        }
        LOGGER.info("initialize() :: END");
    }

}
