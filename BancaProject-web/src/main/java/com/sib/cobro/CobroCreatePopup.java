package com.sib.cobro;

import com.sib.bs.responses.CobroResponse;
import com.sib.ui.models.BancaGrupoCobroModel;
import com.sib.ui.models.CobroModel;
import com.sib.util.NumberFormatUtil;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "cobroCreatePopup")
@ViewScoped
public class CobroCreatePopup extends CobroAbstractPopup implements Serializable {

    public static final Logger LOGGER = Logger.getLogger(CobroCreatePopup.class);

    private List<BancaGrupoCobroModel> bancaGrupoCobroModelList;
    //UI FIELD USADOS EN CREATE POPUP
    private Integer[] monedasValores = new Integer[10];

    private String[] monedasValoresString = new String[10];

    private Integer totalACobrarUIField = 0;
    private Integer devueltaUIField = 0;
    private Integer totalAPagarUIField = 0;
    private String totalAPagarString;
    private String commentarioUIField;
    private Date fechaEfectiva;
    private Integer montoTotalBancasSeleccionadas = 0;

    public String getTotalAPagarString() {
        return totalAPagarString;
    }

    public void setTotalAPagarString(String totalAPagarString) {
        this.totalAPagarString = totalAPagarString;
    }

    public String[] getMonedasValoresString() {
        return monedasValoresString;
    }

    public void setMonedasValoresString(String[] monedasValoresString) {
        this.monedasValoresString = monedasValoresString;
    }

    public Date getFechaEfectiva() {
        return fechaEfectiva;
    }

    public void setFechaEfectiva(Date fechaEfectiva) {
        this.fechaEfectiva = fechaEfectiva;
    }

    public Integer getMontoTotalBancasSeleccionadas() {
        return montoTotalBancasSeleccionadas;
    }

    public void setMontoTotalBancasSeleccionadas(Integer montoTotalBancasSeleccionadas) {
        this.montoTotalBancasSeleccionadas = montoTotalBancasSeleccionadas;
    }

    public Integer getTotalACobrarUIField() {
        return totalACobrarUIField;
    }

    public void setTotalACobrarUIField(Integer totalACobrarUIField) {
        this.totalACobrarUIField = totalACobrarUIField;
    }

    public Integer getDevueltaUIField() {
        return devueltaUIField;
    }

    public void setDevueltaUIField(Integer devueltaUIField) {
        this.devueltaUIField = devueltaUIField;
    }

    public Integer getTotalAPagarUIField() {
        return totalAPagarUIField;
    }

    public void setTotalAPagarUIField(Integer totalAPagarUIField) {
        this.totalAPagarUIField = totalAPagarUIField;
    }

    public String getCommentarioUIField() {
        return commentarioUIField;
    }

    public void setCommentarioUIField(String commentarioUIField) {
        this.commentarioUIField = commentarioUIField;
    }

    public Integer[] getMonedasValores() {
        return monedasValores;
    }

    public void setMonedasValores(Integer[] monedasValores) {
        this.monedasValores = monedasValores;
    }

    public List<BancaGrupoCobroModel> getBancaGrupoCobroModelList() {
        return bancaGrupoCobroModelList;
    }

    public void setBancaGrupoCobroModelList(List<BancaGrupoCobroModel> bancaGrupoCobroModelList) {
        this.bancaGrupoCobroModelList = bancaGrupoCobroModelList;
    }

    @Override
    public String getFileName() {
        return "cobrocreatepopup";
    }

    //  public void 
    @Override
    public void execute(ActionEvent event) {
        LOGGER.info("execute() :: START");
        RequestContext context = RequestContext.getCurrentInstance();
        if (validateFields()) {
            System.out.println("READY IN THE EXECUTE METHOD");

            if (bancaGrupoCobroModelList != null) {

                LOGGER.info("Model --->" + bancaGrupoCobroModelList);

                Integer totalAPagar = totalAPagarUIField;

                CobroResponse cobroResponse = this.getBancaGrupoCobroModelBean().crearCobro(bancaGrupoCobroModelList, Float.parseFloat(totalAPagar.toString()), commentarioUIField, this.fechaEfectiva);
                if (cobroResponse.getMsgEnum().getCode() == 1) {
                    CobroBean cobroBean = (CobroBean) FacesContext.getCurrentInstance().getViewRoot().getViewMap().get("cobroBean");
                    cobroBean.setBancaGrupoCobroModelSelectedList(null);
                    this.getBancaGrupoCobroModelBean().resetModelBean();

                    FacesContext.getCurrentInstance().addMessage("cobroMsg", new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion", "Cobro realizado con exito"));
                    context.addCallbackParam("operationStatus", "000");
                } else {
                    FacesContext.getCurrentInstance().addMessage("createPopupMsg", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Atencion", "Favor contactar al Administrador"));
                    context.addCallbackParam("operationStatus", "001");
                }
            }
        } else {
            context.addCallbackParam("operationStatus", "001");
        }
        LOGGER.info("execute() :: END");
    }

    @Override
    public void cancelProcess(ActionEvent event) {
        LOGGER.info("cancelProcess :: START");
        //this.getBancaGrupoCobroModelBean().resetModelBean();

        LOGGER.info("cancelProcess :: END");
    }

    @Override
    public void reset() {
        this.setModel(new CobroModel());
        bancaGrupoCobroModelList = null;
        initializeMonedasValoresArray();
        totalACobrarUIField = 0;
        totalAPagarUIField = 0;
        commentarioUIField = null;
        devueltaUIField = 0;
        montoTotalBancasSeleccionadas = 0;
    }

    @Override
    public boolean validateFields() {

        LOGGER.info("validateFields() :: START");
        boolean result = false;
        try {
            if (this.totalAPagarString != null && !this.totalAPagarString.trim().isEmpty()) {
                if (!String.valueOf(totalAPagarString).matches("^[0-9]+$")) {

                    FacesContext.getCurrentInstance().addMessage("createPopupMsg", new FacesMessage(FacesMessage.SEVERITY_WARN, "Atencion", "Por favor introduzca un monto válido para el campo Total a Pagar. Ejemplo: 100, 10, 5. "));
                    return false;
                } else {
                    totalAPagarUIField = Integer.valueOf(totalAPagarString);
                }
            }

            if (this.totalAPagarUIField != null && this.totalAPagarUIField > 0) {
                result = true;

            } else {
                FacesContext.getCurrentInstance().addMessage("createPopupMsg", new FacesMessage(FacesMessage.SEVERITY_WARN, "Atencion", "Debe de Introducir el monto total a pagar"));
                return result;

            }

            if (result) {
                if (totalAPagarUIField.intValue() > montoTotalBancasSeleccionadas.intValue()) {
                    FacesContext.getCurrentInstance().addMessage("createPopupMsg", new FacesMessage(FacesMessage.SEVERITY_WARN, "Atencion", "El Total a Pagar debe ser igual o menor al monto total seleccionado"));
                    result = false;
                    return result;
                }
            }

            if (this.commentarioUIField != null && !this.commentarioUIField.isEmpty()) {
                result = true;

            } else {
                FacesContext.getCurrentInstance().addMessage("createPopupMsg", new FacesMessage(FacesMessage.SEVERITY_WARN, "Atencion", "Debe de Introducir un comentario para realizar el cobro"));
                result = false;
                return result;
            }

            for (int i = 0; i < monedasValoresString.length; i++) {
                if (monedasValoresString[i] == null || monedasValoresString[i].trim().isEmpty()) {
                    monedasValoresString[i] = "0";
                    monedasValores[i] = 0;
                    continue;
                }
                if (monedasValoresString[i] != null && !monedasValoresString[i].isEmpty() && !monedasValoresString[i].equals("0")) {

                    if (!String.valueOf(monedasValoresString[i]).matches("^[0-9]+$")) {
                        result = false;
                        FacesContext.getCurrentInstance().addMessage("createPopupMsg", new FacesMessage(FacesMessage.SEVERITY_WARN, "Atencion", "Por favor introduzca un monto válido para el detalle de monedas. Ejemplo: 100, 100.20 o 200.00 "));
                        return result;

                    } else {
                        monedasValores[i] = Integer.valueOf(monedasValoresString[i]);
                    }
                }
            }

            result = false;
            for (int i = 0; i < monedasValores.length; i++) {
                if (monedasValores[i] == null) {
                    monedasValores[i] = 0;
                }
                if (monedasValores[i] != 0) {
                    result = true;
                    break;
                }
            }

            if (!result) {

                FacesContext.getCurrentInstance().addMessage("createPopupMsg", new FacesMessage(FacesMessage.SEVERITY_WARN, "Atencion", "Debe de Introducir el Detalle de moneda para poder realizar un cobro"));
                return result;
            }

            if (result) {
                if (this.totalACobrarUIField.intValue() >= this.totalAPagarUIField.intValue()) {

                } else {
                    FacesContext.getCurrentInstance().addMessage("createPopupMsg", new FacesMessage(FacesMessage.SEVERITY_WARN, "Atencion", "La Sumatoria del desglose de monedas debe ser igual o mayor al campo Total a Pagar"));
                    result = false;
                    return result;

                }
            }

        } catch (Exception e) {
            LOGGER.error("ERROR validando el cobro ::" + e);
        }
        LOGGER.info("validateFields() :: END");

        return result;
    }

    @Override
    public String getPopupTitle() {
        return "Administrar Cobros";
    }

    public void initialize() {
        LOGGER.info("initialize() :: START");
        fechaEfectiva = new Date(); // seteando la fecha actual por default

        for (BancaGrupoCobroModel bancaGrupoCobroModel : bancaGrupoCobroModelList) {
            montoTotalBancasSeleccionadas += bancaGrupoCobroModel.getDeuda().intValue();
        }

        //  montoTotalBancasSeleccionadas = NumberFormatUtil.redondearFloat(montoTotalBancasSeleccionadas);
        LOGGER.info("initialize() :: END");

    }

    public Integer calcularDetalleMoneda() {
        verificarDetalleMonedaNull();
        Integer totalMonedas1 = monedasValores[0].intValue() * 1;
        Integer totalMonedas2 = monedasValores[1].intValue() * 5;
        Integer totalMonedas3 = monedasValores[2].intValue() * 10;
        Integer totalMonedas4 = monedasValores[3].intValue() * 25;
        Integer totalMonedas5 = monedasValores[4].intValue() * 50;
        Integer totalMonedas6 = monedasValores[5].intValue() * 100;
        Integer totalMonedas7 = monedasValores[6].intValue() * 200;
        Integer totalMonedas8 = monedasValores[7].intValue() * 500;
        Integer totalMonedas9 = monedasValores[8].intValue() * 1000;
        Integer totalMonedas10 = monedasValores[9].intValue() * 2000;

        Integer totalAPagar = totalMonedas1 + totalMonedas2 + totalMonedas3 + totalMonedas4 + totalMonedas5 + totalMonedas6 + totalMonedas7 + totalMonedas8 + totalMonedas9 + totalMonedas10;
        return totalAPagar;
    }

    public void refrescarTotalPagarFieldUI() {

        for (int i = 0; i < monedasValoresString.length; i++) {
            if (monedasValoresString[i] == null || monedasValoresString[i].trim().isEmpty()) {
                monedasValoresString[i] = "0";
                monedasValores[i] = 0;
                continue;
            }
            if (monedasValoresString[i] != null && !monedasValoresString[i].isEmpty() && !monedasValoresString[i].equals("0")) {

                if (!String.valueOf(monedasValoresString[i]).matches("^[0-9]+$")) {

                    FacesContext.getCurrentInstance().addMessage("createPopupMsg", new FacesMessage(FacesMessage.SEVERITY_WARN, "Atencion", "Por favor introduzca un monto válido para el detalle de monedas. Ejemplo: 100, 10, 5. "));

                    return;

                } else {
                    monedasValores[i] = Integer.valueOf(monedasValoresString[i]);
                }
            }
        }

        if (this.totalAPagarString != null && !this.totalAPagarString.trim().isEmpty()) {
            if (!String.valueOf(totalAPagarString).matches("^[0-9]+$")) {

                FacesContext.getCurrentInstance().addMessage("createPopupMsg", new FacesMessage(FacesMessage.SEVERITY_WARN, "Atencion", "Por favor introduzca un monto válido para el campo Total a Pagar. Ejemplo: 100, 10, 5. "));

                return;
            } else {
                totalAPagarUIField = Integer.valueOf(totalAPagarString);
            }
        }
        this.setTotalACobrarUIField(calcularDetalleMoneda());
        refrescarDevueltaFieldUI();

    }

    public void refrescarDevueltaFieldUI() {
        if (totalAPagarUIField == null) {
            totalAPagarUIField = 0;
        }
        if (this.totalACobrarUIField.intValue() >= totalAPagarUIField.intValue()) {
            this.devueltaUIField = totalACobrarUIField.intValue() - totalAPagarUIField.intValue();
        } else {
            devueltaUIField = 0;
        }
    }

    private void initializeMonedasValoresArray() {
        LOGGER.info("initializeMonedasValoresArray() :: START");
        // this method va a setear los valores de las 10 posisicones del arreglo a CERO(0)
        for (int i = 0; i < monedasValores.length; i++) {
            monedasValores[i] = 0;
        }
        LOGGER.info("initializeMonedasValoresArray() :: END");

    }

    private void verificarDetalleMonedaNull() {

        for (int i = 0; i < monedasValores.length; i++) {
            if (monedasValores[i] == null) {
                monedasValores[i] = 0;
            }
        }
    }
}
