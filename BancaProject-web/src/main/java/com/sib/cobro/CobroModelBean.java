package com.sib.cobro;

import com.sib.bs.responses.CobroCustomEntityResponse;
import com.sib.bs.responses.CobroResponse;
import com.sib.bs.responses.DetalleGeneralResponse;
import com.sib.ejb.dao.CobroDao;
import com.sib.ejb.dao.DetalleGeneralDao;
import com.sib.entity.Cobro;
import com.sib.entity.CobroCustomEntity;
import com.sib.entity.CobroHistorial;
import com.sib.interfaces.IModelBean;
import com.sib.ui.enums.CobroStatusEnum;
import com.sib.ui.models.CobroCustomEntityModel;
import com.sib.ui.models.CobroModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "cobroModelBean")
@ViewScoped
public class CobroModelBean extends LazyDataModel implements IModelBean, Serializable {

    public static final Logger LOGGER = Logger.getLogger(CobroModelBean.class);

    @ManagedProperty(value = "#{cobroInputBean}")
    private CobroInputBean inputBean;

    private List<CobroModel> result;
    private CobroUtil cobroUtil = new CobroUtil();
    private List<CobroCustomEntityModel> cobroCustomEntityModelList;
    @EJB
    CobroDao cobroDao;
    @EJB
    DetalleGeneralDao detalleGeneralDao;
    private int dataSetSize = 0;

    public CobroModelBean() {
    }

    public List<CobroCustomEntityModel> getCobroCustomEntityModelList() {
        return cobroCustomEntityModelList;
    }

    public void setCobroCustomEntityModelList(List<CobroCustomEntityModel> cobroCustomEntityModelList) {
        this.cobroCustomEntityModelList = cobroCustomEntityModelList;
    }

    public List<CobroModel> getResult() {
        if (result == null) {
            this.initialize();
        }
        return result;
    }

    public void setResult(List<CobroModel> result) {
        this.result = result;
    }

    @Override
    public void resetModelBean() {
        // this.result = null;
        this.dataSetSize = 0;
        this.cobroCustomEntityModelList = null;
    }

    @Override
    public List load(int first, int pageSize, String sortField, SortOrder sortOrder, Map filters) {

        if (getDatasetSizeGastos() > 0) {

            CobroCustomEntityResponse cobroCustomEntityResponse = cobroDao.getCobroCustomEntityLazyList(inputBean.getFormattedInitDate(), inputBean.getFormattedFinalDate(),
                    inputBean.getGrupoIdForFilter(), inputBean.getBancaId(), sortField, SortOrder.ASCENDING.equals(sortOrder) ? "ASC" : "DESC", first, pageSize, inputBean.getStatus());

            cobroCustomEntityModelList = new ArrayList<CobroCustomEntityModel>();
            for (CobroCustomEntity cobroCustomEntity : cobroCustomEntityResponse.getCobroCustomEntityList()) {
                CobroCustomEntityModel model = cobroUtil.convertirCobroCustomEntityACobroCustomEntityModel(cobroCustomEntity);
                cobroCustomEntityModelList.add(model);
            }
            this.setRowCount(cobroCustomEntityModelList.size());
        } else {
            cobroCustomEntityModelList = new ArrayList<CobroCustomEntityModel>();
            this.setRowCount(0);
        }
        return cobroCustomEntityModelList;
    }

    private int getDatasetSizeGastos() {
        try {
            if (this.dataSetSize == 0) {

                LOGGER.info("GRUPO ID::" + inputBean.getGrupoId());
                LOGGER.info("BANCA ID::" + inputBean.getBancaId());
                LOGGER.info("COBRO STATUS " + inputBean.getStatus());
                LOGGER.info("Start Date::" + inputBean.getFormattedInitDate());
                LOGGER.info("End Date::" + inputBean.getFormattedFinalDate());

                dataSetSize = this.cobroDao.getCobroCustomEntityTotalRecords(inputBean.getFormattedInitDate(), inputBean.getFormattedFinalDate(),
                        inputBean.getGrupoId(), inputBean.getBancaId(), "", "", inputBean.getStatus());

            }
            LOGGER.info("dataSetSize :: " + dataSetSize);
        } catch (Exception ex) {
            LOGGER.error("Error retrieving Gastos datasetsize. Error: "
                    + ex.getMessage(), ex);
        }

        return dataSetSize;
    }

    private void initialize() {
        LOGGER.info("initialize() :: START");
        //status V-Valido and A-Anulado
        try {
            System.out.println("Hello");
//            CobroResponse cobroResponse = cobroDao.getCobros(CobroStatusEnum.V.getCodigo());
//            result = new ArrayList<CobroModel>();
//            for (Cobro cobro : cobroResponse.getCobros()) {
//                DetalleGeneralResponse detalleGeneralResponse = detalleGeneralDao.getDetalleGeneralById(cobro.getCobroHistorialList().get(0).getDetalleGeneralId().getId());
//                CobroModel model = cobroUtil.convertirCobroACobroModel(cobro,
//                        detalleGeneralResponse.getDetalleGeneral().getBancaLoteria().getBanca().getId(),
//                        detalleGeneralResponse.getDetalleGeneral().getGeneralId().getGrupoId().getId(),
//                        detalleGeneralResponse.getDetalleGeneral().getBancaLoteria().getBanca().getCodigo(),
//                        detalleGeneralResponse.getDetalleGeneral().getGeneralId().getGrupoId().getCodigo());
            //  result.add(model);
            //}
        } catch (Exception e) {
            LOGGER.info("ERROR tratando de Obtener todos los cobros ::" + e);
        }

        LOGGER.info(
                "initialize() :: END");
    }

    public CobroInputBean getInputBean() {
        return inputBean;
    }

    public void setInputBean(CobroInputBean inputBean) {
        this.inputBean = inputBean;
    }

}
