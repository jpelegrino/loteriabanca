package com.sib.cobro;

import com.sib.grupo.GrupoModelBean;
import com.sib.interfaces.IPopup;
import com.sib.ui.enums.StatusEnum;
import com.sib.ui.models.BancaGrupoCobroModel;
import com.sib.ui.models.GrupoModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "cobroBean")
@ViewScoped
public class CobroBean implements Serializable {

    public static final Logger LOGGER = Logger.getLogger(CobroBean.class);
    @ManagedProperty(value = "#{grupoModelBean}")
    private GrupoModelBean grupoModelBean;
    private IPopup popupHandler;
    private List<SelectItem> gruposSI;
    @ManagedProperty(value = "#{cobroModelBean}")
    private CobroModelBean modelBean;

    @ManagedProperty(value = "#{bancaGrupoCobroModelBean}")
    private BancaGrupoCobroModelBean bancaGrupoCobroModelBean;

    @ManagedProperty(value = "#{cobroCreatePopup}")
    private IPopup createPopup;

    @ManagedProperty(value = "#{cobroViewPopupBean}")
    private IPopup viewCobroPopup;

    @ManagedProperty(value = "#{cobroAnularPopup}")
    private IPopup cobroAnularPopop;

    @ManagedProperty(value = "#{cobroViewSingleDetailPopup}")
    private IPopup cobroViewSingleDetailPopup;

    public IPopup getCobroViewSingleDetailPopup() {
        return cobroViewSingleDetailPopup;
    }

    public void setCobroViewSingleDetailPopup(IPopup cobroViewSingleDetailPopup) {
        this.cobroViewSingleDetailPopup = cobroViewSingleDetailPopup;
    }

    public IPopup getCobroAnularPopop() {
        return cobroAnularPopop;
    }

    public void setCobroAnularPopop(IPopup cobroAnularPopop) {
        this.cobroAnularPopop = cobroAnularPopop;
    }

    public IPopup getViewCobroPopup() {
        return viewCobroPopup;
    }

    public void setViewCobroPopup(IPopup viewCobroPopup) {
        this.viewCobroPopup = viewCobroPopup;
    }

    private List<BancaGrupoCobroModel> bancaGrupoCobroModelSelectedList;

    public List<BancaGrupoCobroModel> getBancaGrupoCobroModelSelectedList() {
        if (bancaGrupoCobroModelSelectedList == null) {
            bancaGrupoCobroModelSelectedList = new ArrayList<BancaGrupoCobroModel>();
        }
        return bancaGrupoCobroModelSelectedList;
    }

    public void setBancaGrupoCobroModelSelectedList(List<BancaGrupoCobroModel> bancaGrupoCobroModelSelectedList) {
        this.bancaGrupoCobroModelSelectedList = bancaGrupoCobroModelSelectedList;
    }

    public IPopup getCreatePopup() {
        return createPopup;
    }

    public void setCreatePopup(IPopup createPopup) {
        this.createPopup = createPopup;
    }

    public CobroModelBean getModelBean() {
        return modelBean;
    }

    public void setModelBean(CobroModelBean modelBean) {
        this.modelBean = modelBean;
    }

    public IPopup getPopupHandler() {
        if (popupHandler == null) {
            popupHandler = createPopup;
        }
        return popupHandler;
    }

    public void setPopupHandler(IPopup popupHandler) {
        this.popupHandler = popupHandler;
    }

    public void tabChange(TabChangeEvent event) {
        LOGGER.info("tabChange() :: START");
        this.getModelBean().resetModelBean();
        this.setGruposSI(null);
        this.setBancaGrupoCobroModelSelectedList(null);
     this.getBancaGrupoCobroModelBean().getInputBean().cleanFields();
        this.getBancaGrupoCobroModelBean().resetModelBean();
        LOGGER.info("tabChange() :: END");
    }

    public void openCreatePopup(ActionEvent event) {
        LOGGER.info("openCreatePopup() :: START");
        LOGGER.info("SELECTED LIST SIZE ::" + this.getBancaGrupoCobroModelSelectedList().size());
        RequestContext context = RequestContext.getCurrentInstance();
        if (this.getBancaGrupoCobroModelSelectedList().isEmpty() && this.getBancaGrupoCobroModelBean().getInputBean().getGrupoId() == 0) {
            FacesContext.getCurrentInstance().addMessage("cobroMsg", new FacesMessage(FacesMessage.SEVERITY_WARN, "Atencion", "Debe de Seleccionar un grupo para luego realizar el cobro."));
            context.addCallbackParam("operationStatus", "001");

        } else if (this.getBancaGrupoCobroModelSelectedList().isEmpty()) {
            FacesContext.getCurrentInstance().addMessage("cobroMsg", new FacesMessage(FacesMessage.SEVERITY_WARN, "Atencion", "Debe de Seleccionar al menos un cobro."));
            context.addCallbackParam("operationStatus", "001");
        } else {

            this.setPopupHandler(createPopup);
            ((CobroCreatePopup) this.getPopupHandler()).reset();
            ((CobroCreatePopup) this.getPopupHandler()).setBancaGrupoCobroModelList(this.bancaGrupoCobroModelSelectedList);
            ((CobroCreatePopup) this.getPopupHandler()).initialize();

            context.addCallbackParam("operationStatus", "000");
        }
        LOGGER.info("openCreatePopup() :: END");

    }

    public BancaGrupoCobroModelBean getBancaGrupoCobroModelBean() {
        return bancaGrupoCobroModelBean;
    }

    public void setBancaGrupoCobroModelBean(BancaGrupoCobroModelBean bancaGrupoCobroModelBean) {
        this.bancaGrupoCobroModelBean = bancaGrupoCobroModelBean;
    }

    public List<SelectItem> getGruposSI() {
        if (gruposSI == null) {
            List<GrupoModel> gruposActivos
                    = this.grupoModelBean.getGruposByStatus(StatusEnum.ACTIVO.getStatusLiteral());
            gruposSI = new ArrayList<SelectItem>();

            if (gruposActivos != null) {
                for (GrupoModel gm : gruposActivos) {
                    gruposSI.add(new SelectItem(gm.getId(), gm.getCodigo()));
                }
            }
        }
        return gruposSI;
    }

    public void setGruposSI(List<SelectItem> gruposSI) {
        this.gruposSI = gruposSI;
    }

    public GrupoModelBean getGrupoModelBean() {
        return grupoModelBean;
    }

    public void setGrupoModelBean(GrupoModelBean grupoModelBean) {
        this.grupoModelBean = grupoModelBean;
    }

    public void onGrupoSelectionChange() {
        LOGGER.info("onGrupoSelectionChange :: START");

        this.getBancaGrupoCobroModelBean().resetModelBean();
        this.setBancaGrupoCobroModelSelectedList(null);
        if (this.getBancaGrupoCobroModelBean().getInputBean().getGrupoId() == 0) {
            this.getBancaGrupoCobroModelBean().setShowCheckBox(false);

        } else {
            if (this.getBancaGrupoCobroModelBean().getBancaGrupoCobroModelList().size() > 0) {
                this.getBancaGrupoCobroModelBean().setShowCheckBox(true);
            }
        }

        LOGGER.info("onGrupoSelectionChange :: END");
    }

    public void rowSelection(SelectEvent event) {
        LOGGER.info("rowSelection() :: START");
        System.out.println("OBEJCT -->" + event.getObject());
        LOGGER.info("rowSelection() :: END");

    }

    public void openViewPopup(ActionEvent event) {
        LOGGER.info("openViewPopup() :: START");
        this.setPopupHandler(this.viewCobroPopup);
        ((CobroViewPopupBean) this.getPopupHandler()).initialize(event);
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("operationStatus", "000");
        LOGGER.info("openViewPopup() :: END");

    }

    public void openCobroAnularPopup(ActionEvent event) {
        LOGGER.info("openCobroAnularPopup() :: START");
        this.setPopupHandler(this.cobroAnularPopop);
        ((CobroAnularPopup) this.getPopupHandler()).initialize(event);

        LOGGER.info("openCobroAnularPopup() :: END");
    }

    public void openViewSingleDetailsPopup(ActionEvent event) {
        LOGGER.info("openViewSingleDetailsPopup() :: START");
        this.setPopupHandler(this.cobroViewSingleDetailPopup);
        ((CobroViewSingleDetailPopup) this.getPopupHandler()).initialize(event);
        LOGGER.info("openViewSingleDetailsPopup() :: END");
    }

    public void executeFilter(ActionEvent event) {
        LOGGER.info("executeFilter() :: START");
        this.getModelBean().resetModelBean();
        LOGGER.info("executeFilter() :: END");
    }

    public void cleanFilter(ActionEvent e) {
        LOGGER.info("cleanFilter() :: START");
        this.getModelBean().getInputBean().cleanFields();
        this.getModelBean().resetModelBean();
        LOGGER.info("cleanFilter() :: START");
    }

}
