package com.sib.cobro;

import com.sib.bs.responses.CobroHistorialResponse;
import com.sib.bs.responses.CobroResponse;
import com.sib.bs.responses.CobroVistaDetalleResponse;
import com.sib.bs.responses.DetalleGeneralResponse;
import static com.sib.cobro.CobroViewPopupBean.LOGGER;
import com.sib.ejb.dao.CobroDao;
import com.sib.ejb.dao.CobroHistorialDao;
import com.sib.ejb.dao.DetalleGeneralDao;
import com.sib.entity.Cobro;
import com.sib.entity.CobroHistorial;
import com.sib.entity.CobroVistaDetalle;
import com.sib.entity.DetalleGeneral;
import com.sib.ui.enums.CobroStatusEnum;
import com.sib.ui.models.CobroCustomEntityModel;
import com.sib.ui.models.CobroModel;
import com.sib.util.NumberFormatUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "cobroAnularPopup")
@ViewScoped
public class CobroAnularPopup extends CobroAbstractPopup implements Serializable {

    public static final Logger LOGGER = Logger.getLogger(CobroAnularPopup.class);
    @EJB
    CobroHistorialDao cobroHistorialDao;

    @EJB
    CobroDao cobroDao;

    @EJB
    DetalleGeneralDao detalleGeneralDao;

    private CobroCustomEntityModel cobroCustomEntityModel;

    public CobroCustomEntityModel getCobroCustomEntityModel() {
        return cobroCustomEntityModel;
    }

    public void setCobroCustomEntityModel(CobroCustomEntityModel cobroCustomEntityModel) {
        this.cobroCustomEntityModel = cobroCustomEntityModel;
    }

    @Override
    public void execute(ActionEvent event) {
        LOGGER.info("execute() :: START");
        LOGGER.info("COBRO ID :: " + cobroCustomEntityModel.getCobroId());

        Cobro cobro = cobroDao.getCobroById(cobroCustomEntityModel.getCobroId()).getCobro();
        cobro.setStatusCobro(CobroStatusEnum.A.getCodigo());
        cobroDao.updateCobro(cobro);   //Actualizo aqui el COBRO PERO HAY ALGO RARO PORQUE NO SE QUIERE ACTUALIZAR AL FINAL

        CobroHistorialResponse cobroHistorialResponse = cobroHistorialDao.getCobroHistorialListByCobroID(cobroCustomEntityModel.getCobroId(), CobroStatusEnum.V.getCodigo());
        if (cobroHistorialResponse.getMsgEnum().getCode() == 1) {
            if (cobroHistorialResponse.getCobrosHistorialList() != null && !cobroHistorialResponse.getCobrosHistorialList().isEmpty()) {

                List<CobroHistorial> cobroHistorialList = cobroHistorialResponse.getCobrosHistorialList();
                LOGGER.info("Cobro Historial List Size ::" + cobroHistorialList.size());
              //  Cobro cobro = cobroDao.getCobroById(cobroModel.getId()).getCobro();

                //     cobro.setStatusCobro(CobroStatusEnum.A.getCodigo());
                //   cobroDao.updateCobro(cobro);
                LOGGER.info("Cobro ID :: " + cobro.getId());
                LOGGER.info("Cobros Historiales  relacionado con el COBRO ID :: " + cobro.getId());
                List<DetalleGeneral> detalleGeneralList = new ArrayList<DetalleGeneral>();
                List<CobroHistorial> cobroHistorialChangedList = new ArrayList<CobroHistorial>();
                for (CobroHistorial cobroHistorial : cobroHistorialList) {
                    LOGGER.info("/*******************************/");
                    LOGGER.info("Cobro Historial ID :: " + cobroHistorial.getId());
                    LOGGER.info("Detalle General ID :: " + cobroHistorial.getDetalleGeneralId().getId());
                    LOGGER.info("Monto Cobrado :: " + cobroHistorial.getMontoCobrado());
                    LOGGER.info("/*******************************/");

                    cobroHistorial.setStatusCobroHistorial(CobroStatusEnum.A.getCodigo());

                    DetalleGeneralResponse detalleGeneralResponse = detalleGeneralDao.getDetalleGeneralById(cobroHistorial.getDetalleGeneralId().getId());
                    if (detalleGeneralResponse.getMsgEnum().getCode() == 1) {
                        if (detalleGeneralResponse.getDetalleGeneral() != null) {
                            Float montoPendienteCobrar = NumberFormatUtil.redondearFloat((detalleGeneralResponse.getDetalleGeneral().getPendienteCobrar() + cobroHistorial.getMontoCobrado()));
                            LOGGER.info("Monto Pendiente a Cobrar :: " + montoPendienteCobrar);
                            detalleGeneralResponse.getDetalleGeneral().setPendienteCobrar(montoPendienteCobrar);
                            detalleGeneralResponse.getDetalleGeneral().setStatusDetalle("P");
                            detalleGeneralList.add(detalleGeneralResponse.getDetalleGeneral());
                            cobroHistorialChangedList.add(cobroHistorial);
                        }

                    } else {
                        LOGGER.error("Error obteniendo el detalle general con ID :: " + cobroHistorial.getDetalleGeneralId().getId());

                    }
                }

                if (!cobroHistorialChangedList.isEmpty() && !detalleGeneralList.isEmpty()) {
                    cobro.setCobroHistorialList(cobroHistorialChangedList);
                    CobroResponse cobroResponse = cobroDao.updateCobro(cobro);

                    if (cobroResponse.getMsgEnum().getCode() == 1) {
                        LOGGER.info("Cobro Actualizado con exito");
                        LOGGER.info("Cobro ID :: " + cobro.getId());
                        for (DetalleGeneral detalleGeneral : detalleGeneralList) {
                            DetalleGeneralResponse response = detalleGeneralDao.editarDetalleGeneral(detalleGeneral);
                            if (response.getMsgEnum().getCode() == 1) {
                                LOGGER.info("Detalle General  con el ID ::" + detalleGeneral.getId() + " Fue Actualizado con exito");
                            } else {
                                LOGGER.info("Detalle General  con el ID ::" + detalleGeneral.getId() + " NO FUE Actualizado con exito");
                            }
                        }

                        FacesContext.getCurrentInstance().addMessage("cobroAnularMsg", new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion", "Cobro Anulado con exito"));
                        RequestContext.getCurrentInstance().addCallbackParam("operationStatus", "000");
                        this.getModelBean().resetModelBean();

                    } else {
                        LOGGER.error("Error tratando de actualizar el cobro con sus cobros historiales relacionados");
                    }
                }

            }
        } else {
            LOGGER.error("Error tratando de obtener la lista de cobro historial para anularla ");
        }
        LOGGER.info("execute() :: END");
    }

    @Override
    public void cancelProcess(ActionEvent event) {
        reset();
    }

    @Override
    public void reset() {
        cobroCustomEntityModel = null;
    }

    @Override
    public boolean validateFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getFileName() {
        return "cobroanularpopup";

    }

    @Override
    public String getPopupTitle() {
        return "Anular Cobro";
    }

    public void initialize(ActionEvent event) {
        LOGGER.info("initialize() :: START");
        this.reset();
        CobroCustomEntityModel cobro = (CobroCustomEntityModel) event.getComponent().getAttributes().get("cobro");
        LOGGER.info("Cobro :: " + cobro);
        if (cobro != null) {
            this.cobroCustomEntityModel = cobro;
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("operationStatus", "000");
        }
        LOGGER.info("initialize() :: END");
    }

}
