package com.sib.cobro;

import com.sib.bs.responses.CobroVistaDetalleResponse;
import com.sib.ejb.dao.CobroDao;
import com.sib.entity.CobroVistaDetalle;
import com.sib.ui.models.CobroCustomEntityModel;
import com.sib.ui.models.CobroModel;
import com.sib.ui.models.CobroVistaDetalleModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "cobroViewPopupBean")
@ViewScoped
public class CobroViewPopupBean extends CobroAbstractPopup implements Serializable {

    public static final Logger LOGGER = Logger.getLogger(CobroViewPopupBean.class);

    @EJB
    CobroDao cobroDao;

    private CobroCustomEntityModel cobroCustomEntityModel;

    private List<CobroVistaDetalleModel> cobroVistaDetalleList;

    public CobroCustomEntityModel getCobroCustomEntityModel() {
        return cobroCustomEntityModel;
    }

    public void setCobroCustomEntityModel(CobroCustomEntityModel cobroCustomEntityModel) {
        this.cobroCustomEntityModel = cobroCustomEntityModel;
    }

    public List<CobroVistaDetalleModel> getCobroVistaDetalleList() {
        if (cobroVistaDetalleList == null) {
            loadList();
        }
        return cobroVistaDetalleList;
    }

    public void setCobroVistaDetalleList(List<CobroVistaDetalleModel> cobroVistaDetalleList) {
        this.cobroVistaDetalleList = cobroVistaDetalleList;
    }

    public void loadList() {
        LOGGER.info("loadList() :: START");
        cobroVistaDetalleList = new ArrayList<CobroVistaDetalleModel>();
        try {
            CobroVistaDetalleResponse cobroVistaDetalleResponse = cobroDao.getCobroVistaDetalleList(cobroCustomEntityModel.getCobroId(), null);

            if (cobroVistaDetalleResponse.getMsgEnum().getCode() == 1) {
                if (cobroVistaDetalleResponse.getCobroVistaDetalleList() != null && !cobroVistaDetalleResponse.getCobroVistaDetalleList().isEmpty()) {
                    LOGGER.info("CobroVistaDetalleList Size :: " + cobroVistaDetalleResponse.getCobroVistaDetalleList().size());
                    CobroUtil cobroUtil = new CobroUtil();
                    for (CobroVistaDetalle cobroVistaDetalle : cobroVistaDetalleResponse.getCobroVistaDetalleList()) {
                        CobroVistaDetalleModel model = cobroUtil.convertirCobroVistaDetalleACobroVistaDetalleModel(cobroVistaDetalle);
                        cobroVistaDetalleList.add(model);

                    }

                }

            }

        } catch (Exception e) {
            LOGGER.info("Error populando la lista de Cobro Vista Detalle :: " + e);
        }
        LOGGER.info("loadList() :: END");
    }

    @Override
    public void execute(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancelProcess(ActionEvent event) {
        this.reset();

    }

    @Override
    public void reset() {
        cobroVistaDetalleList = null;
        cobroCustomEntityModel = null;
    }

    @Override
    public boolean validateFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getFileName() {
        return "cobroviewpopup";
    }

    @Override
    public String getPopupTitle() {
        return "Vista Detalle Cobro Historial";
    }

    public void initialize(ActionEvent event) {
        LOGGER.info("initialize() :: START");
        this.reset();
        CobroCustomEntityModel cobro = (CobroCustomEntityModel) event.getComponent().getAttributes().get("cobro");
        LOGGER.info("Cobro :: " + cobro);
        if (cobro != null) {
            this.cobroCustomEntityModel = cobro;
        }
        LOGGER.info("initialize() :: END");
    }
}
