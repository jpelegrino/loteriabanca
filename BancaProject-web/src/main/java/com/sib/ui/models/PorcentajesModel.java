package com.sib.ui.models;

import com.sib.entity.Porcentajes;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Omar Moronta
 */
public class PorcentajesModel extends Porcentajes 
    implements Serializable{
    public String getFechaCreacionFormateada() {
        String result = "";
        if (this.getFecha()!= null) {
            try {
                DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
                DateFormat inputFormat = new SimpleDateFormat("yyyyMMdd");
                
                Date date = inputFormat.parse(this.getFecha());
                result = outputFormat.format(date);            
            } catch (ParseException ex) {
                Logger.getLogger(GananciaPerdidaModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }    
}
