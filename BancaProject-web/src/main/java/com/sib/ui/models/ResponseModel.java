package com.sib.ui.models;

import java.io.Serializable;

/**
 *
 * @author Omar Moronta
 */
public class ResponseModel implements Serializable{
    private boolean wasSuccessful;
    private String message;
    
    public ResponseModel(){
    }

    public boolean getWasSuccessful() {
        return wasSuccessful;
    }

    public void setWasSuccessful(boolean wasSuccessful) {
        this.wasSuccessful = wasSuccessful;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
