package com.sib.ui.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author julio
 */
@Embeddable
public class BancaLoteriaPKModel implements Serializable{
    
    private int bancaId;
    private int loteriaId;

    public BancaLoteriaPKModel() {
    }

    public BancaLoteriaPKModel(int bancaId, int loteriaId) {
        this.bancaId = bancaId;
        this.loteriaId = loteriaId;
    }

    public int getBancaId() {
        return bancaId;
    }

    public void setBancaId(int bancaId) {
        this.bancaId = bancaId;
    }

    public int getLoteriaId() {
        return loteriaId;
    }

    public void setLoteriaId(int loteriaId) {
        this.loteriaId = loteriaId;
    }
}