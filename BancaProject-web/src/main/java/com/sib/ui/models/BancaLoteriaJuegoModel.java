package com.sib.ui.models;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author omar moronta
 */
public class BancaLoteriaJuegoModel implements Serializable {
    
    protected BancaLoteriaJuegoPKModel bancaLoteriaJuegoPK;
    private int porcentajeCobro;
    private Date fechaCreacion;
    private String status;
    private Date fechaActualizacion;
    private JuegoModel juego;
    private BancaLoteriaModel bancaLoteria;

    public BancaLoteriaJuegoModel() {
    }

    public BancaLoteriaJuegoModel(BancaLoteriaJuegoPKModel bancaLoteriaJuegoPK) {
        this.bancaLoteriaJuegoPK = bancaLoteriaJuegoPK;
    }

    public BancaLoteriaJuegoModel(BancaLoteriaJuegoPKModel bancaLoteriaJuegoPK, int porcentajeCobro, 
            Date fechaCreacion, String status) {
        this.bancaLoteriaJuegoPK = bancaLoteriaJuegoPK;
        this.porcentajeCobro = porcentajeCobro;
        this.fechaCreacion = fechaCreacion;
        this.status = status;
    }

    public BancaLoteriaJuegoModel(int loteriaId, int bancaId, int juegoId) {
        this.bancaLoteriaJuegoPK = new BancaLoteriaJuegoPKModel(loteriaId, bancaId, juegoId);
    }

    public BancaLoteriaJuegoPKModel getBancaLoteriaJuegoPK() {
        return bancaLoteriaJuegoPK;
    }

    public void setBancaLoteriaJuegoPK(BancaLoteriaJuegoPKModel bancaLoteriaJuegoPK) {
        this.bancaLoteriaJuegoPK = bancaLoteriaJuegoPK;
    }

    public int getPorcentajeCobro() {
        return porcentajeCobro;
    }

    public void setPorcentajeCobro(int porcentajeCobro) {
        this.porcentajeCobro = porcentajeCobro;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public JuegoModel getJuego() {
        return juego;
    }

    public void setJuego(JuegoModel juego) {
        this.juego = juego;
    }

    public BancaLoteriaModel getBancaLoteria() {
        return bancaLoteria;
    }

    public void setBancaLoteria(BancaLoteriaModel bancaLoteria) {
        this.bancaLoteria = bancaLoteria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bancaLoteriaJuegoPK != null ? bancaLoteriaJuegoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BancaLoteriaJuegoModel)) {
            return false;
        }
        BancaLoteriaJuegoModel other = (BancaLoteriaJuegoModel) object;
        if ((this.bancaLoteriaJuegoPK == null && other.bancaLoteriaJuegoPK != null) || (this.bancaLoteriaJuegoPK != null && !this.bancaLoteriaJuegoPK.equals(other.bancaLoteriaJuegoPK))) {
            return false;
        }
        return true;
    }
    
}
