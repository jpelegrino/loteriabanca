package com.sib.ui.models;

import java.io.Serializable;

/**
 *
 * @author Omar Moronta
 */
public class CobroHistorialModel implements Serializable {

    private Long id;
    private Float montoCobrado;
    private String statusCobroHistorial;
    private CobroModel cobroId;
    private DetalleGeneralModel detalleGeneralId;

    public CobroHistorialModel() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getMontoCobrado() {
        return montoCobrado;
    }

    public void setMontoCobrado(Float montoCobrado) {
        this.montoCobrado = montoCobrado;
    }

    public String getStatusCobroHistorial() {
        return statusCobroHistorial;
    }

    public void setStatusCobroHistorial(String statusCobroHistorial) {
        this.statusCobroHistorial = statusCobroHistorial;
    }

    public CobroModel getCobroId() {
        return cobroId;
    }

    public void setCobroId(CobroModel cobroId) {
        this.cobroId = cobroId;
    }

    public DetalleGeneralModel getDetalleGeneralId() {
        return detalleGeneralId;
    }

    public void setDetalleGeneralId(DetalleGeneralModel detalleGeneralId) {
        this.detalleGeneralId = detalleGeneralId;
    }
}