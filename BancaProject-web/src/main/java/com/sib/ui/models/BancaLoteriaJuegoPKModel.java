/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sib.ui.models;

import java.io.Serializable;

/**
 *
 * @author omar morona
 */
public class BancaLoteriaJuegoPKModel implements Serializable{
    private int loteriaId;
    private int bancaId;
    private int juegoId;

    public BancaLoteriaJuegoPKModel() {
    }

    public BancaLoteriaJuegoPKModel(int loteriaId, int bancaId, int juegoId) {
        this.loteriaId = loteriaId;
        this.bancaId = bancaId;
        this.juegoId = juegoId;
    }

    public int getLoteriaId() {
        return loteriaId;
    }

    public void setLoteriaId(int loteriaId) {
        this.loteriaId = loteriaId;
    }

    public int getBancaId() {
        return bancaId;
    }

    public void setBancaId(int bancaId) {
        this.bancaId = bancaId;
    }

    public int getJuegoId() {
        return juegoId;
    }

    public void setJuegoId(int juegoId) {
        this.juegoId = juegoId;
    }
}
