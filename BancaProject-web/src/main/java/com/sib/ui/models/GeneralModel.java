package com.sib.ui.models;

import com.sib.entity.Grupo;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Omar Moronta
 */
public class GeneralModel implements Serializable {

    private Long id;
    private String fecha;
    private Date fechaCreacion;
    private Grupo grupoId;
    private List<DetalleGeneralModel> detalleGeneralList;

    public GeneralModel() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Grupo getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(Grupo grupoId) {
        this.grupoId = grupoId;
    }

    public List<DetalleGeneralModel> getDetalleGeneralList() {
        return detalleGeneralList;
    }

    public void setDetalleGeneralList(List<DetalleGeneralModel> detalleGeneralList) {
        this.detalleGeneralList = detalleGeneralList;
    }

}
