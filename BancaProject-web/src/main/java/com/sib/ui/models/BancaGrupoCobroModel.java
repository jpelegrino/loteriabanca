package com.sib.ui.models;

import com.sib.util.NumberFormatUtil;
import java.io.Serializable;

/**
 *
 * @author Snailin Inoa
 */
public class BancaGrupoCobroModel implements Serializable {

    private int idGrupo;
    private int idBanca;
    private Float deuda;
    private String codigoBanca;
    private String codigoGrupo;

    //deudaUI field es usado solo para la UI. Para mostrar el valor formateado sin decimales.
    private String deudaUI;

    public BancaGrupoCobroModel() {
    }

    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    public int getIdBanca() {
        return idBanca;
    }

    public void setIdBanca(int idBanca) {
        this.idBanca = idBanca;
    }

    public Float getDeuda() {
        return deuda;
    }

    public void setDeuda(Float deuda) {
        this.deuda = deuda;
    }

    public String getCodigoBanca() {
        return codigoBanca;
    }

    public void setCodigoBanca(String codigoBanca) {
        this.codigoBanca = codigoBanca;
    }

    public String getCodigoGrupo() {
        return codigoGrupo;
    }

    public void setCodigoGrupo(String codigoGrupo) {
        this.codigoGrupo = codigoGrupo;
    }

    public String getDeudaUI() {
        deudaUI = NumberFormatUtil.formatWithoutDecimal(this.deuda);
        return deudaUI;
    }

    public void setDeudaUI(String deudaUI) {
        this.deudaUI = deudaUI;
    }

}
