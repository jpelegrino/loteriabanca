package com.sib.ui.models;

import java.util.Date;

/**
 *
 * @author Snailin Inoa
 */
public class CobroVistaDetalleModel {

    private Long cobroHistorialId;
    private Long cobroId;
    private Date fechaCobro;
    private Float montoCobrado;
    private String statusCobroHistorial;
    private Long loteriaId;
    private String codigoLoteria;
    private Long bancaId;
    private String codigoBanca;
    private Long grupoId;
    private String codigoGrupo;
    private Long detalleGeneralId;

    public Long getCobroHistorialId() {
        return cobroHistorialId;
    }

    public void setCobroHistorialId(Long cobroHistorialId) {
        this.cobroHistorialId = cobroHistorialId;
    }

    public Long getCobroId() {
        return cobroId;
    }

    public void setCobroId(Long cobroId) {
        this.cobroId = cobroId;
    }

    public Date getFechaCobro() {
        return fechaCobro;
    }

    public void setFechaCobro(Date fechaCobro) {
        this.fechaCobro = fechaCobro;
    }

    public Float getMontoCobrado() {
        return montoCobrado;
    }

    public void setMontoCobrado(Float montoCobrado) {
        this.montoCobrado = montoCobrado;
    }

    public String getStatusCobroHistorial() {
        return statusCobroHistorial;
    }

    public void setStatusCobroHistorial(String statusCobroHistorial) {
        this.statusCobroHistorial = statusCobroHistorial;
    }

    public Long getLoteriaId() {
        return loteriaId;
    }

    public void setLoteriaId(Long loteriaId) {
        this.loteriaId = loteriaId;
    }

    public String getCodigoLoteria() {
        return codigoLoteria;
    }

    public void setCodigoLoteria(String codigoLoteria) {
        this.codigoLoteria = codigoLoteria;
    }

    public Long getBancaId() {
        return bancaId;
    }

    public void setBancaId(Long bancaId) {
        this.bancaId = bancaId;
    }

    public String getCodigoBanca() {
        return codigoBanca;
    }

    public void setCodigoBanca(String codigoBanca) {
        this.codigoBanca = codigoBanca;
    }

    public Long getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(Long grupoId) {
        this.grupoId = grupoId;
    }

    public String getCodigoGrupo() {
        return codigoGrupo;
    }

    public void setCodigoGrupo(String codigoGrupo) {
        this.codigoGrupo = codigoGrupo;
    }

    public Long getDetalleGeneralId() {
        return detalleGeneralId;
    }

    public void setDetalleGeneralId(Long detalleGeneralId) {
        this.detalleGeneralId = detalleGeneralId;
    }

}
