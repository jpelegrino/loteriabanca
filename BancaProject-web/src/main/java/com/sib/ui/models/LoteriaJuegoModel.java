package com.sib.ui.models;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Omar Moronta
 */
public class LoteriaJuegoModel implements Serializable {

    protected LoteriaJuegoPKModel loteriaJuegoPK;
    private Date fechaCreacion;
    private String status;
    private Date fechaActualizacion;
    private JuegoModel juego;
    private LoteriaModel loteria;

    public LoteriaJuegoModel() {
    }

    public LoteriaJuegoPKModel getLoteriaJuegoPK() {
        return loteriaJuegoPK;
    }

    public void setLoteriaJuegoPK(LoteriaJuegoPKModel loteriaJuegoPK) {
        this.loteriaJuegoPK = loteriaJuegoPK;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public JuegoModel getJuego() {
        return juego;
    }

    public void setJuego(JuegoModel juego) {
        this.juego = juego;
    }

    public LoteriaModel getLoteria() {
        return loteria;
    }

    public void setLoteria(LoteriaModel loteria) {
        this.loteria = loteria;
    }
}
