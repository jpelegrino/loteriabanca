package com.sib.ui.models;

import com.sib.entity.DetalleGeneral;
import com.sib.util.DateFormatUtil;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 *
 * @author Omar Moronta
 */
public class DetalleGeneralModel extends DetalleGeneral implements Serializable {

    public DetalleGeneralModel() {
    }

    public String getFechaEfectivaFormateada() {
        String result = "";
        if (this.getFechaEfectiva()!= null) {
            
            result = DateFormatUtil.
                    convertDateToStringWithStandardFormat(
                            this.getFechaEfectiva());
        }
        return result;
    }
}