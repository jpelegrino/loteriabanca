package com.sib.ui.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Omar Moronta
 */
public class LoteriaModel implements Serializable{

    private Integer id;
    private String codigo;
    private Date fechaCreacion;
    private String status;
    private Date fechaActualizacion;
    private List<BancaLoteriaModel> bancaLoteriaList;
    private List<LoteriaJuegoModel> loteriaJuegoList;

    public LoteriaModel() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public List<BancaLoteriaModel> getBancaLoteriaList() {
        return bancaLoteriaList;
    }

    public void setBancaLoteriaList(List<BancaLoteriaModel> bancaLoteriaList) {
        this.bancaLoteriaList = bancaLoteriaList;
    }

    public List<LoteriaJuegoModel> getLoteriaJuegoList() {
        return loteriaJuegoList;
    }

    public void setLoteriaJuegoList(List<LoteriaJuegoModel> loteriaJuegoList) {
        this.loteriaJuegoList = loteriaJuegoList;
    }
}