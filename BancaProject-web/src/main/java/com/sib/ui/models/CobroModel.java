package com.sib.ui.models;

import com.sib.entity.Cobro;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Omar Moronta
 */
public class CobroModel implements Serializable {

    private Long id;
    private String comentario;
    private Date fechaCobro;
    private String statusCobro;
    private List<CobroHistorialModel> cobroHistorialList;
    private Float montoCobrado;
    private Integer idBanca;
    private String codigoBanca;

    private Integer idGrupo;
    private String codigoGrupo;

    public CobroModel() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Date getFechaCobro() {
        return fechaCobro;
    }

    public void setFechaCobro(Date fechaCobro) {
        this.fechaCobro = fechaCobro;
    }

    public String getStatusCobro() {
        return statusCobro;
    }

    public void setStatusCobro(String statusCobro) {
        this.statusCobro = statusCobro;
    }

    public List<CobroHistorialModel> getCobroHistorialList() {
        return cobroHistorialList;
    }

    public void setCobroHistorialList(List<CobroHistorialModel> cobroHistorialList) {
        this.cobroHistorialList = cobroHistorialList;
    }

    public Float getMontoCobrado() {
        return montoCobrado;
    }

    public void setMontoCobrado(Float montoCobrado) {
        this.montoCobrado = montoCobrado;
    }

    public Integer getIdBanca() {
        return idBanca;
    }

    public void setIdBanca(Integer idBanca) {
        this.idBanca = idBanca;
    }

    public Integer getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(Integer idGrupo) {
        this.idGrupo = idGrupo;
    }

    public String getCodigoBanca() {
        return codigoBanca;
    }

    public void setCodigoBanca(String codigoBanca) {
        this.codigoBanca = codigoBanca;
    }

    public String getCodigoGrupo() {
        return codigoGrupo;
    }

    public void setCodigoGrupo(String codigoGrupo) {
        this.codigoGrupo = codigoGrupo;
    }

}
