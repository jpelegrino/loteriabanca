package com.sib.ui.models;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Snailin Inoa
 */
public class CobroCustomEntityModel implements Serializable {

    private Long cobroId;
    private Date fechaEfectiva;
    private String comentario;
    private Long montoTotalCobrado;
    private Long montoTotal;
    private Long cobroHistorialId;
    private Long detalleGeneralId;
    private Long bancaId;
    private String codigoBanca;
    private Long grupoId;
    private String codigoGrupo;
    private String cobroStatus;

    public Long getCobroId() {
        return cobroId;
    }

    public void setCobroId(Long cobroId) {
        this.cobroId = cobroId;
    }

    public Date getFechaEfectiva() {
        return fechaEfectiva;
    }

    public void setFechaEfectiva(Date fechaEfectiva) {
        this.fechaEfectiva = fechaEfectiva;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Long getMontoTotalCobrado() {
        return montoTotalCobrado;
    }

    public void setMontoTotalCobrado(Long montoTotalCobrado) {
        this.montoTotalCobrado = montoTotalCobrado;
    }

    public Long getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(Long montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Long getCobroHistorialId() {
        return cobroHistorialId;
    }

    public void setCobroHistorialId(Long cobroHistorialId) {
        this.cobroHistorialId = cobroHistorialId;
    }

    public Long getDetalleGeneralId() {
        return detalleGeneralId;
    }

    public void setDetalleGeneralId(Long detalleGeneralId) {
        this.detalleGeneralId = detalleGeneralId;
    }

    public Long getBancaId() {
        return bancaId;
    }

    public void setBancaId(Long bancaId) {
        this.bancaId = bancaId;
    }

    public String getCodigoBanca() {
        return codigoBanca;
    }

    public void setCodigoBanca(String codigoBanca) {
        this.codigoBanca = codigoBanca;
    }

    public Long getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(Long grupoId) {
        this.grupoId = grupoId;
    }

    public String getCodigoGrupo() {
        return codigoGrupo;
    }

    public void setCodigoGrupo(String codigoGrupo) {
        this.codigoGrupo = codigoGrupo;
    }

    public String getCobroStatus() {
        return cobroStatus;
    }

    public void setCobroStatus(String cobroStatus) {
        this.cobroStatus = cobroStatus;
    }

}
