package com.sib.ui.models;

import java.io.Serializable;

/**
 *
 * @author Omar Moronta
 */
public class LoteriaJuegoPKModel implements Serializable{

    private int loteriaId;
    private int juegoId;

    public LoteriaJuegoPKModel() {
    }

    public int getLoteriaId() {
        return loteriaId;
    }

    public void setLoteriaId(int loteriaId) {
        this.loteriaId = loteriaId;
    }

    public int getJuegoId() {
        return juegoId;
    }

    public void setJuegoId(int juegoId) {
        this.juegoId = juegoId;
    }
}
