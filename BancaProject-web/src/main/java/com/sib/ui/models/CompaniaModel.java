package com.sib.ui.models;

/**
 *
 * @author Snailin Inoa
 */
public class CompaniaModel {

    private int id;
    private String companiaText;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompaniaText() {
        return companiaText;
    }

    public void setCompaniaText(String companiaText) {
        this.companiaText = companiaText;
    }

    @Override
    public String toString() {
        return "[ compania id = " + id + " \n"
                + "companiaText = " + companiaText + " ]";

    }

}
