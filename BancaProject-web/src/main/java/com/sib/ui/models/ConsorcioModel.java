package com.sib.ui.models;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ManyToOne;

/**
 *
 * @author Snailin Inoa
 */
public class ConsorcioModel {

    private int id;
    private CompaniaModel companiaModel;
    private String consorcioText;
    private char status = 'A';
    private boolean isSelected;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CompaniaModel getCompaniaModel() {
        return companiaModel;
    }

    public void setCompaniaModel(CompaniaModel companiaModel) {
        this.companiaModel = companiaModel;
    }

    public String getConsorcioText() {
        return consorcioText;
    }

    public void setConsorcioText(String consorcioText) {
        this.consorcioText = consorcioText;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public boolean isIsSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    @Override
    public String toString() {
        return "[ consorcio id = " + id + "\n"
                + " companiaModel = " + companiaModel + "\n"
                + " consorcioText = " + consorcioText + " \n"
                + " Status = " + status + " ]";

    }

}
