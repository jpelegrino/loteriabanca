package com.sib.ui.models;

import java.io.Serializable;

/**
 *
 * @author Omar Moronta
 */
public class JuegoAndBLJModel implements Serializable {

    private Integer id;
    private String codigo;
    private String nombre;
    private int porcentajeCobro;

    public JuegoAndBLJModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPorcentajeCobro() {
        return porcentajeCobro;
    }

    public void setPorcentajeCobro(int porcentajeCobro) {
        this.porcentajeCobro = porcentajeCobro;
    }
}
