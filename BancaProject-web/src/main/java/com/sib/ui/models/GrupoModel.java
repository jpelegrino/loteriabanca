package com.sib.ui.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Snailin Inoa
 */
public class GrupoModel implements Serializable {

    private Integer id;
    private String codigo;
    private Date fechaCreacion;
    private String status;
    private Date fechaActualizacion;
    private List<GeneralModel> generalList;
    private List<BancaModel> bancaList;

    public GrupoModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public List<GeneralModel> getGeneralList() {
        return generalList;
    }

    public void setGeneralList(List<GeneralModel> generalList) {
        this.generalList = generalList;
    }

    public List<BancaModel> getBancaList() {
        return bancaList;
    }

    public void setBancaList(List<BancaModel> bancaList) {
        this.bancaList = bancaList;
    }
}