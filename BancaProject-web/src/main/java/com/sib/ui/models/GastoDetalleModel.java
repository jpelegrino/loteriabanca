package com.sib.ui.models;

import java.util.Date;

/**
 *
 * @author Snailin Inoa
 */
public class GastoDetalleModel {

    private Date fecha;
    private int gastoId;
    private Double montoGasto;
    private Character estatus;
    private boolean isSelected;

    public boolean isIsSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getGastoId() {
        return gastoId;
    }

    public void setGastoId(int gastoId) {
        this.gastoId = gastoId;
    }

    public Double getMontoGasto() {
        return montoGasto;
    }

    public void setMontoGasto(Double montoGasto) {
        this.montoGasto = montoGasto;
    }

    public Character getEstatus() {
        return estatus;
    }

    public void setEstatus(Character estatus) {
        this.estatus = estatus;
    }

}
