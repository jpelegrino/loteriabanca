package com.sib.ui.enums;

/**
 *
 * @author Omar Moronta
 */
public enum UIOperationResultEnum {
    SUCCESS("000"),
    FAILURE("001");

    private String statusLiteral;

    private UIOperationResultEnum(String statusLiteral) {
        this.statusLiteral = statusLiteral;
    }

    public String getStatusLiteral() {
        return statusLiteral;
    }

    public void setStatusLiteral(String statusLiteral) {
        this.statusLiteral = statusLiteral;
    }
}
