package com.sib.ui.enums;

/**
 *
 * @author Snailin Inoa
 */
public enum CobroStatusEnum {

    V("V", "Valido"),
    A("A", "Anulado");

    private String codigo;
    private String descripcion;

    private CobroStatusEnum(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public static String lookForDescription(String code) {
        String result = "";
        if (code != null && !code.isEmpty()) {
            for (CobroStatusEnum statusEnum : CobroStatusEnum.values()) {
                if (statusEnum.getCodigo().equals(code)) {
                    result = statusEnum.getDescripcion();
                    return result;
                }
            }

        }
        return result;
    }

}
