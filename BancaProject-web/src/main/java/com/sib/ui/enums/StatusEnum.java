package com.sib.ui.enums;


/**
 *
 * @author Omar Moronta
 */
public enum StatusEnum {

    ACTIVO("A"),
    INACTIVO("I");

    private String statusLiteral;

    private StatusEnum(String statusLiteral) {
        this.statusLiteral = statusLiteral;
    }

    public String getStatusLiteral() {
        return statusLiteral;
    }

    public void setStatusLiteral(String statusLiteral) {
        this.statusLiteral = statusLiteral;
    }
}