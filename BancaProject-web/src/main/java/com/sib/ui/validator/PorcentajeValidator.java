package com.sib.ui.validator;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author Omar Moronta
 */
@FacesValidator(value = "porcentajeValidator")
public class PorcentajeValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, 
            Object value) throws ValidatorException {

    }
    
}
