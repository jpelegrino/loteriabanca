package com.sib.ui.converter;

import com.sib.ui.enums.CobroStatusEnum;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Snailin Inoa
 */
@FacesConverter(value = "cobroStatusConverter")
public class CobroStatusConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {

        return value;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {

        if (value == null) {
            return "";
        }

        String result = CobroStatusEnum.lookForDescription(value.toString());

        return result;
    }

}
