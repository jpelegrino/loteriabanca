package com.sib.tipo.gasto;

import com.sib.entity.TipoGasto;
import com.sib.ui.models.TipoGastoModel;
import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

/**
 *
 * @author Omar Moronta
 */
public class TipoGastoUtil  implements Serializable {
    
    public static final Logger LOG = Logger.getLogger(TipoGastoUtil.class.getName());   

    public TipoGastoUtil() {

    }

    public TipoGastoModel crearTipoGastoModelDeTipoGastoModel(TipoGastoModel orig) {
        TipoGastoModel dest = new TipoGastoModel();

        if (orig != null) {
            byte[] bytesArr = SerializationUtils.serialize(orig);
            dest = (TipoGastoModel)SerializationUtils.deserialize(bytesArr);
        }

        return dest;
    }

    public TipoGastoModel convertirTipoGastoATipoGastoModel(TipoGasto orig) {
        TipoGastoModel dest = null;

        if (orig != null) {
            byte[] bytesArr = SerializationUtils.serialize(orig);
            TipoGasto norig = (TipoGasto)SerializationUtils.deserialize(bytesArr);
            
            if(norig!=null){
                try {
                    dest = new TipoGastoModel();
                    dest.setId(norig.getId());
                    dest.setDescripcion(norig.getDescripcion());
                } catch (Exception ex) {
                    LOG.error(ex.getMessage(), ex);
                } 
            }
        }

        return dest;
    }

    public TipoGasto convertirTipoGastoModelATipoGasto(TipoGastoModel orig) {
        TipoGasto dest = null;
        
        if(orig!=null){
            byte[] bytesArr = SerializationUtils.serialize(orig);
            TipoGastoModel norig = (TipoGastoModel)SerializationUtils.deserialize(bytesArr);
            
            if(norig!=null){
                try {
                    dest = new TipoGasto();
                    dest.setId(norig.getId());
                    dest.setDescripcion(norig.getDescripcion());
                } catch (Exception ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }
        
        return dest;
    }
    
    public TipoGastoModel getTipoGastoModelPorTipoGastoId(Long tipoGastoId, 
            List<TipoGastoModel> tipoGastos){
    
        TipoGastoModel retObj = null;
        
        if(!CollectionUtils.isEmpty(tipoGastos)){
            for(TipoGastoModel tgm : tipoGastos){
                if(tgm.getId().equals(tipoGastoId)){
                    retObj = tgm;
                    break;
                }
            }
        }
        
        return retObj;
    }
}

