package com.sib.tipo.gasto;

import com.sib.bs.responses.MessageEnum;
import com.sib.bs.responses.TipoGastoResponse;
import com.sib.ejb.dao.TipoGastoDao;
import com.sib.entity.TipoGasto;
import com.sib.ui.models.TipoGastoModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;

/**
 *
 * @author 
 */
@ManagedBean(name = "tipoGastoModelBean")
@ViewScoped
public class TipoGastoModelBean implements Serializable {

    public static final Logger LOG
            = Logger.getLogger(TipoGastoModelBean.class.getName());    
    
    private List<TipoGastoModel> result;
    
    @EJB
    private TipoGastoDao tipoGastoDao;

    private void init() {

        TipoGastoResponse out = this.tipoGastoDao.getTipoGastos();

        if (out != null) {
            
            if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {

                List<TipoGasto> origList = out.getTipoGastos();
                result = new ArrayList<TipoGastoModel>();
                
                if (origList != null) {
                    TipoGastoUtil tgUtil = new TipoGastoUtil();
                    for (TipoGasto g : origList) {
                        TipoGastoModel cur = tgUtil.convertirTipoGastoATipoGastoModel(g);
                        result.add(cur);
                    }
                }

            } else {
                LOG.error("Ha ocurrido un error obteniendo Tipo de Gastos"
                        + "Message Enum equals:>" + out.getMsgEnum());
            }
        }        
        
    }

    public List<TipoGastoModel> getResult() {
        if (result == null) {
            init();
        }
        return result;
    }
    
    public TipoGasto getTipoGastoById(long tipoGastoId){
        
        TipoGasto tg = null;
        
        try{
           TipoGastoResponse out = this.tipoGastoDao.getTipoGastoById(tipoGastoId);
           
           if(out!=null && MessageEnum.SUCCESS.equals(out.getMsgEnum())){
               tg = out.getTipoGasto();
           }else{
                LOG.error("Ha ocurrido un error obteniendo Tipo de Gastos"
                        + " por ID:" + tipoGastoId + ". Saliendo con response: " 
                        + out);               
           }
        }catch(Exception ex){
                LOG.error("Ha ocurrido un error obteniendo Tipo de Gastos"
                        + " por ID: " + tipoGastoId + " - Error" 
                        + ex.getMessage(),ex); 
        }
        
        return tg;
    }
}