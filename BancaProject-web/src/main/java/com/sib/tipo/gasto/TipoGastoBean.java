package com.sib.tipo.gasto;

import com.sib.ui.models.TipoGastoModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "tipoGastoBean")
@ViewScoped
public class TipoGastoBean implements Serializable {

    public static final Logger LOG = Logger.getLogger(TipoGastoBean.class.getName());
    
    private List<SelectItem> tipoGastosSI;
    
    @ManagedProperty(value = "#{tipoGastoModelBean}")
    private TipoGastoModelBean modelBean;

    public TipoGastoModelBean getModelBean() {
        return modelBean;
    }

    public void setModelBean(TipoGastoModelBean modelBean) {
        this.modelBean = modelBean;
    }

    public List<SelectItem> getTipoGastosSI() {
        if(tipoGastosSI==null){
            List<TipoGastoModel> tipoGastoList 
                    = this.modelBean.getResult();
            tipoGastosSI = new ArrayList<SelectItem>();
            
            if(tipoGastoList!=null){
                for(TipoGastoModel tpm : tipoGastoList){
                    tipoGastosSI.add(new SelectItem(tpm.getId(),
                            tpm.getDescripcion()));
                }
            }
       
        }
        return tipoGastosSI;
    }

    public void setTipoGastosSI(List<SelectItem> tipoGastosSI) {
        this.tipoGastosSI = tipoGastosSI;
    }
}
