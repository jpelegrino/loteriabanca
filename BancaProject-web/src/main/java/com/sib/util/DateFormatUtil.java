package com.sib.util;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.apache.commons.lang3.StringUtils;

import org.apache.log4j.Logger;

/**
 *
 * @author Snailin Inoa
 */
public class DateFormatUtil {

    public static final Logger LOGGER = Logger.getLogger(DateFormatUtil.class);

    public static Date convertStringToDate(String fecha) {
        Date date = null;
        if (fecha != null && !fecha.isEmpty()) {
            try {
                DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

                date = new Date(dateFormat.parse(fecha).getTime());
            } catch (ParseException ex) {
                LOGGER.error("Error Tratando de parsear la fecha a Date :: " + ex);
            }

        } else {
            date = new Date(new java.util.Date().getTime());
        }

        return date;
    }

    public static String convertDateToStringWithStandardFormat(java.util.Date date) {
        String formato = "YYYY-MM-dd";
        String fechaFormateada="";
        if (date != null && !StringUtils.isBlank(formato)) {
            SimpleDateFormat sdf = new SimpleDateFormat(formato);
            fechaFormateada = sdf.format(date);
        }

        return fechaFormateada;
    }    
    
}
