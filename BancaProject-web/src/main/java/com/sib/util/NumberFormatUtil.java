package com.sib.util;

import java.text.NumberFormat;
import java.util.Locale;
import org.apache.log4j.Logger;

/**
 *
 * @author Snailin Inoa
 */
public class NumberFormatUtil {
    
    public static final Logger LOGGER = Logger.getLogger(NumberFormatUtil.class);
    
    public static String formatWithoutDecimal(Object obj) {
        String result = "0";
        try {
            if (obj != null) {
                NumberFormat numberFormat = NumberFormat.getInstance(Locale.US);
                numberFormat.setMaximumFractionDigits(0);
                result = numberFormat.format(obj);
                
            }
        } catch (Exception e) {
            LOGGER.error("Error formatWithoutDecimal() ::" + e);
        }
        return result;
    }
    
    public static Float redondearFloat(Float value) {
        Float result = value;
        
        try {
            if (value != null) {
                double number = Double.parseDouble(String.valueOf(value.floatValue()));
                number = Math.round(number * Math.pow(10, 0)) / Math.pow(10, 0);
                result = Float.parseFloat(String.valueOf(number));
            }
            
        } catch (Exception e) {
            LOGGER.error("Error tratando de redondear un Float ::" + e);
        }
        return result;
    }
    
}
