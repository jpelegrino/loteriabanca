package com.sib.util;

import java.text.MessageFormat;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

/**
 * This Singleton class will be used for dealing with the messages listed in the resource
 * bundle of the application. 
 *
 * SINGLETON IMPLEMENTATION: Based on Joshua Blosch "Effective Java"
 *
 * @author Omar Moronta
 */
public enum MessageDisplayUtils {
    INSTANCE;
    
    public static final Logger LOG = Logger.getLogger(MessageDisplayUtils.class.getName());
    private final String MSG_RESOURCE_BUNDLE_NAME_VAR_NAME= "sabmsg";
    private ResourceBundle bundle = null;

    private MessageDisplayUtils() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        bundle = facesContext.getApplication().getResourceBundle(
                facesContext, MSG_RESOURCE_BUNDLE_NAME_VAR_NAME);
    }    

    public void printErrorMessageFromBundle(String clientId, String msgKey, 
            Object... params) {
        if (bundle != null) {
            this.showMessage(clientId, msgKey, FacesMessage.SEVERITY_ERROR, params);
        }
    }

    public void printErrorMessage(String clientId, String message) {
        if (bundle != null) {
            this.showMessage(clientId, message, FacesMessage.SEVERITY_ERROR);
        }
    }    
    
    public void printInfoMessageFromBundle(String clientId, String msgKey, 
            Object... params) {
        if (bundle != null) {
            this.showMessage(clientId, msgKey, FacesMessage.SEVERITY_INFO, params);
        }
    }    
    
    public void printInfoMessage(String clientId, String message) {
        if (bundle != null) {
            this.showMessage(clientId, message, FacesMessage.SEVERITY_INFO);
        }
    } 
    
    public void printWarnMessageFromBundle(String clientId, String msgKey, 
            Object... params) {
        if (bundle != null) {
            this.showMessage(clientId, msgKey, FacesMessage.SEVERITY_WARN, params);
        }
    }

    public void printWarnMessage(String clientId, String message) {
        if (bundle != null) {
            this.showMessage(clientId, message, FacesMessage.SEVERITY_WARN);
        }
    }    
    
    private void showMessage(String clientId, String msgKey, Severity severity, 
            Object... params) {

        String result = getMessageFromBundle(msgKey, params);

        FacesMessage fmsg = new FacesMessage(result);
        fmsg.setSeverity(severity);
        FacesContext.getCurrentInstance().addMessage(clientId, fmsg);
    }
    
    public void showMessage(String clientId, String message, Severity severity) {

        FacesMessage fmsg = new FacesMessage(message);
        fmsg.setSeverity(severity);
        FacesContext.getCurrentInstance().addMessage(clientId, fmsg);
    }    

    public String getMessageFromBundle(String msgKey, Object... params) {

        String result = "";

        if (bundle!=null) {
            result=bundle.getString(msgKey);

            if (result != null && !result.isEmpty()) {

                if (params != null) {
                    result = MessageFormat.format(result, params);
                }
            }
        }else{
            MessageDisplayUtils.LOG.error("MESSAGE BUNDLE IS NULL");
        }

        return result;
    }
}