package com.sib.util;

import java.io.Serializable;

/**
 *
 * @author sinoa
 */
public class DetailedChangeEntity implements Serializable{
    private String prefix;
    private Object original;
    private String middle;
    private Object changee;
    private String suffix;

    public Object getChangee() {
        return changee;
    }

    public void setChangee(Object changee) {
        this.changee = changee;
    }

    public String getMiddle() {
        return middle;
    }

    public void setMiddle(String middle) {
        this.middle = middle;
    }

    public Object getOriginal() {
        return original;
    }

    public void setOriginal(Object original) {
        this.original = original;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public void copy(DetailedChangeEntity entity){
        this.prefix = entity.prefix;
        this.original = entity.original;
        this.middle = entity.middle;
        this.changee = entity.changee;
        this.suffix = entity.suffix;
    }
    
    @Override
    public String toString() {
        return  prefix + original + middle + changee + suffix ;
    }

}
