package com.sib.banca;

import com.sib.interfaces.IPopup;
import com.sib.ui.models.BancaModel;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "bancaViewPopup")
@ViewScoped
public class BancaViewPopup implements IPopup, Serializable {

    private BancaModel bancaModel;

    public BancaModel getBancaModel() {
        return bancaModel;
    }

    public void setBancaModel(BancaModel bancaModel) {
        this.bancaModel = bancaModel;
    }

    @Override
    public void execute(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancelProcess(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void reset() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean validateFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getFileName() {
        return "bancaViewPopup";
    }

    @Override
    public String getPopupTitle() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
