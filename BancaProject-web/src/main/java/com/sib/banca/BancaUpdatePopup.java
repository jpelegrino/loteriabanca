package com.sib.banca;

import com.sib.interfaces.IPopup;
import com.sib.ui.enums.UIOperationResultEnum;
import com.sib.ui.models.BancaModel;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "bancaUpdatePopup")
@ViewScoped
public class BancaUpdatePopup  extends BancaAbstractPopup 
    implements Serializable {
    
    public static final Logger LOG
            = Logger.getLogger(BancaUpdatePopup.class.getName());

    private BancaModel untouchedModel;

    public BancaUpdatePopup() {
    }

    public BancaModel getUntouchedModel() {
        return untouchedModel;
    }

    public void setUntouchedModel(BancaModel untouchedModel) {
        this.untouchedModel = untouchedModel;
    }

    @Override
    public void execute(ActionEvent event) {
        if (validateFields()) {
            try {
                LOG.info("Actualizando Banca: " + this.getModel());
                ResponseModel response = getModelBean().actualizarBanca(this.getModel());
                prepararStatusOperacion(UIOperationResultEnum.SUCCESS);
                if (response.getWasSuccessful()) {
                    prepararStatusOperacion(UIOperationResultEnum.SUCCESS);
                    this.displayInfoMessage(response.getMessage(),
                            BancaConstantes.TABLA_MESSAGE_CLIENT_ID);
                } else {
                    LOG.error("Error actualizando Banca: " + response.getMessage());
                    this.displayErrorMessage(response.getMessage(),
                            BancaConstantes.POPUP_MESSAGE_CLIENT_ID);
                    prepararStatusOperacion(UIOperationResultEnum.FAILURE);
                }
            } catch (Exception ex) {
                LOG.error("Error actualizando Banca. Exception: " 
                        + ex.getMessage(), ex);
                this.displayErrorMessageFromBundle("sab.banca.actualizar.error",
                        BancaConstantes.POPUP_MESSAGE_CLIENT_ID);
                prepararStatusOperacion(UIOperationResultEnum.FAILURE);
            }
        } else {
            prepararStatusOperacion(UIOperationResultEnum.FAILURE);
            LOG.info("Fields Validation Failed..");
        }
    }

    @Override
    public void reset() {
        this.untouchedModel=null;                
    }

    @Override
    public boolean validateFields() {
        
        boolean result = true;

        if ((this.getModel().getCodigo() == null
                || this.getModel().getCodigo().isEmpty())) {
            MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(
                    BancaConstantes.POPUP_MESSAGE_CLIENT_ID, "sab.error.campo.vacio");
            result = false;
        }

        if(result){
            if((this.getModel().getCodigo()!=null
                    && this.getModel().getCodigo().equals(this.untouchedModel.getCodigo()))                 
                 && (this.getModel().getStatus()!=null && 
                        this.getModel().getStatus().equals(this.untouchedModel.getStatus()))){
                MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(
                        BancaConstantes.POPUP_MESSAGE_CLIENT_ID, "sab.error.no.cambios");
                result = false;                
            }
        }
        return result;
    }

    @Override
    public String getFileName() {
        return "bancaupdatepopup";
    }

    @Override
    public String getPopupTitle() {
        return MessageDisplayUtils.INSTANCE.getMessageFromBundle("sab.banca.actualizar.lbl");
    }

    @Override
    public Logger getLogger() {
        return LOG;
    }
}