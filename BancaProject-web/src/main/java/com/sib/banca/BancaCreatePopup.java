package com.sib.banca;

import com.sib.ui.enums.StatusEnum;
import com.sib.ui.enums.UIOperationResultEnum;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "bancaCreatePopup")
@ViewScoped
public class BancaCreatePopup extends BancaAbstractPopup 
    implements Serializable {
    
    public static final Logger LOG = Logger.getLogger(BancaCreatePopup.class.getName());

    public BancaCreatePopup() {

    }

    @Override
    public void execute(ActionEvent event) {
        if (validateFields()) {
            try {
                LOG.info("Creando banca con CODIGO: " + this.getModel().getCodigo());
                this.getModel().setStatus(StatusEnum.ACTIVO.getStatusLiteral());                
                ResponseModel response = getModelBean().crearBanca(this.getModel());
                
                if(response.getWasSuccessful()){
                    prepararStatusOperacion(UIOperationResultEnum.SUCCESS);
                    this.displayInfoMessage(response.getMessage(),
                        BancaConstantes.TABLA_MESSAGE_CLIENT_ID);
                }else{
                    LOG.error("Error creando Banca: " + response.getMessage());
                    this.displayErrorMessage(response.getMessage(),
                        BancaConstantes.POPUP_MESSAGE_CLIENT_ID);
                    prepararStatusOperacion(UIOperationResultEnum.FAILURE);                
                }
            } catch (Exception ex) {
                LOG.error("Error creando Banca. Exception: " + ex.getMessage(), ex);
                this.displayErrorMessageFromBundle("sab.banca.agregar.error",
                        BancaConstantes.POPUP_MESSAGE_CLIENT_ID);
                prepararStatusOperacion(UIOperationResultEnum.FAILURE);
            }
        } else {
            prepararStatusOperacion(UIOperationResultEnum.FAILURE);
            LOG.info("Fields Validation Failed..");
        }
    }

    @Override
    public boolean validateFields() {
        boolean result = true;

        if (this.getModel().getCodigo() == null
                || this.getModel().getCodigo().isEmpty()) {
            MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(
                    BancaConstantes.POPUP_MESSAGE_CLIENT_ID,
                    "sab.error.campo.vacio");
            result = false;
        }else if(!this.getModel().getCodigo().matches("[0-9][0-9]{2}")) {
            MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(
                    BancaConstantes.POPUP_MESSAGE_CLIENT_ID,
                    "sab.banca.agregar.error.match");
            result = false;            
        }
        return result;
    }

    @Override
    public String getFileName() {
        return "bancacreatepopup";
    }

    @Override
    public String getPopupTitle() {
        return MessageDisplayUtils.INSTANCE.getMessageFromBundle("sab.banca.agregar.lbl");
    }

    @Override
    public Logger getLogger() {
        return LOG;
    }

}
