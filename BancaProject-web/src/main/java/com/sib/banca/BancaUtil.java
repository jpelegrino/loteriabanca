package com.sib.banca;

import com.sib.entity.Banca;
import com.sib.grupo.GrupoUtil;
import com.sib.ui.models.BancaModel;
import java.io.Serializable;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;

/**
 * This is the Banca <-> BancaModel objects converter helper class.
 *
 * @author Omar Moronta
 */
public class BancaUtil implements Serializable {
    
    public static final Logger LOG = Logger.getLogger(BancaUtil.class.getName());   

    public BancaUtil() {

    }

    public BancaModel crearBancaModelDeBancaModel(BancaModel orig) {
        BancaModel dest = new BancaModel();

        if (orig != null) {
            byte[] bytesArr = SerializationUtils.serialize(orig);
            dest = (BancaModel)SerializationUtils.deserialize(bytesArr);
        }

        return dest;
    }

    public BancaModel convertirBancaABancaModel(Banca orig) {
        BancaModel dest = null;

        if (orig != null) {
            byte[] bytesArr = SerializationUtils.serialize(orig);
            Banca norig = (Banca)SerializationUtils.deserialize(bytesArr);
            
            if(norig!=null){
                try {
                    dest = new BancaModel();
                    dest.setId(norig.getId());
                    dest.setStatus(norig.getStatus());
                    dest.setCodigo(norig.getCodigo());
                    dest.setFechaActualizacion(norig.getFechaActualizacion());
                    dest.setFechaCreacion(norig.getFechaCreacion());
                    
                    if(norig.getGrupoId()!=null){
                        GrupoUtil gutil = new GrupoUtil();
                        dest.setGrupoId(gutil.convertirGrupoAGrupoModel(norig.getGrupoId()));
                    }
                    
                    //TODO: Implement Ignored convertion to BancaLoteriaModel's LIST
                } catch (Exception ex) {
                    LOG.error(ex.getMessage(), ex);
                } 
            }
        }

        return dest;
    }

    public Banca convertirBancaModelABanca(BancaModel orig) {
        Banca dest = null;
        
        if(orig!=null){
            byte[] bytesArr = SerializationUtils.serialize(orig);
            BancaModel norig = (BancaModel)SerializationUtils.deserialize(bytesArr);
            
            if(norig!=null){
                try {
                    dest = new Banca();
                    dest.setId(norig.getId());
                    dest.setStatus(norig.getStatus());
                    dest.setCodigo(norig.getCodigo());
                    dest.setFechaActualizacion(norig.getFechaActualizacion());
                    dest.setFechaCreacion(norig.getFechaCreacion());
                    
                    if(norig.getGrupoId()!=null){
                        GrupoUtil gutil = new GrupoUtil();
                        dest.setGrupoId(gutil.convertirGrupoModelAGrupo(norig.getGrupoId()));
                    }
                    
                    //TODO: Implement Ignored convertion to BancaLoteria's LIST
                } catch (Exception ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }
        
        return dest;
    }
}
