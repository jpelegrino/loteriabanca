package com.sib.banca;

import com.sib.banca.bancajuego.JuegoAndBLJUtil;
import com.sib.bs.responses.BancaLoteriaJuegoResponse;
import com.sib.bs.responses.BancaLoteriaResponse;
import com.sib.bs.responses.BancaResponse;
import com.sib.bs.responses.JuegoBLJResponse;
import com.sib.bs.responses.MessageEnum;
import com.sib.ejb.dao.BancaDao;
import com.sib.ejb.dao.BancaLoteriaDao;
import com.sib.ejb.dao.BancaLoteriaJuegoDao;
import com.sib.entity.Banca;
import com.sib.entity.Grupo;
import com.sib.entity.JuegoAndBLJ;
import com.sib.interfaces.IModelBean;
import com.sib.ui.enums.StatusEnum;
import com.sib.ui.models.BancaModel;
import com.sib.ui.models.JuegoAndBLJModel;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "bancaModelBean")
@ViewScoped
public class BancaModelBean implements IModelBean, Serializable {

    public static final Logger LOG
            = Logger.getLogger(BancaModelBean.class.getName());

    private List<BancaModel> result = null;

    @ManagedProperty(value = "#{bancaInputBean}")
    private BancaInputBean inputBean;

    @EJB
    private BancaDao bancaDao;

    @EJB
    private BancaLoteriaDao bancaLoteriaDao;

    @EJB
    private BancaLoteriaJuegoDao bancaLoteriaJuegoDao;

    public BancaModelBean() {
    }

    public BancaInputBean getInputBean() {
        return inputBean;
    }

    public void setInputBean(BancaInputBean inputBean) {
        this.inputBean = inputBean;
    }

    public List<BancaModel> getBancasDeGrupo(Integer grupoId) {
        BancaResponse out
                = bancaDao.getBancaByGrupoId(grupoId == null ? 0 : grupoId.intValue());

        List<BancaModel> nResult = new ArrayList<BancaModel>();

        if (out != null) {
            if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                List<Banca> bancaOrig = out.getBancas();

                if (bancaOrig != null) {
                    BancaUtil bancaUtil = new BancaUtil();
                    for (Banca j : bancaOrig) {
                        BancaModel cur = bancaUtil.convertirBancaABancaModel(j);
                        nResult.add(cur);
                    }
                }
            } else {
                this.displayErrorMessageFromBundle("sab.banca.error.lbl",
                        BancaConstantes.TABLA_MESSAGE_CLIENT_ID);
            }
        }
        return nResult;
    }

    public List<BancaModel> getResult() {
        if (result == null) {
            result = getBancasDeGrupo(this.inputBean.getGrupoId());
        }
        return result;
    }

    public List<JuegoAndBLJModel> getJuegoAndBLJModelList(int banca, int loteria) {
        List<JuegoAndBLJModel> resultList = null;

        JuegoBLJResponse out
                = this.bancaLoteriaJuegoDao.getJuegosAndPorcByBanAndJue(banca, loteria);

        if (out != null) {
            if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                resultList = new ArrayList<JuegoAndBLJModel>();

                List<JuegoAndBLJ> origList = out.getJuegosAndBLJ();

                if (origList != null) {
                    JuegoAndBLJUtil gUtil = new JuegoAndBLJUtil();
                    for (JuegoAndBLJ g : origList) {
                        JuegoAndBLJModel cur = gUtil.convertirJuegoAJuegoAndBLJModel(g);
                        resultList.add(cur);
                    }
                }

            } else {
                LOG.error("Ha ocurrido un error obteniendo Juegos de BancaLoteriaJuego"
                        + "Message Enum equals:>" + out.getMsgEnum());
            }
        }

        return resultList;
    }

    public ResponseModel crearBanca(BancaModel bancaModel) {
        ResponseModel resp = new ResponseModel();

        try {
            BancaUtil bancaUtil = new BancaUtil();
            Banca banca = bancaUtil.convertirBancaModelABanca(bancaModel);
            banca.setStatus(StatusEnum.ACTIVO.getStatusLiteral());
            Grupo ngrupo = new Grupo();
            ngrupo.setId(this.inputBean.getGrupoId());
            banca.setGrupoId(ngrupo);
            LOG.info("Inicio crear Banca..");
            BancaResponse out = this.bancaDao.crearBanca(banca);
            resp = processResponse(out, "sab.banca.agregar.success",
                    "sab.banca.agregar.error", "sab.banca.agregar.val.error");
            LOG.info("Fin crear Banca..");
        } catch (Exception ex) {
            resp.setWasSuccessful(false);
            resp.setMessage(this.getMessageFromBundle("sab.banca.agregar.error"));
            LOG.error("Error invocando a BancaDao para crear banca."
                    + ex.getMessage(), ex);
        }

        return resp;
    }

    public ResponseModel eliminarBanca(BancaModel bancaModel) {

        ResponseModel resp = new ResponseModel();

        try {
            BancaUtil bancaUtil = new BancaUtil();
            Banca banca = bancaUtil.convertirBancaModelABanca(bancaModel);
            LOG.info("Inicio eliminar Banca..");
            LOG.info("Banca ID:" + bancaModel.getId());
            LOG.info("Codigo ID:" + bancaModel.getCodigo());
            LOG.info("Status ID:" + bancaModel.getStatus());
            LOG.info("Grup ID:" + bancaModel.getGrupoId().getId());
            LOG.info("FA:" + bancaModel.getFechaActualizacion());
            LOG.info("FC:" + bancaModel.getFechaCreacion());
            BancaResponse out = this.bancaDao.deleteBanca(banca);
            resp = processResponse(out, "sab.banca.eliminar.success",
                    "sab.banca.eliminar.error", "sab.banca.eliminar.val.error");
            LOG.info("Fin eliminar Banca..");
        } catch (Exception ex) {
            resp.setWasSuccessful(false);
            resp.setMessage(this.getMessageFromBundle("sab.banca.eliminar.error"));
            LOG.error("Error invocando a BancaDao para eliminar banca."
                    + ex.getMessage(), ex);
        }

        return resp;
    }

    public ResponseModel actualizarBanca(BancaModel bancaModel) {

        ResponseModel resp = new ResponseModel();

        try {
            BancaUtil bancaUtil = new BancaUtil();
            Banca banca = bancaUtil.convertirBancaModelABanca(bancaModel);
            LOG.info("Inicio actualizar Banca..");
            LOG.info("Actualizando banca con Id: " + banca.getId()
                    + " y grupo: " + banca.getGrupoId().getId()
                    + " y codigo:" + banca.getCodigo());
            BancaResponse out = this.bancaDao.updateBanca(banca);
            resp = processResponse(out, "sab.banca.actualizar.success",
                    "sab.banca.actualizar.error", "sab.banca.actualizar.val.error");
            LOG.info("Fin actualizar Banca..");
        } catch (Exception ex) {
            resp.setWasSuccessful(false);
            resp.setMessage(this.getMessageFromBundle("sab.banca.actualizar.error"));
            LOG.error("Error invocando a BancaDao para actualizar banca."
                    + ex.getMessage(), ex);
        }

        return resp;
    }

    public ResponseModel agregarLoteriaABanca(int bancaId, int loteriaId) {
        ResponseModel resp = new ResponseModel();

        try {
            LOG.info("agregar lotería a banca...");
            LOG.info("Agregando lotería a banca con Id: " + bancaId
                    + " y loteriaId: " + loteriaId);

            BancaLoteriaResponse out = this.bancaLoteriaDao.crearBancaLoteria(bancaId, loteriaId);
            if (out != null) {
                if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                    resp.setWasSuccessful(true);
                    resp.setMessage(this.getMessageFromBundle("sab.banca.loteria.agregar.success"));
                } else if (out.getMsgEnum().equals(MessageEnum.VALIDATION)) {
                    resp.setWasSuccessful(false);
                    resp.setMessage(this.getMessageFromBundle("sab.banca.loteria.agregar.val.error"));
                } else {
                    resp.setWasSuccessful(false);
                    resp.setMessage(this.getMessageFromBundle("sab.banca.loteria.agregar.error"));
                    LOG.error("Ha ocurrido un error agregando Lotería a Banca. "
                            + "Message Enum equals:>" + out.getMsgEnum());
                }
            } else {
                resp.setWasSuccessful(false);
                resp.setMessage(this.getMessageFromBundle("sab.banca.loteria.agregar.error"));
                LOG.error("Ha ocurrido un error agregando Lotería a Banca. Out is null.");
            }

        } catch (Exception ex) {
            resp.setWasSuccessful(false);
            resp.setMessage(this.getMessageFromBundle("sab.banca.loteria.agregar.error"));
            LOG.error("Error invocando a BancaDao para agregar lotería a banca."
                    + ex.getMessage(), ex);
        }

        return resp;
    }

    public ResponseModel removerLoteriaDeBanca(int bancaId, int loteriaId) {
        ResponseModel resp = new ResponseModel();

        try {
            LOG.info("Remover lotería de banca...");
            LOG.info("Removiendo lotería de banca con Id: " + bancaId
                    + " y loteriaId: " + loteriaId);

            BancaLoteriaResponse blr = this.bancaLoteriaDao.getBancaLoteriaById(bancaId, loteriaId);

            if (blr != null && blr.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                BancaLoteriaResponse out = this.bancaLoteriaDao.deleteBancaLoteria(blr.getBancaLoteria());
                if (out != null) {
                    if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                        resp.setWasSuccessful(true);
                        resp.setMessage(this.getMessageFromBundle("sab.banca.loteria.eliminar.success"));
                    } else if (out.getMsgEnum().equals(MessageEnum.VALIDATION)) {
                        resp.setWasSuccessful(false);
                        resp.setMessage(this.getMessageFromBundle("sab.banca.loteria.eliminar.val.error"));
                    } else {
                        resp.setWasSuccessful(false);
                        resp.setMessage(this.getMessageFromBundle("sab.banca.loteria.eliminar.error"));
                        LOG.error("Ha ocurrido un error eliminando Lotería de Banca. "
                                + "Message Enum equals:>" + out.getMsgEnum());
                    }
                } else {
                    resp.setWasSuccessful(false);
                    resp.setMessage(this.getMessageFromBundle("sab.banca.loteria.eliminar.error"));
                    LOG.error("Ha ocurrido un error eliminando Lotería de Banca. Out is null.");
                }

            }

        } catch (Exception ex) {
            resp.setWasSuccessful(false);
            resp.setMessage(this.getMessageFromBundle("sab.banca.loteria.eliminar.error"));
            LOG.error("Error invocando a BancaLoteriaDao para eliminar lotería de banca."
                    + ex.getMessage(), ex);
        }

        return resp;
    }

    public ResponseModel agregarJuegoABanca(int bancaId, int loteriaId,
            int juegoId, int porcentaje) {
        ResponseModel resp = new ResponseModel();

        try {
            LOG.info("Agregar juego a banca...");
            LOG.info("Agregando juego a banca con Id: " + bancaId
                    + " - loteriaId: " + loteriaId + " y juegoId: " + juegoId);
            BancaLoteriaJuegoResponse out
                    = bancaLoteriaJuegoDao.crearBancaLoteriaJuego(bancaId, loteriaId, juegoId, porcentaje);

            if (out != null) {
                if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                    resp.setWasSuccessful(true);
                    resp.setMessage(this.getMessageFromBundle("sab.banca.juego.agregar.success"));
                } else if (out.getMsgEnum().equals(MessageEnum.VALIDATION)) {
                    resp.setWasSuccessful(false);
                    resp.setMessage(this.getMessageFromBundle("sab.banca.juego.agregar.error"));
                } else {
                    resp.setWasSuccessful(false);
                    resp.setMessage(this.getMessageFromBundle("sab.banca.juego.agregar.error"));
                    LOG.error("Ha ocurrido un error agregando Juego a Banca. "
                            + "Message Enum equals:>" + out.getMsgEnum());
                }
            } else {
                resp.setWasSuccessful(false);
                resp.setMessage(this.getMessageFromBundle("sab.banca.juego.agregar.error"));
                LOG.error("Ha ocurrido un error agregando Juego a Banca. Out is null.");
            }

        } catch (Exception ex) {
            resp.setWasSuccessful(false);
            resp.setMessage(this.getMessageFromBundle("sab.banca.juego.eliminar.error"));
            LOG.error("Error invocando a BancaLoteriaJuegoDao para "
                    + "agregar juego a banca."
                    + ex.getMessage(), ex);
        }

        return resp;
    }

    public ResponseModel eliminarJuegoDeBanca(int bancaId, int loteriaId,
            int juegoId) {
        ResponseModel resp = new ResponseModel();

        try {
            LOG.info("Eliminar juego de banca...");
            LOG.info("Eliminando juego de banca con Id: " + bancaId
                    + " - loteriaId: " + loteriaId + " y juegoId: " + juegoId);

            BancaLoteriaJuegoResponse blj
                    = bancaLoteriaJuegoDao.getBancaLoteriaJuegoById(bancaId, loteriaId, juegoId);

            if (blj != null) {

                if (blj.getMsgEnum().equals(MessageEnum.SUCCESS)) {

                    BancaLoteriaJuegoResponse out
                            = bancaLoteriaJuegoDao.deleteBancaLoteriaJuego(blj.getBancaLoteriaJuego());

                    if (out != null) {
                        if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                            resp.setWasSuccessful(true);
                            resp.setMessage(this.getMessageFromBundle("sab.banca.juego.eliminar.success"));
                        } else if (out.getMsgEnum().equals(MessageEnum.VALIDATION)) {
                            resp.setWasSuccessful(false);
                            resp.setMessage(this.getMessageFromBundle("sab.banca.juego.eliminar.val.error"));
                        } else {
                            resp.setWasSuccessful(false);
                            resp.setMessage(this.getMessageFromBundle("sab.banca.juego.eliminar.error"));
                            LOG.error("Ha ocurrido un error eliminando Juego de Banca. "
                                    + "Message Enum equals:>" + out.getMsgEnum());
                        }
                    } else {
                        resp.setWasSuccessful(false);
                        resp.setMessage(this.getMessageFromBundle("sab.banca.juego.eliminar.error"));
                        LOG.error("Ha ocurrido un error eliminando Juego de Banca. Out is null.");
                    }
                }
            } else {
                resp.setWasSuccessful(false);
                resp.setMessage(this.getMessageFromBundle("sab.banca.juego.eliminar.error"));
                LOG.error("Ha ocurrido un error eliminando Juego de Banca."
                        + " Out is null para la busqueda de BancaLoteriaJuego.");
            }
        } catch (Exception ex) {
            resp.setWasSuccessful(false);
            resp.setMessage(this.getMessageFromBundle("sab.banca.juego.eliminar.error"));
            LOG.error("Error invocando a BancaLoteriaJuegoDao para "
                    + "eliminar juego de banca."
                    + ex.getMessage(), ex);
        }

        return resp;
    }

    public ResponseModel actualizarPorcentajeACobrarDeBanca(int bancaId,int porcentaje) {
        ResponseModel resp = new ResponseModel();

        try {
            LOG.info("Actualizar Porcentaje de Juegos de Banca...");
            LOG.info("Actualizando Porcentaje de Juegos de Banca con BancaId: " + bancaId
                    +" y porcentaje: " + porcentaje);
            
            BancaLoteriaJuegoResponse out
                    = bancaLoteriaJuegoDao.aplicarPorcientoByBanca(bancaId, porcentaje);

            if (out != null) {
                if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                    resp.setWasSuccessful(true);
                    resp.setMessage(this.getMessageFromBundle("sab.banca.aplicar.porc.success"));
                } else {
                    resp.setWasSuccessful(false);
                    resp.setMessage(this.getMessageFromBundle("sab.banca.aplicar.porc.error"));
                    LOG.error("Ha ocurrido un error actualizando Porcentaje de Juegos de Banca. "
                            + "Message Enum equals:>" + out.getMsgEnum());
                }
            } else {
                resp.setWasSuccessful(false);
                resp.setMessage(this.getMessageFromBundle("sab.banca.aplicar.porc.error"));
                LOG.error("Ha ocurrido un error actualizando Juego de Banca. Out is null.");
            }
        } catch (Exception ex) {
            resp.setWasSuccessful(false);
            resp.setMessage(this.getMessageFromBundle("sab.banca.aplicar.porc.error"));
            LOG.error("Error invocando a BancaLoteriaJuegoDao para "
                    + "actualizar porcentaje de juegos de banca."
                    + ex.getMessage(), ex);
        }

        return resp;
    }    
    
    public ResponseModel actualizarJuegoDeBancaLoteriaJuego(int bancaId, int loteriaId,
            int juegoId, int porcentaje) {
        ResponseModel resp = new ResponseModel();

        try {
            LOG.info("Actualizar juego de banca...");
            LOG.info("Actualizando juego de bancaloteriajuego con Id: " + bancaId
                    + " - loteriaId: " + loteriaId + " y juegoId: " + juegoId
                    + " y porcentaje: " + porcentaje);
            BancaLoteriaJuegoResponse out
                    = bancaLoteriaJuegoDao.aplicarPorcientoByJuego
                        (bancaId, loteriaId, juegoId, porcentaje);

            if (out != null) {
                if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                    resp.setWasSuccessful(true);
                    resp.setMessage(this.getMessageFromBundle("sab.banca.juego.actualizar.success"));
                } else {
                    resp.setWasSuccessful(false);
                    resp.setMessage(this.getMessageFromBundle("sab.banca.juego.actualizar.error"));
                    LOG.error("Ha ocurrido un error actualizando Juego de Banca. "
                            + "Message Enum equals:>" + out.getMsgEnum());
                }
            } else {
                resp.setWasSuccessful(false);
                resp.setMessage(this.getMessageFromBundle("sab.banca.juego.actualizar.error"));
                LOG.error("Ha ocurrido un error actualizando Juego de Banca. Out is null.");
            }
        } catch (Exception ex) {
            resp.setWasSuccessful(false);
            resp.setMessage(this.getMessageFromBundle("sab.banca.juego.actualizar.error"));
            LOG.error("Error invocando a BancaLoteriaJuegoDao para "
                    + "actualizar juego de banca."
                    + ex.getMessage(), ex);
        }

        return resp;
    }

    @Override
    public void resetModelBean() {
        result = null;
    }

    protected void displayErrorMessageFromBundle(String msgproperty, String msgClientId) {
        MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(msgClientId,
                msgproperty);
    }

    protected String getMessageFromBundle(String msgproperty, Object... params) {
        return MessageDisplayUtils.INSTANCE.
                getMessageFromBundle(msgproperty, params);
    }

    private ResponseModel processResponse(BancaResponse response,
            String succesMessage, String failureMessage, String validationFailMessage) {

        ResponseModel resp = new ResponseModel();

        if (response != null) {
            /**
             * In this switch the FAIL enum will be treated as default for
             * displaying general message.
             */
            switch (response.getMsgEnum()) {

                case SUCCESS:
                    resp.setWasSuccessful(true);
                    resp.setMessage(
                            getMessageFromBundle(succesMessage));
                    resetModelBean();
                    break;

                case VALIDATION:
                    resp.setWasSuccessful(false);
                    resp.setMessage(getMessageFromBundle(validationFailMessage));
                    break;

                default:
                    resp.setWasSuccessful(false);
                    resp.setMessage(
                            getMessageFromBundle(failureMessage));
                    break;
            }
        } else {
            resp.setWasSuccessful(false);
            resp.setMessage(
                    getMessageFromBundle(failureMessage));

        }

        return resp;
    }
}
