package com.sib.banca.bancaloteria;

import com.sib.banca.BancaAbstractPopup;
import com.sib.banca.BancaConstantes;
import com.sib.loteria.LoteriaModelBean;
import com.sib.ui.models.LoteriaModel;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "bancaLoteriaDeletePopup")
@ViewScoped
public class BancaLoteriaDeletePopup extends BancaAbstractPopup 
    implements Serializable {
    
    public static final Logger LOG = Logger.getLogger(BancaLoteriaDeletePopup.class.getName());

    @ManagedProperty(value = "#{loteriaModelBean}")
    private LoteriaModelBean loteriaModelBean;

    private List<LoteriaModel> loteriasDeBanca;

    public BancaLoteriaDeletePopup() {
    }

    public LoteriaModelBean getLoteriaModelBean() {
        return loteriaModelBean;
    }

    public void setLoteriaModelBean(LoteriaModelBean loteriaModelBean) {
        this.loteriaModelBean = loteriaModelBean;
    }

    public List<LoteriaModel> getLoteriasDeBanca() {
        if(loteriasDeBanca==null){
            loteriasDeBanca
                    =this.loteriaModelBean.getLoteriasByBancaId(this.getModel().getId());
        }
        return loteriasDeBanca;
    }

    public void setLoteriasDeBanca(List<LoteriaModel> loteriasDeBanca) {
        this.loteriasDeBanca = loteriasDeBanca;
    }

    @Override
    public String getFileName() {
        return "bancaloteria/bancaloteriadeletepopup";
    }

    @Override
    public String getPopupTitle() {
        return MessageDisplayUtils.INSTANCE.getMessageFromBundle("sab.banca.loteria.eliminar.lbl");
    }

    @Override
    public Logger getLogger() {
        return LOG;
    }

    @Override
    public void execute(ActionEvent event) {
        try {
            int loteriaId = this.getSelectedLoteriaModelFromRequest().getId();
            int bancaId = this.getModel().getId();
            ResponseModel response
                    = this.getModelBean().removerLoteriaDeBanca(bancaId,loteriaId);

            if (response.getWasSuccessful()) {
                this.displayInfoMessage(response.getMessage(),
                        BancaConstantes.POPUP_MESSAGE_CLIENT_ID);
                this.loteriasDeBanca = null;
            } else {
                LOG.error("Error creando Loteria: " + response.getMessage());
                this.displayErrorMessage(response.getMessage(),
                        BancaConstantes.POPUP_MESSAGE_CLIENT_ID);
            }
        } catch (Exception ex) {
            LOG.error("Error creando Loteria. Exception: " + ex.getMessage(), ex);
            this.displayErrorMessageFromBundle("sab.banca.loteria.eliminar.error",
                    BancaConstantes.POPUP_MESSAGE_CLIENT_ID);
        }
    }
    
    private LoteriaModel getSelectedLoteriaModelFromRequest(){
        LoteriaModel selectedLoteria = null;
        
        Map<String,String> params = 
                FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        
        String loteriaId = params.get("loteriaId");
        int id = Integer.parseInt(loteriaId);
        
        for(LoteriaModel jm : loteriasDeBanca){
            if(jm.getId()==id){
                selectedLoteria=jm;
            }
        }
    
        return selectedLoteria;
    }      

    @Override
    public boolean validateFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}