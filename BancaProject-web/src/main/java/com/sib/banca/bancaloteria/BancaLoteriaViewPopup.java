package com.sib.banca.bancaloteria;

import com.sib.banca.BancaAbstractPopup;
import com.sib.loteria.LoteriaModelBean;
import com.sib.ui.models.LoteriaModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "bancaLoteriaViewPopup")
@ViewScoped
public class BancaLoteriaViewPopup extends BancaAbstractPopup 
    implements Serializable {
    
    public static final Logger LOG = Logger.getLogger(BancaLoteriaDeletePopup.class.getName());

    @ManagedProperty(value = "#{loteriaModelBean}")
    private LoteriaModelBean loteriaModelBean;

    private List<LoteriaModel> loteriasDeBanca;

    public BancaLoteriaViewPopup() {
    }

    public LoteriaModelBean getLoteriaModelBean() {
        return loteriaModelBean;
    }

    public void setLoteriaModelBean(LoteriaModelBean loteriaModelBean) {
        this.loteriaModelBean = loteriaModelBean;
    }

    public List<LoteriaModel> getLoteriasDeBanca() {
        if(loteriasDeBanca==null){
            loteriasDeBanca
                    =this.loteriaModelBean.getLoteriasByBancaId(this.getModel().getId());
        }
        return loteriasDeBanca;
    }

    public void setLoteriasDeBanca(List<LoteriaModel> loteriasDeBanca) {
        this.loteriasDeBanca = loteriasDeBanca;
    }

    @Override
    public String getFileName() {
        return "bancaloteria/bancaloteriaviewpopup";
    }

    @Override
    public String getPopupTitle() {
        return MessageDisplayUtils.INSTANCE.getMessageFromBundle("sab.banca.loteria.ver.lbl");
    }

    @Override
    public Logger getLogger() {
        return LOG;
    }

    @Override
    public void execute(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }      

    @Override
    public boolean validateFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}