package com.sib.banca;

import java.io.Serializable;

/**
 *
 * @author Omar Moronta
 */
public class BancaConstantes implements Serializable{
    public static final String TABLA_MESSAGE_CLIENT_ID="bancamantmsg";
    public static final String POPUP_MESSAGE_CLIENT_ID = "bancdlg";
}
