package com.sib.banca;

import com.sib.banca.bancajuego.BancaJuegoCreatePopup;
import com.sib.banca.bancajuego.BancaJuegoDeletePopup;
import com.sib.banca.bancajuego.BancaJuegoUpdatePopup;
import com.sib.banca.bancajuego.BancaJuegoViewPopup;
import com.sib.banca.bancaloteria.BancaLoteriaCreatePopup;
import com.sib.banca.bancaloteria.BancaLoteriaViewPopup;
import com.sib.banca.bancaloteria.BancaLoteriaDeletePopup;
import com.sib.grupo.GrupoModelBean;
import com.sib.interfaces.IPopup;
import com.sib.ui.enums.StatusEnum;
import com.sib.ui.models.BancaModel;
import com.sib.ui.models.GrupoModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "bancaBean")
@ViewScoped
public class BancaBean implements Serializable {

    public static final Logger LOG = Logger.getLogger(BancaBean.class.getName());

    @ManagedProperty(value = "#{bancaModelBean}")
    private BancaModelBean modelBean;

    @ManagedProperty(value = "#{grupoModelBean}")
    private GrupoModelBean grupoModelBean;

    private IPopup popupHandler;

    @ManagedProperty(value = "#{bancaCreatePopup}")
    private IPopup createPopup;
    @ManagedProperty(value = "#{bancaUpdatePopup}")
    private IPopup updatePopup;
    @ManagedProperty(value = "#{bancaDeletePopup}")
    private IPopup deletePopup;
    
    @ManagedProperty(value = "#{bancaLoteriaCreatePopup}")
    private IPopup bancaAddLoteriaPopup;
    @ManagedProperty(value = "#{bancaLoteriaDeletePopup}")
    private IPopup bancaRemoveLoteriaPopup;
    @ManagedProperty(value = "#{bancaLoteriaViewPopup}")
    private IPopup bancaViewLoteriaPopup;

    @ManagedProperty(value = "#{bancaJuegoCreatePopup}")
    private IPopup bancaAddJuegoPopup;
    @ManagedProperty(value = "#{bancaJuegoDeletePopup}")
    private IPopup bancaRemoveJuegoPopup;
    @ManagedProperty(value = "#{bancaJuegoViewPopup}")
    private IPopup bancaViewJuegoPopup;
    @ManagedProperty(value = "#{bancaJuegoUpdatePopup}")
    private IPopup bancaUpdateJuegoPopup;
    
    private List<SelectItem> gruposSI;

    public BancaBean() {
    }

    public IPopup getPopupHandler() {
        if (this.popupHandler == null) {
            this.popupHandler = createPopup;
        }
        return popupHandler;
    }

    public void setPopupHandler(IPopup popupHandler) {

        this.popupHandler = popupHandler;
    }

    public IPopup getCreatePopup() {
        return createPopup;
    }

    public void setCreatePopup(IPopup createPopup) {
        this.createPopup = createPopup;
    }

    public IPopup getUpdatePopup() {
        return updatePopup;
    }

    public void setUpdatePopup(IPopup updatePopup) {
        this.updatePopup = updatePopup;
    }

    public IPopup getDeletePopup() {
        return deletePopup;
    }

    public void setDeletePopup(IPopup deletePopup) {
        this.deletePopup = deletePopup;
    }

    public IPopup getBancaAddLoteriaPopup() {
        return bancaAddLoteriaPopup;
    }

    public void setBancaAddLoteriaPopup(IPopup bancaAddLoteriaPopup) {
        this.bancaAddLoteriaPopup = bancaAddLoteriaPopup;
    }

    public IPopup getBancaRemoveLoteriaPopup() {
        return bancaRemoveLoteriaPopup;
    }

    public void setBancaRemoveLoteriaPopup(IPopup bancaRemoveLoteriaPopup) {
        this.bancaRemoveLoteriaPopup = bancaRemoveLoteriaPopup;
    }

    public IPopup getBancaViewLoteriaPopup() {
        return bancaViewLoteriaPopup;
    }

    public void setBancaViewLoteriaPopup(IPopup bancaViewLoteriaPopup) {
        this.bancaViewLoteriaPopup = bancaViewLoteriaPopup;
    }

    public IPopup getBancaAddJuegoPopup() {
        return bancaAddJuegoPopup;
    }

    public void setBancaAddJuegoPopup(IPopup bancaAddJuegoPopup) {
        this.bancaAddJuegoPopup = bancaAddJuegoPopup;
    }

    public IPopup getBancaRemoveJuegoPopup() {
        return bancaRemoveJuegoPopup;
    }

    public void setBancaRemoveJuegoPopup(IPopup bancaRemoveJuegoPopup) {
        this.bancaRemoveJuegoPopup = bancaRemoveJuegoPopup;
    }

    public IPopup getBancaViewJuegoPopup() {
        return bancaViewJuegoPopup;
    }

    public void setBancaViewJuegoPopup(IPopup bancaViewJuegoPopup) {
        this.bancaViewJuegoPopup = bancaViewJuegoPopup;
    }    

    public IPopup getBancaUpdateJuegoPopup() {
        return bancaUpdateJuegoPopup;
    }

    public void setBancaUpdateJuegoPopup(IPopup bancaUpdateJuegoPopup) {
        this.bancaUpdateJuegoPopup = bancaUpdateJuegoPopup;
    }   
    
    public BancaModelBean getModelBean() {
        return modelBean;
    }

    public void setModelBean(BancaModelBean modelBean) {
        this.modelBean = modelBean;
    }

    public GrupoModelBean getGrupoModelBean() {
        return grupoModelBean;
    }

    public void setGrupoModelBean(GrupoModelBean grupoModelBean) {
        this.grupoModelBean = grupoModelBean;
    }

    public List<SelectItem> getGruposSI() {
        if(gruposSI==null){
            List<GrupoModel> gruposActivos 
                    = this.grupoModelBean.getGruposByStatus(StatusEnum.ACTIVO.getStatusLiteral());
            gruposSI = new ArrayList<SelectItem>();
            
            if(gruposActivos!=null){
                for(GrupoModel gm : gruposActivos){
                    gruposSI.add(new SelectItem(gm.getId(),gm.getCodigo()));
                }
            }
        }
        return gruposSI;
    }

    public void setGruposSI(List<SelectItem> gruposSI) {
        this.gruposSI = gruposSI;
    }

    public void onGrupoSelectionChange() {
        this.modelBean.resetModelBean();
    }

    public void openCreatePopup(ActionEvent event) {
        ((BancaCreatePopup) createPopup).setModel(new BancaModel());
        this.setPopupHandler(this.createPopup);
    }

    public void openUpdatePopup(ActionEvent event) {

        BancaModel selected = getSelectedBancaModelFromRequest();

        ((BancaUpdatePopup) updatePopup).setUntouchedModel(selected);

        BancaUtil bUtil = new BancaUtil();
        BancaModel selectedModel
                = bUtil.crearBancaModelDeBancaModel(selected);

        ((BancaUpdatePopup) updatePopup).setModel(selectedModel);

        this.setPopupHandler(this.updatePopup);
    }

    public void openDeletePopup(ActionEvent event) {
        BancaModel selected = getSelectedBancaModelFromRequest();
        ((BancaDeletePopup) deletePopup).setModel(selected);
        this.setPopupHandler(this.deletePopup);
    }

    public void openAddLoteriaABancaPopup(ActionEvent event) {
        BancaModel selected = getSelectedBancaModelFromRequest();
        BancaLoteriaCreatePopup popup 
                = (BancaLoteriaCreatePopup) bancaAddLoteriaPopup;
        popup.setModel(selected);
        popup.setLoteriaNoRelacionadasList(null);
        this.popupHandler=this.bancaAddLoteriaPopup;
    }    

    public void openRemoveLoteriaDeBancaPopup(ActionEvent event) {
        BancaModel selected = getSelectedBancaModelFromRequest();
        BancaLoteriaDeletePopup popup 
                = (BancaLoteriaDeletePopup) bancaRemoveLoteriaPopup;
        popup.setModel(selected);
        popup.setLoteriasDeBanca(null);
        this.popupHandler=this.bancaRemoveLoteriaPopup;
    }    

    public void openViewLoteriaDeBancaPopup(ActionEvent event) {
        BancaModel selected = getSelectedBancaModelFromRequest();
        BancaLoteriaViewPopup popup 
                = (BancaLoteriaViewPopup) bancaViewLoteriaPopup;
        popup.setModel(selected);
        popup.setLoteriasDeBanca(null);    
        this.popupHandler=this.bancaViewLoteriaPopup;
    }
    
    public void openAddJuegoABancaPopup(ActionEvent event) {
        BancaModel selected = getSelectedBancaModelFromRequest();
        BancaJuegoCreatePopup popup 
                = (BancaJuegoCreatePopup) bancaAddJuegoPopup;
        popup.setModel(selected);
        popup.setLoteriasSI(null);
        popup.setJuegoNoRelacionadosList(null);
        popup.setPorcentaje(0);
        this.getModelBean().getInputBean().setLoteriaId(null);
        this.popupHandler=this.bancaAddJuegoPopup;
    }    

    public void openRemoveJuegoDeBancaPopup(ActionEvent event) {
        BancaModel selected = getSelectedBancaModelFromRequest();
        BancaJuegoDeletePopup popup 
                = (BancaJuegoDeletePopup) bancaRemoveJuegoPopup;
        popup.setModel(selected);
        popup.setLoteriasSI(null);
        popup.setJuegosRelacionadosList(null);       
        this.getModelBean().getInputBean().setLoteriaId(null);
        this.popupHandler=this.bancaRemoveJuegoPopup;
    }    

    public void openViewJuegoDeBancaPopup(ActionEvent event) {
        BancaModel selected = getSelectedBancaModelFromRequest();
        BancaJuegoViewPopup popup 
                = (BancaJuegoViewPopup) bancaViewJuegoPopup;
        popup.setModel(selected);
        popup.setLoteriasSI(null);
        popup.setJuegosRelacionadosList(null);
        this.getModelBean().getInputBean().setLoteriaId(null);        
        this.popupHandler=this.bancaViewJuegoPopup;
    }

    public void openUpdateJuegoDeBancaPopup(ActionEvent event) {
        BancaModel selected = getSelectedBancaModelFromRequest();
        BancaJuegoUpdatePopup popup 
                = (BancaJuegoUpdatePopup) bancaUpdateJuegoPopup;
        popup.setModel(selected);
        popup.setLoteriasSI(null);
        popup.setJuegosRelacionadosList(null);
        popup.setPorcentaje(0);        
        this.getModelBean().getInputBean().setLoteriaId(null);        
        this.popupHandler=this.bancaUpdateJuegoPopup;
    }    
    
    private BancaModel getSelectedBancaModelFromRequest() {
        BancaModel selected = new BancaModel();

        Map<String, String> params
                = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

        String bancaId = params.get("bancaId");
        int id = Integer.parseInt(bancaId);

        BancaModelBean jmb = (BancaModelBean) this.getModelBean();

        for (BancaModel jm : jmb.getResult()) {
            if (jm.getId() == id) {
                selected = jm;
            }
        }

        return selected;
    }

}
