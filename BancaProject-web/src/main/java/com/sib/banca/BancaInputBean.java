package com.sib.banca;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "bancaInputBean")
@ViewScoped
public class BancaInputBean implements Serializable {

    private Integer grupoId;
    private Integer loteriaId;

    public BancaInputBean() {
    }

    public Integer getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(Integer grupoId) {
        this.grupoId = grupoId;
    }

    public Integer getLoteriaId() {
        return loteriaId;
    }

    public void setLoteriaId(Integer loteriaId) {
        this.loteriaId = loteriaId;
    }
}
