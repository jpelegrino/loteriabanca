package com.sib.banca.bancajuego;

import com.sib.ui.models.JuegoAndBLJModel;
import java.util.List;

/**
 * Esta clase abstracta será utilizada para crear Popups que utilicen la lista
 * de juegos relacionados a la Banca-Loteria
 *
 * @author Omar Moronta
 */
public class BJAbstractJuegoRelacionadosPopup extends BJAbstractPopup {

    private List<JuegoAndBLJModel> juegosRelacionadosList;

    public List<JuegoAndBLJModel> getJuegosRelacionadosList() {
        if (juegosRelacionadosList == null
                && this.getModelBean().getInputBean().getLoteriaId() != null) {
            juegosRelacionadosList
                    = this.getModelBean().getJuegoAndBLJModelList(this.getModel().getId(), this.getModelBean().getInputBean().getLoteriaId());
        }

        return juegosRelacionadosList;
    }

    public void setJuegosRelacionadosList(List<JuegoAndBLJModel> juegosRelacionadosList) {
        this.juegosRelacionadosList = juegosRelacionadosList;
    }
    
    public void onLoteriaSelectionChange() {
       this.getLogger().info("LOTERIA CHANGED!!!!");
       this.juegosRelacionadosList=null;
    }    
}
