package com.sib.banca.bancajuego;

import com.sib.banca.BancaConstantes;
import com.sib.ui.models.JuegoAndBLJModel;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "bancaJuegoUpdatePopup")
@ViewScoped
public class BancaJuegoUpdatePopup extends BJAbstractJuegoRelacionadosPopup {

    public static final Logger LOG = Logger.getLogger(BancaJuegoUpdatePopup.class.getName());

    private int porcentaje;
    
    public BancaJuegoUpdatePopup() {
    }

    public int getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(int porcentaje) {
        this.porcentaje = porcentaje;
    }   
    
    @Override
    public String getFileName() {
        return "bancajuego/bancajuegoupdatepopup";
    }

    @Override
    public String getPopupTitle() {
        return MessageDisplayUtils.INSTANCE.getMessageFromBundle("sab.banca.juego.actualizar.lbl");
    }

    @Override
    public Logger getLogger() {
        return LOG;
    }

    @Override
    public void execute(ActionEvent event) {

        try {
            int juegoId = this.getSelectedJuegoAndBLJModelFromRequest().getId();

            ResponseModel response
                    = this.getModelBean().
                    actualizarJuegoDeBancaLoteriaJuego(this.getModel().getId(),
                            this.getModelBean().getInputBean().getLoteriaId(),
                            juegoId, this.porcentaje);

            if (response.getWasSuccessful()) {
                this.displayInfoMessage(response.getMessage(),
                        BancaConstantes.POPUP_MESSAGE_CLIENT_ID);
                this.setJuegosRelacionadosList(null);
            } else {
                LOG.error("Error actualizando juego de banca: " + response.getMessage());
                this.displayErrorMessage(response.getMessage(),
                        BancaConstantes.POPUP_MESSAGE_CLIENT_ID);
            }
        } catch (Exception ex) {
            LOG.error("Error actualizando juego de banca. Exception: " + ex.getMessage(), ex);
            this.displayErrorMessageFromBundle("sab.banca.juego.actualizar.error",
                    BancaConstantes.POPUP_MESSAGE_CLIENT_ID);
        }
    }
    
    public void executeApplyUpdateAll(ActionEvent event){
        try {
            ResponseModel response
                    = this.getModelBean().
                    actualizarPorcentajeACobrarDeBanca(this.getModel().getId(),
                            this.porcentaje);

            if (response.getWasSuccessful()) {
                this.displayInfoMessage(response.getMessage(),
                        BancaConstantes.POPUP_MESSAGE_CLIENT_ID);
                this.setJuegosRelacionadosList(null);
            } else {
                LOG.error("Error actualizando porcentaje de juegos de banca: " 
                        + response.getMessage());
                this.displayErrorMessage(response.getMessage(),
                        BancaConstantes.POPUP_MESSAGE_CLIENT_ID);
            }
        } catch (Exception ex) {
            LOG.error("Error actualizando porcentaje de juegos de banca. Exception: " 
                    + ex.getMessage(), ex);
            this.displayErrorMessageFromBundle("sab.banca.aplicar.porc.error",
                    BancaConstantes.POPUP_MESSAGE_CLIENT_ID);
        }        
    }

    private JuegoAndBLJModel getSelectedJuegoAndBLJModelFromRequest() {
        JuegoAndBLJModel selectedJuego = null;

        Map<String, String> params
                = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();

        String juegoId = params.get("juegoId");
        int id = Integer.parseInt(juegoId);

        for (JuegoAndBLJModel jm : getJuegosRelacionadosList()) {
            if (jm.getId() == id) {
                selectedJuego = jm;
            }
        }

        return selectedJuego;
    }

    @Override
    public boolean validateFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
