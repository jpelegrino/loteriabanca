package com.sib.banca.bancajuego;

import com.sib.banca.BancaConstantes;
import com.sib.juego.JuegoModelBean;
import com.sib.ui.models.JuegoModel;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "bancaJuegoCreatePopup")
@ViewScoped
public class BancaJuegoCreatePopup extends BJAbstractPopup {

    public static final Logger LOG = Logger.getLogger(BancaJuegoCreatePopup.class.getName());

    @ManagedProperty(value = "#{juegoModelBean}")
    private JuegoModelBean juegoModelBean;

    private int porcentaje;
    private List<JuegoModel> juegoNoRelacionadosList;

    public BancaJuegoCreatePopup() {
    }

    public JuegoModelBean getJuegoModelBean() {
        return juegoModelBean;
    }

    public void setJuegoModelBean(JuegoModelBean juegoModelBean) {
        this.juegoModelBean = juegoModelBean;
    }

    public List<JuegoModel> getJuegoNoRelacionadosList() {
        if (this.juegoNoRelacionadosList == null
                && this.getModelBean().getInputBean().getLoteriaId() != null) {
            this.juegoNoRelacionadosList
                    = this.juegoModelBean.getJuegosFromLoteriaNotInBLJ
                        (this.getModelBean().getInputBean().getLoteriaId(), 
                                this.getModel().getId());
        }
        return juegoNoRelacionadosList;
    }

    public void setJuegoNoRelacionadosList(List<JuegoModel> juegoNoRelacionadosList) {
        this.juegoNoRelacionadosList = juegoNoRelacionadosList;
    }

    public int getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(int porcentaje) {
        this.porcentaje = porcentaje;
    }

    @Override
    public void execute(ActionEvent event) {
        
        if (validateFields()) {
            try {
                int juegoId = this.getSelectedJuegoModelFromRequest().getId();

                ResponseModel response
                        = this.getModelBean().
                        agregarJuegoABanca(this.getModel().getId(),
                                this.getModelBean().getInputBean().getLoteriaId(),
                                juegoId, this.porcentaje);

                if (response.getWasSuccessful()) {
                    this.displayInfoMessage(response.getMessage(),
                            BancaConstantes.POPUP_MESSAGE_CLIENT_ID);
                    this.juegoNoRelacionadosList = null;
                } else {
                    LOG.error("Error agregando Juego a Banca: " + response.getMessage());
                    this.displayErrorMessage(response.getMessage(),
                            BancaConstantes.POPUP_MESSAGE_CLIENT_ID);
                }
            } catch (Exception ex) {
                LOG.error("Error agregando Juego a Banca. Exception: " + ex.getMessage(), ex);
                this.displayErrorMessageFromBundle("sab.banca.juego.agregar.error",
                        BancaConstantes.POPUP_MESSAGE_CLIENT_ID);
            }

        }
    }

    public void onLoteriaSelectionChange() {
        LOG.info("LOTERIA CHANGED!!!!");
        this.juegoNoRelacionadosList = null;
    }

    private JuegoModel getSelectedJuegoModelFromRequest() {
        JuegoModel selectedJuego = null;

        Map<String, String> params
                = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

        String juegoId = params.get("juegoId");
        int id = Integer.parseInt(juegoId);

        for (JuegoModel jm : juegoNoRelacionadosList) {
            if (jm.getId() == id) {
                selectedJuego = jm;
            }
        }

        return selectedJuego;
    }

    @Override
    public String getFileName() {
        return "bancajuego/bancajuegocreatepopup";
    }

    @Override
    public String getPopupTitle() {
        return MessageDisplayUtils.INSTANCE.getMessageFromBundle("sab.banca.juego.agregar.lbl");
    }

    @Override
    public Logger getLogger() {
        return LOG;
    }

    @Override
    public boolean validateFields() {
        boolean result = true;

        if (porcentaje <= 0) {
            result = false;
            this.displayErrorMessageFromBundle("sab.error.porcentaje.val",
                    BancaConstantes.POPUP_MESSAGE_CLIENT_ID);
        }

        return result;
    }
}
