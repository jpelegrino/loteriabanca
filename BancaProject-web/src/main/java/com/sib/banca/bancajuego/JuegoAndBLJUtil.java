package com.sib.banca.bancajuego;


import com.sib.entity.JuegoAndBLJ;
import com.sib.ui.models.JuegoAndBLJModel;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;

/**
 * This is the JuegoAndBLJ <-> JuegoAndBLJModel objects converter helper class.
 *
 * @author Omar Moronta
 */
public class JuegoAndBLJUtil implements Serializable {
    
    public static final Logger LOG = Logger.getLogger(JuegoAndBLJUtil.class.getName());   

    public JuegoAndBLJUtil() {

    }

    public JuegoAndBLJModel crearJuegoAndBLJModelDeJuegoAndBLJModel(JuegoAndBLJModel jm) {
        JuegoAndBLJModel njm = new JuegoAndBLJModel();

        if (jm != null) {
            byte[] bytesArr = SerializationUtils.serialize(jm);
            njm = (JuegoAndBLJModel)SerializationUtils.deserialize(bytesArr);
        }

        return njm;
    }

    public JuegoAndBLJModel convertirJuegoAJuegoAndBLJModel(JuegoAndBLJ j) {
        JuegoAndBLJModel cur = null;

        if (j != null) {
            byte[] bytesArr = SerializationUtils.serialize(j);
            JuegoAndBLJ ju = (JuegoAndBLJ)SerializationUtils.deserialize(bytesArr);
            
            if(ju!=null){
                try {
                    cur = new JuegoAndBLJModel();
                    BeanUtils.copyProperties(cur, ju);
                } catch (IllegalAccessException ex) {
                    LOG.error(ex.getMessage(), ex);
                } catch (InvocationTargetException ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }

        return cur;
    }

    public JuegoAndBLJ convertirJuegoAndBLJModelAJuego(JuegoAndBLJModel jm) {
        JuegoAndBLJ cur = null;
        
        if(jm!=null){
            byte[] bytesArr = SerializationUtils.serialize(jm);
            JuegoAndBLJModel njm = (JuegoAndBLJModel)SerializationUtils.deserialize(bytesArr);
            
            if(njm!=null){
                try {
                    cur = new JuegoAndBLJ();
                    BeanUtils.copyProperties(cur, njm);
                } catch (IllegalAccessException ex) {
                    LOG.error(ex.getMessage(), ex);
                } catch (InvocationTargetException ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }
        
        return cur;
    }
}
