package com.sib.banca.bancajuego;

import com.sib.banca.BancaConstantes;
import com.sib.ui.models.JuegoAndBLJModel;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "bancaJuegoDeletePopup")
@ViewScoped
public class BancaJuegoDeletePopup extends BJAbstractJuegoRelacionadosPopup {

    public static final Logger LOG = Logger.getLogger(BancaJuegoDeletePopup.class.getName());

    public BancaJuegoDeletePopup() {
    }

    @Override
    public String getFileName() {
        return "bancajuego/bancajuegodeletepopup";
    }

    @Override
    public String getPopupTitle() {
        return MessageDisplayUtils.INSTANCE.getMessageFromBundle("sab.banca.juego.eliminar.lbl");
    }

    @Override
    public Logger getLogger() {
        return LOG;
    }

    @Override
    public void execute(ActionEvent event) {

        try {
            int juegoId = this.getSelectedJuegoAndBLJModelFromRequest().getId();

            ResponseModel response
                    = this.getModelBean().
                    eliminarJuegoDeBanca(this.getModel().getId(),
                            this.getModelBean().getInputBean().getLoteriaId(),
                            juegoId);

            if (response.getWasSuccessful()) {
                this.displayInfoMessage(response.getMessage(),
                        BancaConstantes.POPUP_MESSAGE_CLIENT_ID);
                this.setJuegosRelacionadosList(null);
            } else {
                LOG.error("Error eliminando juego de banca: " + response.getMessage());
                this.displayErrorMessage(response.getMessage(),
                        BancaConstantes.POPUP_MESSAGE_CLIENT_ID);
            }
        } catch (Exception ex) {
            LOG.error("Error eliminando juego de banca. Exception: " + ex.getMessage(), ex);
            this.displayErrorMessageFromBundle("sab.banca.juego.eliminar.error",
                    BancaConstantes.POPUP_MESSAGE_CLIENT_ID);
        }
    }

    private JuegoAndBLJModel getSelectedJuegoAndBLJModelFromRequest() {
        JuegoAndBLJModel selectedJuego = null;

        Map<String, String> params
                = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();

        String juegoId = params.get("juegoId");
        int id = Integer.parseInt(juegoId);

        for (JuegoAndBLJModel jm : getJuegosRelacionadosList()) {
            if (jm.getId() == id) {
                selectedJuego = jm;
            }
        }

        return selectedJuego;
    }

    @Override
    public boolean validateFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}