package com.sib.banca.bancajuego;

import com.sib.banca.BancaAbstractPopup;
import com.sib.loteria.LoteriaModelBean;
import com.sib.ui.models.LoteriaModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedProperty;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
public class BJAbstractPopup extends BancaAbstractPopup 
    implements Serializable {

    @ManagedProperty(value = "#{loteriaModelBean}")
    private LoteriaModelBean loteriaModelBean;    
    
    private List<SelectItem> loteriasSI;    
    
    public LoteriaModelBean getLoteriaModelBean() {
        return loteriaModelBean;
    }

    public void setLoteriaModelBean(LoteriaModelBean loteriaModelBean) {
        this.loteriaModelBean = loteriaModelBean;
    }
    
    public List<SelectItem> getLoteriasSI() {
        if(loteriasSI==null){
            List<LoteriaModel> loteriasList
                   = this.getLoteriaModelBean().getLoteriasByBancaId
                        (this.getModel().getId());
            
            loteriasSI = new ArrayList<SelectItem>();
            
            if(loteriasList!=null){
                for(LoteriaModel lm : loteriasList){
                    loteriasSI.add(new SelectItem(lm.getId(), lm.getCodigo()));
                }
            }
        }
        return loteriasSI;
    }

    public void setLoteriasSI(List<SelectItem> gruposSI) {
        this.loteriasSI = gruposSI;
    }    
    
    @Override
    public Logger getLogger() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void execute(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean validateFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getFileName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getPopupTitle() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
