package com.sib.banca.bancajuego;

import com.sib.util.MessageDisplayUtils;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "bancaJuegoViewPopup")
@ViewScoped
public class BancaJuegoViewPopup extends BJAbstractJuegoRelacionadosPopup{
    
    public static final Logger LOG = Logger.getLogger(BancaJuegoViewPopup.class.getName());
    
    public BancaJuegoViewPopup() {
    }

    @Override
    public String getFileName() {
        return "bancajuego/bancajuegoviewpopup";
    }

    @Override
    public String getPopupTitle() {
        return MessageDisplayUtils.INSTANCE.getMessageFromBundle("sab.banca.juego.ver.lbl");
    }

    @Override
    public Logger getLogger() {
        return LOG;
    }

    @Override
    public void execute(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }      

    @Override
    public boolean validateFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}