package com.sib.banca;

import com.sib.ui.enums.UIOperationResultEnum;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "bancaDeletePopup")
@ViewScoped
public class BancaDeletePopup extends BancaAbstractPopup 
    implements Serializable{

    public static final Logger LOG
            = Logger.getLogger(BancaDeletePopup.class.getName());

    public BancaDeletePopup() {
    }

    @Override
    public void execute(ActionEvent event) {
        try {
            LOG.info("Eliminando Banca: " + this.getModel().getCodigo());
            ResponseModel response = getModelBean().eliminarBanca(this.getModel());

            if (response.getWasSuccessful()) {
                prepararStatusOperacion(UIOperationResultEnum.SUCCESS);
                this.displayInfoMessage(response.getMessage(),
                        BancaConstantes.TABLA_MESSAGE_CLIENT_ID);
            } else {
                LOG.error("Error eliminando Banca: " + response.getMessage());
                this.displayErrorMessage(response.getMessage(),
                        BancaConstantes.POPUP_MESSAGE_CLIENT_ID);
                prepararStatusOperacion(UIOperationResultEnum.FAILURE);
            }
        } catch (Exception ex) {
            LOG.error("Error eliminando Banca. Exception: " 
                    + ex.getMessage(), ex);
            this.displayErrorMessageFromBundle("sab.banca.eliminar.error",
                    BancaConstantes.POPUP_MESSAGE_CLIENT_ID);
            prepararStatusOperacion(UIOperationResultEnum.FAILURE);
        }
    }

    @Override
    public boolean validateFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getFileName() {
        return "bancadeletepopup";
    }

    @Override
    public String getPopupTitle() {
        return MessageDisplayUtils.INSTANCE.getMessageFromBundle("sab.banca.eliminar.lbl");
    }

    @Override
    public Logger getLogger() {
        return LOG;
    }
}