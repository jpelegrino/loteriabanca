package com.sib.juego;

import com.sib.entity.Juego;
import com.sib.ui.models.JuegoModel;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;

/**
 * This is the Juego <-> JuegoModel objects converter helper class.
 *
 * @author Omar Moronta
 */
public class JuegoUtil implements Serializable {
    
    public static final Logger LOG = Logger.getLogger(JuegoUtil.class.getName());   

    public JuegoUtil() {

    }

    public JuegoModel crearJuegoModelDeJuegoModel(JuegoModel jm) {
        JuegoModel njm = new JuegoModel();

        if (jm != null) {
            byte[] bytesArr = SerializationUtils.serialize(jm);
            njm = (JuegoModel)SerializationUtils.deserialize(bytesArr);
        }

        return njm;
    }

    public JuegoModel convertirJuegoAJuegoModel(Juego j) {
        JuegoModel cur = null;

        if (j != null) {
            byte[] bytesArr = SerializationUtils.serialize(j);
            Juego ju = (Juego)SerializationUtils.deserialize(bytesArr);
            
            if(ju!=null){
                try {
                    cur = new JuegoModel();
                    BeanUtils.copyProperties(cur, ju);
                } catch (IllegalAccessException ex) {
                    LOG.error(ex.getMessage(), ex);
                } catch (InvocationTargetException ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }

        return cur;
    }

    public Juego convertirJuegoModelAJuego(JuegoModel jm) {
        Juego cur = null;
        
        if(jm!=null){
            byte[] bytesArr = SerializationUtils.serialize(jm);
            JuegoModel njm = (JuegoModel)SerializationUtils.deserialize(bytesArr);
            
            if(njm!=null){
                try {
                    cur = new Juego();
                    BeanUtils.copyProperties(cur, njm);
                } catch (IllegalAccessException ex) {
                    LOG.error(ex.getMessage(), ex);
                } catch (InvocationTargetException ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }
        
        return cur;
    }
}
