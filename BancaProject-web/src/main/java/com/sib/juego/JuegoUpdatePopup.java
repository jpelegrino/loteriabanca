package com.sib.juego;

import com.sib.ui.enums.UIOperationResultEnum;
import com.sib.ui.models.JuegoModel;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "juegoUpdatePopup")
@ViewScoped
public class JuegoUpdatePopup extends JuegoAbstractPopup implements Serializable {

    public static final Logger LOG
            = Logger.getLogger(JuegoUpdatePopup.class.getName());

    private JuegoModel untouchedModel;

    public JuegoUpdatePopup() {
    }

    public JuegoModel getUntouchedModel() {
        return untouchedModel;
    }

    public void setUntouchedModel(JuegoModel untouchedModel) {
        this.untouchedModel = untouchedModel;
    }

    @Override
    public void execute(ActionEvent event) {
        if (validateFields()) {
            try {
                LOG.info("Actualizando juego: " + this.getModel());
                ResponseModel response = getModelBean().actualizarJuego(this.getModel());
                prepararStatusOperacion(UIOperationResultEnum.SUCCESS);
                if (response.getWasSuccessful()) {
                    prepararStatusOperacion(UIOperationResultEnum.SUCCESS);
                    this.displayInfoMessage(response.getMessage(),
                            JuegoConstantes.TABLA_MESSAGE_CLIENT_ID);
                } else {
                    LOG.error("Error actualizando Juego: " + response.getMessage());
                    this.displayErrorMessage(response.getMessage(),
                            JuegoConstantes.POPUP_MESSAGE_CLIENT_ID);
                    prepararStatusOperacion(UIOperationResultEnum.FAILURE);
                }
            } catch (Exception ex) {
                LOG.error("Error actualizando Juego. Exception: " 
                        + ex.getMessage(), ex);
                this.displayErrorMessageFromBundle("sab.juego.actualizar.error",
                        JuegoConstantes.POPUP_MESSAGE_CLIENT_ID);
                prepararStatusOperacion(UIOperationResultEnum.FAILURE);
            }
        } else {
            prepararStatusOperacion(UIOperationResultEnum.FAILURE);
            LOG.info("Fields Validation Failed..");
        }
    }

    @Override
    public void reset() {
        this.untouchedModel = null;
        super.reset();
    }

    @Override
    public boolean validateFields() {

        boolean result = true;

        if ((this.getModel().getCodigo() == null
                || this.getModel().getCodigo().isEmpty())
                || (this.getModel().getNombre() == null
                || this.getModel().getNombre().isEmpty())) {
            MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(
                    JuegoConstantes.POPUP_MESSAGE_CLIENT_ID,
                    "sab.error.campo.vacio");
            result = false;
        }

        if (result) {
            if ((this.getModel().getCodigo() != null
                    && this.getModel().getCodigo().equals(this.untouchedModel.getCodigo()))
                    && (this.getModel().getNombre() != null
                    && this.getModel().getNombre().equals(this.untouchedModel.getNombre()))
                    && (this.getModel().getStatus() != null
                    && this.getModel().getStatus().equals(this.untouchedModel.getStatus()))) {
                MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(
                        JuegoConstantes.POPUP_MESSAGE_CLIENT_ID,
                        "sab.error.no.cambios");
                result = false;
            }
        }
        return result;
    }

    @Override
    public String getFileName() {
        return "juegoupdatepopup";
    }

    @Override
    public String getPopupTitle() {
        return MessageDisplayUtils.INSTANCE.getMessageFromBundle("sab.juego.actualizar.lbl");
    }

    @Override
    public Logger getLogger() {
        return LOG;
    }
}
