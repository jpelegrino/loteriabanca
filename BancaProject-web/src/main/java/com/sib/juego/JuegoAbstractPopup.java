package com.sib.juego;

import com.sib.interfaces.IPopup;
import com.sib.ui.enums.UIOperationResultEnum;
import com.sib.ui.models.JuegoModel;
import com.sib.util.MessageDisplayUtils;
import javax.faces.bean.ManagedProperty;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Omar Moronta
 */
public abstract class JuegoAbstractPopup implements IPopup{
    
    private JuegoModel model;
    
    @ManagedProperty(value = "#{juegoModelBean}")
    private JuegoModelBean modelBean;
    
    public JuegoModel getModel() {
        return model;
    }

    public void setModel(JuegoModel model) {
        this.model = model;
    }
    
    public JuegoModelBean getModelBean() {
        return modelBean;
    }

    public void setModelBean(JuegoModelBean modelBean) {
        this.modelBean = modelBean;
    }    
    
    @Override
    public void cancelProcess(ActionEvent event) {
        getLogger().info("Cancelling Process");
    }
    
    @Override
    public void reset() {
        this.model=null;
    }
    
    protected void prepararStatusOperacion(UIOperationResultEnum operationStatus){
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("operationStatus", operationStatus.getStatusLiteral());
    }    

    protected void displayErrorMessageFromBundle(String msgproperty, String msgClientId) {
        MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(msgClientId,
                msgproperty);
    }    
    
    protected void displayErrorMessage(String message, String msgClientId) {
        MessageDisplayUtils.INSTANCE.printErrorMessage(msgClientId,
                message);
    }

    protected void displayInfoMessage(String message, String msgClientId) {
        MessageDisplayUtils.INSTANCE.printInfoMessage(msgClientId,
                message);
    }    
    
    public abstract Logger getLogger();
            
}
