package com.sib.juego;

import com.sib.ui.enums.StatusEnum;
import com.sib.ui.enums.UIOperationResultEnum;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "juegoCreatePopup")
@ViewScoped
public class JuegoCreatePopup extends JuegoAbstractPopup implements Serializable {

    public static final Logger LOG = Logger.getLogger(JuegoCreatePopup.class.getName());
    
    public JuegoCreatePopup() {
    }

    @Override
    public void execute(ActionEvent event) {
        if (validateFields()) {
            try {
                LOG.info("Creando juego con CODIGO: " + this.getModel().getCodigo());
                this.getModel().setStatus(StatusEnum.ACTIVO.getStatusLiteral());                
                ResponseModel response = getModelBean().crearJuego(this.getModel());
                
                if(response.getWasSuccessful()){
                    prepararStatusOperacion(UIOperationResultEnum.SUCCESS);
                    this.displayInfoMessage(response.getMessage(),
                        JuegoConstantes.TABLA_MESSAGE_CLIENT_ID);                
                }else{
                    LOG.error("Error creando Juego: " + response.getMessage());
                    this.displayErrorMessage(response.getMessage(),
                        JuegoConstantes.POPUP_MESSAGE_CLIENT_ID);
                    prepararStatusOperacion(UIOperationResultEnum.FAILURE);                
                }
            } catch (Exception ex) {
                LOG.error("Error creando Juego. Exception: " + ex.getMessage(), ex);
                this.displayErrorMessageFromBundle("sab.juego.agregar.error",
                        JuegoConstantes.POPUP_MESSAGE_CLIENT_ID);
                prepararStatusOperacion(UIOperationResultEnum.FAILURE);
            }
        } else {
            prepararStatusOperacion(UIOperationResultEnum.FAILURE);
            LOG.info("Fields Validation Failed..");
        }
    }

    @Override
    public boolean validateFields() {
        boolean result = true;

        if ((this.getModel().getCodigo() == null
                || this.getModel().getCodigo().isEmpty())
                || (this.getModel().getNombre() == null
                || this.getModel().getNombre().isEmpty())) {
            this.displayErrorMessageFromBundle("sab.error.campo.vacio", 
                    JuegoConstantes.POPUP_MESSAGE_CLIENT_ID);
            result = false;
        }

        return result;
    }
    
    @Override
    public String getFileName() {
        return "juegocreatepopup";
    }

    @Override
    public String getPopupTitle() {
        return MessageDisplayUtils.INSTANCE.getMessageFromBundle("sab.juego.agregar.lbl");
    }

    @Override
    public Logger getLogger() {
        return LOG;
    }
}