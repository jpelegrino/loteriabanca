package com.sib.juego;

import java.io.Serializable;

/**
 *
 * @author Omar Moronta
 */
public class JuegoConstantes implements Serializable{
    public static final String TABLA_MESSAGE_CLIENT_ID="juegomantmsg";
    public static final String POPUP_MESSAGE_CLIENT_ID = "juegdlg";
}
