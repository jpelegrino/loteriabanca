package com.sib.juego;

import com.sib.ui.enums.UIOperationResultEnum;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "juegoDeletePopup")
@ViewScoped
public class JuegoDeletePopup extends JuegoAbstractPopup implements Serializable {

    public static final Logger LOG
            = Logger.getLogger(JuegoDeletePopup.class.getName());

    public JuegoDeletePopup() {
    }

    @Override
    public void execute(ActionEvent event) {
        try {
            LOG.info("Eliminando Juego: " + this.getModel().getCodigo());
            ResponseModel response = getModelBean().eliminarJuego(this.getModel());

            if (response.getWasSuccessful()) {
                prepararStatusOperacion(UIOperationResultEnum.SUCCESS);
                this.displayInfoMessage(response.getMessage(),
                        JuegoConstantes.TABLA_MESSAGE_CLIENT_ID);
            } else {
                LOG.error("Error eliminando Juego: " + response.getMessage());
                this.displayErrorMessage(response.getMessage(),
                        JuegoConstantes.POPUP_MESSAGE_CLIENT_ID);
                prepararStatusOperacion(UIOperationResultEnum.FAILURE);
            }
        } catch (Exception ex) {
            LOG.error("Error eliminando Juego. Exception: " 
                    + ex.getMessage(), ex);
            this.displayErrorMessageFromBundle("sab.juego.eliminar.error",
                    JuegoConstantes.POPUP_MESSAGE_CLIENT_ID);
            prepararStatusOperacion(UIOperationResultEnum.FAILURE);
        }
    }

    @Override
    public boolean validateFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getFileName() {
        return "juegodeletepopup";
    }

    @Override
    public String getPopupTitle() {
        return MessageDisplayUtils.INSTANCE.getMessageFromBundle("sab.juego.eliminar.lbl");
    }

    @Override
    public Logger getLogger() {
        return LOG;
    }
}
