package com.sib.juego;

import com.sib.interfaces.IModelBean;
import com.sib.interfaces.IPopup;
import com.sib.ui.models.JuegoModel;
import java.io.Serializable;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "juegoBean")
@ViewScoped
public class JuegoBean implements Serializable {

    public static final Logger LOG = Logger.getLogger(JuegoBean.class.getName());

    @ManagedProperty(value = "#{juegoModelBean}")
    private IModelBean modelBean;

    private IPopup popupHandler;

    @ManagedProperty(value = "#{juegoCreatePopup}")
    private IPopup createPopup;
    @ManagedProperty(value = "#{juegoUpdatePopup}")
    private IPopup updatePopup;
    @ManagedProperty(value = "#{juegoDeletePopup}")
    private IPopup deletePopup;

    public JuegoBean() {
    }

    public IPopup getPopupHandler() {
        if (this.popupHandler == null) {
            this.popupHandler = createPopup;
        }
        return popupHandler;
    }

    public void setPopupHandler(IPopup popupHandler) {
        this.popupHandler = popupHandler;
    }

    public IPopup getCreatePopup() {
        return createPopup;
    }

    public void setCreatePopup(IPopup createPopup) {
        this.createPopup = createPopup;
    }

    public IPopup getUpdatePopup() {
        return updatePopup;
    }

    public void setUpdatePopup(IPopup updatePopup) {
        this.updatePopup = updatePopup;
    }

    public IPopup getDeletePopup() {
        return deletePopup;
    }

    public void setDeletePopup(IPopup deletePopup) {
        this.deletePopup = deletePopup;
    }

    public IModelBean getModelBean() {
        return modelBean;
    }

    public void setModelBean(IModelBean modelBean) {
        this.modelBean = modelBean;
    }
    
    public void openCreatePopup(ActionEvent event) {
        ((JuegoCreatePopup)createPopup).setModel(new JuegoModel());
        this.setPopupHandler(this.createPopup);
    }

    public void openUpdatePopup(ActionEvent event) {
        
        JuegoModel selectedJuego = getSelectedJuegoModelFromRequest();
        
        ((JuegoUpdatePopup)updatePopup).setUntouchedModel(selectedJuego);
        
        JuegoUtil juegoUtil = new JuegoUtil();
        JuegoModel selectedModel
            = juegoUtil.crearJuegoModelDeJuegoModel(selectedJuego);

        ((JuegoUpdatePopup)updatePopup).setModel(selectedModel);
        
        this.setPopupHandler(this.updatePopup);
    }

    public void openDeletePopup(ActionEvent event) {
        JuegoModel selectedJuego = getSelectedJuegoModelFromRequest();
        ((JuegoDeletePopup)deletePopup).setModel(selectedJuego);
        this.setPopupHandler(this.deletePopup);
    }    

    private JuegoModel getSelectedJuegoModelFromRequest(){
        JuegoModel selectedJuego = null;
        
        Map<String,String> params = 
                FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        
        String juegoId = params.get("juegoId");
        int id = Integer.parseInt(juegoId);
        
        JuegoModelBean jmb = (JuegoModelBean)this.getModelBean();
        
        for(JuegoModel jm : jmb.getJuegoList()){
            if(jm.getId()==id){
                selectedJuego=jm;
            }
        }
    
        return selectedJuego;
    }
}
