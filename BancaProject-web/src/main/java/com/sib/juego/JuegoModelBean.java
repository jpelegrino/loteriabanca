package com.sib.juego;

import com.sib.bs.responses.JuegoResponse;
import com.sib.bs.responses.MessageEnum;
import com.sib.ejb.dao.JuegoDao;
import com.sib.ejb.dao.LoteriaJuegoDao;
import com.sib.entity.Juego;
import com.sib.interfaces.IModelBean;
import com.sib.ui.enums.StatusEnum;
import com.sib.ui.models.JuegoModel;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "juegoModelBean")
@ViewScoped
public class JuegoModelBean implements IModelBean, Serializable {

    public static final Logger LOG
            = Logger.getLogger(JuegoModelBean.class.getName());

    @EJB
    private JuegoDao juegoDao;
    
    @EJB
    private LoteriaJuegoDao loteriaJuegoDao;
  
    private List<JuegoModel> result = null;

    public JuegoModelBean() {
    }

    private void init() {
        JuegoResponse out = juegoDao.getJuegos(null);

        result = new ArrayList<JuegoModel>();

        if (out != null) {
            if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                List<Juego> juegoOrig = out.getJuegos();

                if (juegoOrig != null) {
                    JuegoUtil juegoUtil = new JuegoUtil();
                    for (Juego j : juegoOrig) {
                        JuegoModel cur = juegoUtil.convertirJuegoAJuegoModel(j);
                        result.add(cur);
                    }
                }
            } else {
                this.displayErrorMessageFromBundle("sab.juego.error.lbl",
                        JuegoConstantes.TABLA_MESSAGE_CLIENT_ID);
            }
        }

    }

    public List<JuegoModel> getJuegoList() {
        if (result == null) {
            init();
        }
        return result;
    }

    public ResponseModel crearJuego(JuegoModel juegoModel) {
        ResponseModel resp = new ResponseModel();

        try {
            JuegoUtil juegoUtil = new JuegoUtil();
            Juego juego = juegoUtil.convertirJuegoModelAJuego(juegoModel);
            LOG.info("Inicio crear Juego..");
            JuegoResponse out = this.juegoDao.crearJuego(juego);
            resp = processResponse(out, "sab.juego.agregar.success",
                    "sab.juego.agregar.error", "sab.juego.agregar.val.error");
            LOG.info("Fin crear Juego..");
        } catch (Exception ex) {
            resp.setWasSuccessful(false);
            resp.setMessage(this.getMessageFromBundle("sab.juego.agregar.error"));            
            LOG.error("Error invocando a JuegoDao para crear juego."
                    + ex.getMessage(), ex);
        }

        return resp;
    }

    public ResponseModel eliminarJuego(JuegoModel juegoModel) {

        ResponseModel resp = new ResponseModel();

        try {
            JuegoUtil juegoUtil = new JuegoUtil();
            Juego juego = juegoUtil.convertirJuegoModelAJuego(juegoModel);
            LOG.info("Inicio eliminar Juego..");
            JuegoResponse out = this.juegoDao.deleteJuego(juego);
            resp = processResponse(out, "sab.juego.eliminar.success",
                    "sab.juego.eliminar.error", "sab.juego.eliminar.val.error");
            LOG.info("Fin eliminar Juego..");
        } catch (Exception ex) {
            resp.setWasSuccessful(false);
            resp.setMessage(this.getMessageFromBundle("sab.juego.eliminar.error"));            
            LOG.error("Error invocando a JuegoDao para eliminar juego."
                    + ex.getMessage(), ex);
        }

        return resp;
    }

    public ResponseModel actualizarJuego(JuegoModel juegoModel) {

        ResponseModel resp = new ResponseModel();

        try {
            JuegoUtil juegoUtil = new JuegoUtil();
            Juego juego = juegoUtil.convertirJuegoModelAJuego(juegoModel);
            LOG.info("Inicio actualizar Juego..");
            LOG.info("Actualizando juego con Id: " + juego.getId());
            JuegoResponse out = this.juegoDao.editarJuego(juego);
            resp = processResponse(out, "sab.juego.actualizar.success",
                    "sab.juego.actualizar.error", "sab.juego.actualizar.val.error");
            LOG.info("Fin actualizar Juego..");
        } catch (Exception ex) {
            resp.setWasSuccessful(false);
            resp.setMessage(this.getMessageFromBundle("sab.juego.actualizar.error"));          
            LOG.error("Error invocando a JuegoDao para actualizar juego."
                    + ex.getMessage(), ex);
        }

        return resp;
    }

    public List<JuegoModel> getJuegosNotRelatedToLoteria(int loteriaId){
        List<JuegoModel> results = null;
        
        JuegoResponse out = juegoDao.getJuegosNOtRelatedInLJByLoteria(loteriaId);

        if (out != null) {
            if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                results = new ArrayList<JuegoModel>();
                List<Juego> juegosOrig = out.getJuegos();

                if (juegosOrig != null) {
                    JuegoUtil gUtil = new JuegoUtil();
                    for (Juego g : juegosOrig) {
                        JuegoModel cur = gUtil.convertirJuegoAJuegoModel(g);
                        results.add(cur);
                    }
                }
            } else {
                LOG.error("Ha ocurrido un error obteniendo Juegos no relacionados"
                        + "a Lotería por codigo de Lotería. "
                        + "Message Enum equals:>"+out.getMsgEnum());
            }
        }
        
        return results;
    }
    
    public List<JuegoModel> getJuegosByLoteriaId(int loteriaId){
        List<JuegoModel> results = null;
        
        JuegoResponse out = loteriaJuegoDao.getJuegosByLoteriaId(loteriaId, 
                StatusEnum.ACTIVO.getStatusLiteral());

        if (out != null) {
            if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                results = new ArrayList<JuegoModel>();
                List<Juego> juegosOrig = out.getJuegos();

                if (juegosOrig != null) {
                    JuegoUtil gUtil = new JuegoUtil();
                    for (Juego g : juegosOrig) {
                        JuegoModel cur = gUtil.convertirJuegoAJuegoModel(g);
                        results.add(cur);
                    }
                }
            } else {
                LOG.error("Ha ocurrido un error obteniendo Juegos por loteria"
                        + "Message Enum equals:>"+out.getMsgEnum());
            }
        }
        
        return results;
    }
    
    /**
     * Este método retornará la lista de juegos para una lotería X que no esté
     * relacionada a una banca y en Banca-Loteria-Juego.
     * 
     * @param loteriaId
     * @return 
     */
    public List<JuegoModel> getJuegosFromLoteriaNotInBLJ(int loteriaId, int bancaId){
        List<JuegoModel> results = null;
        
        JuegoResponse out = juegoDao.getJuegosByLoteriaNotRInBLJ(loteriaId,bancaId);

        if (out != null) {
            if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                results = new ArrayList<JuegoModel>();
                List<Juego> juegosOrig = out.getJuegos();

                if (juegosOrig != null) {
                    JuegoUtil gUtil = new JuegoUtil();
                    for (Juego g : juegosOrig) {
                        JuegoModel cur = gUtil.convertirJuegoAJuegoModel(g);
                        results.add(cur);
                    }
                }
            } else {
                LOG.error("Ha ocurrido un error obteniendo Juegos no relacionados"
                        + "a Lotería en Banca-Loteria-Juego. "
                        + "Message Enum equals:>"+out.getMsgEnum());
            }
        }
        
        return results;
    }    
    
    @Override
    public void resetModelBean() {
        result = null;
    }

    protected void displayErrorMessageFromBundle(String msgproperty, String msgClientId) {
        MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(msgClientId,
                msgproperty);
    }

    protected String getMessageFromBundle(String msgproperty, Object... params) {
        return MessageDisplayUtils.INSTANCE.
                getMessageFromBundle(msgproperty, params);
    }

    private ResponseModel processResponse(JuegoResponse response,
            String succesMessage, String failureMessage, String validationFailMessage) {

        ResponseModel resp = new ResponseModel();

        if (response != null) {
            /**
             * In this switch the FAIL enum will be treated as default for
             * displaying general message.
             */
            switch (response.getMsgEnum()) {

                case SUCCESS:
                    resp.setWasSuccessful(true);
                    resp.setMessage(
                            getMessageFromBundle(succesMessage));
                    resetModelBean();
                    break;

                case VALIDATION:
                    resp.setWasSuccessful(false);
                    resp.setMessage(getMessageFromBundle(validationFailMessage));
                    break;

                default:
                    resp.setWasSuccessful(false);
                    resp.setMessage(
                            getMessageFromBundle(failureMessage));
                    break;
            }
        } else {
            resp.setWasSuccessful(false);
            resp.setMessage(
                    getMessageFromBundle(failureMessage));

        }

        return resp;
    }
}
