package com.sib.loteria;

import com.sib.juego.JuegoModelBean;
import com.sib.ui.models.JuegoModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "loteriaJuegoViewPopup")
@ViewScoped
public class LoteriaJuegoViewPopup extends LoteriaAbstractPopup 
    implements Serializable {
    
    public static final Logger LOG = Logger.getLogger(LoteriaJuegoViewPopup.class.getName());

    @ManagedProperty(value = "#{juegoModelBean}")
    private JuegoModelBean juegoModelBean;
    
    private List<JuegoModel> juegosDeLoteria;
    
    public LoteriaJuegoViewPopup() {
    }

    public JuegoModelBean getJuegoModelBean() {
        return juegoModelBean;
    }

    public void setJuegoModelBean(JuegoModelBean juegoModelBean) {
        this.juegoModelBean = juegoModelBean;
    }
    
    public List<JuegoModel> getJuegosDeLoteria() {
        if(this.juegosDeLoteria==null){
            juegosDeLoteria
                    =this.juegoModelBean.getJuegosByLoteriaId(getModel().getId());
        }
        
        return juegosDeLoteria;
    }

    public void setJuegosDeLoteria(List<JuegoModel> juegosDeLoteria) {
        this.juegosDeLoteria=juegosDeLoteria;
    }   

    @Override
    public String getFileName() {
        return "loteriajuego/loteriajuegoviewpopup";
    }

    @Override
    public String getPopupTitle() {
        return MessageDisplayUtils.INSTANCE.getMessageFromBundle("sab.loteria.juego.ver.lbl");
    }

    @Override
    public Logger getLogger() {
        return LOG;
    }

    @Override
    public void execute(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean validateFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
