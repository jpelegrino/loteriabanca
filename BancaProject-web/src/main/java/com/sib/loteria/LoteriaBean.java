package com.sib.loteria;

import com.sib.interfaces.IModelBean;
import com.sib.interfaces.IPopup;
import com.sib.ui.models.LoteriaModel;
import java.io.Serializable;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "loteriaBean")
@ViewScoped
public class LoteriaBean implements Serializable {

    public static final Logger LOG = Logger.getLogger(LoteriaBean.class.getName());

    @ManagedProperty(value = "#{loteriaModelBean}")
    private IModelBean modelBean;

    private IPopup popupHandler;

    @ManagedProperty(value = "#{loteriaCreatePopup}")
    private IPopup createPopup;
    @ManagedProperty(value = "#{loteriaUpdatePopup}")
    private IPopup updatePopup;
    @ManagedProperty(value = "#{loteriaDeletePopup}")
    private IPopup deletePopup;
    
    @ManagedProperty(value = "#{loteriaJuegoCreatePopup}")
    private IPopup loteriaAddJuegoPopup;
    @ManagedProperty(value = "#{loteriaJuegoDeletePopup}")
    private IPopup loteriaRemoveJuegoPopup;
    @ManagedProperty(value = "#{loteriaJuegoViewPopup}")
    private IPopup loteriaViewJuegoPopup;
    
    public LoteriaBean() {
    }

    public IPopup getPopupHandler() {
        if (this.popupHandler == null) {
            this.popupHandler = createPopup;
        }
        return popupHandler;
    }

    public void setPopupHandler(IPopup popupHandler) {
        this.popupHandler = popupHandler;
    }

    public IPopup getCreatePopup() {
        return createPopup;
    }

    public void setCreatePopup(IPopup createPopup) {
        this.createPopup = createPopup;
    }

    public IPopup getUpdatePopup() {
        return updatePopup;
    }

    public void setUpdatePopup(IPopup updatePopup) {
        this.updatePopup = updatePopup;
    }

    public IPopup getDeletePopup() {
        return deletePopup;
    }

    public void setDeletePopup(IPopup deletePopup) {
        this.deletePopup = deletePopup;
    }

    public IPopup getLoteriaAddJuegoPopup() {
        return loteriaAddJuegoPopup;
    }

    public void setLoteriaAddJuegoPopup(IPopup loteriaAddJuegoPopup) {
        this.loteriaAddJuegoPopup = loteriaAddJuegoPopup;
    }

    public IPopup getLoteriaRemoveJuegoPopup() {
        return loteriaRemoveJuegoPopup;
    }

    public void setLoteriaRemoveJuegoPopup(IPopup loteriaRemoveJuegoPopup) {
        this.loteriaRemoveJuegoPopup = loteriaRemoveJuegoPopup;
    }

    public IPopup getLoteriaViewJuegoPopup() {
        return loteriaViewJuegoPopup;
    }

    public void setLoteriaViewJuegoPopup(IPopup loteriaViewJuegoPopup) {
        this.loteriaViewJuegoPopup = loteriaViewJuegoPopup;
    }

    public IModelBean getModelBean() {
        return modelBean;
    }

    public void setModelBean(IModelBean modelBean) {
        this.modelBean = modelBean;
    }

    public void openCreatePopup(ActionEvent event) {
        ((LoteriaCreatePopup) createPopup).setModel(new LoteriaModel());
        this.setPopupHandler(this.createPopup);
    }

    public void openUpdatePopup(ActionEvent event) {

        LoteriaModel selected = getSelectedLoteriaModelFromRequest();

        ((LoteriaUpdatePopup) updatePopup).setUntouchedModel(selected);

         LoteriaUtil loteUtil = new LoteriaUtil();
            LoteriaModel selectedModel
            = loteUtil.crearLoteriaModelDeLoteriaModel(selected);

        ((LoteriaUpdatePopup) updatePopup).setModel(selectedModel);

        this.setPopupHandler(this.updatePopup);
    }

    public void openDeletePopup(ActionEvent event) {
        LoteriaModel selected = getSelectedLoteriaModelFromRequest();
        ((LoteriaDeletePopup) deletePopup).setModel(selected);
        this.setPopupHandler(this.deletePopup);
    }

    public void openAddJuegoALoteriaPopup(ActionEvent event) {
        LoteriaModel selected = getSelectedLoteriaModelFromRequest();
        LoteriaJuegoCreatePopup ljCreatePopup 
                =((LoteriaJuegoCreatePopup) loteriaAddJuegoPopup);
        ljCreatePopup.setModel(selected);
        ljCreatePopup.setJuegosNoRelacionadosList(null);
        this.setPopupHandler(this.loteriaAddJuegoPopup);
    }    

    public void openRemoveJuegoDeLoteriaPopup(ActionEvent event) {
        LoteriaModel selected = getSelectedLoteriaModelFromRequest();
        LoteriaJuegoDeletePopup ljDeletePopup 
                =((LoteriaJuegoDeletePopup) loteriaRemoveJuegoPopup);
        ljDeletePopup.setModel(selected);
        ljDeletePopup.setJuegosDeLoteria(null);
        this.setPopupHandler(this.loteriaRemoveJuegoPopup);
    }
    
    public void openViewJuegosLoteriaPopup(ActionEvent event) {
        LoteriaModel selected = getSelectedLoteriaModelFromRequest();
        LoteriaJuegoViewPopup ljViewPopup 
                =((LoteriaJuegoViewPopup) loteriaViewJuegoPopup);
        ljViewPopup.setModel(selected);
        ljViewPopup.setJuegosDeLoteria(null);
        this.setPopupHandler(this.loteriaViewJuegoPopup);
    }
    
    private LoteriaModel getSelectedLoteriaModelFromRequest() {
        LoteriaModel selected = null;

        Map<String, String> params
                = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

        String loteriaId = params.get("loteriaId");
        int id = Integer.parseInt(loteriaId);

        LoteriaModelBean jmb = (LoteriaModelBean) this.getModelBean();

        for (LoteriaModel jm : jmb.getResult()) {
            if (jm.getId() == id) {
                selected = jm;
            }
        }

        return selected;
    }
}
