package com.sib.loteria;

import java.io.Serializable;

/**
 *
 * @author Omar Moronta
 */
public class LoteriaConstantes implements Serializable{
    public static final String TABLA_MESSAGE_CLIENT_ID="loteriamantmsg";
    public static final String POPUP_MESSAGE_CLIENT_ID = "loterdlg";
}
