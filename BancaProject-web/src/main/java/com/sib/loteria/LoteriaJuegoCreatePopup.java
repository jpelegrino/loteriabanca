package com.sib.loteria;

import com.sib.juego.JuegoModelBean;
import com.sib.ui.models.JuegoModel;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "loteriaJuegoCreatePopup")
@ViewScoped
public class LoteriaJuegoCreatePopup extends LoteriaAbstractPopup 
    implements Serializable {
    
    public static final Logger LOG = Logger.getLogger(LoteriaJuegoCreatePopup.class.getName());

    @ManagedProperty(value = "#{juegoModelBean}")
    private JuegoModelBean juegoModelBean;
    
    private List<JuegoModel> juegosNoRelacionadosList;
    
    public LoteriaJuegoCreatePopup() {
    }

    public JuegoModelBean getJuegoModelBean() {
        return juegoModelBean;
    }

    public void setJuegoModelBean(JuegoModelBean juegoModelBean) {
        this.juegoModelBean = juegoModelBean;
    }

    public List<JuegoModel> getJuegosNoRelacionadosList() {
        if(this.juegosNoRelacionadosList==null){
            juegosNoRelacionadosList
                    =this.juegoModelBean.getJuegosNotRelatedToLoteria(getModel().getId());
        }
        
        return juegosNoRelacionadosList;
    }

    public void setJuegosNoRelacionadosList(List<JuegoModel> juegosNoRelacionadosList) {
        this.juegosNoRelacionadosList = juegosNoRelacionadosList;
    }    
    
    @Override
    public void execute(ActionEvent event) {
        try {
            int juegoId = this.getSelectedJuegoModelFromRequest().getId();
            int loteriaId = this.getModel().getId();
            ResponseModel response
                    = this.getModelBean().agregarJuegoALoteria(juegoId, loteriaId);

            if (response.getWasSuccessful()) {
                this.displayInfoMessage(response.getMessage(),
                        LoteriaConstantes.POPUP_MESSAGE_CLIENT_ID);
                this.juegosNoRelacionadosList = null;
            } else {
                LOG.error("Error creando Loteria: " + response.getMessage());
                this.displayErrorMessage(response.getMessage(),
                        LoteriaConstantes.POPUP_MESSAGE_CLIENT_ID);
            }
        } catch (Exception ex) {
            LOG.error("Error creando Loteria. Exception: " + ex.getMessage(), ex);
            this.displayErrorMessageFromBundle("sab.loteria.juego.agregar.error",
                    LoteriaConstantes.POPUP_MESSAGE_CLIENT_ID);
        }
    }

    private JuegoModel getSelectedJuegoModelFromRequest(){
        JuegoModel selectedJuego = null;
        
        Map<String,String> params = 
                FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        
        String juegoId = params.get("juegoId");
        int id = Integer.parseInt(juegoId);
        
        for(JuegoModel jm : juegosNoRelacionadosList){
            if(jm.getId()==id){
                selectedJuego=jm;
            }
        }
    
        return selectedJuego;
    }    
    
    @Override
    public String getFileName() {
        return "loteriajuego/loteriajuegocreatepopup";
    }

    @Override
    public String getPopupTitle() {
        return MessageDisplayUtils.INSTANCE.getMessageFromBundle("sab.loteria.juego.agregar.lbl");
    }

    @Override
    public Logger getLogger() {
        return LOG;
    }

    @Override
    public boolean validateFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
