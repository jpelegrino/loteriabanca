package com.sib.loteria;

import com.sib.bs.responses.LoteriaJuegoResponse;
import com.sib.bs.responses.LoteriaResponse;
import com.sib.bs.responses.MessageEnum;
import com.sib.ejb.dao.BancaLoteriaDao;
import com.sib.ejb.dao.LoteriaDao;
import com.sib.ejb.dao.LoteriaJuegoDao;
import com.sib.entity.Loteria;
import com.sib.entity.LoteriaJuego;
import com.sib.interfaces.IModelBean;
import com.sib.ui.models.LoteriaModel;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "loteriaModelBean")
@ViewScoped
public class LoteriaModelBean implements IModelBean, Serializable {

    public static final Logger LOG
            = Logger.getLogger(LoteriaModelBean.class.getName());

    @EJB
    private LoteriaDao loteriaDao;

    @EJB
    private BancaLoteriaDao bancaLoteriaDao;

    @EJB
    private LoteriaJuegoDao loteriaJuegoDao;

    private List<LoteriaModel> result = null;

    public LoteriaModelBean() {
    }

    private void init() {
        LoteriaResponse out = loteriaDao.getLoterias(null);

        result = new ArrayList<LoteriaModel>();

        if (out != null) {
            if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                List<Loteria> loteriaOrig = out.getLoterias();

                if (loteriaOrig != null) {
                    LoteriaUtil loteriaUtil = new LoteriaUtil();
                    for (Loteria j : loteriaOrig) {
                        LoteriaModel cur = loteriaUtil.convertirLoteriaALoteriaModel(j);
                        result.add(cur);
                    }
                }
            } else {
                this.displayErrorMessageFromBundle("sab.loteria.error.lbl",
                        LoteriaConstantes.TABLA_MESSAGE_CLIENT_ID);
            }
        }

    }

    public List<LoteriaModel> getResult() {
        if (result == null) {
            init();
        }
        return result;
    }

    public ResponseModel crearLoteria(LoteriaModel loteriaModel) {
        ResponseModel resp = new ResponseModel();

        try {
            LoteriaUtil loteriaUtil = new LoteriaUtil();
            Loteria loteria = loteriaUtil.convertirLoteriaModelALoteria(loteriaModel);
            LOG.info("Inicio crear Loteria..");
            LoteriaResponse out = this.loteriaDao.crearLoteria(loteria);
            resp = processResponse(out, "sab.loteria.agregar.success",
                    "sab.loteria.agregar.error", "sab.loteria.agregar.val.error");
            LOG.info("Fin crear Loteria..");
        } catch (Exception ex) {
            resp.setWasSuccessful(false);
            resp.setMessage(this.getMessageFromBundle("sab.loteria.agregar.error"));
            LOG.error("Error invocando a LoteriaDao para crear loteria."
                    + ex.getMessage(), ex);
        }

        return resp;
    }

    public ResponseModel eliminarLoteria(LoteriaModel loteriaModel) {

        ResponseModel resp = new ResponseModel();

        try {
            LoteriaUtil loteriaUtil = new LoteriaUtil();
            Loteria loteria = loteriaUtil.convertirLoteriaModelALoteria(loteriaModel);
            LOG.info("Inicio eliminar Loteria..");
            LoteriaResponse out = this.loteriaDao.deleteLoteria(loteria);
            resp = processResponse(out, "sab.loteria.eliminar.success",
                    "sab.loteria.eliminar.error", "sab.loteria.eliminar.val.error");
            LOG.info("Fin eliminar Loteria..");
        } catch (Exception ex) {
            resp.setWasSuccessful(false);
            resp.setMessage(this.getMessageFromBundle("sab.loteria.eliminar.error"));
            LOG.error("Error invocando a LoteriaDao para eliminar loteria."
                    + ex.getMessage(), ex);
        }

        return resp;
    }

    public ResponseModel actualizarLoteria(LoteriaModel loteriaModel) {

        ResponseModel resp = new ResponseModel();

        try {
            LoteriaUtil loteriaUtil = new LoteriaUtil();
            Loteria loteria = loteriaUtil.convertirLoteriaModelALoteria(loteriaModel);
            LOG.info("Inicio actualizar Loteria..");
            LOG.info("Actualizando loteria con Id: " + loteria.getId());
            LoteriaResponse out = this.loteriaDao.editarLoteria(loteria);
            resp = processResponse(out, "sab.loteria.actualizar.success",
                    "sab.loteria.actualizar.error", "sab.loteria.actualizar.val.error");
            LOG.info("Fin actualizar Loteria..");
        } catch (Exception ex) {
            resp.setWasSuccessful(false);
            resp.setMessage(this.getMessageFromBundle("sab.loteria.actualizar.error"));
            LOG.error("Error invocando a LoteriaDao para actualizar loteria."
                    + ex.getMessage(), ex);
        }

        return resp;
    }

    public ResponseModel agregarJuegoALoteria(int juegoId, int loteriaId) {
        ResponseModel resp = new ResponseModel();

        try {
            LOG.info("Inicio agregar juego a loteria con loteriaId: " + loteriaId
                    + " y juegoId: " + juegoId);
            LoteriaJuegoResponse out = this.loteriaJuegoDao.addJuegoToLoteria(juegoId, loteriaId);

            if (out != null) {
                if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                    resp.setWasSuccessful(true);
                    resp.setMessage(this.getMessageFromBundle("sab.loteria.juego.agregar.success"));
                } else if (out.getMsgEnum().equals(MessageEnum.VALIDATION)) {
                    resp.setWasSuccessful(false);
                    resp.setMessage(this.getMessageFromBundle("sab.loteria.juego.agregar.val.error"));
                } else {
                    resp.setWasSuccessful(false);
                    resp.setMessage(this.getMessageFromBundle("sab.loteria.juego.agregar.error"));
                    LOG.error("Ha ocurrido un error agregando Juegos a Lotería. "
                            + "Message Enum equals:>" + out.getMsgEnum());
                }
            } else {
                resp.setWasSuccessful(false);
                resp.setMessage(this.getMessageFromBundle("sab.loteria.juego.agregar.error"));
                LOG.error("Ha ocurrido un error agregando Juegos a Lotería. Out is null.");
            }

            LOG.info("Fin agregar juego a loteria");
        } catch (Exception ex) {
            resp.setWasSuccessful(false);
            resp.setMessage(this.getMessageFromBundle("sab.loteria.juego.agregar.error"));
            LOG.error("Error invocando a LoteriaJuegoDao para crear loteria-juego."
                    + ex.getMessage(), ex);
        }

        return resp;
    }

    public ResponseModel removerJuegoDeLoteria(int juegoId, int loteriaId) {
        ResponseModel resp = new ResponseModel();

        try {
            LOG.info("Inicio remover juego de loteria con loteriaId: " + loteriaId
                    + " y juegoId: " + juegoId);

            LoteriaJuegoResponse ljr
                    = this.loteriaJuegoDao.getLoteriaJuego(juegoId, loteriaId);
            
            
            if (ljr != null && ljr.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                LoteriaJuego lj=ljr.getLoteriaJuego();
                LoteriaJuegoResponse out
                        = this.loteriaJuegoDao.deleteLoteriaJuego(lj);

                if (out != null) {

                    if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                        resp.setWasSuccessful(true);
                        resp.setMessage(this.getMessageFromBundle("sab.loteria.juego.eliminar.success"));
                    } else if (out.getMsgEnum().equals(MessageEnum.VALIDATION)) {
                        resp.setWasSuccessful(false);
                        resp.setMessage(this.getMessageFromBundle("sab.loteria.juego.eliminar.val.error"));
                    } else {
                        resp.setWasSuccessful(false);
                        resp.setMessage(this.getMessageFromBundle("sab.loteria.juego.eliminar.error"));
                        LOG.error("Ha ocurrido un error eliminando Juegos de Lotería. "
                                + "Message Enum equals:>" + out.getMsgEnum());
                    }
                } else {
                    resp.setWasSuccessful(false);
                    resp.setMessage(this.getMessageFromBundle("sab.loteria.juego.eliminar.error"));
                    LOG.error("Ha ocurrido un error eliminando Juegos de Lotería. Out is null.");
                }
            }

            LOG.info("Fin eliminar juego de loteria");
        } catch (Exception ex) {
            resp.setWasSuccessful(false);
            resp.setMessage(this.getMessageFromBundle("sab.loteria.juego.eliminar.error"));
            LOG.error("Error invocando a LoteriaJuegoDao para eliminar loteria-juego."
                    + ex.getMessage(), ex);
        }

        return resp;
    }

    public List<LoteriaModel> getLoteriasByBancaId(int bancaId) {
        LoteriaResponse out = bancaLoteriaDao.getLoteriasByBanca(bancaId);

        List<LoteriaModel> loteriasBanca = new ArrayList<LoteriaModel>();

        if (out != null) {
            if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                List<Loteria> loteriaOrig = out.getLoterias();

                if (loteriaOrig != null) {
                    LoteriaUtil loteriaUtil = new LoteriaUtil();
                    for (Loteria j : loteriaOrig) {
                        LoteriaModel cur = loteriaUtil.convertirLoteriaALoteriaModel(j);
                        loteriasBanca.add(cur);
                    }
                }
            } else {
                this.displayErrorMessageFromBundle("sab.loteria.error.lbl",
                        LoteriaConstantes.TABLA_MESSAGE_CLIENT_ID);
            }
        }

        return loteriasBanca;
    }

    public List<LoteriaModel> getLoteriasNotInBanca(int bancaId) {
        LoteriaResponse out = bancaLoteriaDao.getLoteriasNotInBanca(bancaId);

        List<LoteriaModel> loteriasBanca = new ArrayList<LoteriaModel>();

        if (out != null) {
            if (out.getMsgEnum().equals(MessageEnum.SUCCESS)) {
                List<Loteria> loteriaOrig = out.getLoterias();

                if (loteriaOrig != null) {
                    LoteriaUtil loteriaUtil = new LoteriaUtil();
                    for (Loteria j : loteriaOrig) {
                        LoteriaModel cur = loteriaUtil.convertirLoteriaALoteriaModel(j);
                        loteriasBanca.add(cur);
                    }
                }
            } else {
                this.displayErrorMessageFromBundle("sab.loteria.error.lbl",
                        LoteriaConstantes.TABLA_MESSAGE_CLIENT_ID);
            }
        }

        return loteriasBanca;
    }

    @Override
    public void resetModelBean() {
        result = null;
    }

    protected void displayErrorMessageFromBundle(String msgproperty, String msgClientId) {
        MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(msgClientId,
                msgproperty);
    }

    protected String getMessageFromBundle(String msgproperty, Object... params) {
        return MessageDisplayUtils.INSTANCE.
                getMessageFromBundle(msgproperty, params);
    }

    private ResponseModel processResponse(LoteriaResponse response,
            String succesMessage, String failureMessage, String validationFailMessage) {

        ResponseModel resp = new ResponseModel();

        if (response != null) {
            /**
             * In this switch the FAIL enum will be treated as default for
             * displaying general message.
             */
            switch (response.getMsgEnum()) {

                case SUCCESS:
                    resp.setWasSuccessful(true);
                    resp.setMessage(
                            getMessageFromBundle(succesMessage));
                    resetModelBean();
                    break;

                case VALIDATION:
                    resp.setWasSuccessful(false);
                    resp.setMessage(getMessageFromBundle(validationFailMessage));
                    break;

                default:
                    resp.setWasSuccessful(false);
                    resp.setMessage(
                            getMessageFromBundle(failureMessage));
                    break;
            }
        } else {
            resp.setWasSuccessful(false);
            resp.setMessage(
                    getMessageFromBundle(failureMessage));

        }

        return resp;
    }
}
