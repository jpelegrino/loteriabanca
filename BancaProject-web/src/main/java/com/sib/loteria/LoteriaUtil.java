package com.sib.loteria;

import com.sib.entity.Loteria;
import com.sib.ui.models.LoteriaModel;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;

/**
 * This is the Loteria <-> LoteriaModel objects converter helper class.
 *
 * @author Omar Moronta
 */
public class LoteriaUtil implements Serializable {
    
    public static final Logger LOG = Logger.getLogger(LoteriaUtil.class.getName());   

    public LoteriaUtil() {

    }

    public LoteriaModel crearLoteriaModelDeLoteriaModel(LoteriaModel orig) {
        LoteriaModel dest = new LoteriaModel();

        if (orig != null) {
            byte[] bytesArr = SerializationUtils.serialize(orig);
            dest = (LoteriaModel)SerializationUtils.deserialize(bytesArr);
        }

        return dest;
    }

    public LoteriaModel convertirLoteriaALoteriaModel(Loteria orig) {
        LoteriaModel dest = null;

        if (orig != null) {
            byte[] bytesArr = SerializationUtils.serialize(orig);
            Loteria norig = (Loteria)SerializationUtils.deserialize(bytesArr);
            
            if(norig!=null){
                try {
                    dest = new LoteriaModel();
                    BeanUtils.copyProperties(dest, norig);
                } catch (IllegalAccessException ex) {
                    LOG.error(ex.getMessage(), ex);
                } catch (InvocationTargetException ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }

        return dest;
    }

    public Loteria convertirLoteriaModelALoteria(LoteriaModel orig) {
        Loteria dest = null;
        
        if(orig!=null){
            byte[] bytesArr = SerializationUtils.serialize(orig);
            LoteriaModel norig = (LoteriaModel)SerializationUtils.deserialize(bytesArr);
            
            if(norig!=null){
                try {
                    dest = new Loteria();
                    BeanUtils.copyProperties(dest, norig);
                } catch (IllegalAccessException ex) {
                    LOG.error(ex.getMessage(), ex);
                } catch (InvocationTargetException ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }
        
        return dest;
    }
}
