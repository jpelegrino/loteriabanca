package com.sib.loteria;

import com.sib.ui.enums.StatusEnum;
import com.sib.ui.enums.UIOperationResultEnum;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "loteriaCreatePopup")
@ViewScoped
public class LoteriaCreatePopup extends LoteriaAbstractPopup 
    implements Serializable {

    public static final Logger LOG = Logger.getLogger(LoteriaCreatePopup.class.getName());

    public LoteriaCreatePopup() {
    }

    @Override
    public void execute(ActionEvent event) {
        if (validateFields()) {
            try {
                LOG.info("Creando loteria con CODIGO: " + this.getModel().getCodigo());
                this.getModel().setStatus(StatusEnum.ACTIVO.getStatusLiteral());                
                ResponseModel response = getModelBean().crearLoteria(this.getModel());
                
                if(response.getWasSuccessful()){
                    prepararStatusOperacion(UIOperationResultEnum.SUCCESS);
                    this.displayInfoMessage(response.getMessage(),
                        LoteriaConstantes.TABLA_MESSAGE_CLIENT_ID);                
                }else{
                    LOG.error("Error creando Loteria: " + response.getMessage());
                    this.displayErrorMessage(response.getMessage(),
                        LoteriaConstantes.POPUP_MESSAGE_CLIENT_ID);
                    prepararStatusOperacion(UIOperationResultEnum.FAILURE);                
                }
            } catch (Exception ex) {
                LOG.error("Error creando Loteria. Exception: " + ex.getMessage(), ex);
                this.displayErrorMessageFromBundle("sab.loteria.agregar.error",
                        LoteriaConstantes.POPUP_MESSAGE_CLIENT_ID);
                prepararStatusOperacion(UIOperationResultEnum.FAILURE);
            }
        } else {
            prepararStatusOperacion(UIOperationResultEnum.FAILURE);
            LOG.info("Fields Validation Failed..");
        }
    }

    @Override
    public boolean validateFields() {
        boolean result = true;

        if ((this.getModel().getCodigo() == null
                || this.getModel().getCodigo().isEmpty())) {
            MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(
                    LoteriaConstantes.POPUP_MESSAGE_CLIENT_ID,
                    "sab.error.campo.vacio");
            result = false;
        }else if(!this.getModel().getCodigo().matches("[0-9]{2}")) {
            MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(
                    LoteriaConstantes.POPUP_MESSAGE_CLIENT_ID,
                    "sab.loteria.agregar.error.match");
            result = false;            
        }

        return result;
    }

    @Override
    public String getFileName() {
        return "loteriacreatepopup";
    }

    @Override
    public String getPopupTitle() {
        return MessageDisplayUtils.INSTANCE.getMessageFromBundle("sab.loteria.agregar.lbl");
    }

    @Override
    public Logger getLogger() {
        return LOG;
    }
}
