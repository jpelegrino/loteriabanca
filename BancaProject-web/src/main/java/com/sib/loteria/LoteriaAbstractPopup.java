package com.sib.loteria;

import com.sib.loteria.*;
import com.sib.interfaces.IPopup;
import com.sib.ui.enums.UIOperationResultEnum;
import com.sib.ui.models.LoteriaModel;
import com.sib.util.MessageDisplayUtils;
import javax.faces.bean.ManagedProperty;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Omar Moronta
 */
public abstract class LoteriaAbstractPopup implements IPopup{
    
    private LoteriaModel model;
    
    @ManagedProperty(value = "#{loteriaModelBean}")
    private LoteriaModelBean modelBean;
    
    public LoteriaModel getModel() {
        return model;
    }

    public void setModel(LoteriaModel model) {
        this.model = model;
    }
    
    public LoteriaModelBean getModelBean() {
        return modelBean;
    }

    public void setModelBean(LoteriaModelBean modelBean) {
        this.modelBean = modelBean;
    }    
    
    @Override
    public void cancelProcess(ActionEvent event) {
        getLogger().info("Cancelling Process");
    }
    
    @Override
    public void reset() {
        this.model=null;
    }
    
    protected void prepararStatusOperacion(UIOperationResultEnum operationStatus){
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("operationStatus", operationStatus.getStatusLiteral());
    }    

    protected void displayErrorMessageFromBundle(String msgproperty, String msgClientId) {
        MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(msgClientId,
                msgproperty);
    }    
    
    protected void displayErrorMessage(String message, String msgClientId) {
        MessageDisplayUtils.INSTANCE.printErrorMessage(msgClientId,
                message);
    }

    protected void displayInfoMessage(String message, String msgClientId) {
        MessageDisplayUtils.INSTANCE.printInfoMessage(msgClientId,
                message);
    }    
    
    public abstract Logger getLogger();
            
}
