package com.sib.loteria;

import com.sib.ui.enums.UIOperationResultEnum;
import com.sib.ui.models.LoteriaModel;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "loteriaUpdatePopup")
@ViewScoped
public class LoteriaUpdatePopup  extends LoteriaAbstractPopup 
    implements Serializable {
    
    public static final Logger LOG
            = Logger.getLogger(LoteriaUpdatePopup.class.getName());

    private LoteriaModel untouchedModel;

    public LoteriaUpdatePopup() {
    }

    public LoteriaModel getUntouchedModel() {
        return untouchedModel;
    }

    public void setUntouchedModel(LoteriaModel untouchedModel) {
        this.untouchedModel = untouchedModel;
    }

    @Override
    public void execute(ActionEvent event) {
        if (validateFields()) {
            try {
                LOG.info("Actualizando Loteria: " + this.getModel());
                ResponseModel response = getModelBean().actualizarLoteria(this.getModel());
                prepararStatusOperacion(UIOperationResultEnum.SUCCESS);
                if (response.getWasSuccessful()) {
                    prepararStatusOperacion(UIOperationResultEnum.SUCCESS);
                    this.displayInfoMessage(response.getMessage(),
                            LoteriaConstantes.TABLA_MESSAGE_CLIENT_ID);
                } else {
                    LOG.error("Error actualizando Loteria: " + response.getMessage());
                    this.displayErrorMessage(response.getMessage(),
                            LoteriaConstantes.POPUP_MESSAGE_CLIENT_ID);
                    prepararStatusOperacion(UIOperationResultEnum.FAILURE);
                }
            } catch (Exception ex) {
                LOG.error("Error actualizando Loteria. Exception: " 
                        + ex.getMessage(), ex);
                this.displayErrorMessageFromBundle("sab.loteria.actualizar.error",
                        LoteriaConstantes.POPUP_MESSAGE_CLIENT_ID);
                prepararStatusOperacion(UIOperationResultEnum.FAILURE);
            }
        } else {
            prepararStatusOperacion(UIOperationResultEnum.FAILURE);
            LOG.info("Fields Validation Failed..");
        }
    }

    @Override
    public void reset() {
        this.untouchedModel=null;                
    }

    @Override
    public boolean validateFields() {
        
        boolean result = true;

        if ((this.getModel().getCodigo() == null
                || this.getModel().getCodigo().isEmpty())) {
            MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(
                    LoteriaConstantes.POPUP_MESSAGE_CLIENT_ID, "sab.error.campo.vacio");
            result = false;
        }

        if(result){
            if((this.getModel().getCodigo()!=null
                    && this.getModel().getCodigo().equals(this.untouchedModel.getCodigo()))                 
                 && (this.getModel().getStatus()!=null && 
                        this.getModel().getStatus().equals(this.untouchedModel.getStatus()))){
                MessageDisplayUtils.INSTANCE.printErrorMessageFromBundle(
                        LoteriaConstantes.POPUP_MESSAGE_CLIENT_ID, "sab.error.no.cambios");
                result = false;                
            }
        }
        return result;
    }

    @Override
    public String getFileName() {
        return "loteriaupdatepopup";
    }

    @Override
    public String getPopupTitle() {
        return MessageDisplayUtils.INSTANCE.getMessageFromBundle("sab.loteria.actualizar.lbl");
    }

    @Override
    public Logger getLogger() {
        return LOG;
    }
}
