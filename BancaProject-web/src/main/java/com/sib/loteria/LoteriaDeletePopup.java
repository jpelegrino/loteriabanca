package com.sib.loteria;

import com.sib.interfaces.IPopup;
import com.sib.ui.enums.UIOperationResultEnum;
import com.sib.ui.models.LoteriaModel;
import com.sib.ui.models.ResponseModel;
import com.sib.util.MessageDisplayUtils;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author Omar Moronta
 */
@ManagedBean(name = "loteriaDeletePopup")
@ViewScoped
public class LoteriaDeletePopup extends LoteriaAbstractPopup 
    implements Serializable {
    
    public static final Logger LOG
            = Logger.getLogger(LoteriaDeletePopup.class.getName());

    public LoteriaDeletePopup() {
    }

    @Override
    public void execute(ActionEvent event) {
        try {
            LOG.info("Eliminando Loteria: " + this.getModel().getCodigo());
            ResponseModel response = getModelBean().eliminarLoteria(this.getModel());

            if (response.getWasSuccessful()) {
                prepararStatusOperacion(UIOperationResultEnum.SUCCESS);
                this.displayInfoMessage(response.getMessage(),
                        LoteriaConstantes.TABLA_MESSAGE_CLIENT_ID);
            } else {
                LOG.error("Error eliminando Loteria: " + response.getMessage());
                this.displayErrorMessage(response.getMessage(),
                        LoteriaConstantes.POPUP_MESSAGE_CLIENT_ID);
                prepararStatusOperacion(UIOperationResultEnum.FAILURE);
            }
        } catch (Exception ex) {
            LOG.error("Error eliminando Loteria. Exception: " 
                    + ex.getMessage(), ex);
            this.displayErrorMessageFromBundle("sab.loteria.eliminar.error",
                    LoteriaConstantes.POPUP_MESSAGE_CLIENT_ID);
            prepararStatusOperacion(UIOperationResultEnum.FAILURE);
        }
    }

    @Override
    public boolean validateFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getFileName() {
        return "loteriadeletepopup";
    }

    @Override
    public String getPopupTitle() {
        return MessageDisplayUtils.INSTANCE.getMessageFromBundle("sab.loteria.eliminar.lbl");
    }

    @Override
    public Logger getLogger() {
        return LOG;
    }
}
