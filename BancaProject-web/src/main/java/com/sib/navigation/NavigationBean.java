package com.sib.navigation;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Snailin Inoa
 */
@ManagedBean(name = "navigationBean")
@ViewScoped
public class NavigationBean implements Serializable {

    HttpSession session;
    private String navigationFileName = "index";

    public String getNavigationFileName() {
//        session.invalidate();
//        session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        return navigationFileName;
    }

    public void setNavigationFileName(String navigationFileName) {
        this.navigationFileName = navigationFileName;
    }

    @PostConstruct
    public void initialize() {
        session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    }

}
