/*
SQLyog Community Edition- MySQL GUI v8.12 
MySQL - 5.5.30 : Database - mydb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`mydb` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `mydb`;

/*Table structure for table `ban_general` */

DROP TABLE IF EXISTS `ban_general`;

CREATE TABLE `ban_general` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `grupo_id` int(11) unsigned NOT NULL,
  `fecha` varchar(10) DEFAULT NULL,
  `fecha_creacion` timestamp NULL DEFAULT NULL,
  `status` varchar(1) DEFAULT 'A',
  `status_general` varchar(1) DEFAULT 'P',
  PRIMARY KEY (`id`),
  KEY `BAN_GENERAL_FKIndex1` (`grupo_id`),
  CONSTRAINT `fk_{45A6F6AA-2F29-45DD-896C-D00346371315}` FOREIGN KEY (`grupo_id`) REFERENCES `grupo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13654 DEFAULT CHARSET=utf8 PACK_KEYS=0;

/*Table structure for table `banca` */

DROP TABLE IF EXISTS `banca`;

CREATE TABLE `banca` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `grupo_id` int(11) unsigned NOT NULL,
  `codigo` varchar(100) NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(1) NOT NULL,
  `fecha_actualizacion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `BANCA_FKIndex1` (`grupo_id`),
  CONSTRAINT `fk_{BBCCE671-56C2-4798-89CF-A9B5C64A21D6}` FOREIGN KEY (`grupo_id`) REFERENCES `grupo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 PACK_KEYS=0;

/*Table structure for table `banca_has_loteria` */

DROP TABLE IF EXISTS `banca_has_loteria`;

CREATE TABLE `banca_has_loteria` (
  `banca_id` int(11) unsigned NOT NULL,
  `loteria_id` int(11) unsigned NOT NULL,
  `fecha_creacion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`banca_id`,`loteria_id`),
  KEY `BANCA_has_LOTERIA_FKIndex1` (`banca_id`),
  KEY `BANCA_has_LOTERIA_FKIndex2` (`loteria_id`),
  CONSTRAINT `fk_{0D90EDFF-7F27-45F7-B354-A2B20FDBEC93}` FOREIGN KEY (`banca_id`) REFERENCES `banca` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_{DCB19539-19B7-473E-84CA-9DF458F9FB29}` FOREIGN KEY (`loteria_id`) REFERENCES `loteria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `banca_has_loteria_has_juego` */

DROP TABLE IF EXISTS `banca_has_loteria_has_juego`;

CREATE TABLE `banca_has_loteria_has_juego` (
  `loteria_id` int(11) unsigned NOT NULL,
  `banca_id` int(11) unsigned NOT NULL,
  `juego_id` int(11) unsigned NOT NULL,
  `porcentaje_cobro` int(11) unsigned NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(1) NOT NULL,
  `fecha_actualizacion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`loteria_id`,`banca_id`,`juego_id`),
  KEY `BANCA_has_LOTERIA_has_JUEGO_FKIndex1` (`banca_id`,`loteria_id`),
  KEY `BANCA_has_LOTERIA_has_JUEGO_FKIndex2` (`juego_id`),
  CONSTRAINT `fk_{3D80E2CD-C239-49FA-8D2C-CEA4F6156317}` FOREIGN KEY (`juego_id`) REFERENCES `juego` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_{B0FE0AF7-5602-4D04-B6AC-9410D1765FDF}` FOREIGN KEY (`banca_id`, `loteria_id`) REFERENCES `banca_has_loteria` (`banca_id`, `loteria_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

/*Table structure for table `cobro` */

DROP TABLE IF EXISTS `cobro`;

CREATE TABLE `cobro` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comentario` text,
  `fecha_creacion` date DEFAULT NULL,
  `status_cobro` varchar(1) DEFAULT NULL,
  `monto_total_cobrado` float DEFAULT NULL,
  `fecha_efectiva` date DEFAULT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 PACK_KEYS=0;

/*Table structure for table `cobro_historial` */

DROP TABLE IF EXISTS `cobro_historial`;

CREATE TABLE `cobro_historial` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cobro_id` bigint(20) NOT NULL,
  `detalle_general_id` bigint(20) NOT NULL,
  `monto_cobrado` float DEFAULT NULL,
  `status_cobro_historial` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `COBRO_HISTORIAL_FKIndex1` (`detalle_general_id`),
  KEY `COBRO_HISTORIAL_FKIndex2` (`cobro_id`),
  CONSTRAINT `fk_{74ED17E5-2D71-4600-BDB5-E84BAA8F7D5F}` FOREIGN KEY (`cobro_id`) REFERENCES `cobro` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_{80593669-0689-42E0-915C-0F3B8295EF5D}` FOREIGN KEY (`detalle_general_id`) REFERENCES `detalle_general` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 PACK_KEYS=0;

/*Table structure for table `detalle_general` */

DROP TABLE IF EXISTS `detalle_general`;

CREATE TABLE `detalle_general` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `loteria_id` int(11) unsigned NOT NULL,
  `banca_id` int(11) unsigned NOT NULL,
  `general_id` bigint(20) NOT NULL,
  `valor_venta_total` float DEFAULT NULL,
  `valor_premio_total` float DEFAULT NULL,
  `total_pagar` float DEFAULT NULL,
  `monto_final_operaciones` float DEFAULT NULL,
  `pendiente_cobrar` float DEFAULT NULL,
  `status_detalle` varchar(1) DEFAULT NULL,
  `juego1_venta` float DEFAULT NULL,
  `juego1_premio` float DEFAULT NULL,
  `juego1_porciento_pagar` float DEFAULT NULL,
  `juego1_monto_pagar` float DEFAULT NULL,
  `juego2_venta` float DEFAULT NULL,
  `juego2_premio` float DEFAULT NULL,
  `juego2_porciento_pagar` float DEFAULT NULL,
  `juego2_monto_pagar` float DEFAULT NULL,
  `juego3_venta` float DEFAULT NULL,
  `juego3_premio` float DEFAULT NULL,
  `juego3_porciento_pagar` float DEFAULT NULL,
  `juego3_monto_pagar` float DEFAULT NULL,
  `juego4_venta` float DEFAULT NULL,
  `juego4_premio` float DEFAULT NULL,
  `juego4_porciento_pagar` float DEFAULT NULL,
  `juego4_monto_pagar` float DEFAULT NULL,
  `fecha_efectiva` date DEFAULT NULL,
  `fecha_actualizacion` timestamp NULL DEFAULT NULL,
  `fecha_creacion` timestamp NULL DEFAULT NULL,
  `status` varchar(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `DETALLE_GENERAL_FKIndex1` (`general_id`),
  KEY `DETALLE_GENERAL_FKIndex2` (`banca_id`,`loteria_id`),
  CONSTRAINT `fk_{3993C97B-17E6-498C-89CB-098038A616C9}` FOREIGN KEY (`banca_id`, `loteria_id`) REFERENCES `banca_has_loteria` (`banca_id`, `loteria_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_{6545F7D6-1AA3-419C-ACB9-D45987633982}` FOREIGN KEY (`general_id`) REFERENCES `ban_general` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 PACK_KEYS=0;

/*Table structure for table `gasto` */

DROP TABLE IF EXISTS `gasto`;

CREATE TABLE `gasto` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_tipo_gasto` bigint(20) NOT NULL,
  `id_banca` int(11) unsigned NOT NULL,
  `monto` float NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'V',
  `fecha_efectiva` date NOT NULL,
  `fecha_creacion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_gasto_banca` (`id_banca`),
  KEY `FK_gasto_tipo_gasto` (`id_tipo_gasto`),
  CONSTRAINT `FK_gasto_banca` FOREIGN KEY (`id_banca`) REFERENCES `banca` (`id`),
  CONSTRAINT `FK_gasto_tipo_gasto` FOREIGN KEY (`id_tipo_gasto`) REFERENCES `tipo_gasto` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `grupo` */

DROP TABLE IF EXISTS `grupo`;

CREATE TABLE `grupo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(100) NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(1) NOT NULL,
  `fecha_actualizacion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo_UNIQUE` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 PACK_KEYS=0;

/*Table structure for table `juego` */

DROP TABLE IF EXISTS `juego`;

CREATE TABLE `juego` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(100) NOT NULL,
  `status` varchar(1) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_actualizacion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo_UNIQUE` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 PACK_KEYS=0;

/*Table structure for table `loteria` */

DROP TABLE IF EXISTS `loteria`;

CREATE TABLE `loteria` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(100) NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(1) NOT NULL,
  `fecha_actualizacion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo_UNIQUE` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 PACK_KEYS=0;

/*Table structure for table `loteria_has_juego` */

DROP TABLE IF EXISTS `loteria_has_juego`;

CREATE TABLE `loteria_has_juego` (
  `loteria_id` int(11) unsigned NOT NULL,
  `juego_id` int(11) unsigned NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(1) NOT NULL,
  `fecha_actualizacion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`loteria_id`,`juego_id`),
  KEY `LOTERIA_has_JUEGO_FKIndex1` (`loteria_id`),
  KEY `LOTERIA_has_JUEGO_FKIndex2` (`juego_id`),
  CONSTRAINT `fk_{4DF0DC3A-4825-4BB2-9FA5-243475ACEEBF}` FOREIGN KEY (`loteria_id`) REFERENCES `loteria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_{4EBA7B84-B804-4558-8EAE-44F3419CCBE4}` FOREIGN KEY (`juego_id`) REFERENCES `juego` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `sequence` */

DROP TABLE IF EXISTS `sequence`;

CREATE TABLE `sequence` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tipo_gasto` */

DROP TABLE IF EXISTS `tipo_gasto`;

CREATE TABLE `tipo_gasto` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
